![Screen Shot 2014-09-04 at 6.43.51 PM.png](https://bitbucket.org/repo/do9RGL/images/1963522539-Screen%20Shot%202014-09-04%20at%206.43.51%20PM.png)

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Summary: A Solar Monitoring Mobile Application used to give the consumer a better way of viewing their solar system through an iPhone.
* Version: Beta 0.1

### How do I get set up? ###

* Summary of set up: You must have login credentials to view your solar system
* Configuration: ---
* Dependencies: Aurora Vision Database, CorePlot (For Graphing)
* Database configuration: ---
* How to run tests: ---
* Deployment instructions: ---

### Contribution guidelines ###

* Writing tests: ---
* Code review: ---
* Other guidelines: ---

### Who do I talk to? ###

* Repo owner or admin: Contact jsetting32@gmail.com
* Other community or team contact: ---