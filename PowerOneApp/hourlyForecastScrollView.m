//
//  hourlyForecastScrollView.m
//  PowerOneApp
//
//  Created by John Setting on 6/24/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "hourlyForecastScrollView.h"

@implementation hourlyForecastScrollView

- (id)init:(NSString*)city state:(NSString *)state icons:(NSArray *)icons times:(NSArray *)times temps:(NSArray *)temps
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0, 280, 70);
        self.layer.borderWidth = 1.0f;
        self.layer.borderColor = [[UIColor grayColor] CGColor];
        self.layer.cornerRadius = 0.0f;
        self.showsHorizontalScrollIndicator = NO;
        //self.clipsToBounds = YES;
        self.bounces = YES;
    }
    
    [self setupHorizontalScrollView:icons temps:temps times:times];
    
    return self;

}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


- (void) setupHorizontalScrollView:(NSArray *)icons temps:(NSArray*)temps times:(NSArray*)times
{    
    
    
    [self setBackgroundColor:[UIColor blackColor]];
    [self setCanCancelContentTouches:NO];
    
    self.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    self.clipsToBounds = NO;
    self.scrollEnabled = YES;
    
    CGFloat cx = 5;
    for (int i = 0; i < [icons count]; i++)
    {
        _icon = [icons objectAtIndex:i];
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:_icon];
        _timeTemp = [[UILabel alloc] init];
        _timeTemp.backgroundColor = [UIColor clearColor];
        
        
        _timeTemp.text = [NSString stringWithFormat:@"%@°",[temps objectAtIndex:i]];
        [_timeTemp setFont:[UIFont fontWithName:@"Arial" size:8.0f]];
        _timeTemp.textColor = [UIColor whiteColor];
        _timeTemp.textAlignment = NSTextAlignmentCenter;
        
        _dateTime = [[UILabel alloc] init];
        _dateTime.backgroundColor = [UIColor clearColor];
        _dateTime.text = [times objectAtIndex:i];
        [_dateTime setFont:[UIFont fontWithName:@"Arial" size:8.0f]];
        _dateTime.textColor = [UIColor whiteColor];
        _dateTime.textAlignment = NSTextAlignmentCenter;
        
        CGRect rect = imageView.frame;
        rect.size.height = 30;
        rect.size.width = 30;
        rect.origin.x = cx;
        rect.origin.y = 20;
        
        CGRect time = _dateTime.frame;
        time.size.height = 20;
        time.size.width = 40;
        time.origin.x = cx - 5;
        time.origin.y = 0;
        
        CGRect temp = _timeTemp.frame;
        temp.size.height = 20;
        temp.size.width = 40;
        temp.origin.x = cx - 5;
        temp.origin.y = 50;
        
        imageView.frame = rect;
        _dateTime.frame = time;
        _timeTemp.frame = temp;
        
        [self addSubview:imageView];
        [self addSubview:_dateTime];
        [self addSubview:_timeTemp];
        
        cx += imageView.frame.size.width+10;
    }
    
    self.contentSize = CGSizeMake(cx, 70);


}

@end