//
//  CDDDetailsViewController.h
//  PowerOneApp
//
//  Created by John Setting on 7/16/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDDDetailsViewController : UIViewController

@property (strong, nonatomic) NSArray *detailsArray;

@end
