//
//  PullEasyViewData.m
//  PowerOneApp
//
//  Created by John Setting on 6/11/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "PullEasyViewData.h"

#import "FranksVC.h"

# pragma mark - JSON Interface
@interface NSDictionary(JSONCategories)

+(NSDictionary*)dictionaryWithContentsOfJSONURLString:(NSString*)urlAddress;
-(NSData*)toJSON;

@end

# pragma mark - JSON Implementation
@implementation NSDictionary(JSONCategories)

+ (NSDictionary*)dictionaryWithContentsOfJSONURLString:(NSString*)urlAddress
{
    NSData* data = [NSData dataWithContentsOfURL: [NSURL URLWithString: urlAddress] ];
    __autoreleasing NSError* error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    if (error != nil) return nil;
    return result;
}

- (NSData*)toJSON
{
    NSError* error = nil;
    id result = [NSJSONSerialization dataWithJSONObject:self options:kNilOptions error:&error];
    if (error != nil) return nil;
    return result;
}

@end


@implementation PullEasyViewData


+ (id)sharedManager {
    static AuthenticationData *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id) init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

#pragma mark - wUnderground Fetch Parsing Function

- (void)fetchHourlyForecast:(NSData *)responseData {
    NSLog(@"Fetching Hourly Forecast");
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    _hourlyIcons = [[NSMutableArray alloc] init];
    _hourlyTemps = [[NSMutableArray alloc] init];
    _hourlyTimes = [[NSMutableArray alloc] init];
    
    NSArray* data = [json objectForKey:@"hourly_forecast"];
    
    NSArray *hourTimes = [data valueForKeyPath:@"FCTTIME.civil"];
    NSArray *hourlyTemps = [data valueForKeyPath:@"temp.english"];
    NSArray *hourlyIcons = [data valueForKeyPath:@"icon_url"];
    
    
    for (int i = 0; i < [hourTimes count]; i++)
    {
        [_hourlyTimes addObject:[hourTimes objectAtIndex:i]];
        [_hourlyTemps addObject:[hourlyTemps objectAtIndex:i]];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", [hourlyIcons objectAtIndex:i]]];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *img = [[UIImage alloc] initWithData:data];
        [_hourlyIcons addObject:img];
    }
}

- (void)fetchFullForeCast:(NSData *)responseData {
    
    NSLog(@"Fetching Full Forecast Data");
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray *array = [json valueForKeyPath:@"forecast.txt_forecast.forecastday"];
    NSArray *humidity = [json valueForKeyPath:@"forecast.simpleforecast.forecastday"];
    
    _todayString = [[array objectAtIndex:0] objectForKey:@"title"];
    _todayDetails = [[array objectAtIndex:0] objectForKey:@"fcttext"];
    _tonightString = [[array objectAtIndex:1] objectForKey:@"title"];
    _tonightDetails = [[array objectAtIndex:1] objectForKey:@"fcttext"];
    _avgHumidity = [[humidity objectAtIndex:0] valueForKeyPath:@"avehumidity"];
}


- (void)fetchForecastConditions:(NSData *)responseData
{
    NSLog(@"Fetching Conditions Data");
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    _feelsLike = [json valueForKeyPath:@"current_observation.feelslike_f"];
    _visibility = [json valueForKeyPath:@"current_observation.visibility_mi"];
    _windPower = [json valueForKeyPath:@"current_observation.wind_mph"];
    _barometerPower = [json valueForKeyPath:@"current_observation.pressure_in"];
    
    NSString * iconURL = [json valueForKeyPath:@"current_observation.icon_url"];
    NSURL *url = [NSURL URLWithString:iconURL];
    NSData *iconData = [NSData dataWithContentsOfURL:url];
    
    _currentIcon = [UIImage imageWithData:iconData];

    
    
}

/*
- (void)fetchSunriseAndSunsetTimes:(NSData *)responseData
{
    NSLog(@"Fetching SunRise and SunSet Data");
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    
    NSString *sunsetHour = [json valueForKeyPath:@"moon_phase.sunset.hour"];
    NSString *sunsetMinute = [json valueForKeyPath:@"moon_phase.sunset.minute"];
    NSString *sunriseHour = [json valueForKeyPath:@"moon_phase.sunrise.hour"];
    NSString *sunriseMinute = [json valueForKeyPath:@"moon_phase.sunrise.minute"];
                            
    _SunriseTime = [NSString stringWithFormat:@"%@:%@", sunriseHour, sunriseMinute];
    _SunsetTime = [NSString stringWithFormat:@"%@:%@", sunsetHour, sunsetMinute];
}
 */

- (void)fetchEnergyCosts:(NSData *)responseData
{
    NSLog(@"Fetching EnergyCosts Data");
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    //NSLog(@"%@", json);
    
    NSArray* data = [json objectForKey:@"series_data"];
    NSArray* moreData = [data valueForKey:@"data"];

    
    NSArray*finalData = [[moreData objectAtIndex:0] objectAtIndex:0];
    
    
    _currentEnergyCosts = [finalData objectAtIndex:1];
    _currentEnergyCostsDate = [finalData objectAtIndex:0];
}


#pragma mark - JSON Fetch Parsing Function

- (void)fetchEasyViewData:(NSData *)responseData {
    
    NSLog(@"Fetching Easy View Data");
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    NSArray* data = [json objectForKey:@"fields"];
    
    //NSLog(@"%@", json);
    
    NSDictionary* now = [data objectAtIndex:0];
    NSDictionary* today = [data objectAtIndex:1];
    NSDictionary* week = [data objectAtIndex:2];
    NSDictionary* month = [data objectAtIndex:3];
    NSDictionary* lifetime = [data objectAtIndex:4];
    
    //_entityName = [now objectForKey:@"entityName"];
    
    _nowValue = [now objectForKey:@"value"];
    _todayValue = [today objectForKey:@"value"];
    _weekValue = [week objectForKey:@"value"];
    _monthValue = [month objectForKey:@"value"];
    _lifeValue = [lifetime objectForKey:@"value"];
    
    
    _UpdatedForCurrentPowerTime = [now objectForKey:@"startLabel"];
    
    
    /* ----------------Set Precision of values to 2 decimal points ------------------ */
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    
    NSNumber * nowNum = [f numberFromString:_nowValue];
    NSNumber * todayNum = [f numberFromString:_todayValue];
    NSNumber * weekNum = [f numberFromString:_weekValue];
    NSNumber * monthNum = [f numberFromString:_monthValue];
    NSNumber * lifeNum = [f numberFromString:_lifeValue];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:2];
    [formatter setMinimumFractionDigits:2];
    
    _ComputerSavings = [formatter stringFromNumber:[NSNumber numberWithFloat:([lifeNum floatValue] / 130.0)]];
    _TVSavings = [formatter stringFromNumber:[NSNumber numberWithFloat:([lifeNum floatValue] * 6.95)]];
    _nowValueFormatted = [formatter stringFromNumber:nowNum];
    _todayValueFormatted = [formatter stringFromNumber:todayNum];
    _weekValueFormatted = [formatter stringFromNumber:weekNum];
    _monthValueFormatted = [formatter stringFromNumber:monthNum];
    _lifeValueFormatted = [formatter stringFromNumber:lifeNum];
    /* ----------------------------- End Precision Code ----------------------------- */
    
    

    
}

- (void)fetchHierarchyRequestData:(NSData *)responseData
{
    NSLog(@"fetching Hierarchy Data");
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* data = [json objectForKey:@"result"];
    
    //NSLog(@"%@", json);
    
    _entityID = [data valueForKeyPath:@"entityId"];
    _entityName = [data valueForKeyPath:@"meta.displayName"];
    _entityDescription = [data valueForKeyPath:@"meta.description"];
    
    _siteCity = [data valueForKeyPath:@"address.city"];
    _siteCountry = [data valueForKeyPath:@"address.country"];
    
    //NSString *countryCode = [data valueForKeyPath:@"address.countryCode"];
    _siteZipCode = [data valueForKeyPath:@"address.postalCode"];
    //NSString *region = [data valueForKeyPath:@"address.region"];
    _siteRegionCode = [data valueForKeyPath:@"address.regionCode"];
    _siteStreet = [data valueForKeyPath:@"address.street1"];
    
    NSArray *array = [[NSArray alloc] initWithObjects:_siteRegionCode, _siteCity, nil];
    [FranksVC setRegionAndCity:array];
    
    
    NSArray *moreData = [[json objectForKey:@"result"] objectForKey:@"configuredFunctions"];
    NSArray * string;

    for (int i = 0; i < [moreData count]; i++) {
        if ([[[moreData objectAtIndex:i] valueForKeyPath:@"category"] isEqualToString:@"Benefit"])
        {
            NSArray *moremoreData = [moreData objectAtIndex:i];
            string = [moremoreData valueForKeyPath:@"parameters.value"];
            break;
        }
    }
    
    //NSLog(@"%@", string);
    for (int i = 1; i < [string count]; i++)
    {
        if (i == 1) {
            _CO2Multiplier = [NSNumber numberWithFloat:[[string objectAtIndex:i] floatValue]];
        } else if (i == 2) {
            _NOXMultiplier = [NSNumber numberWithFloat:[[string objectAtIndex:i] floatValue]];
        } else if (i == 3) {
            _SO2Multiplier = [NSNumber numberWithFloat:[[string objectAtIndex:i] floatValue]];
        }
    }
    
    NSNumber * lifetimeCO2Avoided = [NSNumber numberWithFloat:([_CO2Multiplier floatValue] * [[self getRealLifeValue] floatValue])];
    NSNumber * lifetimeNOXAvoided = [NSNumber numberWithFloat:([_NOXMultiplier floatValue] * [[self getRealLifeValue] floatValue])];
    NSNumber * lifetimeSO2Avoided = [NSNumber numberWithFloat:([_SO2Multiplier floatValue] * [[self getRealLifeValue] floatValue])];
    
    float metricTon = 2204.622;
    NSNumber *carbonOffset = [NSNumber numberWithFloat:([lifetimeCO2Avoided floatValue] / metricTon)];
    NSNumber *carbonOffsetAcres = [NSNumber numberWithFloat:([carbonOffset floatValue] / 4.69)];
    NSNumber *carSavings = [NSNumber numberWithFloat:([lifetimeCO2Avoided floatValue] / 10000.0)];
    
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    [f setMaximumFractionDigits:2];
    [f setMinimumFractionDigits:2];
    [f setRoundingMode:NSNumberFormatterRoundUp];
    
    
    NSNumber *roundedcarbonOffset = [f numberFromString:[NSString stringWithFormat:@"%f", [carbonOffset floatValue]]];
    NSNumber *roundedcarbonOffsetAcres = [f numberFromString:[NSString stringWithFormat:@"%f", [carbonOffsetAcres floatValue]]];
    NSNumber *roundedcarSavings = [f numberFromString:[NSString stringWithFormat:@"%f", [carSavings floatValue]]];
    NSNumber *roundedlifetimeCO2Avoided = [f numberFromString:[NSString stringWithFormat:@"%f", [lifetimeCO2Avoided floatValue]]];
    NSNumber *roundedlifetimeNOXAvoided  = [f numberFromString:[NSString stringWithFormat:@"%f", [lifetimeNOXAvoided floatValue]]];
    NSNumber *roundedlifetimeSO2Avoided  = [f numberFromString:[NSString stringWithFormat:@"%f", [lifetimeSO2Avoided floatValue]]];
    
    _CarbonOffsetMetricTonsSavings = [f stringFromNumber:roundedcarbonOffset];    
    _CarbonOffsetAcresSavings = [f stringFromNumber:roundedcarbonOffsetAcres];
    _CarSavings = [f stringFromNumber:roundedcarSavings];
    _CO2Savings = [f stringFromNumber:roundedlifetimeCO2Avoided];
    _NOXSavings = [f stringFromNumber:roundedlifetimeNOXAvoided];
    _SO2Savings = [f stringFromNumber:roundedlifetimeSO2Avoided];

    _siteAddress = [NSString stringWithFormat:@"%@, %@, %@, %@, %@", _siteStreet, _siteZipCode, _siteCity, _siteRegionCode ,_siteCountry];
    
    NSArray *systemSizeMachArray = [data valueForKeyPath:@"installedCapacities.value"];
    
    _EntityLat = [data valueForKeyPath:@"location.latitude"];
    _EntityLong = [data valueForKeyPath:@"location.longitude"];
    _EntityElevation = [data valueForKeyPath:@"location.elevation"];
    
    _systemSizeValue = [systemSizeMachArray objectAtIndex:0];

    _currentGaugeValue = [_nowValueFormatted floatValue];
    //NSLog(@"%@", _nowValueFormatted);
    _realGaugeValue = _currentGaugeValue / [_systemSizeValue floatValue];
        
    float someFloat = _currentGaugeValue/[[systemSizeMachArray objectAtIndex:0] floatValue] * 100;
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:2];
    [formatter setMinimumFractionDigits:2];
    
    _powerNowPercentage  = [formatter stringFromNumber:[NSNumber numberWithFloat:someFloat]];

}

- (void)fetchWeatherStationRequestData:(NSData *)responseData
{
    NSLog(@"fetching Weather Station Data");
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* data = [json objectForKey:@"fields"];
    
    //NSLog(@"%@", json);
    
    NSDictionary* Irradiance = [data objectAtIndex:0];
    NSDictionary* Insolation = [data objectAtIndex:1];
    NSDictionary* CellTemp = [data objectAtIndex:2];
    NSDictionary* AmbientTemp = [data objectAtIndex:3];
    
    /* ----------------Set Precision of values to 2 decimal points ------------------ */
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    
    NSNumber * irradianceValue = [f numberFromString:[Irradiance objectForKey:@"value"]];
    NSNumber * insolationValue = [f numberFromString:[Insolation objectForKey:@"value"]];
    NSNumber * cellTempValue = [f numberFromString:[CellTemp objectForKey:@"value"]];
    NSNumber * ambientTempValue = [f numberFromString:[AmbientTemp objectForKey:@"value"]];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setRoundingMode:NSNumberFormatterRoundHalfUp];
    [formatter setMaximumFractionDigits:2];
    [formatter setMinimumFractionDigits:2];
    
    _irradianceFormatted = [formatter stringFromNumber:irradianceValue];
    _insolationFormatted = [formatter stringFromNumber:insolationValue];
    _cellTempFormatted = [formatter stringFromNumber:cellTempValue];
    _ambientTempFormatted = [formatter stringFromNumber:ambientTempValue];
    /* ----------------------------- End Precision Code ----------------------------- */

}

- (void)fetchServicesSummaryRequestData:(NSData *)responseData
{
    NSLog(@"fetching Services Summary Data");
    
    //NSError* error;
    //NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    //NSLog(@"%@", json);
    
}

- (void)fetchServicesDocumentRequestData:(NSData *)responseData
{
    NSLog(@"fetching Services Documents Data");
    
    //NSError* error;
    //NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    //NSLog(@"%@", json);
    
}



- (void)fetchInverterServicesSummaryData:(NSData *)responseData {
    NSLog(@"Fetching Inverter Summary Data");
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* data = [json objectForKey:@"fields"];
    
    //NSLog(@"%@", json);
    
    NSMutableDictionary *array = [[NSMutableDictionary alloc] init];
    
    NSDateFormatter *Dformatter = [[NSDateFormatter alloc] init];
    Dformatter.dateFormat = @"yyyyMMddHHmmss";
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];

    
    _inverterTimes = [[NSMutableArray alloc] init];

    
    for (int i = 0; i < [data count]; i++)
    {
        NSDictionary *someObject = [data objectAtIndex:i];
        NSString *inverterNameString = [someObject objectForKey:@"entityName"];
    
        /*NSArray *inverterDates = [someObject valueForKeyPath:@"values.startLabel"];

        for (int i = 0; i < [inverterDates count]; i++)
        {
            NSDate *date = [Dformatter dateFromString:[inverterDates objectAtIndex:i]];
            NSString * stringDate = [formatter stringFromDate:date];
            [_inverterTimes addObject:stringDate];
        }*/
        
        NSArray *inverterValues = [someObject valueForKeyPath:@"values.value"];
        [array setValue:inverterValues forKey:inverterNameString];
    }
    
    NSDictionary *someObject = [data objectAtIndex:0];
    NSArray *inverterDates = [someObject valueForKeyPath:@"values.startLabel"];
    for (int i = 0; i < [inverterDates count]; i++)
    {
        NSDate *date = [Dformatter dateFromString:[inverterDates objectAtIndex:i]];
        NSString * stringDate = [formatter stringFromDate:date];
        [_inverterTimes addObject:stringDate];
    }
    
    _inverters = [[NSDictionary alloc] initWithDictionary:array];
    
    
    NSLog(@"%@", _inverters);
    
    
}

- (void)fetchInverterPositions:(NSData *)responseData {
    NSLog(@"Fetching Inverter Position Data");
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    //NSArray* data = [json objectForKey:@"fields"];
    
    //NSLog(@"%@", json);
}

- (void)fetchInstallDateData:(NSData *)responseData {
    NSLog(@"Fetching Install Date Data");
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* data = [json objectForKey:@"result"];
    
    _entityInstallDate = [data valueForKeyPath:@"installDate"];
    [PowerCalculatorViewController setInstallDateString:_entityInstallDate];
}



- (void)fetchEasyViewStatus:(NSData *)responseData
{
    NSLog(@"Fetching Status");
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSString *status = [json objectForKey:@"status"];
    if ([status isEqual: @"NORM"]) {
        _SystemStatus = @"Normal";
        _SystemStatusIcon = [UIImage imageNamed:@"norm.png"];
    } else if ([status isEqual: @"HIGH"]) {
        _SystemStatus = @"High";
        _SystemStatusIcon = [UIImage imageNamed:@"high.png"];
    } else if ([status isEqual: @"LOW"]) {
        _SystemStatus = @"Low";
        _SystemStatusIcon = [UIImage imageNamed:@"low.png"];
    } else if ([status isEqual: @"MEDIUM"]) {
        _SystemStatus = @"Medium";
        _SystemStatusIcon = [UIImage imageNamed:@"medium.png"];
    } else if ([status isEqual: @"INFO"]) {
        _SystemStatus = @"Check Information";
        _SystemStatusIcon = [UIImage imageNamed:@"info.png"];
    }
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    //NSLog(@"%@", [json objectForKey:@"events"]);
    
    //NSLog(@"%@", json);
    
    _systemStatusArrayAlerts = [json objectForKey:@"events"];
    
    for (int i = 0; i < [[json objectForKey:@"events"] count]; i++)
    {
        NSString *eventID = [json valueForKeyPath:@"events.eventId"];
        //NSLog(@"%@", eventID);
        NSString *eventDescription= [json valueForKeyPath:@"events.name"];
        //NSLog(@"%@", eventDescription);
        NSString *startTime = [json valueForKeyPath:@"events.formattedStartTime"];
        //NSLog(@"%@", startTime);
        NSString *eventTitle = [json valueForKeyPath:@"events.code"];
        //NSLog(@"%@", eventTitle);
        NSString *eventGroup = [json valueForKeyPath:@"events.eventGroup"];
        //NSLog(@"%@", eventGroup);
        NSArray *array = [[NSArray alloc] initWithObjects:eventID, eventDescription, startTime, eventGroup,nil];
        [dict setValue:array forKey:[NSString stringWithFormat:@"%@%i", eventID, i]];
    }
    
    
    
    if ([dict count] != 0)
        _SystemStatusAlerts = [[NSDictionary alloc] initWithDictionary:dict];
    [NotificationsViewController setStatusData:_systemStatusArrayAlerts];
    //NSLog(@"%@", _SystemStatusAlerts);
}

#pragma mark - Get Graph Data Functions

- (void)fetchGraphDataRequestDataDay:(NSData *)responseData
{
    NSLog(@"Fetching Day Graph Data");
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* data = [json objectForKey:@"fields"];
    
    //NSLog(@"%@", json);
    
    NSArray *graphValues = [data valueForKeyPath:@"values.value"];
    NSMutableArray *graphStartDate = [data valueForKeyPath:@"values.startLabel"];
    
    NSArray *startDateArray = [graphStartDate objectAtIndex:1];
    
    _graphValuesDay = [graphValues objectAtIndex:1];
    
    NSMutableArray *startDateArrayFormatted = [[NSMutableArray alloc] init];
    NSDateFormatter *Dformatter = [[NSDateFormatter alloc] init];
    Dformatter.dateFormat = @"yyyyMMddHHmmss";

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
    
     
    for (int i = 0; i < [startDateArray count] && i < [_graphValuesDay count]; i++)
    {
        NSDate *date = [Dformatter dateFromString:[startDateArray objectAtIndex:i]];
        //NSString *localDate = [date descriptionWithLocale:[NSLocale currentLocale]];
        NSString *stringDate = [formatter stringFromDate:date];
        [startDateArrayFormatted addObject:stringDate];
    }
    
    _graphStartDateDAY = [[NSArray alloc] initWithArray:startDateArrayFormatted];
    
    //for (int i = 0; i < [_graphValuesDay count] && i < [_graphStartDateDAY count]; i++)
    //    NSLog(@"%@ : %@", [_graphStartDateDAY objectAtIndex:i], [_graphValuesDay objectAtIndex:i]);
    
    
    NSMutableArray *maxValueArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [_graphValuesDay count]; i++)
    {
        if ([_graphValuesDay objectAtIndex:i] != [NSNull null])
        {
            NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
            [f setNumberStyle:NSNumberFormatterDecimalStyle];
            NSNumber *myNumber = [f numberFromString:[_graphValuesDay objectAtIndex:i]];
            [maxValueArray addObject:myNumber];
        }
    }
    
    NSNumber *highestNumber = [NSNumber numberWithFloat:0.0];
    
    for (NSNumber *theNumber in maxValueArray)
    {
        NSComparisonResult Val;
        Val = [theNumber compare: highestNumber];

        if (Val == NSOrderedSame) {
        } else if (Val == NSOrderedAscending) {
        } else if (Val == NSOrderedDescending) {
            highestNumber = theNumber;        
        }
    }
    _maxDayValue = highestNumber;
}



- (void)fetchGraphDataRequestData7D:(NSData *)responseData
{
    NSLog(@"Fetching 7D Graph Data");
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* data = [json objectForKey:@"fields"];
    
    //NSLog(@"%@", json);
    
    NSArray *graphValues = [data valueForKeyPath:@"values.value"];
    NSMutableArray *graphStartDate = [data valueForKeyPath:@"values.startLabel"];
    
    NSArray *startDateArray = [graphStartDate objectAtIndex:1];
    
    _graphValues7D = [graphValues objectAtIndex:1];
    
    NSDateFormatter *Dformatter = [[NSDateFormatter alloc] init];
    Dformatter.dateFormat = @"yyyyMMddHHmmss";
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEEE"];
    
    NSMutableArray *startDateArrayFormatted = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [startDateArray count] && i < [_graphValues7D count]; i++)
    {
        NSDate *date = [Dformatter dateFromString:[startDateArray objectAtIndex:i]];
        //NSString *localDate = [date descriptionWithLocale:[NSLocale currentLocale]];
        NSString *stringDate = [formatter stringFromDate:date];
        stringDate = [stringDate substringToIndex:3];
        [startDateArrayFormatted addObject:stringDate];
    }
    
    _graphStartDate7D = [[NSArray alloc] initWithArray:startDateArrayFormatted];
    
    
    NSMutableArray *maxValueArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [_graphValues7D count]; i++)
    {
        if ([_graphValues7D objectAtIndex:i] != [NSNull null])
        {
            NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
            [f setNumberStyle:NSNumberFormatterDecimalStyle];
            NSNumber *myNumber = [f numberFromString:[_graphValues7D objectAtIndex:i]];
            [maxValueArray addObject:myNumber];
        }
    }
    
    NSNumber *highestNumber = [NSNumber numberWithFloat:0.0];
    
    for (NSNumber *theNumber in maxValueArray)
    {
        NSComparisonResult Val;
        Val = [theNumber compare: highestNumber];

        if (Val == NSOrderedSame) {
        } else if (Val == NSOrderedAscending) {
        } else if (Val == NSOrderedDescending) {
            highestNumber = theNumber;        
        }
    }
    _max7DValue = highestNumber;
    
    
    //for (int i = 0; i < [_graphStartDate7D count] && i < [_graphValues7D count]; i++)
    //    NSLog(@"%@ : %@", [_graphStartDate7D objectAtIndex:i], [_graphValues7D objectAtIndex:i]);
}



- (void)fetchGraphDataRequestData30D:(NSData *)responseData
{
    NSLog(@"Fetching 30D Graph Data");
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* data = [json objectForKey:@"fields"];
    
    //NSLog(@"%@", json);
    
    NSArray *graphStartDate = [data valueForKeyPath:@"values.startLabel"];
    NSArray *graphValues = [data valueForKeyPath:@"values.value"];
    
    
    NSArray *startDateArray = [graphStartDate objectAtIndex:1];
    NSArray *values = [graphValues objectAtIndex:1];

    NSMutableArray *startDateArrayFormatted = [[NSMutableArray alloc] init];
    NSDateFormatter *Dformatter = [[NSDateFormatter alloc] init];
    Dformatter.dateFormat = @"yyyyMMddHHmmss";
    
    NSDateFormatter *monthformatter = [[NSDateFormatter alloc] init];
    [monthformatter setDateFormat:@"MM"];
    
    NSDateFormatter *dayformatter = [[NSDateFormatter alloc] init];
    [dayformatter setDateFormat:@"dd"];
    
    for (int i = 0; i < [startDateArray count] && i < [values count]; i++)
    {
        NSDate *date = [Dformatter dateFromString:[startDateArray objectAtIndex:i]];
        //NSString *localDate = [date descriptionWithLocale:[NSLocale currentLocale]];
        NSString *stringDate = [monthformatter stringFromDate:date];
        NSString *dayStringDate = [dayformatter stringFromDate:date];
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        NSString *monthName = [[df monthSymbols] objectAtIndex:([stringDate integerValue]-1)];
        monthName = [monthName substringToIndex:3];
        
        NSString *string = [[NSString alloc] initWithFormat:@"%@ %@", monthName, dayStringDate];
        
        [startDateArrayFormatted addObject:string];
    }
    
    
    _graphStartDate30D = [[NSArray alloc] initWithArray:startDateArrayFormatted];
    _graphValues30D = [[NSArray alloc] initWithArray:values];
    
    //for (int i = 0; i < [_graphValues30D count] && i < [_graphStartDate30D count]; i++)
    //    NSLog(@"%@ : %@", [_graphValues30D objectAtIndex:i], [_graphStartDate30D objectAtIndex:i]);

    
    NSMutableArray *maxValueArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [_graphValues30D count]; i++)
    {
        if ([_graphValues30D objectAtIndex:i] != [NSNull null])
        {
            NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
            [f setNumberStyle:NSNumberFormatterDecimalStyle];
            NSNumber *myNumber = [f numberFromString:[_graphValues30D objectAtIndex:i]];
            [maxValueArray addObject:myNumber];
        }
    }
    
    NSNumber *highestNumber = [NSNumber numberWithFloat:0.0];
    
    for (NSNumber *theNumber in maxValueArray)
    {
        NSComparisonResult Val;
        Val = [theNumber compare: highestNumber];

        if (Val == NSOrderedSame) {
        } else if (Val == NSOrderedAscending) {
        } else if (Val == NSOrderedDescending) {
            highestNumber = theNumber;        
        }
    }
    
    _max30DValue = highestNumber;
    

}



- (void)fetchGraphDataRequestData365D:(NSData *)responseData
{
    NSLog(@"Fetching 365D Graph Data");
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* data = [json objectForKey:@"fields"];
    
    //NSLog(@"%@", json);
    
    NSArray *graphValues = [data valueForKeyPath:@"values.value"];
    NSMutableArray *graphStartDate = [data valueForKeyPath:@"values.startLabel"];
    
    NSArray *startDateArray = [graphStartDate objectAtIndex:1];
    
    _graphValues365D = [graphValues objectAtIndex:1];
    
    NSMutableArray *startDateArrayFormatted = [[NSMutableArray alloc] init];
    NSDateFormatter *Dformatter = [[NSDateFormatter alloc] init];
    Dformatter.dateFormat = @"yyyyMMddHHmmss";
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM"];
    
    
    for (int i = 0; i < [startDateArray count] && i < [_graphValues365D count]; i++)
    {
        NSDate *date = [Dformatter dateFromString:[startDateArray objectAtIndex:i]];
        //NSString *localDate = [date descriptionWithLocale:[NSLocale currentLocale]];
        NSString *stringDate = [formatter stringFromDate:date];
        
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        NSString *monthName = [[df monthSymbols] objectAtIndex:([stringDate integerValue]-1)];
        monthName = [monthName substringToIndex:3];
        
        [startDateArrayFormatted addObject:monthName];
    }
    _graphStartDate365D = [[NSArray alloc] initWithArray:startDateArrayFormatted];
    
    
    //for (int i = 0; i < [startDateArray count] && i < [graphValuesDAY count]; i++)
    //    NSLog(@"%@ : %@", [graphStartDateDAY objectAtIndex:i], [graphValuesDAY objectAtIndex:i]);
    
    
    NSMutableArray *maxValueArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [_graphValues365D count]; i++)
    {
        if ([_graphValues365D objectAtIndex:i] != [NSNull null])
        {
            NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
            [f setNumberStyle:NSNumberFormatterDecimalStyle];
            NSNumber *myNumber = [f numberFromString:[_graphValues365D objectAtIndex:i]];
            [maxValueArray addObject:myNumber];
        }
    }
    
    NSNumber *highestNumber = [NSNumber numberWithFloat:0.0];
    
    for (NSNumber *theNumber in maxValueArray)
    {
        NSComparisonResult Val;
        Val = [theNumber compare: highestNumber];

        if (Val == NSOrderedSame) {
        } else if (Val == NSOrderedAscending) {
        } else if (Val == NSOrderedDescending) {
            highestNumber = theNumber;        
        }
    }
    _max365DValue = highestNumber;
}


- (void)fetchGraphDataRequestDataWTD:(NSData *)responseData
{
    NSLog(@"Fetching WTD Graph Data");
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* data = [json objectForKey:@"fields"];
    
    //NSLog(@"%@", json);
    
    NSArray *graphValues = [data valueForKeyPath:@"values.value"];
    NSMutableArray *graphStartDate = [data valueForKeyPath:@"values.startLabel"];
    
    NSArray *startDateArray = [graphStartDate objectAtIndex:1];
    
    _graphValuesWTD = [graphValues objectAtIndex:1];
    
    NSMutableArray *startDateArrayFormatted = [[NSMutableArray alloc] init];
    NSDateFormatter *Dformatter = [[NSDateFormatter alloc] init];
    Dformatter.dateFormat = @"yyyyMMddHHmmss";
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM"];

    NSDateFormatter *dayformatter = [[NSDateFormatter alloc] init];
    [dayformatter setDateFormat:@"dd"];
    
    for (int i = 0; i < [startDateArray count] && i < [_graphValuesWTD count]; i++)
    {
        NSDate *date = [Dformatter dateFromString:[startDateArray objectAtIndex:i]];
        //NSString *localDate = [date descriptionWithLocale:[NSLocale currentLocale]];
        NSString *stringDate = [formatter stringFromDate:date];
        NSString *dayStringDate = [dayformatter stringFromDate:date];

        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        NSString *monthName = [[df monthSymbols] objectAtIndex:([stringDate integerValue]-1)];
        monthName = [monthName substringToIndex:3];
        
        NSString *string = [[NSString alloc] initWithFormat:@"%@ %@", monthName, dayStringDate];
        
        [startDateArrayFormatted addObject:string];
    }
    _graphStartDateWTD = [[NSArray alloc] initWithArray:startDateArrayFormatted];
    
    
    //for (int i = 0; i < [startDateArray count] && i < [graphValuesDAY count]; i++)
    //    NSLog(@"%@ : %@", [graphStartDateDAY objectAtIndex:i], [graphValuesDAY objectAtIndex:i]);
    
    
    NSMutableArray *maxValueArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [_graphValuesWTD count]; i++)
    {
        if ([_graphValuesWTD objectAtIndex:i] != [NSNull null])
        {
            NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
            [f setNumberStyle:NSNumberFormatterDecimalStyle];
            NSNumber *myNumber = [f numberFromString:[_graphValuesWTD objectAtIndex:i]];
            [maxValueArray addObject:myNumber];
        }
    }
    
    NSNumber *highestNumber = [NSNumber numberWithFloat:0.0];
    
    for (NSNumber *theNumber in maxValueArray)
    {
        NSComparisonResult Val;
        Val = [theNumber compare: highestNumber];

        if (Val == NSOrderedSame) {
        } else if (Val == NSOrderedAscending) {
        } else if (Val == NSOrderedDescending) {
            highestNumber = theNumber;        
        }
    }
    
    _maxWTDValue = highestNumber;
}


- (void)fetchGraphDataRequestDataMTD:(NSData *)responseData
{
    NSLog(@"Fetching MTD Graph Data");
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* data = [json objectForKey:@"fields"];
    
    //NSLog(@"%@", json);
    
    NSArray *graphValues = [data valueForKeyPath:@"values.value"];
    NSMutableArray *graphStartDate = [data valueForKeyPath:@"values.startLabel"];
    
    NSArray *startDateArray = [graphStartDate objectAtIndex:1];
    
    _graphValuesMTD = [graphValues objectAtIndex:1];
    
    NSMutableArray *startDateArrayFormatted = [[NSMutableArray alloc] init];
    NSDateFormatter *Dformatter = [[NSDateFormatter alloc] init];
    Dformatter.dateFormat = @"yyyyMMddHHmmss";
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM"];
    
    NSDateFormatter *dayformatter = [[NSDateFormatter alloc] init];
    [dayformatter setDateFormat:@"dd"];
    
    for (int i = 0; i < [startDateArray count] && i < [_graphValuesMTD count]; i++)
    {
        NSDate *date = [Dformatter dateFromString:[startDateArray objectAtIndex:i]];
        //NSString *localDate = [date descriptionWithLocale:[NSLocale currentLocale]];
        NSString *stringDate = [formatter stringFromDate:date];
        NSString *dayStringDate = [dayformatter stringFromDate:date];

        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        NSString *monthName = [[df monthSymbols] objectAtIndex:([stringDate integerValue]-1)];
        monthName = [monthName substringToIndex:3];
        
        NSString *string = [[NSString alloc] initWithFormat:@"%@ %@", monthName, dayStringDate];
        
        [startDateArrayFormatted addObject:string];
    }
    
    
    _graphStartDateMTD = [[NSArray alloc] initWithArray:startDateArrayFormatted];
    
    
    //for (int i = 0; i < [startDateArray count] && i < [graphValuesDAY count]; i++)
    //    NSLog(@"%@ : %@", [graphStartDateDAY objectAtIndex:i], [graphValuesDAY objectAtIndex:i]);
    
    NSMutableArray *maxValueArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [_graphValuesMTD count]; i++)
    {
        if ([_graphValuesMTD objectAtIndex:i] != [NSNull null])
        {
            NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
            [f setNumberStyle:NSNumberFormatterDecimalStyle];
            NSNumber *myNumber = [f numberFromString:[_graphValuesMTD objectAtIndex:i]];
            [maxValueArray addObject:myNumber];
        }
    }
    
    NSNumber *highestNumber = [NSNumber numberWithFloat:0.0];
    
    for (NSNumber *theNumber in maxValueArray)
    {
        NSComparisonResult Val;
        Val = [theNumber compare: highestNumber];

        if (Val == NSOrderedSame) {
        } else if (Val == NSOrderedAscending) {
        } else if (Val == NSOrderedDescending) {
            highestNumber = theNumber;        
        }
    }
    
    _maxMTDValue = highestNumber;
}



- (void)fetchGraphDataRequestDataYTD:(NSData *)responseData
{
    NSLog(@"Fetching YTD Graph Data");
    
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    
    NSArray* data = [json objectForKey:@"fields"];
    
    //NSLog(@"%@", json);
    
    NSArray *graphValues = [data valueForKeyPath:@"values.value"];
    NSMutableArray *graphStartDate = [data valueForKeyPath:@"values.startLabel"];
    
    NSArray *startDateArray = [graphStartDate objectAtIndex:1];
    
    _graphValuesYTD = [graphValues objectAtIndex:1];
    
    NSMutableArray *startDateArrayFormatted = [[NSMutableArray alloc] init];
    NSDateFormatter *Dformatter = [[NSDateFormatter alloc] init];
    Dformatter.dateFormat = @"yyyyMMddHHmmss";
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM"];
    
    for (int i = 0; i < [startDateArray count] && i < [_graphValuesYTD count]; i++)
    {
        NSDate *date = [Dformatter dateFromString:[startDateArray objectAtIndex:i]];
        //NSString *localDate = [date descriptionWithLocale:[NSLocale currentLocale]];
        NSString *stringDate = [formatter stringFromDate:date];

        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        NSString *monthName = [[df monthSymbols] objectAtIndex:([stringDate integerValue]-1)];
        monthName = [monthName substringToIndex:3];
        [startDateArrayFormatted addObject:monthName];

    }
    
    
    _graphStartDateYTD = [[NSArray alloc] initWithArray:startDateArrayFormatted];
    
    
    //for (int i = 0; i < [startDateArray count] && i < [graphValuesDAY count]; i++)
    //    NSLog(@"%@ : %@", [graphStartDateDAY objectAtIndex:i], [graphValuesDAY objectAtIndex:i]);
    
    
    NSMutableArray *maxValueArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [_graphValuesYTD count]; i++)
    {
        if ([_graphValuesYTD objectAtIndex:i] != [NSNull null])
        {
            NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
            [f setNumberStyle:NSNumberFormatterDecimalStyle];
            NSNumber *myNumber = [f numberFromString:[_graphValuesYTD objectAtIndex:i]];
            [maxValueArray addObject:myNumber];
        }
    }
    
    NSNumber *highestNumber = [NSNumber numberWithFloat:0.0];
    
    for (NSNumber *theNumber in maxValueArray)
    {
        NSComparisonResult Val;
        Val = [theNumber compare: highestNumber];

        if (Val == NSOrderedSame) {
        } else if (Val == NSOrderedAscending) {
        } else if (Val == NSOrderedDescending) {
            highestNumber = theNumber;        
        }
    }
    
    _maxYTDValue = highestNumber;
}


#pragma mark - Getter Functions

- (NSString*) getPerformanceNowPercentage
{
    return _powerNowPercentage;
}

- (NSString*) getEntityName
{
    return _entityName;
}

- (NSString*) getEntityId
{
    return _entityID;
}

- (NSString*) getEntityDescription
{
    return _entityDescription;
}

- (NSString*) getEntityInstallDate
{
    return _entityInstallDate;
}



- (NSString*) getSystemStatus
{
    return _SystemStatus;
}

- (NSDictionary *) getSystemStatusAlerts
{
    return _SystemStatusAlerts;
}

- (UIImage *)getSystemStatusIcon
{
    return _SystemStatusIcon;
}


- (NSString*) getNowValue
{
    return _nowValueFormatted;
}

- (NSString*) getTodayValue
{
    return _todayValueFormatted;
}

- (NSString*) getWeekValue
{
    return _weekValueFormatted;
}

- (NSString*) getMonthValue
{
    return _monthValueFormatted;
}

- (NSString*) getLifeValue
{
    return _lifeValueFormatted;
}

- (NSNumber*) getRealLifeValue
{
    return [NSNumber numberWithFloat:[_lifeValue floatValue]];
}

- (NSString*) getSystemSizeValue
{
    return _systemSizeValue;
}

- (float) getRealGuageValue
{
    return _realGaugeValue;
}

- (NSString*) getIrradiance
{
    return _irradianceFormatted;
}

- (NSString*) getInsolation
{
    return _insolationFormatted;
}

- (NSString*) getCellTemp
{
    return _cellTempFormatted;
}

- (NSString*) getAmbientTemp
{
    return _ambientTempFormatted;
}

- (NSString*) getTVSavings
{
    return _TVSavings;
}

- (NSString*) getCarSavings
{
    return _CarSavings;
}

- (NSString*) getComputerSavings
{
    return _ComputerSavings;
}

- (NSString*) getCO2Savings
{
    return _CO2Savings;
}

- (NSString*) getNOXSavings
{
    return _NOXSavings;
}

- (NSString*) getSO2Savings
{
    return _SO2Savings;
}

- (NSString*) getCarbonOffsetMetricTonsSavings
{
    return _CarbonOffsetMetricTonsSavings;
}

- (NSString*) getCarbonOffsetAcresSavings
{
    return _CarbonOffsetAcresSavings;
}

- (NSArray*) getGraphValuesDay
{
    return _graphValuesDay;
}

- (NSArray*) getGraphDatesDay
{
    return _graphStartDateDAY;
}

- (NSArray*) getGraphValues7D
{
    return _graphValues7D;
}

- (NSArray*) getGraphDates7D
{
    return _graphStartDate7D;
}

- (NSArray*) getGraphValues30D
{
    return _graphValues30D;
}

- (NSArray*) getGraphDates30D
{
    return _graphStartDate30D;
}

- (NSArray*) getGraphValues365D
{
    return _graphValues365D;
}

- (NSArray*) getGraphDates365D
{
    return _graphStartDate365D;
}

- (NSArray*) getGraphValuesWTD
{
    return _graphValuesWTD;
}

- (NSArray*) getGraphDatesWTD
{
    return _graphStartDateWTD;
}

- (NSArray*) getGraphValuesMTD
{
    return _graphValuesMTD;
}

- (NSArray*) getGraphDatesMTD
{
    return _graphStartDateMTD;
}

- (NSArray*) getGraphValuesYTD
{
    return _graphValuesYTD;
}

- (NSArray*) getGraphDatesYTD
{
    return _graphStartDateYTD;
}

- (NSString*) getSiteAddress
{
    return _siteAddress;
}

- (NSString*) getSiteZipCode
{
    return _siteZipCode;
}

- (NSString*) getSiteRegionCode
{
    return _siteRegionCode;
}

- (NSString*) getSiteCity
{
    return _siteCity;
}

- (NSMutableArray*) getInverterDates
{
    return _inverterTimes;
}

- (NSDictionary*) getInverterNamesAndValues
{
    return _inverters;
}

- (NSNumber*) getMaxDayValue
{
    return _maxDayValue;
}

- (NSNumber*) getMax7DValue
{
    return _max7DValue;
}


- (NSNumber*) getMax30DValue
{
    return _max30DValue;
}


- (NSNumber*) getMax365DValue
{
    return _max365DValue;
}


- (NSNumber*) getMaxWTDValue
{
    return _maxWTDValue;
}


- (NSNumber*) getMaxMTDValue
{
    return _maxMTDValue;
}


- (NSNumber*) getMaxYTDValue
{
    return _maxYTDValue;
}


- (BOOL) percentageChecker
{
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    NSNumber * myNumber = [f numberFromString:[self getPerformanceNowPercentage]];
        
    NSNumber *checker = [[NSNumber alloc] initWithInt:70.00];
    
    NSComparisonResult Val;
    Val = [myNumber compare:checker];
    
    if (Val == NSOrderedSame)
        return TRUE;
    else if (Val == NSOrderedAscending)
        return FALSE;
    else
        return TRUE;
}

- (NSString*) getEntityLat
{
    return _EntityLat;
}

- (NSString*) getEntityLong
{
    return _EntityLong;
}

- (NSString*) getEntityElevation
{
    return _EntityElevation;
}

- (NSString*) getTodayString
{
    return _todayString;
}

- (NSString*) getTonightString
{
    return _tonightString;
}

- (NSString *) getAvgHumidity
{
    return _avgHumidity;
}

- (NSString *) getFeelsLike
{
    return _feelsLike;
}

- (NSString *) getVisibility
{
    return _visibility;
}

- (NSString *) getTodayDetails
{
    return _todayDetails;
}

- (NSString *) getTonightDetails
{
    return _tonightDetails;
}

- (UIImage *) getCurrentIcon
{
    return _currentIcon;
}

- (NSString *) getNowPrecipitation
{
    return _nowPrecipitation;
}

- (NSString *) getSoonestPrecipitation
{
    return _soonestPrecipitation;
}

- (NSString *) getSoonPrecipitation
{
    return _soonPrecipitation;
}

- (NSString *) getLaterPrecipitation
{
    return _laterPrecipitation;
}

- (NSString *) getWindPower
{
    return _windPower;
}

- (NSString *) getBarometerPower
{
    return _barometerPower;
}

- (NSString *) getMoonPhase
{
    return  _moonPhase;
}

- (UIImage *) getMoonIcon
{
    return  _moonIcon;
}

- (NSString *) getSunriseTime
{
    return _SunriseTime;
}

- (NSString *) getSunsetTime
{
    return _SunsetTime;
}

- (NSMutableArray *) getHourlyTimes
{
    return _hourlyTimes;
}

- (NSMutableArray *) getHourlyTemps
{
    return _hourlyTemps;
}

- (NSMutableArray *) getHourlyIcons
{
    return _hourlyIcons;
}

- (NSString *) getEnergyCosts
{
    return _currentEnergyCosts;
}

- (NSString *) getEnergyCostsDate
{
    return _currentEnergyCostsDate;
}

@end
