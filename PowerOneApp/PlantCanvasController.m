//
//  PlantCanvasController.m
//  PowerOneApp
//
//  Created by John Setting on 6/10/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#define plantLayoutCanvasBackground [NSURL URLWithString: @"https://easyview.auroravision.net/easyview/services/document/1167815/plant_layout_canvas_background"]


#import "PlantCanvasController.h"

@interface PlantCanvasController ()

@end

@implementation PlantCanvasController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //self.title = NSLocalizedString(@"Module Name", nil);
    
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];

    SWRevealViewController *revealController = [self revealViewController];
    
    //[self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"]
                                                                         style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    UIBarButtonItem *rightRevealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"]
                                                                              style:UIBarButtonItemStyleBordered target:revealController action:@selector(rightRevealToggle:)];
    
    self.navigationItem.rightBarButtonItem = rightRevealButtonItem;
    
    NSURL *url = plantLayoutCanvasBackground;
    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage *img = [UIImage imageWithData:data];
    
    
    //UIImageView *image = [[UIImageView alloc] initWithImage:[PlantCanvasController resizeImage:img newSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height)]];//navbar.bounds.size.height)]];
    
    
    UIScrollView *scroller = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    scroller.delegate = self;
    scroller.showsHorizontalScrollIndicator = YES;
    scroller.showsVerticalScrollIndicator = YES;
    scroller.scrollEnabled = YES;
    scroller.contentSize = CGSizeMake(img.size.width, img.size.height);
    [self.view addSubview:scroller];
    
     
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(scroller.frame.origin.x, scroller.frame.origin.y, scroller.frame.size.width, scroller.frame.size.height)];
    image.image = img;
    image.contentMode = UIViewContentModeScaleAspectFill;
    [scroller addSubview:image];
    

    
    numbers = [[NSMutableArray alloc] init];
    [numbers addObject:[NSNumber numberWithInt:-3]];
    [numbers addObject:[NSNumber numberWithInt:0]];
    [numbers addObject:[NSNumber numberWithInt:2]];
    [numbers addObject:[NSNumber numberWithInt:4]];
    [numbers addObject:[NSNumber numberWithInt:7]];
    [numbers addObject:[NSNumber numberWithInt:10]];
    [numbers addObject:[NSNumber numberWithInt:12]];
    
    //int fullSize = [myBottle.barBottem intValue] - [myBottle.barTop intValue];
    CGRect frame = CGRectMake(50, 150, 220, 23);
    slider = [[UISlider alloc] initWithFrame:frame];
    [slider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    [slider setBackgroundColor:[UIColor clearColor]];
    slider.continuous = YES; // Make the slider 'stick' as it is moved.
    [slider setMinimumValue:0];
    [slider setMaximumValue:((float)[numbers count] - 1)];
    //slider.transform = CGAffineTransformMakeRotation(M_PI * - 0.5);
    self.navigationItem.titleView = slider;
    
    
    
}

- (void)valueChanged:(UISlider*)sender
{
    
    
    NSUInteger index = (NSUInteger)(slider.value + 0.5); // Round the number.
    [slider setValue:index animated:NO];
    //NSLog(@"index: %i", index);
    
    //NSNumber *number = [numbers objectAtIndex:index]; // <-- This is the number you want.
    //NSLog(@"number: %@", number);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+ (UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize {
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGImageRef imageRef = image.CGImage;
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height);
    
    CGContextConcatCTM(context, flipVertical);
    // Draw into the context; this scales the image
    CGContextDrawImage(context, newRect, imageRef);
    
    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    CGImageRelease(newImageRef);
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
