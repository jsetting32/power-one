//
//  PowerOneLoginTabBar.h
//  PowerOneApp
//
//  Created by John Setting on 7/6/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PowerOneLogin.h"
#import "PowerOneSignUp.h"
#import "PowerOneTutorial.h"

@interface PowerOneLoginTabBar : UITabBarController <UITabBarControllerDelegate>

@end
