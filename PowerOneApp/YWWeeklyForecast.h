//
//  YWWeeklyForecast.h
//  PowerOneApp
//
//  Created by John Setting on 6/24/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YWWeeklyForecastCell.h"

@interface YWWeeklyForecast : NSObject

@property (nonatomic, retain) NSString *day;
@property (nonatomic, retain) NSString *date;
@property (nonatomic) int low;
@property (nonatomic) int high;
@property (nonatomic, retain) NSString *text;
@property (nonatomic, retain) NSString *currentTemp;
@property (nonatomic, retain) NSString *currentCity;
@property (nonatomic, retain) NSString *currentState;
@property (nonatomic) int code;
@property (nonatomic, retain) NSData *pic;






- (YWWeeklyForecastCell *) getCell:(UITableView *)tableView;

@end
