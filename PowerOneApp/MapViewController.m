/*

 Copyright (c) 2013 Joan Lluch <joan.lluch@sweetwilliamsl.com>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is furnished
 to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

*/

#import "MapViewController.h"
#define hierarchyRequest [NSURL URLWithString: @"https://easyview.auroravision.net/easyview/services/gai/plant/hierarchy.json?entityId=1167815&v=1.4.7"]
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) //1

@implementation MapViewController

@synthesize mapView;

#pragma mark - View lifecycle

- (void)viewDidLoad
{
	[super viewDidLoad];
    coords = [PullEasyViewData sharedManager];
    
    SWRevealViewController *revealController = [self revealViewController];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];

    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"]
        style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    UIBarButtonItem *rightRevealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"]
        style:UIBarButtonItemStyleBordered target:revealController action:@selector(rightRevealToggle:)];
    
    self.navigationItem.rightBarButtonItem = rightRevealButtonItem;
    
    dispatch_async(dispatch_get_main_queue(), ^{
            
        [mapView setMapType:MKMapTypeStandard];
        [mapView setZoomEnabled:YES];
        [mapView setScrollEnabled:YES];
        MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
        
        //NSLog(@"%f", [[coords getEntityLat] doubleValue]);
        //NSLog(@"%f", [[coords getEntityLong] doubleValue]);
        region.center.latitude = [[coords getEntityLat] doubleValue];
        region.center.longitude = [[coords getEntityLong] doubleValue];
        region.span.longitudeDelta = 0.01f;
        region.span.latitudeDelta = 0.01f;
        [mapView setRegion:region animated:YES];
        
        [mapView setDelegate:self];
            
        self.title = NSLocalizedString([coords getEntityName], nil);
        DisplayMap *ann = [[DisplayMap alloc] init];
        ann.title = [coords getEntityName];
        ann.subtitle = [coords getEntityDescription];
        ann.coordinate = region.center;
        [mapView addAnnotation:ann];
    });
    
    segmentController = [[UISegmentedControl alloc]initWithItems:[NSArray arrayWithObjects: @"Standard", @"Satellite", @"Hybrid", nil]];
    [segmentController addTarget:self action:@selector(indexDidChangeForSegmentedControl:) forControlEvents:UIControlEventValueChanged];
    [segmentController setFrame:CGRectMake(self.view.bounds.size.width - 160.0f, self.view.bounds.size.height - 30.0f ,150, 22)];//set frame which you want
    UIFont *font = [UIFont boldSystemFontOfSize:8.0f];
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:font forKey:UITextAttributeFont];
    [segmentController setTitleTextAttributes:attributes forState:UIControlStateNormal];
    [segmentController setSelectedSegmentIndex:0];
    [self.view addSubview:segmentController];
    

}


- (void)indexDidChangeForSegmentedControl:(UISegmentedControl *)aSegmentedControl {
    switch (aSegmentedControl.selectedSegmentIndex) {
        case 0:
            mapView.mapType = MKMapTypeStandard;
            break;
        case 1:
            mapView.mapType = MKMapTypeSatellite;
            break;
        case 2:
            mapView.mapType = MKMapTypeHybrid;
            break;
            
        default:
            break;
    }
}

//- (void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//    NSLog( @"%@: MAP", NSStringFromSelector(_cmd));
//}
//
//- (void)viewWillDisappear:(BOOL)animated
//{
//    [super viewWillDisappear:animated];
//    NSLog( @"%@: MAP", NSStringFromSelector(_cmd));
//}
//
//- (void)viewDidAppear:(BOOL)animated
//{
//    [super viewDidAppear:animated];
//    NSLog( @"%@: MAP", NSStringFromSelector(_cmd));
//}
//
//- (void)viewDidDisappear:(BOOL)animated
//{
//    [super viewDidDisappear:animated];
//    NSLog( @"%@: MAP", NSStringFromSelector(_cmd));
//}

@end