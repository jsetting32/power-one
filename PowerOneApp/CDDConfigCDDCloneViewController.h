//
//  CDDConfigCDDCloneViewController.h
//  PowerOneApp
//
//  Created by John Setting on 7/22/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDDConfigGroundfaultEnergyViewController.h"

@interface CDDConfigCDDCloneViewController : UIViewController
{
    @public
    NSString *ipAddress;
    NSString *encode;
}
- (IBAction)nextViewAction:(id)sender;
@end
