//
//  HelpCenterViewController.m
//  PowerOneApp
//
//  Created by John Setting on 6/21/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "HelpCenterViewController.h"

@interface HelpCenterViewController ()

@end

@implementation HelpCenterViewController

@synthesize helpCenterTableView = _helpCenterTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Help Center";
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];

    SWRevealViewController *revealController = [self revealViewController];
    
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"]
                                                                         style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    UIBarButtonItem *rightRevealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"19-gear.png"] style:UIBarButtonItemStyleBordered target:revealController action:@selector(rightRevealToggle:)];
    self.navigationItem.rightBarButtonItem = rightRevealButtonItem;
    
    _helpCenterTableView.delegate = self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma marl - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 3;
            break;
        case 1:
            return 7;
            break;
        case 2:
            return 7;
            break;
        default:
            break;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    switch (section)
    {
        case 0:
            sectionName = @"";
            break;
        case 1:
            sectionName = NSLocalizedString(@"Information", @"Information");
            break;
        case 2:
            sectionName = NSLocalizedString(@"Tools", @"Tools");
            break;
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    
    UIView *tempView = [[UIView alloc]initWithFrame:CGRectMake(0,0,300,20)];
    tempView = [[UIView alloc]initWithFrame:CGRectMake(0,0,300,20)];
    tempView.backgroundColor=[UIColor grayColor];
    
    UILabel *tempLabel = [[UILabel alloc]initWithFrame:CGRectMake(10,0,300,20)];
    tempLabel.backgroundColor = [UIColor clearColor];
    tempLabel.shadowColor = [UIColor blackColor];
    tempLabel.shadowOffset = CGSizeMake(0,2);
    tempLabel.textColor = [UIColor whiteColor]; //here you can change the text color of header.
    tempLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0f];
    tempLabel.font = [UIFont boldSystemFontOfSize:14.0f];
    
    switch (section)
    {
        case 0:
        {
            sectionName = NSLocalizedString(@"General", @"General");
            tempLabel.text = sectionName;
            [tempView addSubview:tempLabel];
        }
            break;
            
        case 1:
        {
            
            sectionName = NSLocalizedString(@"Information", @"Information");
            tempLabel.text = sectionName;
            [tempView addSubview:tempLabel];
        }
            break;
            
        case 2:
        {
            sectionName = NSLocalizedString(@"Tools", @"Tools");
            tempLabel.text = sectionName;
            [tempView addSubview:tempLabel];
        }
            break;
        default:
            sectionName = @"";
            break;
    }
    return tempView;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellIdentifier = @"Cell";
	UITableViewCell *cell = [_helpCenterTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    NSInteger row = indexPath.row;
	
	if (nil == cell)
	{
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        UIButton *myAccessoryButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 10, 16)];
        [myAccessoryButton setBackgroundColor:[UIColor clearColor]];
        [myAccessoryButton setImage:[UIImage imageNamed:@"left-arrow-right-hi.png"] forState:UIControlStateNormal];
        [myAccessoryButton setImage:[UIImage imageNamed:@"left-arrow-right-hi.png"] forState:UIControlStateSelected];
        [cell setAccessoryView:myAccessoryButton];
        [cell.textLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10.0f]];
        cell.textLabel.textColor = [UIColor lightGrayColor];
    }
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = cell.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor darkGrayColor]CGColor], (id)[[UIColor blackColor]CGColor], nil];
    [cell.textLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:16.0f]];
	
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    switch (indexPath.section) {
            
        case 0:
            
            if (row == 0) {
                cell.textLabel.text = @"John Setting";
                [cell.layer insertSublayer:gradient atIndex:0];
                
            } else if (row == 1) {
                cell.textLabel.text = @"Presentation Mode";
                [cell.layer insertSublayer:gradient atIndex:0];
                
            } else if (row == 2) {
                cell.textLabel.text = @"Resign Presentation Mode";
                [cell.layer insertSublayer:gradient atIndex:0];
            }
            
            break;
            
        case 1:
            
            if (row == 0) {
                cell.textLabel.text = @"Map View";
                [cell.layer insertSublayer:gradient atIndex:0];
                
            } else if (row == 1) {
                cell.textLabel.text = @"Installers";
                [cell.layer insertSublayer:gradient atIndex:0];
                
            } else if (row == 2) {
                cell.textLabel.text = @"Homeowners";
                [cell.layer insertSublayer:gradient atIndex:0];
                
            } else if (row == 3) {
                cell.textLabel.text = @"Designers";
                [cell.layer insertSublayer:gradient atIndex:0];
                
            } else if (row == 4) {
                cell.textLabel.text = @"Products";
                [cell.layer insertSublayer:gradient atIndex:0];
                
            } else if (row == 5) {
                cell.textLabel.text = @"Why Solar?";
                [cell.layer insertSublayer:gradient atIndex:0];
                
            } else if (row == 6) {
                cell.textLabel.text = @"Power Calculator";
                [cell.layer insertSublayer:gradient atIndex:0];
                
            }
            
            break;
            
        case 2:
            
            if (row == 0) {
                cell.textLabel.text = @"Send Feedback";
                [cell.layer insertSublayer:gradient atIndex:0];
                
            } else if (row == 1) {
                cell.textLabel.text = @"Help Center";
                [cell.layer insertSublayer:gradient atIndex:0];
                
            } else if (row == 2) {
                cell.textLabel.text = @"Account Settings";
                [cell.layer insertSublayer:gradient atIndex:0];
                
            } else if (row == 3) {
                cell.textLabel.text = @"Terms & Policies";
                [cell.layer insertSublayer:gradient atIndex:0];
                
            } else if (row == 4) {
                cell.textLabel.text = @"Report a Problem";
                [cell.layer insertSublayer:gradient atIndex:0];
                
            } else if (row == 5) {
                cell.textLabel.text = @"Personalize";
                [cell.layer insertSublayer:gradient atIndex:0];
                
            } else if (row == 6) {
                cell.textLabel.text = @"Logout";
                [cell.layer insertSublayer:gradient atIndex:0];
                
            }
            
            break;
            
        default:
            break;
    }
    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //SWRevealViewController *revealController = self.revealViewController;
    
    //UINavigationController *frontNavigationController = (id)revealController.frontViewController;  // <-- we know it is a NavigationController
    NSInteger row = indexPath.row;
    
    
    switch (indexPath.section) {
        case 0:
            
            if (row == 0) {
                
                UIViewController *view = [[UIViewController alloc] init];
                [self.navigationController pushViewController:view animated:YES];
                
            } else if (row == 1) {
                
                UIViewController *view = [[UIViewController alloc] init];
                [self.navigationController pushViewController:view animated:YES];
            
            } else if (row == 2) {
                
                UIViewController *view = [[UIViewController alloc] init];
                [self.navigationController pushViewController:view animated:YES];
            }
            
            break;
            
        case 1:
            if (row == 0) {
                
                UIViewController *view = [[UIViewController alloc] init];
                [self.navigationController pushViewController:view animated:YES];

            } else if (row == 1) {
                
                UIViewController *view = [[UIViewController alloc] init];
                [self.navigationController pushViewController:view animated:YES];
                
            } else if (row == 2) {
                
                UIViewController *view = [[UIViewController alloc] init];
                [self.navigationController pushViewController:view animated:YES];
                
            } else if (row == 3) {
                
                UIViewController *view = [[UIViewController alloc] init];
                [self.navigationController pushViewController:view animated:YES];
                
            } else if (row == 4) {
                
                UIViewController *view = [[UIViewController alloc] init];
                [self.navigationController pushViewController:view animated:YES];
                
            } else if (row == 5) {
                
                UIViewController *view = [[UIViewController alloc] init];
                [self.navigationController pushViewController:view animated:YES];
                
            } else if (row == 6) {
                
                UIViewController *view = [[UIViewController alloc] init];
                [self.navigationController pushViewController:view animated:YES];
                
            }
            
            break;
            
        default:
            break;
    }
}

@end
