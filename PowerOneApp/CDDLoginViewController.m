//
//  CDDLoginViewController.m
//  PowerOneApp
//
//  Created by John Setting on 7/19/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "CDDLoginViewController.h"
#import "CDDTabBarViewController.h"

@interface CDDLoginViewController ()

@property (nonatomic, strong) CDDTabBarViewController* mainView;

@end

@implementation CDDLoginViewController

@synthesize ipAddressTextField, password, username;
@synthesize loginButton;

@synthesize allEntries = _allEntries;
@synthesize configEntries = _configEntries;
@synthesize statusEntries = _statusEntries;
@synthesize feeds = _feeds;
@synthesize queue = _queue;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Logger Login";
        self.mainView = [[CDDTabBarViewController alloc] init];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [[[UIAlertView alloc] initWithTitle:@"Device Information" message:@"Please enter your devices information to start the configuration process." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil] show];
    
    self.ipAddressTextField.text = @"10.0.2.127";
    self.ipAddressTextField.text = @"10.4.1.138";
    self.username.text = @"admin";
    self.password.text = @"admin";
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"1-carbon-fiber-standard-weave-iphone-background.jpg"]];
    
    
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    //SWRevealViewController *revealController = [self revealViewController];
    
    //[self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    //[self.view addGestureRecognizer:revealController.panGestureRecognizer];
    
    //UIBarButtonItem *leftRevealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"] style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    //self.navigationItem.leftBarButtonItem = leftRevealButtonItem;
    
    //UIBarButtonItem *rightRevealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"19-gear.png"] style:UIBarButtonItemStyleBordered target:revealController action:@selector(rightRevealToggle:)];
    //self.navigationItem.rightBarButtonItem = rightRevealButtonItem;
    
    allData = [[NSMutableArray alloc] init];
    cddData = [[NSMutableArray alloc] init];
    
    ipAddressTextField.delegate = self;
    username.delegate = self;
    password.delegate = self;
    
    [loginButton addTarget:self action:@selector(pushToLoggerStatusAndConfig) forControlEvents:UIControlEventTouchUpInside];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (textField == ipAddressTextField) {
        ipAddressTextField.text = textField.text;
    } else if (textField == username) {
        username.text = textField.text;
    } else if (textField == password) {
        password.text = textField.text;
    }
    
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) pushToLoggerStatusAndConfig
{
    if ([self.username.text isEqualToString:@""] || [self.password.text isEqualToString:@""] || [self.ipAddressTextField.text isEqualToString:@""])
    {
        //NSLog(@"Please fill out login crediantials to login");
        UIAlertView *loginCredentials = [[UIAlertView alloc] initWithTitle:@"Login Credentials" message:@"Please fill in both the username and password text fields to login" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [loginCredentials show];
        
    } else {
        self.allEntries = [NSMutableArray array];
        self.configEntries = [NSMutableArray array];
        self.statusEntries = [NSMutableArray array];
        self.queue = [[NSOperationQueue alloc] init];
        //self.feeds = [NSArray arrayWithObjects:@"http://10.7.0.172/plant.xml",
        self.feeds = [NSArray arrayWithObjects:[NSString stringWithFormat:@"http://%@/plant.xml", self.ipAddressTextField.text],
                      //@"http://10.0.2.127/au/logger/v1/status",
                      nil];
        
        [self refresh];
        
    }
    

}



- (void)refresh {
    
    for (NSString *feed in _feeds) {
        
        NSURL *url = [NSURL URLWithString:feed];
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request setDelegate:self];
        [_queue addOperation:request];
    }
    
}
- (NSMutableArray *) traverseElement:(TBXMLElement *)element {
    
    
    do {
        
        if ([[TBXML elementName:element] isEqualToString:@"data"]) {
            NSMutableDictionary *loggerData = [[NSMutableDictionary alloc] init];
            
            // Display the name of the element
            //NSLog(@"%@",[TBXML elementName:element]);
            
            // Obtain first attribute from element
            TBXMLAttribute * attribute = element->firstAttribute;
            
            // if attribute is valid
            while (attribute) {
                // Display name and value of attribute to the log window
                //NSLog(@"%@->%@ = %@",  [TBXML elementName:element], [TBXML attributeName:attribute], [TBXML attributeValue:attribute]);
                
                [loggerData setObject:[TBXML attributeValue:attribute] forKey:[TBXML attributeName:attribute]];
                
                // Obtain the next attribute
                attribute = attribute->next;
            }
            //NSLog(@"%@", loggerData);
            [allData addObject:loggerData];
            //NSLog(@"%@", loggerData);
        } else if ([[TBXML elementName:element] isEqualToString:@"cdd"]){
            
            NSMutableDictionary *cddDictionaryData = [[NSMutableDictionary alloc] init];
            
            // Display the name of the element
            //NSLog(@"%@",[TBXML elementName:element]);
            
            // Obtain first attribute from element
            TBXMLAttribute * attribute = element->firstAttribute;
            
            // if attribute is valid
            while (attribute) {
                // Display name and value of attribute to the log window
                //NSLog(@"%@->%@ = %@",  [TBXML elementName:element], [TBXML attributeName:attribute], [TBXML attributeValue:attribute]);
                
                [cddDictionaryData setObject:[TBXML attributeValue:attribute] forKey:[TBXML attributeName:attribute]];
                
                // Obtain the next attribute
                attribute = attribute->next;
            }
            
            [cddData addObject:cddDictionaryData];
            [allData addObject:cddData];
            
        } else if ([[TBXML elementName:element] isEqualToString:@"edd"]) {
            
            
            NSMutableDictionary *eddDictionaryData = [[NSMutableDictionary alloc] init];
            
            // Display the name of the element
            //NSLog(@"%@",[TBXML elementName:element]);
            
            // Obtain first attribute from element
            TBXMLAttribute * attribute = element->firstAttribute;
            
            // if attribute is valid
            while (attribute) {
                // Display name and value of attribute to the log window
                //NSLog(@"%@->%@ = %@",  [TBXML elementName:element], [TBXML attributeName:attribute], [TBXML attributeValue:attribute]);
                
                [eddDictionaryData setObject:[TBXML attributeValue:attribute] forKey:[TBXML attributeName:attribute]];
                
                // Obtain the next attribute
                attribute = attribute->next;
            }
            
            [cddData addObject:eddDictionaryData];
        }
        
        
        
        //if the element has child elements, process them
        if (element->firstChild)
            [self traverseElement:element->firstChild];
        
        // Obtain next sibling element
    } while ((element = element->nextSibling));
    
    //NSLog(@"%@", allData);
    
    return allData;
    
}

- (void)requestFinished:(ASIHTTPRequest *)request {
    
    [_queue addOperationWithBlock:^{
        int i = 0;
        TBXML *configTBXML = [[TBXML alloc] initWithXMLData:[request responseData]];
        TBXMLElement *root = configTBXML.rootXMLElement;
        
        if (root) {
            [self traverseElement:root];
        }
        
        [_allEntries insertObject:allData atIndex:i];
    }];
    
    self.mainView->ipAddress = ipAddressTextField.text;
    self.mainView->encodedInfo = [[NSString stringWithFormat:@"%@:%@", username.text, password.text] base64EncodedString];
    self.mainView.title = @"CDD Data";
    //[self.navigationController pushViewController:self.mainView animated:YES];
    [self presentViewController:self.mainView animated:YES completion:nil];
}
- (void)requestFailed:(ASIHTTPRequest *)request {
    NSError *error = [request error];
    NSLog(@"Error: %@", error);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Issue" message:@"We are having issues connecting to the logger. Please pull the tableview down to refresh and try again." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
    [alert show];
}

- (IBAction)cancelButtonAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
