//
//  TermsAndPoliciesViewController.m
//  PowerOneApp
//
//  Created by John Setting on 6/21/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "TermsAndPoliciesViewController.h"

@interface TermsAndPoliciesViewController ()

@end

@implementation TermsAndPoliciesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Terms and Policies";
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];

    SWRevealViewController *revealController = [self revealViewController];
    
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"]
                                                                         style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    UIBarButtonItem *rightRevealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"19-gear.png"] style:UIBarButtonItemStyleBordered target:revealController action:@selector(rightRevealToggle:)];
    self.navigationItem.rightBarButtonItem = rightRevealButtonItem;
    
    
    MBProgressHUD *HUD;
	HUD = [[MBProgressHUD alloc] initWithView:self.view];
	[self.view addSubview:HUD];
	HUD.delegate = self;
	[HUD showWhileExecuting:@selector(performFetchOnMainThread) onTarget:self withObject:nil animated:YES];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) getTerms {
    NSURL *termAndPoliciesURL = [NSURL URLWithString:@"http://www.power-one.com/sites/power-one.com/files/documents/renewable-energy/warranty/terms_and_conditions_of_warranty_2012.pdf"];
    UIWebView *webview = [[UIWebView alloc] initWithFrame:self.view.bounds];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:termAndPoliciesURL];
    [webview loadRequest:requestObj];
    webview.delegate=self;
    webview.scalesPageToFit=YES;
    [self.view addSubview:webview];
}

-(void) performFetchOnMainThread    {
    [self performSelectorOnMainThread:@selector(getTerms) withObject:nil waitUntilDone:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    // Disable user selection
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitUserSelect='none';"];
    // Disable callout
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitTouchCallout='none';"];
    
    [webView stringByEvaluatingJavaScriptFromString:@"document.body.style.webkitTouchCallout='none'; document.body.style.KhtmlUserSelect='none'"];
}
@end
