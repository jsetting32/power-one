//
//  YWWeeklyWOEID.h
//  PowerOneApp
//
//  Created by John Setting on 6/24/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YWWeeklyWOEID : NSObject

@property (nonatomic) int woeid;
@property (nonatomic, retain) NSString *city;

@end
