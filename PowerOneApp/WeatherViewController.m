//
//  WeatherViewController.m
//  PowerOneApp
//
//  Created by John Setting on 6/21/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "WeatherViewController.h"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) //1


@implementation WeatherViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:self action:@selector(popped)];

    }
    return self;
}



//It is important that the animation durations within these animation blocks add up to 4
//(the time interval of the timer). If you change the time interval then the time intervals
//in these blocks must also be changed to refelect the amount of time an image is displayed.
//Failing to do this will mean your fading animation will go out of phase with the switching of images.





-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    float scrollViewHeight = _weatherView.frame.size.height;
    float scrollContentSizeHeight = _weatherView.contentSize.height;
    float scrollOffset = _weatherView.contentOffset.y;
    
    if (scrollOffset == 0)
    {
        // then we are at the top
    }
    else if (scrollOffset + scrollViewHeight == scrollContentSizeHeight)
    {
        // then we are at the end
        _weatherView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);

    }
}

-(void)onTimer{
    NSTimeInterval time = 10;
    
    [UIView animateWithDuration:time animations:^{
        self.image.alpha = 0.0;
    }];
    [UIView animateWithDuration:time animations:^{
        self.image.alpha = 1.0;
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSLog(@"%@", images);
    
    self.image = [[UIImageView alloc] initWithFrame:CGRectMake(0, -20, self.view.frame.size.width, self.view.frame.size.height - 44)];
    self.image.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.image];

    NSTimeInterval time = 400.0;
    self.image.animationImages = [WeatherViewController getImages];
    self.image.animationDuration = time;
    self.image.animationRepeatCount = 0;
    [self.image startAnimating];
    
    NSTimeInterval timerr = 30.0;
    
    NSTimer *timer = [NSTimer timerWithTimeInterval:timerr
                                             target:self
                                           selector:@selector(onTimer)
                                           userInfo:nil
                                            repeats:YES];
    
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
    [timer fire];
    
    
    _weatherView = [[UIScrollView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + 10, self.view.bounds.size.width, self.view.bounds.size.height - 44)];
    _weatherView.contentSize = CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height * 4 - 44);
    _weatherView.backgroundColor = [UIColor clearColor];
    _weatherView.showsVerticalScrollIndicator = NO;
    _weatherView.userInteractionEnabled = YES;
    
    [self.view addSubview:_weatherView];
    
    
    
    _weatherNow = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x + 5, self.view.frame.origin.y + 275, 120, 140)];
    _weatherNow.backgroundColor = [UIColor clearColor];
    
    /************************ Scroller Subviews ***************************/
    [_weatherView addSubview:_weatherNow];
    [_weatherView addSubview:[self returnPanelDetailsView]];
    [_weatherView addSubview:[self returnForecastView]];
    [_weatherView addSubview:[self returnDetailsView]];
    [_weatherView addSubview:[self returnMapView]];
    [_weatherView addSubview:[self returnPrecipitationView]];
    [_weatherView addSubview:[self returnWindAndPressureView]];
    [_weatherView addSubview:[self returnSunAndMoonView]];
    /************************ Scroller Subviews ***************************/
    
    NSArray *array;
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Weather Data Fault" message:@"Currently, we are having issues connecting to the weather data. Please check your internet or maybe the weather station is down." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles: nil];
    
    if ([NSString stringWithFormat:@"%i", [_zipCode intValue]] == NULL || [_zipCode intValue] == 0) {
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
    } else {
        array = [[NSArray alloc] initWithArray:[YWWeeklyHelper getWeather:[_zipCode intValue]]];
    }
    
    NSUInteger index = 0;
    
    for (YWWeeklyForecast *forecast in array) {
        if (index == 0)
        {            

            self.title =  [NSString stringWithFormat:@"%@, %@" ,forecast.currentCity, forecast.currentState];
            
            
            
            _todayNowTemp = [[UILabel alloc] initWithFrame:CGRectMake(10, 60, 120, 60)];
            [_todayNowTemp setFont:[UIFont fontWithName:@"Arial" size:70.0f]];
            _todayNowTemp.text = [NSString stringWithFormat:@"%@°", forecast.currentTemp];
            _todayNowTemp.textColor = [UIColor whiteColor];
            _todayNowTemp.backgroundColor = [UIColor clearColor];
            [_weatherNow addSubview:_todayNowTemp];
            
            UIImageView *lowImageIcon = [[UIImageView alloc] initWithFrame:CGRectMake(55, 42, 5, 15)];
            lowImageIcon.image = [UIImage imageNamed:@"arrow_white_down.png"];
            [_weatherNow addSubview:lowImageIcon];
            
            _todayDayLow = [[UILabel alloc] initWithFrame:CGRectMake(65, 40, 40, 20)];
            [_todayDayLow setFont:[UIFont fontWithName:@"Arial" size:14.0f]];
            _todayDayLow.text = [NSString stringWithFormat:@"%i°", forecast.low];
            _todayDayLow.textColor = [UIColor whiteColor];
            _todayDayLow.backgroundColor = [UIColor clearColor];
            [_weatherNow addSubview:_todayDayLow];
            
            
            UIImageView *highImageIcon = [[UIImageView alloc] initWithFrame:CGRectMake(15, 42, 5, 15)];
            highImageIcon.image = [UIImage imageNamed:@"arrow_white_up.png"];
            [_weatherNow addSubview:highImageIcon];
        
            _todayDayHigh = [[UILabel alloc] initWithFrame:CGRectMake(25, 40, 40, 20)];
            _todayDayHigh.text = [NSString stringWithFormat:@"%i°", forecast.high];
            [_todayDayHigh setFont:[UIFont fontWithName:@"Arial" size:14.0f]];
            _todayDayHigh.textColor = [UIColor whiteColor];
            _todayDayHigh.backgroundColor = [UIColor clearColor];
            [_weatherNow addSubview:_todayDayHigh];
            
            _nowImageDescription = [[UILabel alloc] initWithFrame:CGRectMake(35, 20, 140, 20)];
            _nowImageDescription.text = forecast.text;
            [_nowImageDescription setFont:[UIFont fontWithName:@"Arial" size:12.0f]];
            _nowImageDescription.textColor = [UIColor whiteColor];
            _nowImageDescription.backgroundColor = [UIColor clearColor];
            [_weatherNow addSubview:_nowImageDescription];
            
            _NowImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 20, 20, 20)];
            NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"http://l.yimg.com/a/i/us/we/52/%i.gif", forecast.code]];
            NSData *data = [NSData dataWithContentsOfURL:URL];
            UIImage *img = [[UIImage alloc] initWithData:data];
            _NowImage.image = img;
            [_weatherNow addSubview:_NowImage];
            /********************* Current Weather ************************/
            
            
        
            
            _todayHighs = [[UILabel alloc] initWithFrame:CGRectMake(200, 0, 40, 40)];
            _todayHighs.text = [NSString stringWithFormat:@"%i°", forecast.high];
            _todayHighs.textColor = [UIColor whiteColor];
            _todayHighs.backgroundColor = [UIColor clearColor];
            [_todayOverallForecast addSubview:_todayHighs];
            
            _todayLows = [[UILabel alloc] initWithFrame:CGRectMake(240, 0, 40, 40)];
            _todayLows.text = [NSString stringWithFormat:@"%i°", forecast.low];
            _todayLows.textColor = [UIColor blueColor];
            _todayLows.backgroundColor = [UIColor clearColor];
            [_todayOverallForecast addSubview:_todayLows];
            
            _todayDayTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 70, 40)];
            _todayDayTitle.text = forecast.day;
            _todayDayTitle.textColor = [UIColor whiteColor];
            [_todayDayTitle setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10.0f]];
            _todayDayTitle.backgroundColor = [UIColor clearColor];
            [_todayOverallForecast addSubview:_todayDayTitle];
            
            _todayDayImage = [[UIImageView alloc] initWithFrame:CGRectMake(120, 10, 20, 20)];
            URL = [NSURL URLWithString:[NSString stringWithFormat:@"http://l.yimg.com/a/i/us/we/52/%i.gif", forecast.code]];
            data = [NSData dataWithContentsOfURL:URL];
            img = [[UIImage alloc] initWithData:data];
            _todayDayImage.image = img;
            [_todayOverallForecast addSubview:_todayDayImage];
            
        } else if (index == 1) {
            //NSLog(@"%@: %@: %d: %d: %@: %d", forecast.day, forecast.date, forecast.low, forecast.high, forecast.text, forecast.code);
            _nextDayHigh = [[UILabel alloc] initWithFrame:CGRectMake(200, 0, 40, 40)];
            _nextDayHigh.text = [NSString stringWithFormat:@"%i°", forecast.high];
            _nextDayHigh.textColor = [UIColor whiteColor];
            _nextDayHigh.backgroundColor = [UIColor clearColor];
            [_nextDayOverallForecast addSubview:_nextDayHigh];

            _nextDayLow = [[UILabel alloc] initWithFrame:CGRectMake(240, 0, 40, 40)];
            _nextDayLow.text = [NSString stringWithFormat:@"%i°", forecast.low];
            _nextDayLow.textColor = [UIColor blueColor];
            _nextDayLow.backgroundColor = [UIColor clearColor];
            [_nextDayOverallForecast addSubview:_nextDayLow];

            _nextDayImage = [[UIImageView alloc] initWithFrame:CGRectMake(120, 10, 20, 20)];
            NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"http://l.yimg.com/a/i/us/we/52/%i.gif", forecast.code]];
            NSData *data = [NSData dataWithContentsOfURL:URL];
            UIImage *img = [[UIImage alloc] initWithData:data];
            _nextDayImage.image = img;
            [_nextDayOverallForecast addSubview:_nextDayImage];
            
            _nextDayTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 70, 40)];
            _nextDayTitle.text = forecast.day;
            _nextDayTitle.textColor = [UIColor whiteColor];
            [_nextDayTitle setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10.0f]];
            _nextDayTitle.backgroundColor = [UIColor clearColor];
            [_nextDayOverallForecast addSubview:_nextDayTitle];
            
        } else if (index == 2) {
            //NSLog(@"%@: %@: %d: %d: %@: %d", forecast.day, forecast.date, forecast.low, forecast.high, forecast.text, forecast.code);
            _nextnextDayHigh = [[UILabel alloc] initWithFrame:CGRectMake(200, 0, 40, 40)];
            _nextnextDayHigh.text = [NSString stringWithFormat:@"%i°", forecast.high];
            _nextnextDayHigh.textColor = [UIColor whiteColor];
            _nextnextDayHigh.backgroundColor = [UIColor clearColor];
            [_nextnextDayOverallForecast addSubview:_nextnextDayHigh];

            _nextnextDayLow = [[UILabel alloc] initWithFrame:CGRectMake(240, 0, 40, 40)];
            _nextnextDayLow.text = [NSString stringWithFormat:@"%i°", forecast.low];
            _nextnextDayLow.textColor = [UIColor blueColor];
            _nextnextDayLow.backgroundColor = [UIColor clearColor];
            [_nextnextDayOverallForecast addSubview:_nextnextDayLow];

            _nextnextDayImage = [[UIImageView alloc] initWithFrame:CGRectMake(120, 10, 20, 20)];
            NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"http://l.yimg.com/a/i/us/we/52/%i.gif", forecast.code]];
            NSData *data = [NSData dataWithContentsOfURL:URL];
            UIImage *img = [[UIImage alloc] initWithData:data];
            _nextnextDayImage.image = img;
            [_nextnextDayOverallForecast addSubview:_nextnextDayImage];
            
            _nextnextDayTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 70, 40)];
            _nextnextDayTitle.text = forecast.day;
            _nextnextDayTitle.textColor = [UIColor whiteColor];
            [_nextnextDayTitle setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10.0f]];
            _nextnextDayTitle.backgroundColor = [UIColor clearColor];
            [_nextnextDayOverallForecast addSubview:_nextnextDayTitle];
            
        } else if (index == 3) {
            //NSLog(@"%@: %@: %d: %d: %@: %d", forecast.day, forecast.date, forecast.low, forecast.high, forecast.text, forecast.code);
            _nextnextnextDayHigh = [[UILabel alloc] initWithFrame:CGRectMake(200, 0, 40, 40)];
            _nextnextnextDayHigh.text = [NSString stringWithFormat:@"%i°", forecast.high];
            _nextnextnextDayHigh.textColor = [UIColor whiteColor];
            _nextnextnextDayHigh.backgroundColor = [UIColor clearColor];
            [_nextnextnextDayOverallForecast addSubview:_nextnextnextDayHigh];

            _nextnextnextDayLow = [[UILabel alloc] initWithFrame:CGRectMake(240, 0, 40, 40)];
            _nextnextnextDayLow.text = [NSString stringWithFormat:@"%i°", forecast.low];
            _nextnextnextDayLow.textColor = [UIColor blueColor];
            _nextnextnextDayLow.backgroundColor = [UIColor clearColor];
            [_nextnextnextDayOverallForecast addSubview:_nextnextnextDayLow];

            _nextnextnextDayImage = [[UIImageView alloc] initWithFrame:CGRectMake(120, 10, 20, 20)];
            NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"http://l.yimg.com/a/i/us/we/52/%i.gif", forecast.code]];
            NSLog(@"%i", forecast.code);
            NSData *data = [NSData dataWithContentsOfURL:URL];
            UIImage *img = [[UIImage alloc] initWithData:data];
            _nextnextnextDayImage.image = img;
            [_nextnextnextDayOverallForecast addSubview:_nextnextnextDayImage];
            
            _nextnextnextDayTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 70, 40)];
            _nextnextnextDayTitle.text = forecast.day;
            [_nextnextnextDayTitle setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10.0f]];
            _nextnextnextDayTitle.textColor = [UIColor whiteColor];
            _nextnextnextDayTitle.backgroundColor = [UIColor clearColor];
            [_nextnextnextDayOverallForecast addSubview:_nextnextnextDayTitle];
            
        } else if (index == 4) {
            //NSLog(@"%@: %@: %d: %d: %@: %d", forecast.day, forecast.date, forecast.low, forecast.high, forecast.text, forecast.code);
            _nextnextnextnextDayHigh = [[UILabel alloc] initWithFrame:CGRectMake(200, 0, 40, 40)];
            _nextnextnextnextDayHigh.text = [NSString stringWithFormat:@"%i°", forecast.high];
            _nextnextnextnextDayHigh.textColor = [UIColor whiteColor];
            _nextnextnextnextDayHigh.backgroundColor = [UIColor clearColor];
            [_nextnextnextnextDayOverallForecast addSubview:_nextnextnextnextDayHigh];

            _nextnextnextnextDayLow = [[UILabel alloc] initWithFrame:CGRectMake(240, 0, 40, 40)];
            _nextnextnextnextDayLow.text = [NSString stringWithFormat:@"%i°", forecast.low];
            _nextnextnextnextDayLow.textColor = [UIColor blueColor];
            _nextnextnextnextDayLow.backgroundColor = [UIColor clearColor];
            [_nextnextnextnextDayOverallForecast addSubview:_nextnextnextnextDayLow];

            _nextnextnextnextDayImage = [[UIImageView alloc] initWithFrame:CGRectMake(120, 10, 20, 20)];
            NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"http://l.yimg.com/a/i/us/we/52/%i.gif", forecast.code]];
            NSData *data = [NSData dataWithContentsOfURL:URL];
            UIImage *img = [[UIImage alloc] initWithData:data];
            _nextnextnextnextDayImage.image = img;
            [_nextnextnextnextDayOverallForecast addSubview:_nextnextnextnextDayImage];
            
            _nextnextnextnextDayTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 70, 40)];
            _nextnextnextnextDayTitle.text = forecast.day;
            [_nextnextnextnextDayTitle setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10.0f]];
            _nextnextnextnextDayTitle.textColor = [UIColor whiteColor];
            _nextnextnextnextDayTitle.backgroundColor = [UIColor clearColor];
            [_nextnextnextnextDayOverallForecast addSubview:_nextnextnextnextDayTitle];
            
        }
        index++;
    }
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"View Will Appear");
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postAlert:) name:@"WeatherStation" object:nil];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSLog(@"View Did Appear");
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postAlert:) name:@"WeatherStation" object:nil];
}

- (void) postAlert:(NSNotification*)notice
{
    NSLog(@"HELLO");
    
    NSString *checker = [notice object];
    
    NSLog(@"%@", checker);
    NSLog(@"%@", [checker class]);
    
    if ([checker isEqualToString:@"NO"])
        [[[UIAlertView alloc] initWithTitle:@"Weather Station" message:@"Currently, your site doesn't have a weather station installed so you are limited to viewing the ambient temperature, cell temperature, insolation, and irradiance of your panels at your site" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
}

- (UIView *)returnPanelDetailsView
{
    _plantDetailsView = [[UIView alloc] initWithFrame:CGRectMake(150, self.view.frame.origin.y + 275, 160, 120)];
    _plantDetailsView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    
    UILabel *plantLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 140, 15)];
    plantLabel.text = @"Weather Station";
    plantLabel.textColor = [UIColor whiteColor];
    [plantLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:12.0f]];
    plantLabel.textAlignment = NSTextAlignmentCenter;
    plantLabel.backgroundColor = [UIColor clearColor];
    [_plantDetailsView addSubview:plantLabel];
    
    NSString *squaredSymbol = @"\u00B2";

    _InsolationValue = [[UILabel alloc] initWithFrame:CGRectMake(10, 30, 140, 15)];
    _InsolationValue.text = [NSString stringWithFormat:@"Insolation: %@ kWh/m%@",_InsolationValueText, squaredSymbol];
    _InsolationValue.textColor = [UIColor whiteColor];
    _InsolationValue.textAlignment = NSTextAlignmentCenter;
    [_InsolationValue setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10.0f]];
    _InsolationValue.backgroundColor = [UIColor clearColor];
    [_plantDetailsView addSubview:_InsolationValue];
    
    _IrradianceValue = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, 140, 15)];
    _IrradianceValue.text = [NSString stringWithFormat:@"Irradiance: %@ W/m%@",_IrradianceValueText, squaredSymbol];
    _IrradianceValue.textColor = [UIColor whiteColor];
    _IrradianceValue.textAlignment = NSTextAlignmentCenter;
    [_IrradianceValue setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10.0f]];
    _IrradianceValue.backgroundColor = [UIColor clearColor];
    [_plantDetailsView addSubview:_IrradianceValue];
    
    _CellTempValue = [[UILabel alloc] initWithFrame:CGRectMake(10, 70, 140, 15)];
    _CellTempValue.text = [NSString stringWithFormat:@"Cell Temp: %@ °C", _CellTempValueText];
    _CellTempValue.textColor = [UIColor whiteColor];
    _CellTempValue.textAlignment = NSTextAlignmentCenter;
    [_CellTempValue setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10.0f]];
    _CellTempValue.backgroundColor = [UIColor clearColor];
    [_plantDetailsView addSubview:_CellTempValue];
    
    _AmbientTempValue = [[UILabel alloc] initWithFrame:CGRectMake(10, 90, 140, 15)];
    _AmbientTempValue.text = [NSString stringWithFormat:@"Ambient Temp: %@ °C",_AmbientValueText];
    _AmbientTempValue.textColor = [UIColor whiteColor];
    _AmbientTempValue.textAlignment = NSTextAlignmentCenter;
    [_AmbientTempValue setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10.0f]];
    _AmbientTempValue.backgroundColor = [UIColor clearColor];
    [_plantDetailsView addSubview:_AmbientTempValue];
    
    return _plantDetailsView;
}

- (UIView *)returnForecastView
{
    _ForecastView = [[UIView alloc] initWithFrame:CGRectMake(20, self.view.frame.origin.y + 440, 280, 290)]; // starts at 440 ends at 730
    _ForecastView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    
    UILabel *forecastLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 2, 100, 15)];
    forecastLabel.text = @"Forecast";
    forecastLabel.textColor = [UIColor whiteColor];
    forecastLabel.backgroundColor = [UIColor clearColor];
    [_ForecastView addSubview:forecastLabel];
    
    _hourlyForecast = [[hourlyForecastScrollView alloc] init:_city state:_state icons:_icons times:_times temps:_temps];
    _hourlyForecast.backgroundColor = [UIColor clearColor];
    
    // By default, a view will not clip a subview, set clipsToBounds = YES/TRUE or if using a xib file, checkmark the box that says "Clip Subviews"
    _hourlyForecast.clipsToBounds = YES;
    
    _hourlyForecast.frame = CGRectMake(self.ForecastView.bounds.origin.x, 20, self.ForecastView.bounds.size.width, 70);
    [_ForecastView addSubview:_hourlyForecast];
    
    _todayOverallForecast = [[UIView alloc] initWithFrame:CGRectMake(0, 90, 280, 40)];
    _todayOverallForecast.backgroundColor = [UIColor clearColor];
    [_ForecastView addSubview:_todayOverallForecast];
    
    _nextDayOverallForecast = [[UIView alloc] initWithFrame:CGRectMake(0, 130, 280, 40)];
    _nextDayOverallForecast.backgroundColor = [UIColor clearColor];
    [_ForecastView addSubview:_nextDayOverallForecast];
    
    _nextnextDayOverallForecast = [[UIView alloc] initWithFrame:CGRectMake(0, 170, 280, 40)];
    _nextnextDayOverallForecast.backgroundColor = [UIColor clearColor];
    [_ForecastView addSubview:_nextnextDayOverallForecast];
    
    _nextnextnextDayOverallForecast = [[UIView alloc] initWithFrame:CGRectMake(0, 210, 280, 40)];
    _nextnextnextDayOverallForecast.backgroundColor = [UIColor clearColor];
    [_ForecastView addSubview:_nextnextnextDayOverallForecast];
    
    _nextnextnextnextDayOverallForecast = [[UIView alloc] initWithFrame:CGRectMake(0, 250, 280, 40)];
    _nextnextnextnextDayOverallForecast.backgroundColor = [UIColor clearColor];
    [_ForecastView addSubview:_nextnextnextnextDayOverallForecast];
    
    
    return _ForecastView;
}

- (UIView *)returnDetailsView
{
    _forecastDetailView = [[UIView alloc] initWithFrame:CGRectMake(20, 500 + 290, 280, 190)]; // starts at 790 ends at 980
    _forecastDetailView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];

    UILabel *detailsLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 100, 15)];
    detailsLabel.text = @"Details";
    detailsLabel.textColor = [UIColor whiteColor];
    detailsLabel.backgroundColor = [UIColor clearColor];
    [_forecastDetailView addSubview:detailsLabel];
    
    UIImageView *icon = [[UIImageView alloc] initWithImage:_todayDetailIcon];
    icon.frame = CGRectMake(40, 40, 50, 50);
    [_forecastDetailView addSubview:icon];
    
    _feelsLikeLabel = [[UILabel alloc] initWithFrame:CGRectMake(160, 30, 110, 20)];
    _feelsLikeLabel.text = [NSString stringWithFormat:@"Feels like         %@°", _feelsLikeString];
    [_feelsLikeLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10.0f]];
    _feelsLikeLabel.backgroundColor = [UIColor clearColor];
    _feelsLikeLabel.textColor = [UIColor whiteColor];
    [_forecastDetailView addSubview:_feelsLikeLabel];
    
    _humidityLabel = [[UILabel alloc] initWithFrame:CGRectMake(160, 55, 110, 20)];
    _humidityLabel.text = [NSString stringWithFormat:@"Humidity         %@%%", [atmosphereData objectAtIndex:0]];
    [_humidityLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10.0f]];
    _humidityLabel.textColor = [UIColor whiteColor];
    _humidityLabel.backgroundColor = [UIColor clearColor];
    [_forecastDetailView addSubview:_humidityLabel];
    
    _visibilityLabel = [[UILabel alloc] initWithFrame:CGRectMake(160, 80, 110, 20)];
    if ([[atmosphereData objectAtIndex:1] isEqual:@""]) {
        _visibilityLabel.text = @"Visibility          0 mi";
    } else {
        _visibilityLabel.text = [NSString stringWithFormat:@"Visibility          %@ mi", [atmosphereData objectAtIndex:1]];
    }
    [_visibilityLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10.0f]];
    _visibilityLabel.backgroundColor = [UIColor clearColor];
    _visibilityLabel.textColor = [UIColor whiteColor];
    [_forecastDetailView addSubview:_visibilityLabel];
    
    _todayDescription = [[UILabel alloc] initWithFrame:CGRectMake(5, 110, 260, 40)];
    _todayDescription.text = [NSString stringWithFormat:@"Today - %@",_todayDescriptionString];
    _todayDescription.numberOfLines = 2;
    _todayDescription.backgroundColor = [UIColor clearColor];
    [_todayDescription setFont:[UIFont fontWithName:@"Arial-BoldMT" size:9.0f]];
    _todayDescription.textColor = [UIColor whiteColor];
    [_forecastDetailView addSubview:_todayDescription];
    
    _tonightDescription = [[UILabel alloc] initWithFrame:CGRectMake(5, 150, 260, 40)];
    _tonightDescription.text = [NSString stringWithFormat:@"Tonight - %@",_tonightDescriptionString];
    _tonightDescription.numberOfLines = 2;
    _tonightDescription.backgroundColor = [UIColor clearColor];
    [_tonightDescription setFont:[UIFont fontWithName:@"Arial-BoldMT" size:9.0f]];
    _tonightDescription.textColor = [UIColor whiteColor];
    [_forecastDetailView addSubview:_tonightDescription];
    
    return _forecastDetailView;
}

- (UIView *)returnMapView
{
    _mapView = [[UIView alloc] initWithFrame:CGRectMake(20, 1050, 280, 200)];
    _mapView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];

    return _mapView;
}

- (UIView *)returnPrecipitationView
{
    _precipitationView = [[UIView alloc] initWithFrame:CGRectMake(20, 1310, 280, 200)];
    _precipitationView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];

    return _precipitationView;
}

- (UIView *)returnWindAndPressureView
{

    _windAndPressureView = [[UIView alloc] initWithFrame:CGRectMake(20, 1570, 280 , 200)];
    _windAndPressureView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];

    
    
    _windPowerLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 100, 80, 20)];
    [_windPowerLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:8.0f]];
    _windPowerLabel.numberOfLines = 2;
    _windPowerLabel.backgroundColor = [UIColor clearColor];
    _windPowerLabel.textColor = [UIColor whiteColor];
    [_windAndPressureView addSubview:_windPowerLabel];
    
    if (([[windSpeedAndDirection objectAtIndex:0] integerValue] >= 340) || ([[windSpeedAndDirection objectAtIndex:0] integerValue] <= 20)) {
        _windPowerLabel.text = [NSString stringWithFormat:@"Wind\n%@ mph N", [windSpeedAndDirection objectAtIndex:1]];
    }else if (([[windSpeedAndDirection objectAtIndex:0]integerValue] > 20) && ([[windSpeedAndDirection objectAtIndex:0]integerValue] <= 60)) {
        _windPowerLabel.text = [NSString stringWithFormat:@"Wind\n%@ mph NE", [windSpeedAndDirection objectAtIndex:1]];
    }else if (([[windSpeedAndDirection objectAtIndex:0]integerValue] > 60) && ([[windSpeedAndDirection objectAtIndex:0]integerValue] <= 100)) {
        _windPowerLabel.text = [NSString stringWithFormat:@"Wind\n%@ mph E", [windSpeedAndDirection objectAtIndex:1]];
    }else if (([[windSpeedAndDirection objectAtIndex:0]integerValue] > 100) && ([[windSpeedAndDirection objectAtIndex:0]integerValue] <= 140)) {
        _windPowerLabel.text = [NSString stringWithFormat:@"Wind\n%@ mph SE", [windSpeedAndDirection objectAtIndex:1]];
    }else if (([[windSpeedAndDirection objectAtIndex:0]integerValue] > 180) && ([[windSpeedAndDirection objectAtIndex:0]integerValue] <= 220)) {
        _windPowerLabel.text = [NSString stringWithFormat:@"Wind\n%@ mph S", [windSpeedAndDirection objectAtIndex:1]];
    }else if (([[windSpeedAndDirection objectAtIndex:0]integerValue] > 220) && ([[windSpeedAndDirection objectAtIndex:0]integerValue] <= 260)) {
        _windPowerLabel.text = [NSString stringWithFormat:@"Wind\n%@ mph SW", [windSpeedAndDirection objectAtIndex:1]];
    }else if (([[windSpeedAndDirection objectAtIndex:0]integerValue] > 260) && ([[windSpeedAndDirection objectAtIndex:0]integerValue] <= 300)) {
        _windPowerLabel.text = [NSString stringWithFormat:@"Wind\n%@ mph W", [windSpeedAndDirection objectAtIndex:1]];
    }else if (([[windSpeedAndDirection objectAtIndex:0]integerValue] > 300) && ([[windSpeedAndDirection objectAtIndex:0]integerValue] < 340)) {
        _windPowerLabel.text = [NSString stringWithFormat:@"Wind\n%@ mph NW", [windSpeedAndDirection objectAtIndex:1]];
    }
    
    
    
    _barometerLabel = [[UILabel alloc] initWithFrame:CGRectMake(220, 100, 80, 20)];
    _barometerLabel.text = [NSString stringWithFormat:@"%@ in", [atmosphereData objectAtIndex:2]];
    [_barometerLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10.0f]];
    _barometerLabel.backgroundColor = [UIColor clearColor];
    _barometerLabel.textAlignment = NSTextAlignmentCenter;
    _barometerLabel.textColor = [UIColor whiteColor];
    [_windAndPressureView addSubview:_barometerLabel];
    
    
    UIImageView *pinIcon = [[UIImageView alloc] initWithFrame:CGRectMake(28, 30, 20, 100)];
    [pinIcon setImage:[UIImage imageNamed:@"pinWindmill.png"]];
    [_windAndPressureView addSubview:pinIcon];
    
    UIImageView *windmill = [[UIImageView alloc] initWithFrame:CGRectMake(20, 15, 40, 40)];
    [windmill setImage:[UIImage imageNamed:@"Windmill_logo.gif"]];
     [_windAndPressureView addSubview:windmill];

    CABasicAnimation *fullRotationAnimation;
    fullRotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    fullRotationAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    fullRotationAnimation.toValue = [NSNumber numberWithFloat:2 * M_PI];
    //fullRotationAnimation.duration = 30.0 - ([_windPowerString integerValue] * .1);
    if ([_windPowerString integerValue] > 0) {
        fullRotationAnimation.duration = 30.0 - [_windPowerString integerValue] * 2;
    } else if (30.0 - [_windPowerString integerValue] * 2 < 0) {
        fullRotationAnimation.duration = .5;
    }
    //else if ([_windPowerString integerValue] == 0) {
    //    fullRotationAnimation.duration = -1;
    //    NSLog(@"ZERO");
    //}
    
    //NSLog(@"%i",[_windPowerString integerValue]);
    
    fullRotationAnimation.repeatCount = HUGE_VALF;
    [windmill.layer addAnimation:fullRotationAnimation forKey:@"360"];
    
    return _windAndPressureView;
}

- (UIView *)returnSunAndMoonView
{
    _sunriseAndSunsetView = [[UIView alloc] initWithFrame:CGRectMake(20, 1830, 280 , 200)];
    _sunriseAndSunsetView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];

    _sunriseTime = [[UILabel alloc] initWithFrame:CGRectMake(60, 100, 80, 20)];
    _sunriseTime.text = [sunRiseandSetTimes objectAtIndex:0];
    [_sunriseTime setFont:[UIFont fontWithName:@"Arial-BoldMT" size:8.0f]];
    _sunriseTime.backgroundColor = [UIColor clearColor];
    _sunriseTime.textColor = [UIColor whiteColor];
    _sunriseTime.textAlignment = NSTextAlignmentCenter;
    [_sunriseAndSunsetView addSubview:_sunriseTime];
    
    _sunsetTime = [[UILabel alloc] initWithFrame:CGRectMake(220, 100, 80, 20)];
    _sunsetTime.text = [sunRiseandSetTimes objectAtIndex:1];
    [_sunsetTime setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10.0f]];
    _sunsetTime.backgroundColor = [UIColor clearColor];
    _sunsetTime.textColor = [UIColor whiteColor];
    _sunsetTime.textAlignment = NSTextAlignmentCenter;
    [_sunriseAndSunsetView addSubview:_sunsetTime];
    

    
    return _sunriseAndSunsetView;
}

- (void) popped
{
    //NSLog(@"Popped");
    
    [UIView  transitionWithView:self.navigationController.view duration:0.4  options:UIViewAnimationOptionTransitionFlipFromRight
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         [self.navigationController popViewControllerAnimated:YES];
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

static NSArray* images;

+ (NSArray *)getImages
{
    return images;
}

+ (void)setImages:(NSArray*)newImages
{
    if (images != newImages) {
    	images = [newImages copy];
    }
}

static NSArray * sunRiseandSetTimes;

#pragma mark - Class methods
+ (NSArray*)setsunRiseandSetTimes
{
    return sunRiseandSetTimes;
}

+ (void)setsunRiseandSetTimes:(NSArray*)withSunRiseandSetTimes
{
    if (sunRiseandSetTimes != withSunRiseandSetTimes) {
    	//[str release];
    	sunRiseandSetTimes = [withSunRiseandSetTimes copy];
    }
}

static NSArray * windSpeedAndDirection;

#pragma mark - Class methods
+ (NSArray*)setWindSpeedAndDirection
{
    return windSpeedAndDirection;
}

+ (void)setWindSpeedAndDirection:(NSArray*)windSpeedAndDirectionSetter
{
    if (windSpeedAndDirection != windSpeedAndDirectionSetter) {
    	//[str release];
    	windSpeedAndDirection = [windSpeedAndDirectionSetter copy];
    }
}

static NSArray * atmosphereData;

#pragma mark - Class methods
+ (NSArray*)setAtmosphereData
{
    return atmosphereData;
}

+ (void)setAtmosphereData:(NSArray*)atmosphereDataSetter
{
    if (atmosphereData != atmosphereDataSetter) {
    	//[str release];
    	atmosphereData = [atmosphereDataSetter copy];
    }
}


@end
