//
//  CDDConfigDateTimeViewController.m
//  PowerOneApp
//
//  Created by John Setting on 7/22/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "CDDConfigDateTimeViewController.h"

@interface CDDConfigDateTimeViewController ()

@end

@implementation CDDConfigDateTimeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Date/Time";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)ConfirmAction:(id)sender {
    

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)nextViewAction:(id)sender {
   
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/protect/datehour.htm", ipAddress]]];
    
    NSLog(@"%@", encode);
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
    [request setValue:@"gzip, deflate" forHTTPHeaderField:@"Accept-Encoding"];
    [request setValue:[NSString stringWithFormat:@"Basic %@", encode] forHTTPHeaderField:@"Authorization"];
    [request setValue:@"keep-alive" forHTTPHeaderField:@"Connection"];
    [request setValue:ipAddress forHTTPHeaderField:@"Host"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    NSString* astr = @"day=22&mon=07&year=13&hour=11&min=17&sec=32&epoc=1";
    
    [request setHTTPBody:[astr dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSError *error;
    NSURLResponse *response;
    
    NSLog(@"%@", response);
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString* another = [NSString stringWithUTF8String:[data bytes]];
    
    NSLog(@"%@ : %@", newStr, another);
    NSLog(@"Error: %@", error);

    
    CDDConfigCDDCloneViewController *Clone = [[CDDConfigCDDCloneViewController alloc] initWithNibName:@"CDDConfigCDDCloneViewController" bundle:NULL];
    Clone->ipAddress = ipAddress;
    Clone->encode = encode;
    [self.navigationController pushViewController:Clone animated:YES];
}
@end
