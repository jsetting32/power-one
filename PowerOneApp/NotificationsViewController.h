//
//  NotificationsViewController.h
//  PowerOneApp
//
//  Created by John Setting on 6/27/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationsCustomCell.h"

@interface NotificationsViewController : UITableViewController
{
}
@property (nonatomic, strong) NSArray *alertsInfo;
@property (nonatomic, strong) id aKey;
@property (nonatomic, strong) id keys;
@property (nonatomic, strong) id anObject;


+ (NSArray*)getStatusData;
+ (void)setStatusData:(NSArray*)withStatusData;


@end
