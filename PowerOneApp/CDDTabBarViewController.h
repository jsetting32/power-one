//
//  CDDTabBarViewController.h
//  PowerOneApp
//
//  Created by John Setting on 7/19/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CDDDetailsViewController.h"
#import "CDDConfigPlantViewController.h"
#import "CDDTableViewController.h"
#import "CDDRegisterViewController.h"

@interface CDDTabBarViewController : UITabBarController <UITabBarControllerDelegate>
{
    @public
    NSString *ipAddress;
    NSString *encodedInfo;
    
}

@property (nonatomic) BOOL isAnimating;

@end
