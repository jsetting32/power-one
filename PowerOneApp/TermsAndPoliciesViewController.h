//
//  TermsAndPoliciesViewController.h
//  PowerOneApp
//
//  Created by John Setting on 6/21/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "MBProgressHUD.h"

@interface TermsAndPoliciesViewController : UIViewController < UIWebViewDelegate, MBProgressHUDDelegate>
{
    MBProgressHUD *hud;
}
@end
