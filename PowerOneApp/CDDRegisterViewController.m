//
//  CDDRegisterViewController.m
//  PowerOneApp
//
//  Created by John Setting on 7/23/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "CDDRegisterViewController.h"

@interface CDDRegisterViewController ()

@end

@implementation CDDRegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pushToAddressAction:(id)sender {
    CDDRegisterAddressViewController *addressView = [[CDDRegisterAddressViewController alloc] initWithNibName:@"CDDRegisterAddressViewController" bundle:nil];
    [self.navigationController pushViewController:addressView animated:YES];
}
@end
