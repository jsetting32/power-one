//
//  CDDRegisterPlantViewController.m
//  PowerOneApp
//
//  Created by John Setting on 7/23/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "CDDRegisterPlantViewController.h"

@interface CDDRegisterPlantViewController ()

@end

@implementation CDDRegisterPlantViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSMutableURLRequest *request2 = [[NSMutableURLRequest alloc] init];
    [request2 setURL:[NSURL URLWithString:@"https://acct.auroravision.net/selfregister/registerDevices"]];
    
    [request2 setHTTPMethod:@"POST"];
    [request2 setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    NSString* astr2 = @"installerName=John+Setting&installerPhoneNumber=4152469016&installerEmail=jsetting32%40yahoo.com&systemName=Setting+Residence&description=Primary+Residence&selectTimeZoneID=America%2FLos_Angeles&installationDate=07%2F23%2F2013&address=2448+Lansford+Ave.&city=San+Jose&selectCountry=1_US&selectState=16_CA&selectStateDefaultHidden=Select+State&zip=&jsonCustomerAsset=%7B%22name%22%3A%22Setting+Residence%22%2C%22address%22%3A%7B%22city%22%3A%22San+Jose%22%2C%22countryCode%22%3A%22US%22%2C%22regionCode%22%3A%22CA%22%2C%22postalCode%22%3A%22%22%2C%22street1%22%3A%222448+Lansford+Ave.%22%7D%2C%22meta%22%3A%7B%22displayName%22%3A%22Setting+Residence%22%2C%22description%22%3A%22Primary+Residence%22%7D%2C%22location%22%3A%7B%22latitude%22%3A%2237.283131%22%2C%22longitude%22%3A%22-121.89289300000002%22%2C%22elevation%22%3A%2246.22127532958984%22%7D%2C%22type%22%3A%22Customer%22%7D&jsonSiteAsset=%7B%22name%22%3A%22Setting+Residence%22%2C%22address%22%3A%7B%22city%22%3A%22San+Jose%22%2C%22countryCode%22%3A%22US%22%2C%22regionCode%22%3A%22CA%22%2C%22postalCode%22%3A%22%22%2C%22street1%22%3A%222448+Lansford+Ave.%22%7D%2C%22meta%22%3A%7B%22displayName%22%3A%22Setting+Residence%22%2C%22description%22%3A%22Primary+Residence%22%7D%2C%22location%22%3A%7B%22latitude%22%3A%2237.283131%22%2C%22longitude%22%3A%22-121.89289300000002%22%2C%22elevation%22%3A%2246.22127532958984%22%7D%2C%22timeZone%22%3A%22America%2FLos_Angeles%22%2C%22installDate%22%3A1374562800000%2C%22type%22%3A%22Site%22%7D&addPlantLatitudeValue=37.283131&addPlantLongitudeValue=-121.89289300000002&addPlantElevationValue=46.22127532958984&mac=00%3A12%3A4B%3A00%3A02%3A29%3AE3%3AFA";
    [request2 setHTTPBody:[astr2 dataUsingEncoding:NSUTF8StringEncoding]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
