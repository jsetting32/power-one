//
//  CDDConfigNetworkViewController.h
//  PowerOneApp
//
//  Created by John Setting on 7/22/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDDConfigAcquisitionWizardViewController.h"
#import "UICheckbox.h"

@interface CDDConfigNetworkViewController : UIViewController
{
    @public
    NSString *ipAddress;
    NSString *encode;
}

- (IBAction)nextViewAction:(id)sender;
@property (weak, nonatomic) IBOutlet UICheckbox *avmethodCheckbox;
@property (weak, nonatomic) IBOutlet UICheckbox *dhcpcheckbox;
@property (weak, nonatomic) IBOutlet UICheckbox *senddatatoportalCheckbox;
@property (weak, nonatomic) IBOutlet UICheckbox *sendeventstoportalCheckbox;
@property (weak, nonatomic) IBOutlet UICheckbox *checkautomaticupdatesCheckbox;
- (IBAction)boxCheck:(id)sender;

@end
