//
//  PowerCalculatorViewController.h
//  PowerOneApp
//
//  Created by John Setting on 6/21/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "SWRevealViewController.h"
#import "UICheckbox.h"
#import "PullEasyViewData.h"
#import "UITextfieldScrollViewController.h"

@class UICheckbox;

@interface PowerCalculatorViewController : UITextfieldScrollViewController <UITextFieldDelegate, SWRevealViewControllerDelegate>
{
    PullEasyViewData *energyCosts;
}

@property (strong, nonatomic) IBOutlet UILabel *installDate;

@property (strong, nonatomic) IBOutlet UITextField *zipCode;
@property (strong, nonatomic) IBOutlet UITextField *costOfInstallment;
@property (strong, nonatomic) IBOutlet UITextField *kWhUsagePerMonth;
@property (strong, nonatomic) IBOutlet UITextField *monthlyEstimate;

@property (weak, nonatomic) IBOutlet UICheckbox *residentialCheckbox;
@property (weak, nonatomic) IBOutlet UICheckbox *commercialCheckbox;

@property (strong, nonatomic) NSString *choice;
@property (strong, nonatomic) NSString *entityInstallDate;


- (IBAction)boxCheck:(id)sender;
+ (NSString*)installDateString;
+ (void)setInstallDateString:(NSString*)withNewInstallDateString;


@end
