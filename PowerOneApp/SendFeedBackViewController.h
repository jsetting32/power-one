//
//  SendFeedBackViewController.h
//  PowerOneApp
//
//  Created by John Setting on 6/21/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "SWRevealViewController.h"
#import "SKPSMTPMessage.h"
#import "FeedbackTypes.h"
#import "ComboBox.h"
#import "DLStarRatingControl.h"

@protocol SendFeedBackDelegate

- (void) didReceiveType:(NSString *) message;

@end

@interface SendFeedBackViewController : UIViewController <SKPSMTPMessageDelegate, UITableViewDataSource,UITableViewDelegate, SendFeedBackDelegate, DLStarRatingDelegate>
{
    UIActivityIndicatorView * spinner;
    UIImageView * bgimage;
    IBOutlet UILabel * loadingLabel;
    IBOutlet UITextView *TextViewBody;
    IBOutlet UIButton *reportButton;
    IBOutlet UITableView *feedbackTableView;
    IBOutlet UIScrollView *Scroller;
    
    NSString *subject;
    NSString *starRating;
    IBOutlet UILabel *stars;
}
@property (retain, nonatomic) IBOutlet UILabel *stars;

@property (strong, nonatomic) IBOutlet UIScrollView *Scroller;
@property (strong, nonatomic) IBOutlet UITableView *feedbackTableView;
@property (strong, nonatomic) IBOutlet UIButton *reportButton;
@property (strong, nonatomic) IBOutlet UITextView *TextViewBody;
@property (nonatomic, retain) IBOutlet UILabel * loadingLabel;
@property (nonatomic, retain)UIImageView * bgimage;
@property (nonatomic, retain)UIActivityIndicatorView * spinner;
-(IBAction)sendEmail:(id)sender;
-(void)removeWaitOverlay;
-(void)createWaitOverlay;
-(void)stopSpinner;
-(void)startSpinner;

@end
