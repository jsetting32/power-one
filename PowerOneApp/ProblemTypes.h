//
//  ProblemTypes.h
//  PowerOneApp
//
//  Created by John Setting on 6/26/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportAProblemViewController.h"

@protocol ReportAProblemDelegate;

@interface ProblemTypes : UITableViewController
{
    id<ReportAProblemDelegate> delegate;
}

@property (nonatomic, assign) id<ReportAProblemDelegate> delegate;

@end
