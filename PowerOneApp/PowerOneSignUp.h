//
//  PowerOneSignUp.h
//  PowerOneApp
//
//  Created by John Setting on 7/6/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "UITextfieldScrollViewController.h"
#import "CDDLoginViewController.h"

@interface PowerOneSignUp : UITextfieldScrollViewController < UITextFieldDelegate>
{
    IBOutlet UIImageView *image1;
    IBOutlet UIImageView *image2;
    IBOutlet UIImageView *image3;
    IBOutlet UIButton *loginButton;
}


@property (strong, nonatomic) IBOutlet UIImageView *image1;
@property (strong, nonatomic) IBOutlet UIImageView *image2;
@property (strong, nonatomic) IBOutlet UIImageView *image3;
@property (strong, nonatomic) IBOutlet UITextField *emailAddress;
@property (strong, nonatomic) IBOutlet UITextField *desiredUsername;
@property (strong, nonatomic) IBOutlet UITextField *desiredPassword;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;
- (IBAction)signUpAction:(id)sender;

@end
