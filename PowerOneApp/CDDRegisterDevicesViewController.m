//
//  CDDRegisterDevicesViewController.m
//  PowerOneApp
//
//  Created by John Setting on 7/23/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "CDDRegisterDevicesViewController.h"

@interface CDDRegisterDevicesViewController ()

@end

@implementation CDDRegisterDevicesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    NSMutableURLRequest *request3 = [[NSMutableURLRequest alloc] init];
    [request3 setURL:[NSURL URLWithString:@"https://acct.auroravision.net/selfregister/claimLogger"]];
    
    [request3 setHTTPMethod:@"POST"];
    [request3 setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    NSString* astr3 = @"mac=00%3A12%3A4B%3A00%3A02%3A29%3AE3%3A97&siteEid=1607003&f=CLAIM";
    [request3 setHTTPBody:[astr3 dataUsingEncoding:NSUTF8StringEncoding]];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
