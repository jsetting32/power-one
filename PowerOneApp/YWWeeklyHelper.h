//
//  YWWeeklyHelper.h
//  PowerOneApp
//
//  Created by John Setting on 6/24/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "TBXML.h"
#import "YWWeeklyForecast.h"
#import "YWWeeklyWOEID.h"

@interface YWWeeklyHelper : NSObject

+ (YWWeeklyWOEID *) getWOEID:(float)latitude longitude:(float)longitude yahooAPIKey:(NSString *)yahooAPIKey;
+ (NSArray *) getWeather:(int)woeid;
+ (NSString *) returnGoodString :(NSString *)formatThisString;

@end
