//
//  AccountSettingsViewController.h
//  PowerOneApp
//
//  Created by John Setting on 6/26/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface AccountSettingsViewController : UITableViewController

@end
