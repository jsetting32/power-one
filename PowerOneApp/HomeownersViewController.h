//
//  HomeownersViewController.h
//  PowerOneApp
//
//  Created by John Setting on 6/14/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "MBProgressHUD.h"
@interface HomeownersViewController : UIViewController < UIWebViewDelegate, MBProgressHUDDelegate>

@end
