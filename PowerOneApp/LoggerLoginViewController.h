//
//  LoggerLoginViewController.h
//  PowerOneApp
//
//  Created by John Setting on 7/11/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "LoggerTableViewController.h"

@interface LoggerLoginViewController : UIViewController <UITextFieldDelegate>
{
    IBOutlet UITextField *ipAddressTextField;
    IBOutlet UIButton *loginButton;
}

@property (strong, nonatomic) IBOutlet UITextField *ipAddressTextField;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;

@end
