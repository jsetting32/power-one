/*

 Copyright (c) 2013 Joan Lluch <joan.lluch@sweetwilliamsl.com>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is furnished
 to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

*/

#import "InvertersTableView.h"
#import "SWRevealViewController.h"

// 1D URL for Inverters
// https://easyview.auroravision.net/easyview/services/gmi/summary/GenerationPower.json?eids=1168868%2C1168839%2C1168810%2C1169042%2C1169071%2C1168984%2C1169013%2C1168897%2C1168926%2C1168955&tz=US%2FMountain&bins=true&start=20130625&end=20130626&range=1D&hasUsage=false&summary=false&binSize=Min15&v=1.4.7

// 7D URL for Inverters
// https://easyview.auroravision.net/easyview/services/gmi/summary/GenerationPower.json?eids=1168868%2C1168839%2C1168810%2C1169042%2C1169071%2C1168984%2C1169013%2C1168897%2C1168926%2C1168955&tz=US%2FMountain&bins=true&start=20130619&end=20130626&range=7D&hasUsage=false&summary=false&binSize=Hour&v=1.4.7

// 30D URL for Inverters
// https://easyview.auroravision.net/easyview/services/gmi/summary/GenerationEnergy.json?type=GenerationEnergy&eids=1168868%2C1168839%2C1168810%2C1169042%2C1169071%2C1168984%2C1169013%2C1168897%2C1168926%2C1168955&tz=US%2FMountain&start=20130527&end=20130626&range=30D&hasUsage=false&label=30D&dataProperty=inverterData&bins=false&plantPowerNow=false&function=Avg&summary=true&v=1.4.7

// 365D URL for Inverters
// https://easyview.auroravision.net/easyview/services/gmi/summary/GenerationEnergy.json?type=GenerationEnergy&eids=1168868%2C1168839%2C1168810%2C1169042%2C1169071%2C1168984%2C1169013%2C1168897%2C1168926%2C1168955&tz=US%2FMountain&start=20120601&end=20130626&range=365D&hasUsage=false&label=365D&dataProperty=inverterData&bins=false&plantPowerNow=false&function=Avg&summary=true&v=1.4.7

// WTD URL for Inverters
// https://easyview.auroravision.net/easyview/services/gmi/summary/GenerationPower.json?eids=1168868%2C1168839%2C1168810%2C1169042%2C1169071%2C1168984%2C1169013%2C1168897%2C1168926%2C1168955&tz=US%2FMountain&bins=true&start=20130623&end=20130626&range=WTD&hasUsage=false&summary=false&binSize=Hour&v=1.4.7

// MTD URL for Inverters
// https://easyview.auroravision.net/easyview/services/gmi/summary/GenerationEnergy.json?type=GenerationEnergy&eids=1168868%2C1168839%2C1168810%2C1169042%2C1169071%2C1168984%2C1169013%2C1168897%2C1168926%2C1168955&tz=US%2FMountain&start=20130601&end=20130626&range=MTD&hasUsage=false&label=MTD&dataProperty=inverterData&bins=false&plantPowerNow=false&function=Avg&summary=true&v=1.4.7

// YTD URL for Inverters
// https://easyview.auroravision.net/easyview/services/gmi/summary/GenerationEnergy.json?type=GenerationEnergy&eids=1168868%2C1168839%2C1168810%2C1169042%2C1169071%2C1168984%2C1169013%2C1168897%2C1168926%2C1168955&tz=US%2FMountain&start=20130101&end=20130626&range=YTD&hasUsage=false&label=YTD&dataProperty=inverterData&bins=false&plantPowerNow=false&function=Avg&summary=true&v=1.4.7


@interface InvertersTableView ()

@end

@implementation InvertersTableView

- (void) viewDidLoad
{
    pulledData = [PullEasyViewData sharedManager];
    self.view.opaque = NO;
    self.view.backgroundColor = [UIColor clearColor];
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

}


#pragma marl - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [[pulledData getInverterNamesAndValues] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    // Configure the cell...
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = cell.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor darkGrayColor]CGColor], (id)[[UIColor blackColor]CGColor], nil];
    [cell.textLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:16.0f]];
	
    _keys = [[pulledData getInverterNamesAndValues]  allKeys];
    _aKey = [_keys objectAtIndex:indexPath.row];
    _anObject = [[pulledData getInverterNamesAndValues]  objectForKey:_aKey];
    cell.textLabel.text = _aKey;
    
    
    /* Check for NULL values BUT MAKE EXCEPTION.... in the end there may be more data points than date points */
    float total = 0;
    int i = 0;
    for(NSString *str in _anObject)
    {
        if (!str || [str isKindOfClass:[NSNull class]]) {
            //NSLog(@"Null Value at index %i: %@", i, str);
        } else {
            total += [str floatValue];
        }
        i++;
    }
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    
    [formatter setRoundingMode: NSNumberFormatterRoundUp];
    [formatter setMaximumFractionDigits:2];
    [formatter setMinimumFractionDigits:2];
    
    _inverterEnergyGeneration  = [formatter stringFromNumber:[NSNumber numberWithFloat:total]];
    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Generated: %@ kW Today", _inverterEnergyGeneration];
    [cell.layer insertSublayer:gradient atIndex:0];
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    SWRevealViewController *revealController = self.revealViewController;
    
    // We know the frontViewController is a NavigationController
    //UINavigationController *frontNavigationController = (id)revealController.frontViewController;  // <-- we know it is a NavigationController
    //NSInteger row = indexPath.row;
    
    // Here you'd implement some of your own logic... I simply take for granted that the first row (=0) corresponds to the "FrontViewController".
    
    // Now let's see if we're not attempting to swap the current frontViewController for a new instance of ITSELF, which'd be highly redundant.
    //if ( ![frontNavigationController.topViewController isKindOfClass:[InvertersDetailViewController class]] ) {


    id Object = [[pulledData getInverterNamesAndValues]  objectForKey:[_keys objectAtIndex:indexPath.row]];
    float total = 0;
    int i = 0;
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for(NSString *str in Object)
    {
        if (!str || [str isKindOfClass:[NSNull class]]) {
            NSLog(@"Null Value at index %i: %@", i, str);
            [array addObject:str];
        } else {
            total += [str floatValue];
            [array addObject:str];
        }
        i++;
    }
    
    InvertersDetailViewController *frontViewController = [[InvertersDetailViewController alloc] initWithNibName:@"InvertersDetailViewController" bundle:nil];
    frontViewController.title = [_keys objectAtIndex:indexPath.row];
    frontViewController.inverterValues = array;
    frontViewController.inverterDates = [pulledData getInverterDates];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    
    [formatter setRoundingMode: NSNumberFormatterRoundUp];
    [formatter setMaximumFractionDigits:2];
    [formatter setMinimumFractionDigits:2];
    
    NSString *new  = [formatter stringFromNumber:[NSNumber numberWithFloat:total]];
    frontViewController.valuesString = new;

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:frontViewController];
    [revealController setFrontViewController:navigationController animated:NO];
    [revealController rightRevealToggle:self];
    
    
    //[revealController.navigationController pushViewController:frontViewController animated:YES];
    //[self presentViewController:frontViewController animated:YES completion:nil];
    
    //}
    // Seems the user attempts to 'switch' to exactly the same controller he came from!
    //else {
        //[revealController rightRevealToggle:self];
    //}
}


@end
