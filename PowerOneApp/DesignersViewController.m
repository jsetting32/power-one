//
//  DesignersViewController.m
//  PowerOneApp
//
//  Created by John Setting on 6/14/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "DesignersViewController.h"

@interface DesignersViewController ()

@end

@implementation DesignersViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Designers Guide";
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];

    SWRevealViewController *revealController = [self revealViewController];
    
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"]
                                                                         style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    UIBarButtonItem *rightRevealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"19-gear.png"] style:UIBarButtonItemStyleBordered target:revealController action:@selector(rightRevealToggle:)];
    self.navigationItem.rightBarButtonItem = rightRevealButtonItem;
    
    MBProgressHUD *HUD;
	HUD = [[MBProgressHUD alloc] initWithView:self.view];
	[self.view addSubview:HUD];
	HUD.delegate = self;
	[HUD showWhileExecuting:@selector(performFetchOnMainThread) onTarget:self withObject:nil animated:YES];
    
}

- (void) getTerms {
    NSURL *termAndPoliciesURL = [NSURL URLWithString:@"http://www.power-one.com/renewable-energy/designers"];
    UIWebView *webview = [[UIWebView alloc] initWithFrame:self.view.bounds];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:termAndPoliciesURL];
    [webview loadRequest:requestObj];
    webview.delegate=self;
    webview.scalesPageToFit=YES;
    [self.view addSubview:webview];
}

-(void) performFetchOnMainThread    {
    [self performSelectorOnMainThread:@selector(getTerms) withObject:nil waitUntilDone:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
