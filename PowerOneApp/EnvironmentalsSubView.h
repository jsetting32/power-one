//
//  EnvironmentalsSubView.h
//  PowerOneApp
//
//  Created by John Setting on 6/17/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLCycleScrollView.h"
#import "PullEasyViewData.h"

@interface EnvironmentalsSubView : UIView <XLCycleScrollViewDelegate, XLCycleScrollViewDatasource>
{
    XLCycleScrollView *csView;
    
    UIView *theView;
    
    PullEasyViewData *environmentalsData;
    
    int j;
}

+ (NSArray *) setArrayOne;
+ (void)setArrayOne:(NSArray *)objectSetter;

+ (NSArray *) setArrayTwo;
+ (void)setArrayTwo:(NSArray *)objectSetter;

@property (nonatomic, strong) NSString* getTVSavings;
@property (nonatomic, strong) NSString* getCarSavings;
@property (nonatomic, strong) NSString* getComputerSavings;
@property (nonatomic, strong) NSString* getCO2Savings;
@property (nonatomic, strong) NSString* getNOXSavings;
@property (nonatomic, strong) NSString* getSO2Savings;
@property (nonatomic, strong) NSString* getCarbonOffsetMetricTonsSavings;
@property (nonatomic, strong) NSString* getCarbonOffsetAcresSavings;

@end
