//
//  CPDScatterPlotViewController.m
//  CorePlotDemo
//
//  Created by Fahim Farook on 19/5/12.
//  Copyright 2012 RookSoft Pte. Ltd. All rights reserved.
//
#define degreesToRadians(x) (M_PI * x / 180.0)

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0) //1

#import "DayGraphViewController.h"

@implementation DayGraphViewController

@synthesize hostView = hostView_;
@synthesize datapoints, times;

-(void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - UIViewController lifecycle methods

-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    graphData = [[PullEasyViewData alloc] init];

    NSLog(@"Astronomy Times: %@", astronomyTimes);
    dispatch_async(kBgQueue, ^{
        
        
        NSDate *now = [NSDate date];
        
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:now]; // Get necessary date components
        
        [components month]; //gives you month
        [components day]; //gives you day
        [components year]; // gives you year
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyyMMdd"];
        NSDate *tomorrow = [NSDate dateWithTimeIntervalSinceNow: (60.0f*60.0f*24.0f)];
        
        NSString *currentDate = [dateFormat stringFromDate:now];
        NSString *tomorrowDate = [dateFormat stringFromDate:tomorrow];
        
        
        // Instantiate and allocate memory to prepare for an alert popup if any connections fail
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Status" message:@"Some connections to your site are down. In result, some data will not present. Please pull to refresh to try again!" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        
        NSData* data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://easyview.auroravision.net/easyview/services/gmi/summary/GenerationPower.json?eids=1167815&tz=US%%2FMountain&start=%@&end=%@&range=1D&hasUsage=false&binSize=Min15&bins=true&v=1.3.2&_=1370148864257", currentDate, tomorrowDate]]];
        if (data == nil) {
            NSLog(@"Day Graph Data is nil");
            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        } else {
            NSLog(@"Day Graph");
    
            [graphData performSelectorOnMainThread:@selector(fetchGraphDataRequestDataDay:) withObject:data waitUntilDone:YES];
            //[graphData performSelectorInBackground:@selector(fetchGraphDataRequestDataDay:) withObject:data];
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            formatter.dateFormat = @"hh:mm a";
            
            NSDate *datee = [formatter dateFromString:[astronomyTimes objectAtIndex:0]];
            
            // compare the sunrise and sunset (date objects) to graphdata date objects
            int j;
            for (j = 0; j < [[graphData getGraphDatesDay] count]; j++)
            {
                NSDate *dateee = [formatter dateFromString:[[graphData getGraphDatesDay] objectAtIndex:j]];
                if ([dateee compare:datee] == NSOrderedDescending) {
                    break;
                }
            }
            
            times = [[NSMutableArray alloc] init];
            datapoints = [[NSMutableArray alloc] init];
            
            for (int i = j-2; i < [[graphData getGraphDatesDay] count] && [[graphData getGraphValuesDay] count]; i++)
            {
                [times addObject:[[graphData getGraphDatesDay] objectAtIndex:i]];
                [datapoints addObject:[[graphData getGraphValuesDay] objectAtIndex:i]];
            }
            
            //dispatch_async(kBgQueue, ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self initPlot];
            });
        }
    });
    
    
    //data = [AuthenticationData sharedManager];
    //dispatch_async(kBgQueue, ^{
        
    /*
        NSDate *now = [NSDate date];

        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:now]; // Get necessary date components
        
        [components month]; //gives you month
        [components day]; //gives you day
        [components year]; // gives you year
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyyMMdd"];
        NSDate *tomorrow = [NSDate dateWithTimeIntervalSinceNow: (60.0f*60.0f*24.0f)];
        
        NSString *currentDate = [dateFormat stringFromDate:now];
        NSString *tomorrowDate = [dateFormat stringFromDate:tomorrow];
        
    
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"hh:mm a";
        
        NSDate *datee = [formatter dateFromString:[astronomyTimes objectAtIndex:0]];
            
        // compare the sunrise and sunset (date objects) to graphdata date objects
        int j;
        for (j = 0; j < [[data getGraphDatesDay] count]; j++)
        {
            NSDate *dateee = [formatter dateFromString:[[data getGraphDatesDay] objectAtIndex:j]];
            if ([dateee compare:datee] == NSOrderedDescending) {
                break;
            }
        }
            
        times = [[NSMutableArray alloc] init];
        datapoints = [[NSMutableArray alloc] init];
        
        for (int i = j-2; i < [[data getGraphDatesDay] count] && [[data getGraphValuesDay] count]; i++)
        {
            [times addObject:[[data getGraphDatesDay] objectAtIndex:i]];
            [datapoints addObject:[[data getGraphValuesDay] objectAtIndex:i]];
        }
        
        NSLog(@"Astronomy Times: %@", astronomyTimes);
       */ 
        //dispatch_async(dispatch_get_main_queue(), ^{
        //    [self initPlot];
        //});
    //});
}


#pragma mark - Chart behavior
-(void)initPlot {
    [self configureHost];
    [self configureGraph];
    [self configurePlots];
    [self configureAxes];
}

-(void)configureHost {
    self.hostView = [(CPTGraphHostingView *) [CPTGraphHostingView alloc] initWithFrame:CGRectMake(-5, -35, 320, 190)];
    self.hostView.backgroundColor = [UIColor clearColor];
    self.hostView.allowPinchScaling = YES;
	[self.view addSubview:self.hostView];
}

-(void)configureGraph {
    
    
	// 1 - Create the graph
    CPTGraph *graph = [[CPTXYGraph alloc] initWithFrame:self.hostView.bounds];
    [graph applyTheme:[CPTTheme themeNamed:kCPTDarkGradientTheme]];
	self.hostView.hostedGraph = graph;
    
	// 3 - Create and set text style
	CPTMutableTextStyle *titleStyle = [CPTMutableTextStyle textStyle];
	titleStyle.color = [CPTColor whiteColor];
	titleStyle.fontName = @"Helvetica-Bold";
	titleStyle.fontSize = 12.0f;
	graph.titleTextStyle = titleStyle;
	graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
	graph.titleDisplacement = CGPointMake(0.0f, 10.0f);
    
	// 4 - Set padding for plot area
	[graph.plotAreaFrame setPaddingLeft:10.0f];
	[graph.plotAreaFrame setPaddingBottom:10.0f];
    
	// 5 - Enable user interactions for plot space
	CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
	plotSpace.allowsUserInteraction = YES;
}

-(void)configurePlots {
    
    
	// 1 - Get graph and plot space
	CPTGraph *graph = self.hostView.hostedGraph;
	CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    
	// 2 - Create the three plots
	CPTScatterPlot *aaplPlot = [[CPTScatterPlot alloc] init];
	aaplPlot.dataSource = self;
    [aaplPlot.dataSource retain];
	aaplPlot.identifier = @"APPL";
    aaplPlot.dataLineStyle = nil;
	CPTColor *aaplColor = [CPTColor yellowColor];
	[graph addPlot:aaplPlot toPlotSpace:plotSpace];
    
	// 3 - Set up plot space
	[plotSpace scaleToFitPlots:[NSArray arrayWithObjects:aaplPlot, nil]];
    plotSpace.delegate = self;
	CPTMutablePlotRange *xRange = [plotSpace.xRange mutableCopy];
	[xRange expandRangeByFactor:CPTDecimalFromCGFloat(1.1f)];
	plotSpace.xRange = xRange;
	CPTMutablePlotRange *yRange = [plotSpace.yRange mutableCopy];
	[yRange expandRangeByFactor:CPTDecimalFromCGFloat(1.2f)];
	plotSpace.yRange = yRange;
    
    // 4 - Put an area gradient under the plot above
    CPTColor *areaColor       = [CPTColor yellowColor];
    //CPTGradient *areaGradient = [CPTGradient gradientWithBeginningColor:areaColor endingColor:[CPTColor clearColor]];
    //areaGradient.angle = -90.0;
    //CPTFill *areaGradientFill = [CPTFill fillWithGradient:areaGradient];
    CPTFill *area = [CPTFill fillWithColor:areaColor];
    aaplPlot.areaFill      = area;
    aaplPlot.areaBaseValue = CPTDecimalFromString(@"0.0");
    
	// 5 - Create styles and symbols
	CPTMutableLineStyle *aaplLineStyle = [aaplPlot.dataLineStyle mutableCopy];
	aaplLineStyle.lineWidth = 0.5;
	aaplLineStyle.lineColor = aaplColor;
	aaplPlot.dataLineStyle = aaplLineStyle;
    
	CPTMutableLineStyle *aaplSymbolLineStyle = [CPTMutableLineStyle lineStyle];
	aaplSymbolLineStyle.lineColor = aaplColor;
	
    CPTPlotSymbol *aaplSymbol = [CPTPlotSymbol ellipsePlotSymbol];
	aaplSymbol.fill = [CPTFill fillWithColor:aaplColor];
	aaplSymbol.lineStyle = aaplSymbolLineStyle;
	aaplSymbol.size = CGSizeMake(1.0f, 1.0f);
	aaplPlot.plotSymbol = aaplSymbol;
    
}

-(void)configureAxes {
    
	// 1 - Create styles
	CPTMutableTextStyle *axisTitleStyle = [CPTMutableTextStyle textStyle];
	axisTitleStyle.color = [CPTColor whiteColor];
	axisTitleStyle.fontName = @"Helvetica-Bold";
	axisTitleStyle.fontSize = 8.0;
	
    CPTMutableLineStyle *axisLineStyle = [CPTMutableLineStyle lineStyle];
	axisLineStyle.lineWidth = 0.5f;
	axisLineStyle.lineColor = [CPTColor whiteColor];
	
    CPTMutableTextStyle *axisTextStyle = [[CPTMutableTextStyle alloc] init];
	axisTextStyle.color = [CPTColor whiteColor];
	axisTextStyle.fontName = @"Helvetica-Bold";
	axisTextStyle.fontSize = 7.0f;
	
    CPTMutableLineStyle *tickLineStyle = [CPTMutableLineStyle lineStyle];
	tickLineStyle.lineColor = [CPTColor whiteColor];
	tickLineStyle.lineWidth = 0.5f;
	
    CPTMutableLineStyle *gridLineStyle = [CPTMutableLineStyle lineStyle];
	tickLineStyle.lineColor = [CPTColor blackColor];
	tickLineStyle.lineWidth = 0.25f;
    
    
	// 2 - Get axis set
	CPTXYAxisSet *axisSet = (CPTXYAxisSet *) self.hostView.hostedGraph.axisSet;
    

	// 3 - Configure x-axis
	CPTAxis *x = axisSet.xAxis;
	//x.title = @"Day Hour";
	x.titleTextStyle = axisTitleStyle;
	//x.titleOffset = 10.0f;
	x.axisLineStyle = axisLineStyle;
	x.labelingPolicy = CPTAxisLabelingPolicyNone;
	x.labelTextStyle = axisTextStyle;
	x.majorTickLineStyle = axisLineStyle;
	x.majorTickLength = 2.0f;
	x.tickDirection = CPTSignNegative;
	CGFloat dateCount = [times count];
	NSMutableSet *xLabels = [NSMutableSet setWithCapacity:dateCount];
	NSMutableSet *xLocations = [NSMutableSet setWithCapacity:dateCount];
	NSInteger i;
	
    for (i = [[graphData getGraphDatesDay] count] / 4; i < [[graphData getGraphDatesDay] count]; i=i+8)
    {
        NSString *date = [[graphData getGraphDatesDay] objectAtIndex:i];
        CPTAxisLabel *label = [[CPTAxisLabel alloc] initWithText:date  textStyle:x.labelTextStyle];
		CGFloat location = i;
		label.tickLocation = CPTDecimalFromCGFloat(location);
		label.offset = x.majorTickLength;
		
        if (label)
        {
			[xLabels addObject:label];
			[xLocations addObject:[NSNumber numberWithFloat:location]];
		}
	}
     
    
	x.axisLabels = xLabels;
	x.majorTickLocations = xLocations;
    
    
	// 4 - Configure y-axis
	CPTAxis *y = axisSet.yAxis;
	y.title = @"W";
	y.titleTextStyle = axisTitleStyle;
	y.titleOffset = 15.0f;
	y.axisLineStyle = axisLineStyle;
	y.majorGridLineStyle = gridLineStyle;
	y.labelingPolicy = CPTAxisLabelingPolicyNone;
	y.labelTextStyle = axisTextStyle;
	y.labelOffset = 16.0f;
	y.majorTickLineStyle = axisLineStyle;
	y.majorTickLength = 2.0f;
	y.minorTickLength = 1.0f;
	y.tickDirection = CPTSignPositive;
	
    NSInteger majorIncrement = [[graphData getMaxDayValue] integerValue] * 0.125;
	NSInteger minorIncrement = [[graphData getMaxDayValue] integerValue] * 0.125;
    
    CGFloat yMax = [[graphData getMaxDayValue] floatValue] * 1.1;  // should determine dynamically based on max price
        
	NSMutableSet *yLabels = [NSMutableSet set];
	NSMutableSet *yMajorLocations = [NSMutableSet set];
	NSMutableSet *yMinorLocations = [NSMutableSet set];
	
    for (NSInteger j = minorIncrement; j <= yMax; j += minorIncrement)
    {
		NSUInteger mod = 0;
		
        if (majorIncrement == 0) {
            mod = 0;
        } else {
            mod = j % majorIncrement;
        }
        
        if (mod == 0) {
			CPTAxisLabel *label = [[CPTAxisLabel alloc] initWithText:[NSString stringWithFormat:@"%i", j] textStyle:y.labelTextStyle];
			NSDecimal location = CPTDecimalFromInteger(j);
			label.tickLocation = location;
			label.offset = -y.majorTickLength - y.labelOffset;
			
            if (label)
            {
				[yLabels addObject:label];
			}
            
			[yMajorLocations addObject:[NSDecimalNumber decimalNumberWithDecimal:location]];
		} else {
			[yMinorLocations addObject:[NSDecimalNumber decimalNumberWithDecimal:CPTDecimalFromInteger(j)]];
		}
	}
    
	y.axisLabels = yLabels;
	y.majorTickLocations = yMajorLocations;
	y.minorTickLocations = yMinorLocations;
    
}



#pragma mark - Rotation
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Attempt to hide navigationbar on side orientation
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

#pragma mark - CPTPlotDataSource methods
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    return [[graphData getGraphDatesDay] count];
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index {
    NSInteger valueCount = [[graphData getGraphDatesDay] count];
    switch (fieldEnum)
    {
		case CPTScatterPlotFieldX:
            if (index < valueCount)
				return [NSNumber numberWithUnsignedInteger:index];
			break;
			
		case CPTScatterPlotFieldY:
            return [[graphData getGraphValuesDay] objectAtIndex:index];
            break;
	}
	return [NSDecimalNumber zero];
    
}

static NSArray *astronomyTimes;

+(NSArray *)getTimes
{
    return astronomyTimes;
}

+(void)getTimes:(NSArray *)setTimes
{
    if (astronomyTimes != setTimes) {
        astronomyTimes = [setTimes copy];
    }
}

@end
