//
//  PersonalizationsTableViewController.h
//  PowerOneApp
//
//  Created by John Setting on 6/29/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "SWRevealViewController.h"

@interface PersonalizationsTableViewController : UITableViewController

@end
