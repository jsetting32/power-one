//
//  PowerOneLogin.h
//  PowerOneAuroraApp
//
//  Created by John Setting on 5/29/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ASIFormDataRequest.h"


#import "UITextfieldScrollViewController.h"
#import "SWRevealViewController.h"

#import "MainViewController.h"
#import "FranksVC.h"

#import "RearViewController.h"
#import "InvertersTableView.h"
#import "Reachability.h"
#import "MBProgressHUD.h"

#import "PDKeychainBindings.h"

#import "LoginNotificationNames.h"

#import "Base64.h"
#import "PortfolioViewController.h"
@class UICheckbox;


@interface PowerOneLogin : UITextfieldScrollViewController <UITextFieldDelegate, UIAlertViewDelegate, SWRevealViewControllerDelegate, MBProgressHUDDelegate>
{
	MBProgressHUD *HUD;

	long long expectedLength;
	long long currentLength;
    
    Reachability* internetReachable;
    Reachability* hostReachable;
    
    PullEasyViewData *pulledData;
    
    UIActivityIndicatorView *waiting;
    
    
}

- (void)Demo;
- (void)Login;

@property (strong, nonatomic)NSString *auth;

@property (strong, nonatomic) IBOutlet UIView *topBar;

@property (weak, nonatomic) IBOutlet UICheckbox *rememberMe;
- (IBAction)checked:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *image1;
@property (strong, nonatomic) IBOutlet UIImageView *image2;
@property (strong, nonatomic) IBOutlet UIImageView *image3;

@property (strong, nonatomic) IBOutlet UITextField *userName;
@property (strong, nonatomic) IBOutlet UITextField *passWord;

@property (retain, nonatomic) IBOutlet UIButton *loginButton;
- (IBAction)loginButtonAction:(id)sender;

@property (retain, nonatomic) IBOutlet UIButton *demoButton;
- (IBAction)demoAction:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *entityId;
@end
