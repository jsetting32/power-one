//
//  CDDRegisterSecurityQsViewController.m
//  PowerOneApp
//
//  Created by John Setting on 7/23/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "CDDRegisterSecurityQsViewController.h"
#import "UICheckbox.h"

@interface CDDRegisterSecurityQsViewController ()

@end

@implementation CDDRegisterSecurityQsViewController

@synthesize termsAndPolicyCheckbox = _termsAndPolicyCheckbox;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        webViewController = [[UIViewController alloc] init];
        webview = [[UIWebView alloc] initWithFrame:webViewController.view.bounds];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    

    self.termsAndPolicyLabel.text = @"I have read and understand the Terms of Use and Privacy Policy";
    self.termsAndPolicyLabel.delegate = self;
    [self.view addSubview:self.termsAndPolicyLabel];
    NSRange termsofuse = [self.termsAndPolicyLabel.text rangeOfString:@"Terms of Use"];
    NSRange privacypolicy = [self.termsAndPolicyLabel.text rangeOfString:@"Privacy Policy"];
    [self.termsAndPolicyLabel addLinkToURL:[NSURL URLWithString:@"http://www.power-one.com/corporate/terms-use"] withRange:termsofuse];
    [self.termsAndPolicyLabel addLinkToURL:[NSURL URLWithString:@"http://www.power-one.com/corporate/privacy-policy"] withRange:privacypolicy];
    

    //@"https://www.google.com/recaptcha/api/image"
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    
    
    webViewController.hidesBottomBarWhenPushed = YES;
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webview loadRequest:requestObj];
    webview.delegate = self;
    webview.scalesPageToFit = YES;
    webview.scrollView.frame = webViewController.view.bounds;
    [webViewController.view addSubview:webview];
    
    if ([url isEqual:[NSURL URLWithString:@"http://www.power-one.com/corporate/privacy-policy"]]) {
        webViewController.title = @"Privacy Policy";
    } else {
        webViewController.title = @"Terms of Use";
    }
    
    [self.navigationController pushViewController:webViewController animated:YES];
}

- (IBAction)submitContactFormActionAndProceed:(id)sender {
    if (self.termsAndPolicyCheckbox.checked == FALSE) {
        [[[UIAlertView alloc] initWithTitle:@"Cannot Proceed" message:@"You must conform to the Terms of Use as well as the Privacy Policy to proceed with setting up your site." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:@"Exit Setup", nil] show];
    } else {
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://register.auroravision.net/selfregister/createUser"]];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        NSString* astr = @"firstName=Juan&lastName=Setting&email=jsetting32%40yahoo.com&repeatEmail=jsetting32%40yahoo.com&phoneNumber=&userID=jjsetting&address=2448+Lansford+Ave.&city=San+Jose&selectCountry=1&selectState=16&zip=&password=football33&repeatPassword=football33&securityQuestion1=What+is+the+name+of+the+first+street+you+lived+on%3F&questionAnswer1=Oliva+ct.&securityQuestion2=In+what+city+was+your+mother+born%3F&questionAnswer2=Manila&termsPrivacyReqBox=true&_termsPrivacyReqBox=on&recaptcha_challenge_field=03AHJ_VuvE_d4lJSL--SEaUkILg_L4SOUYmF9KNpI20_CPG0CcSyB5teVa6CiI8HT6mkKc2dgLO_ceFy97qeRVmpitqf7AuMUkOWTyksCAWrayaDUayk9QmvEYNhx4WupltHvKwSjM-oheg9CSUV3KnNLa_1CoIv7O-A&recaptcha_response_field=romgric+493&alertMessageHidden=You+must+read+and+understand+Terms+of+Use+and+Privacy+Policy+before+becoming+a+user.&mac=00%3A12%3A4B%3A00%3A02%3A29%3AE3%3AFA";
        [request setHTTPBody:[astr dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        //https://www.google.com/recaptcha/api/image?c=03AHJ_VutaXsNtmHNjWh3UT-5itcRXyDsVDLt5h4Z1RilB176kjO68M06GL06aEayRTdwY-P2wXVGqbfXTjRU72VjZ8oYjM87FNL-oVbyKKy0-gRaGs40t661qaZSWjkBV-aQKj1CKf32C-JJ-ksL0Xj8W9wAxTeTPYg
        NSError *error;
        NSURLResponse *response;
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        NSLog(@"%@", newStr);
        
        [[[UIAlertView alloc] initWithTitle:@"Step 1 Completed" message:@"You have successfully added yourself to our database" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    

    
    


    
    if ([alertView.title isEqualToString:@"Step 1 Completed"]) {

    } else {
        if (buttonIndex == 0) {
            NSLog(@"%i", buttonIndex);
        } else {
            self.tabBarController.selectedIndex = 0;
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

- (IBAction)termsAndPolicyAction:(id)sender {
    NSLog(@"Terms Checked = %@", (self.termsAndPolicyCheckbox.checked) ? @"YES" : @"NO");
}

@end
