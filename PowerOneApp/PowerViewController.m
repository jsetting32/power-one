//
//  PowerViewController.m
//  PowerOneApp
//
//  Created by John Setting on 6/21/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "PowerViewController.h"
#define hierarchyRequest [NSURL URLWithString: @"https://easyview.auroravision.net/easyview/services/gai/plant/hierarchy.json?entityId=1167815&v=1.4.7"]

@implementation PowerViewController

//@synthesize TodayValue = _TodayValue;
//@synthesize WeekValue = _WeekValue;
//@synthesize MonthValue = _MonthValue;
//@synthesize LifetimeValue = _LifetimeValue;

@synthesize powerWeekValue = _powerWeekValue;
@synthesize powerTodayValue = _powerTodayValue;
@synthesize powerMonthValue = _powerMonthValue;
@synthesize powerLifetimeValue = _powerLifetimeValue;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:self action:@selector(popped)];
    }
    return self;
}

- (void)viewDidLoad
{
    
    _TodayValue.text = _powerTodayValue;
    _WeekValue.text = _powerWeekValue;
    _MonthValue.text = _powerMonthValue;
    _LifetimeValue.text = _powerLifetimeValue;

    NSString *newString = [_powerLifetimeValue stringByReplacingOccurrencesOfString:@"," withString:@""];
    //NSLog(@"%@", newString);
    //NSLog(@"%f", [newString floatValue]);
    
    //NSLog(@"Cost Per KWH: %f", ([_costPerKWH floatValue] * .01));
    
    _todaySavings.text = [NSString stringWithFormat:@"$%f", ([_costPerKWH floatValue] * .01) * ([_powerTodayValue floatValue])];
    _weeklySavings.text = [NSString stringWithFormat:@"$%f", ([_costPerKWH floatValue] * .01) * ([_powerWeekValue floatValue])];
    _monthlySavings.text = [NSString stringWithFormat:@"$%f", ([_costPerKWH floatValue] * .01) * ([_powerMonthValue floatValue])];
    _LifetimeSavings.text = [NSString stringWithFormat:@"$%f", ([_costPerKWH floatValue] * .01) * ([newString floatValue])];
    
    
    //NSLog(@"Today's Savings: %f", ([_costPerKWH floatValue] * .01) * ([_powerTodayValue floatValue]));
    //NSLog(@"Week Savings: %f", ([_costPerKWH floatValue] * .01) * ([_powerWeekValue floatValue]));
    //NSLog(@"Month Savings: %f", ([_costPerKWH floatValue] * .01) * ([_powerMonthValue floatValue]));
    //NSLog(@"Lifetime Savings: %f", ([_costPerKWH floatValue] * .01) * ([newString floatValue]));
    
    
    //NSLog(@"Today Value: %f", [_powerTodayValue floatValue]);
    //NSLog(@"Week Value: %f", [_powerWeekValue floatValue]);
    //NSLog(@"Month Value: %f", [_powerMonthValue floatValue]);
    //NSLog(@"LifeTime Value: %f", [newString floatValue]);
    
    //NSLog(@"REAL Lifetime Value: %@", _powerLifetimeValue);

    //NSLog(@"%@", _costPerKWHDate);
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}



- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) popped
{
    //NSLog(@"Popped");
    
    [UIView  transitionWithView:self.navigationController.view duration:0.8  options:UIViewAnimationOptionTransitionFlipFromRight
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         [self.navigationController popViewControllerAnimated:YES];
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
}

@end
