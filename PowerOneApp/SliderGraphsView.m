//
//  SliderGraphsView.m
//  PowerOneApp
//
//  Created by John Setting on 6/19/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "SliderGraphsView.h"

@implementation SliderGraphsView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // Initialization code
        
		_scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
		[_scrollView setPagingEnabled:YES];
        _scrollView.bounces = NO;   
		[_scrollView setContentSize:CGSizeMake(7*_scrollView.frame.size.width, _scrollView.frame.size.height)];
		[_scrollView setShowsHorizontalScrollIndicator:NO];
		[_scrollView setDelegate:self];
		[self addSubview:_scrollView];
		
        NSLog(@"%@", astronomyTimes);

		
		for (int i = 0; i < 7; i++)
		{
			UIView *view = [[UIView alloc] initWithFrame:CGRectMake(i*_scrollView.frame.size.width,0,_scrollView.frame.size.width,_scrollView.frame.size.height)];
            if (i == 0) {
                
                CGRect frame = CGRectMake(self.frame.size.width * i, 0, self.frame.size.width, self.frame.size.height);
                //DayGraphViewController *ScatterDayView = [[[DayGraphViewController alloc] init] autorelease];
                DayGraphViewController *ScatterDayView = [[DayGraphViewController alloc] init];
                ScatterDayView.authToken = self.authToken;
                ScatterDayView.hostView.frame = frame;
                //[ScatterDayView.view retain];
                [view addSubview:ScatterDayView.view];
                
            } else if (i == 1) {
                
                CGRect frame = CGRectMake(self.frame.size.width * i, 0, self.frame.size.width, self.frame.size.height);
                //_DGraphViewController *Scatter7DView = [[[_DGraphViewController alloc] init] autorelease];
                _DGraphViewController *Scatter7DView = [[_DGraphViewController alloc] init];
                Scatter7DView.hostView.frame = frame;
                //[Scatter7DView.view retain];
                [view addSubview:Scatter7DView.view];
                
            } else if (i == 2) {

                CGRect frame = CGRectMake(self.frame.size.width * i, 0, self.frame.size.width, self.frame.size.height);
                //_0DGraphViewController *Bar30DView = [[[_0DGraphViewController alloc] init] autorelease];
                _0DGraphViewController *Bar30DView = [[_0DGraphViewController alloc] init];
                Bar30DView.hostView.frame = frame;
                //[Bar30DView.view retain];
                [view addSubview:Bar30DView.view];
                
            } else if (i == 3) {

                CGRect frame = CGRectMake(self.frame.size.width * i, 0, self.frame.size.width, self.frame.size.height);
                //_65DGraphViewController *Bar365DView = [[[_65DGraphViewController alloc] init] autorelease];
                _65DGraphViewController *Bar365DView = [[_65DGraphViewController alloc] init];
                Bar365DView.authToken = self.authToken;
                Bar365DView.hostView.frame = frame;
                //[Bar365DView.view retain];
                [view addSubview:Bar365DView.view];
                
            } else if (i == 4) {

                CGRect frame = CGRectMake(self.frame.size.width * i, 0, self.frame.size.width, self.frame.size.height);
                //WTDGraphViewController *ScatterWTDView = [[[WTDGraphViewController alloc] init] autorelease];
                WTDGraphViewController *ScatterWTDView = [[WTDGraphViewController alloc] init];
                ScatterWTDView.hostView.frame = frame;
                //[ScatterWTDView.view retain];
                [view addSubview:ScatterWTDView.view];
                
            } else if (i == 5) {

                CGRect frame = CGRectMake(self.frame.size.width * i, 0, self.frame.size.width, self.frame.size.height);
                //MTDGraphViewController *BarMTDView = [[[MTDGraphViewController alloc] init]autorelease];
                MTDGraphViewController *BarMTDView = [[MTDGraphViewController alloc] init];
                BarMTDView.hostView.frame = frame;
                //[BarMTDView.view retain];
                [view addSubview:BarMTDView.view];
                
            } else if (i == 6) {

                CGRect frame = CGRectMake(self.frame.size.width * i, 0, self.frame.size.width, self.frame.size.height);
                //YTDGraphViewController *BarYTDView = [[[YTDGraphViewController alloc] init]autorelease];
                YTDGraphViewController *BarYTDView = [[YTDGraphViewController alloc] init];
                BarYTDView.hostView.frame = frame;
                //[BarYTDView.view retain];
                [view addSubview:BarYTDView.view];
            
            }
             
            [_scrollView addSubview:view];
		}
        
		[self changeToPage:0 animated:NO];
        
        
        
        _sliderPageControl = [[SliderPageControl  alloc] initWithFrame:CGRectMake(0,[self bounds].size.height-15,[self bounds].size.width,15)];
		[_sliderPageControl addTarget:self action:@selector(onPageChanged:) forControlEvents:UIControlEventValueChanged];
		[_sliderPageControl setDelegate:self];
		[_sliderPageControl setShowsHint:YES];
		[self addSubview:_sliderPageControl];
		[_sliderPageControl setNumberOfPages:7];
		[_sliderPageControl setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin];
		
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark scrollview delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView_
{
	self.pageControlUsed = NO;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.pageControlUsed)
	{
        return;
    }
	
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
	
	[self.sliderPageControl setCurrentPage:page animated:YES];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView_
{
	self.pageControlUsed = NO;
}

#pragma mark sliderPageControlDelegate

- (NSString *)sliderPageController:(id)controller hintTitleForPage:(NSInteger)page
{
	NSString *hintTitle = [[self.demoContent objectAtIndex:page] objectForKey:@"title"];
	return hintTitle;
}

- (void)onPageChanged:(id)sender
{
	self.pageControlUsed = YES;
	[self slideToCurrentPage:YES];
}

- (void)slideToCurrentPage:(bool)animated
{
	int page = self.sliderPageControl.currentPage;
	
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [self.scrollView scrollRectToVisible:frame animated:animated];
}

- (void)changeToPage:(int)page animated:(BOOL)animated
{
	[self.sliderPageControl setCurrentPage:page animated:YES];
	[self slideToCurrentPage:animated];
}

static NSArray *astronomyTimes;

+(NSArray *)getTimes
{
    return astronomyTimes;
}

+(void)getTimes:(NSArray *)setTimes
{
    if (astronomyTimes != setTimes) {
        astronomyTimes = [setTimes copy];
    }
}

@end
