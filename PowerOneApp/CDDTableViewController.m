//
//  CDDTableViewController.m
//  PowerOneApp
//
//  Created by John Setting on 7/10/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "CDDTableViewController.h"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) //1


@interface CDDTableViewController ()

@end

@implementation CDDTableViewController

@synthesize configData, statusData;
@synthesize allEntries = _allEntries;
@synthesize configEntries = _configEntries;
@synthesize statusEntries = _statusEntries;
@synthesize feeds = _feeds;
@synthesize queue = _queue;

- (void)refresh {
    
    
    for (NSString *feed in _feeds) {
        
        NSURL *url = [NSURL URLWithString:feed];
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request setDelegate:self];
        [_queue addOperation:request];
    }
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    allData = [[NSMutableArray alloc] init];
    cddData = [[NSMutableArray alloc] init];
    //eddData = [[NSMutableArray alloc] init];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    SWRevealViewController *revealController = [self revealViewController];
    
    //[self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"]
                                                                         style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    UIBarButtonItem *rightRevealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"]
                                                                              style:UIBarButtonItemStyleBordered target:self action:@selector(refresh)];
    
    self.navigationItem.rightBarButtonItem = rightRevealButtonItem;
    
    
    scroller = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, self.view.bounds.size.height)];
    scroller.center = CGPointMake(self.view.bounds.size.width/2.0, self.view.bounds.size.height/2.0 - 50);
    scroller.hidesWhenStopped = YES;
    [scroller setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview:scroller];
    [scroller startAnimating];
    
    //dispatch_async(kBgQueue, ^{
    
    //NSLog(@"%@", [LoggerTableViewController getStatusInfo:statusData]);
    //NSLog(@"%@", [LoggerTableViewController getConfigInfo:configData]);
    
    //[LoggerTableViewController getStatusInfo:statusData keys:_keys];
    //NSLog(@"%@", _keys);
    //_aKey = [_keys objectAtIndex:0];
    //_anObject = [[LoggerTableViewController getStatusInfo:statusData] objectForKey:_aKey];
    //NSLog(@"%@", _keys);
    //NSLog(@"%@", _aKey);
    //NSLog(@"%@", _anObject);
    
    //long long int timeStamp = 1373469386709;
    //NSLog(@"%@", [LoggerViewController getStatusInfo:timeStamp]);
    //NSLog(@"%@", [LoggerViewController getConfigInfo:timeStamp]);
    //[scroller stopAnimating];
    //[scroller removeFromSuperview];
    
    
    //});
    
    
    
    self.allEntries = [NSMutableArray array];
    self.configEntries = [NSMutableArray array];
    self.statusEntries = [NSMutableArray array];
    self.queue = [[NSOperationQueue alloc] init];
    //self.feeds = [NSArray arrayWithObjects:@"http://10.7.0.172/plant.xml",
    self.feeds = [NSArray arrayWithObjects:@"http://10.4.1.138/alarm.xml",
                  //@"http://10.0.2.127/au/logger/v1/status",
                  nil];
    
    
    [self refresh];
    

    //[AccelerometerSolarInstallationViewController solarPosition:TRUE secs:37 mins:45 hours:9 lat:33.65 lon:-84.43 timeZone:-5.0 daynum:203 year:1999 day:22 month:7 temp:27.0 aspect:135.0 pressure:1006.0];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    // return [[LoggerTableViewController getStatusInfo:statusData keys:_keys] count];
    //return [_statusEntries count] + [_configEntries count];
    NSLog(@"%i", [_allEntries count]);
    return [_allEntries count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    
    // Configure the cell...
    
    //if (indexPath.row >= [_configEntries count]) {
    /*
     NSArray *entry = [_statusEntries objectAtIndex:indexPath.row - [_configEntries count]];
     
     cell.textLabel.text = [entry objectAtIndex:0];
     cell.textLabel.font = [UIFont systemFontOfSize:16.0f];
     cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@ - %@", [entry objectAtIndex:1], [entry objectAtIndex:2], [entry objectAtIndex:3]];
     */
    //} else {
    
    NSLog(@"%@", [_allEntries objectAtIndex:indexPath.row]);
    //NSLog(@"%@", [entry objectAtIndex:indexPath.row]);
    //NSLog(@"%i", [keys count]);
    //NSLog(@"%@", keys);
    //NSLog(@"%@", [keys objectAtIndex:0]);
    
    /*
    NSLog(@"%@", [[_keys objectAtIndex:1] objectAtIndex:0]);
    
    for (int i = 1; i < [[_keys objectAtIndex:1] count]; i++)
    {
        NSLog(@"%@", [[[_keys objectAtIndex:1] objectAtIndex:i] objectForKey:@"x"]);
        NSLog(@"%@", [[_keys objectAtIndex:1] objectAtIndex:i]);
    }
    */
    
    //_aKey = [_keys objectAtIndex:0];
    //_anObject = [[[_keys objectAtIndex:1] objectAtIndex:0] objectForKey:_aKey];
    //NSLog(@"Anobject: %@", anObject);
    
    //NSLog(@"%@", [[keys objectAtIndex:1] objectAtIndex:0]);
    cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@", [[_allEntries objectAtIndex:indexPath.row] valueForKey:@"sn"], [[_allEntries objectAtIndex:indexPath.row] valueForKey:@"code"]];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@", [[_allEntries objectAtIndex:indexPath.row] valueForKey:@"desc"] ,[[_allEntries objectAtIndex:indexPath.row] valueForKey:@"time"]];
    //NSArray *entry = [_configEntries objectAtIndex:indexPath.row];
    //cell.textLabel.text = [entry objectAtIndex:0];
    //cell.textLabel.font = [UIFont systemFontOfSize:16.0f];
    //cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@", [entry objectAtIndex:1], [entry objectAtIndex:2]];
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //CDDDetailsViewController *detailView = [[CDDDetailsViewController alloc] initWithNibName:@"CDDDetailsViewController" bundle:nil];
    //detailView.detailsArray = _keys;
    //[self.navigationController pushViewController:detailView animated:YES];
}


- (NSMutableArray *) traverseElement:(TBXMLElement *)element {
    
    
    do {
        /*
        if ([[TBXML elementName:element] isEqualToString:@"data"]) {
            NSMutableDictionary *loggerData = [[NSMutableDictionary alloc] init];
            
            // Display the name of the element
            //NSLog(@"%@",[TBXML elementName:element]);
            
            // Obtain first attribute from element
            TBXMLAttribute * attribute = element->firstAttribute;
            
            // if attribute is valid
            while (attribute) {
                // Display name and value of attribute to the log window
                //NSLog(@"%@->%@ = %@",  [TBXML elementName:element], [TBXML attributeName:attribute], [TBXML attributeValue:attribute]);
                
                [loggerData setObject:[TBXML attributeValue:attribute] forKey:[TBXML attributeName:attribute]];
                
                // Obtain the next attribute
                attribute = attribute->next;
            }
            //NSLog(@"%@", loggerData);
            [allData addObject:loggerData];
            
        } else if ([[TBXML elementName:element] isEqualToString:@"cdd"]){
            
            NSMutableDictionary *cddDictionaryData = [[NSMutableDictionary alloc] init];
            
            // Display the name of the element
            //NSLog(@"%@",[TBXML elementName:element]);
            
            // Obtain first attribute from element
            TBXMLAttribute * attribute = element->firstAttribute;
            
            // if attribute is valid
            while (attribute) {
                // Display name and value of attribute to the log window
                //NSLog(@"%@->%@ = %@",  [TBXML elementName:element], [TBXML attributeName:attribute], [TBXML attributeValue:attribute]);
                
                [cddDictionaryData setObject:[TBXML attributeValue:attribute] forKey:[TBXML attributeName:attribute]];
                
                // Obtain the next attribute
                attribute = attribute->next;
            }
            
            [cddData addObject:cddDictionaryData];
            [allData addObject:cddData];
            
        } else */if ([[TBXML elementName:element] isEqualToString:@"alarm"]) {
            
            
            NSMutableDictionary *eddDictionaryData = [[NSMutableDictionary alloc] init];
            
            // Display the name of the element
            //NSLog(@"%@",[TBXML elementName:element]);
            
            // Obtain first attribute from element
            TBXMLAttribute * attribute = element->firstAttribute;
            
            // if attribute is valid
            while (attribute) {
                // Display name and value of attribute to the log window
                //NSLog(@"%@->%@ = %@",  [TBXML elementName:element], [TBXML attributeName:attribute], [TBXML attributeValue:attribute]);
                
                [eddDictionaryData setObject:[TBXML attributeValue:attribute] forKey:[TBXML attributeName:attribute]];
                
                // Obtain the next attribute
                attribute = attribute->next;
            }
            
            
            [allData addObject:eddDictionaryData];
        }
        
        
        
        //if the element has child elements, process them
        if (element->firstChild)
            [self traverseElement:element->firstChild];
        
        // Obtain next sibling element
    } while ((element = element->nextSibling));
    
    return allData;
    
}

/*
- (NSMutableArray *) traverseElement:(TBXMLElement *)element {
    
    
    do {

        if ([[TBXML elementName:element] isEqualToString:@"data"]) {
            NSMutableDictionary *loggerData = [[NSMutableDictionary alloc] init];

            // Display the name of the element
            //NSLog(@"%@",[TBXML elementName:element]);

            // Obtain first attribute from element
            TBXMLAttribute * attribute = element->firstAttribute;
            
            // if attribute is valid
            while (attribute) {
                // Display name and value of attribute to the log window
                //NSLog(@"%@->%@ = %@",  [TBXML elementName:element], [TBXML attributeName:attribute], [TBXML attributeValue:attribute]);
                
                [loggerData setObject:[TBXML attributeValue:attribute] forKey:[TBXML attributeName:attribute]];
                
                // Obtain the next attribute
                attribute = attribute->next;
            }
            //NSLog(@"%@", loggerData);
            [allData addObject:loggerData];
            
        } else if ([[TBXML elementName:element] isEqualToString:@"cdd"]){
            
            NSMutableDictionary *cddDictionaryData = [[NSMutableDictionary alloc] init];

            // Display the name of the element
            //NSLog(@"%@",[TBXML elementName:element]);

            // Obtain first attribute from element
            TBXMLAttribute * attribute = element->firstAttribute;
        
            // if attribute is valid
            while (attribute) {
                // Display name and value of attribute to the log window
                //NSLog(@"%@->%@ = %@",  [TBXML elementName:element], [TBXML attributeName:attribute], [TBXML attributeValue:attribute]);
            
                [cddDictionaryData setObject:[TBXML attributeValue:attribute] forKey:[TBXML attributeName:attribute]];
                
                // Obtain the next attribute
                attribute = attribute->next;
            }
            
            [cddData addObject:cddDictionaryData];
            [allData addObject:cddData];

        } else if ([[TBXML elementName:element] isEqualToString:@"edd"]) {
           
            
            NSMutableDictionary *eddDictionaryData = [[NSMutableDictionary alloc] init];
            
            // Display the name of the element
            //NSLog(@"%@",[TBXML elementName:element]);

            // Obtain first attribute from element
            TBXMLAttribute * attribute = element->firstAttribute;
            
            // if attribute is valid
            while (attribute) {
                // Display name and value of attribute to the log window
                //NSLog(@"%@->%@ = %@",  [TBXML elementName:element], [TBXML attributeName:attribute], [TBXML attributeValue:attribute]);
                
                [eddDictionaryData setObject:[TBXML attributeValue:attribute] forKey:[TBXML attributeName:attribute]];
                
                // Obtain the next attribute
                attribute = attribute->next;
            }

            [cddData addObject:eddDictionaryData];
        }
        
    
        
        //if the element has child elements, process them
        if (element->firstChild)
            [self traverseElement:element->firstChild];

        // Obtain next sibling element
    } while ((element = element->nextSibling));
    
    
    return allData;
    
}
*/

- (void)requestFinished:(ASIHTTPRequest *)request {
    
    [_queue addOperationWithBlock:^{
        TBXML *configTBXML = [[TBXML alloc] initWithXMLData:[request responseData]];
        TBXMLElement *root = configTBXML.rootXMLElement;

        if (root) {
            [self traverseElement:root];
        }
        
        for (int i = 0; i < [allData count]; i++)
        {
            [_allEntries insertObject:[allData objectAtIndex:i] atIndex:i];
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        }

        [scroller stopAnimating];
    }];
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    NSError *error = [request error];
    NSLog(@"Error: %@", error);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Issue" message:@"We are having issues connecting to the logger. Please pull the tableview down to refresh and try again." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
    [alert show];
}

@end
