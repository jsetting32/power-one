//
//  YWWeeklyForecastCell.h
//  PowerOneApp
//
//  Created by John Setting on 6/24/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

@interface YWWeeklyForecastCell : NSObject

@property (nonatomic, retain) IBOutlet UILabel *day;
@property (nonatomic, retain) IBOutlet UILabel *date;
@property (nonatomic, retain) IBOutlet UILabel *low;
@property (nonatomic, retain) IBOutlet UILabel *high;
@property (nonatomic, retain) IBOutlet UILabel *currentTemp;
@property (nonatomic, retain) IBOutlet UILabel *text;
@property (nonatomic, retain) IBOutlet UILabel *code;
@property (nonatomic, retain) IBOutlet UILabel *currentCity;
@property (nonatomic, retain) IBOutlet UILabel *currentState;
@property (nonatomic, retain) IBOutlet NSData *pic;



@end
