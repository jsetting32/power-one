//
//  SearchTableCell.m
//  ourApplication
//
//  Created by John Setting on 4/21/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "NotificationsCustomCell.h"

@implementation NotificationsCustomCell
@synthesize EventDescription, EventTitle, EventStartTime;
@synthesize avatarImage = _avatarImage;
@synthesize avatarName = _avatarName;





- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        //avatarImage = [[FBProfilePictureView alloc] initWithFrame:CGRectMake(10, 10, 40, 40)];
        
    }
    return self;
}

- (NSUInteger)count
{
    return 0;
}
- (id)objectForKey:(id)aKey
{
    return aKey;
}
- (NSEnumerator *)keyEnumerator
{
    NSEnumerator *num;
    return num;
}
- (void)removeObjectForKey:(id)aKey
{
    
}
- (void)setObject:(id)anObject forKey:(id)aKey
{

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)facebookUpload:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Facebook Upload" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Share to Facebook!", nil];
    [actionSheet showInView:self];
}


- (IBAction)twitterUpload:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Twitter Upload" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Tweet!", nil];
    [actionSheet showInView:self];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //Get the name of the current pressed button
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    //if  ([buttonTitle isEqualToString:@"Destructive Button"]) {
    //    NSLog(@"Destructive pressed --> Delete Something");
    //}
    
    /*
    if ([buttonTitle isEqualToString:@"Share to Facebook!"]) {
        if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) //check if Facebook Account is linked
        {
            NSLog(@"Linked");
            mySLComposerSheet = [[SLComposeViewController alloc] init]; //initiate the Social Controller
        
            mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            //Tell him with what social plattform to use it, e.g. facebook or twitter
            
            [mySLComposerSheet setInitialText:[NSString stringWithFormat:@"Test Here is my event%@",mySLComposerSheet.serviceType]];
            //the message you want to post
        
            //[mySLComposerSheet addImage:yourimage]; //an image you could post
            //for more instance methodes, go here:https://developer.apple.com/library/ios/#documentation/NetworkingInternet/Reference/SLComposeViewController_Class/Reference/Reference.html#//apple_ref/doc/uid/TP40012205
            [self addSubview:mySLComposerSheet.view];
            //[self presentViewController:mySLComposerSheet animated:YES completion:nil];
        } else {
            NSLog(@"Not Linked");
        }
        [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            NSString *output;
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    output = @"Action Cancelled";
                    break;
                case SLComposeViewControllerResultDone:
                    output = @"Post Successfull";
                    NSLog(@"Posted to Facebook!");
                    break;
                default:
                    NSLog(@"Something weird happened");
                    break;
            } //check if everythink worked properly. Give out a message on the state.
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }];
     
     
        
    }
     */
    if ([buttonTitle isEqualToString:@"Tweet!"]) {
        NSLog(@"Tweeted!");
    }
    if ([buttonTitle isEqualToString:@"Cancel"]) {
        NSLog(@"Cancel pressed --> Cancel ActionSheet");
    }
}


@end
