//
//  SharingActivityProvider.h
//  PowerOneAuroraApp
//
//  Created by John Setting on 5/21/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "SharingActivityProvider.h"

@implementation SharingActivityProvider
{
    
}

- (id)activityViewController:(UIActivityViewController *)activityViewController itemForActivityType:(NSString *)activityType {
    // Log out the activity type that we are sharing with
    //NSLog(@"%@", activityType);
    // Create the default sharing string
    

    
    NSString *shareString = [[NSString alloc] initWithFormat:@"Currently, %@ is saving the Environment.\n With a total of %@ acres of trees saved, equivalent to %@ metric tons!!!\n In addition to saving trees, %@ saves the lives of people by limiting the amount of harmful gases released to our atmosphere: Carbon Dioxide - %@lbs, Nitrous Oxide - %@lbs, and %@lbs of Sulfur Dioxide. Also, the energy produced by %@ can power %@ computers for an entire year, avoids the pollution an average passenger car emits over %@ years, and the energy to power a television for %@ year(s) as well! Would you like to get on-board with Power-One? Refer to \"http://www.power-one.com\" for more information!", _EntityName, _CarbonOffsetAcresSavings, _CarbonOffsetMetricTonsSavings, _EntityName,  _CO2Savings, _NOXSavings, _SO2Savings, _EntityName, _ComputerSavings, _CarSavings, _TVSavings];
    
    //NSLog(@"%@", shareString);
    
    // customize the sharing string for facebook, twitter, weibo, and google+
    if ([activityType isEqualToString:UIActivityTypePostToFacebook]) {
        shareString = [NSString stringWithFormat:@"Attention Facebook: %@", shareString];
    } else if ([activityType isEqualToString:UIActivityTypePostToTwitter]) {
        shareString = [NSString stringWithFormat:@"Attention Twitter: %@", shareString];
    } else if ([activityType isEqualToString:UIActivityTypeMessage]) {
        shareString = [NSString stringWithFormat:@"Attention Message: %@", shareString];
    } else if ([activityType isEqualToString:UIActivityTypePrint]) {
        shareString = [NSString stringWithFormat:@"Attention Print: %@", shareString];
    } else if ([activityType isEqualToString:UIActivityTypeMail]) {
        shareString = [NSString stringWithFormat:@"Attention Mail: %@", shareString];
    } else if ([activityType isEqualToString:UIActivityTypeCopyToPasteboard]) {
        shareString = [NSString stringWithFormat:@"Attention Copy: %@", shareString];
    }
    
    return shareString;
    

}

- (id)activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController {
    return @"";
}

@end