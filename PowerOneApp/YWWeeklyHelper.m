//
//  YWWeeklyHelper.m
//  PowerOneApp
//
//  Created by John Setting on 6/24/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "YWWeeklyHelper.h"

@implementation YWWeeklyHelper

+ (NSArray *) getWeather:(int)woeid
{
    NSString *urlString = [NSString stringWithFormat:@"http://xml.weather.yahoo.com/forecastrss/%d_f.xml", woeid];
    NSURL *url = [NSURL URLWithString:urlString];
    TBXML *tbxml = [TBXML tbxmlWithURL:url];
    TBXMLElement *root = tbxml.rootXMLElement;
    TBXMLElement *channelElement = [TBXML childElementNamed:@"channel" parentElement:root];
    TBXMLElement *itemElement = [TBXML childElementNamed:@"item" parentElement:channelElement];
    TBXMLElement *yweatherElement = [TBXML childElementNamed:@"yweather:forecast" parentElement:itemElement];
    TBXMLElement *yweatherToday = [TBXML childElementNamed:@"yweather:condition" parentElement:itemElement];
    TBXMLElement *yweatherRegion = [TBXML childElementNamed:@"yweather:location" parentElement:channelElement];
  
    
    YWWeeklyForecast *forecast = [[YWWeeklyForecast alloc] init];
    

    NSMutableArray *forecasts = [[NSMutableArray alloc] init];
    do
    {
        if (yweatherElement)
        {
            // TODO this is the only way I know for now how to test if the forecast is blank
            if ([TBXML valueOfAttributeNamed:@"day" forElement:yweatherElement])
            {
                forecast = [[YWWeeklyForecast alloc] init];
                forecast.day = [TBXML valueOfAttributeNamed:@"day" forElement:yweatherElement];
                forecast.day = [self returnGoodString:forecast.day];

                forecast.date = [TBXML valueOfAttributeNamed:@"date" forElement:yweatherElement];
                forecast.low = [[TBXML valueOfAttributeNamed:@"low" forElement:yweatherElement] intValue];
                forecast.high = [[TBXML valueOfAttributeNamed:@"high" forElement:yweatherElement] intValue];
                forecast.text = [TBXML valueOfAttributeNamed:@"text" forElement:yweatherElement];
                forecast.code = [[TBXML valueOfAttributeNamed:@"code" forElement:yweatherElement] intValue];
                [forecasts addObject:forecast];
            }
            forecast.currentTemp = [TBXML valueOfAttributeNamed:@"temp" forElement:yweatherToday];
            forecast.currentCity = [TBXML valueOfAttributeNamed:@"city" forElement:yweatherRegion];
            forecast.currentState = [TBXML valueOfAttributeNamed:@"region" forElement:yweatherRegion];

        }

    } while ((yweatherElement = yweatherElement->nextSibling));
    
    return forecasts;
}

#pragma mark - WOEID

+ (YWWeeklyWOEID *) getWOEID:(float)latitude longitude:(float)longitude yahooAPIKey:(NSString *)yahooAPIKey
{
    NSString *urlString = [NSString stringWithFormat:@"http://where.yahooapis.com/geocode?q=%f,%f&gflags=R&appid=%@", latitude, longitude, yahooAPIKey];
    NSURL *url = [NSURL URLWithString:urlString];
    TBXML *tbxml = [TBXML tbxmlWithURL:url];
    TBXMLElement *root = tbxml.rootXMLElement;
    TBXMLElement *resultElement = [TBXML childElementNamed:@"Result" parentElement:root];
    
    YWWeeklyWOEID *woeid = [[YWWeeklyWOEID alloc] init];
    
    TBXMLElement *woeidElement = [TBXML childElementNamed:@"woeid" parentElement:resultElement];
    woeid.woeid = [[TBXML textForElement:woeidElement] intValue];
    
    TBXMLElement *cityElement = [TBXML childElementNamed:@"city" parentElement:resultElement];
    woeid.city = [TBXML textForElement:cityElement];
    
    return woeid;
}


+ (NSString *) returnGoodString :(NSString *)formatThisString
{
    static NSString *string;
    
    if ([formatThisString isEqualToString:@"Mon"]) {
        string = @"Monday";
    } else if ([formatThisString isEqualToString:@"Tue"]) {
        string = @"Tuesday";
    } else if ([formatThisString isEqualToString:@"Wed"]) {
        string = @"Wednesday";
    } else if ([formatThisString isEqualToString:@"Thu"]) {
        string = @"Thursday";
    } else if ([formatThisString isEqualToString:@"Fri"]) {
        string = @"Friday";
    } else if ([formatThisString isEqualToString:@"Sat"]) {
        string = @"Saturday";
    } else if ([formatThisString isEqualToString:@"Sun"]) {
        string = @"Sunday";
    }
    
    return string;
}

@end
