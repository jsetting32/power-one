//
//  EnvironmentalsSubView.m
//  PowerOneApp
//
//  Created by John Setting on 6/17/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "EnvironmentalsSubView.h"

#define easyViewData [NSURL URLWithString: @"https://easyview.auroravision.net/easyview/services/gmi/summary/PlantEnergy.json?eids=1167815&tz=US%2FMountain&hasUsage=false&dateLabel=E+MMM+d%2C+yyyy+h%3Amm%3Ass+a+z&locale=en&v=1.3.2"] //2
#define hierarchyRequest [NSURL URLWithString: @"https://easyview.auroravision.net/easyview/services/gai/plant/hierarchy.json?entityId=1167815&v=1.4.7"]
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) //1


@implementation EnvironmentalsSubView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        environmentalsData = [[PullEasyViewData alloc] init];
        
        // Instantiate and allocate memory to prepare for an alert popup if any connections fail
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Status" message:@"Some connections to your site are down. In result, some data will not present. Please pull to refresh to try again!" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // -------------------- Get JSON Object from easyView -------------------------- //
            
            NSData* easyViewNSData = [NSData dataWithContentsOfURL: easyViewData];
            if (easyViewNSData == nil) {
                NSLog(@"easyViewNSData is nil");
                [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
                j = 0;
            } else {
                [environmentalsData performSelectorOnMainThread:@selector(fetchEasyViewData:) withObject:easyViewNSData waitUntilDone:YES];
                //[environmentalsData performSelectorInBackground:@selector(fetchEasyViewData:) withObject:easyViewNSData];
                j = 1;
            }
            // -------------------- Get JSON Object from easyView -------------------------- //
            
            // -------------------- Get JSON Object from hierarchyData -------------------------- //
            NSData* hierarchyNSData = [NSData dataWithContentsOfURL: hierarchyRequest];
            if (hierarchyNSData == nil) {
                NSLog(@"hierarchyNSData is nil");
                [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
                j = 0;
            } else {
                [environmentalsData performSelectorOnMainThread:@selector(fetchHierarchyRequestData:) withObject:hierarchyNSData waitUntilDone:YES];
                //[environmentalsData performSelectorInBackground:@selector(fetchHierarchyRequestData:) withObject:hierarchyNSData];
                j = 1;
            }
        
            csView = [[XLCycleScrollView alloc] initWithFrame:self.bounds];
            csView.backgroundColor = [UIColor clearColor];
            csView.delegate = self;
            csView.datasource = self;
            [self addSubview:csView];
        
        });
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


- (NSInteger)numberOfPages
{
    return 7;
}

- (UIView *)pageAtIndex:(NSInteger)index
{
    theView = [[UIView alloc] initWithFrame:self.bounds];
    

    UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(theView.bounds.origin.x, 35, theView.bounds.size.width, 10)];
    [l setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10.0f]];
    l.textAlignment = NSTextAlignmentCenter;
    l.backgroundColor = [UIColor clearColor];
    l.textColor = [UIColor lightGrayColor];
    
    UILabel *savings = [[UILabel alloc] init];
    savings.numberOfLines = 2;  
    savings.textColor = [UIColor lightGrayColor];
    savings.backgroundColor = [UIColor clearColor];
    savings.font = [UIFont systemFontOfSize:10];
    
    UILabel *subSavings = [[UILabel alloc] init];
    subSavings.numberOfLines = 2;
    subSavings.textColor = [UIColor lightGrayColor];
    subSavings.backgroundColor = [UIColor clearColor];
    subSavings.font = [UIFont systemFontOfSize:10];
    
    UIImageView *icon = [[UIImageView alloc] init];
    icon.backgroundColor = [UIColor clearColor];
    
    if (j == 1)
    {
        if (index == 0) {
            l.text = @"Environmental Equivalents";
            icon.image = [UIImage imageNamed:@"tv.png"];
            icon.frame = CGRectMake(20, 40, 60, 40);
        
            //NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:[environmentalsData getTVSavings]];
            //NSRange range =  [[NSString stringWithFormat:@"The energy to operate a TV for\n%@ hours", [environmentalsData getTVSavings]] rangeOfString:[environmentalsData getTVSavings]];
        
            //[savings.attributedText attribute:savings.text atIndex:0 effectiveRange:&range];
            NSString *labelText = [NSString stringWithFormat:@"The energy to operate a TV for\n%@ hours", [environmentalsData getTVSavings]];
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText];
            NSRange range = [labelText rangeOfString:[environmentalsData getTVSavings]];
            [attributedString addAttribute:NSStrokeWidthAttributeName value:[NSNumber numberWithFloat:-3.0] range:range];
            [attributedString addAttribute:NSStrokeColorAttributeName value:[UIColor greenColor] range:range];
            //[attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
        
            savings.attributedText = attributedString;
            savings.frame = CGRectMake(120, 55, theView.bounds.size.width, 30);
            [theView addSubview:savings];
            [theView addSubview:l];
            [theView addSubview:icon];
            return theView;
        } else if (index == 1) {
        
            l.text = @"Environmental Equivalents";
            icon.image = [UIImage imageNamed:@"sign_car-256.png"];
            icon.frame = CGRectMake(20, 40, 60, 40);

        
            NSString *labelText = [NSString stringWithFormat:@"The pollution an average passenger\ncar emits over %@ years", [environmentalsData getCarSavings]];
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText];
            NSRange range = [labelText rangeOfString:[environmentalsData getCarSavings]];
            [attributedString addAttribute:NSStrokeWidthAttributeName value:[NSNumber numberWithFloat:-3.0] range:range];
            [attributedString addAttribute:NSStrokeColorAttributeName value:[UIColor greenColor] range:range];
            //[attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
        
            savings.attributedText = attributedString;
            savings.frame = CGRectMake(120, 55, theView.bounds.size.width, 30);

            [theView addSubview:savings];
            [theView addSubview:l];
            [theView addSubview:icon];

            return theView;
        } else if (index == 2) {
        
            l.text = @"Environmental Equivalents";
            icon.image = [UIImage imageNamed:@"1359643388_monitor.png"];
            icon.frame = CGRectMake(20, 40, 60, 40);

            NSString *labelText = [NSString stringWithFormat:@"The energy to power %@\ncomputers for 1 year", [environmentalsData getComputerSavings]];
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText];
            NSRange range = [labelText rangeOfString:[environmentalsData getComputerSavings]];
            [attributedString addAttribute:NSStrokeWidthAttributeName value:[NSNumber numberWithFloat:-3.0] range:range];
            [attributedString addAttribute:NSStrokeColorAttributeName value:[UIColor greenColor] range:range];
            //[attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
        
            savings.attributedText = attributedString;
            savings.frame = CGRectMake(120, 55, theView.bounds.size.width, 30);

            [theView addSubview:savings];
            [theView addSubview:l];
            [theView addSubview:icon];
        
            return theView;
        } else if (index == 3) {
        
            l.text = @"Greenhouse Gases";
            icon.image = [UIImage imageNamed:@"CO2-Icon.gif"];
            icon.frame = CGRectMake(20, 40, 40, 40);
            
            NSString *labelText = [NSString stringWithFormat:@"You have prevented the\nproduction of %@ lb", [environmentalsData getCO2Savings]];
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText];
            NSRange range = [labelText rangeOfString:[environmentalsData getCO2Savings]];
            [attributedString addAttribute:NSStrokeWidthAttributeName value:[NSNumber numberWithFloat:-3.0] range:range];
            [attributedString addAttribute:NSStrokeColorAttributeName value:[UIColor greenColor] range:range];
            //[attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
        
            savings.attributedText = attributedString;
            savings.frame = CGRectMake(120, 55, theView.bounds.size.width, 30);

            [theView addSubview:savings];
            [theView addSubview:l];
            [theView addSubview:icon];
        
            return theView;
        } else if (index == 4) {
        
            l.text = @"Greenhouse Gases";
            icon.image = [UIImage imageNamed:@"NOx-Icon.gif"];
            icon.frame = CGRectMake(20, 40, 40, 40);

            NSString *labelText = [NSString stringWithFormat:@"You have prevented the\nproduction of %@ lb", [environmentalsData getNOXSavings]];
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText];
            NSRange range = [labelText rangeOfString:[environmentalsData getNOXSavings]];
            [attributedString addAttribute:NSStrokeWidthAttributeName value:[NSNumber numberWithFloat:-3.0] range:range];
            [attributedString addAttribute:NSStrokeColorAttributeName value:[UIColor greenColor] range:range];
            //[attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
        
            savings.attributedText = attributedString;
            savings.frame = CGRectMake(120, 55, theView.bounds.size.width, 30);

            [theView addSubview:savings];
            [theView addSubview:l];
            [theView addSubview:icon];
        
            return theView;
        } else if (index == 5) {
        
            l.text = @"Greenhouse Gases";
            icon.image = [UIImage imageNamed:@"monitor-icon.png"];
            icon.frame = CGRectMake(20, 40, 40, 40);

            NSString *labelText = [NSString stringWithFormat:@"You have prevented the\nproduction of %@ lb", [environmentalsData getSO2Savings]];
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText];
            NSRange range = [labelText rangeOfString:[environmentalsData getSO2Savings]];
            [attributedString addAttribute:NSStrokeWidthAttributeName value:[NSNumber numberWithFloat:-3.0] range:range];
            [attributedString addAttribute:NSStrokeColorAttributeName value:[UIColor greenColor] range:range];
            //[attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
        
            savings.attributedText = attributedString;
            savings.frame = CGRectMake(120, 55, theView.bounds.size.width, 30);

            [theView addSubview:savings];
            [theView addSubview:l];
            [theView addSubview:icon];
        
            return theView;
        } else if (index == 6) {
        
            l.text = @"Carbon Offset";
            icon.image = [UIImage imageNamed:@"trees.png"];
            icon.frame = CGRectMake(20, 40, 60, 40);

            NSString *labelText = [NSString stringWithFormat:@"You have saved %@ metric tons", [environmentalsData getCarbonOffsetMetricTonsSavings]];
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText];
            NSRange range = [labelText rangeOfString:[environmentalsData getCarbonOffsetMetricTonsSavings]];
            [attributedString addAttribute:NSStrokeWidthAttributeName value:[NSNumber numberWithFloat:-3.0] range:range];
            [attributedString addAttribute:NSStrokeColorAttributeName value:[UIColor greenColor] range:range];
            //[attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:range];
        
            savings.attributedText = attributedString;

            NSString *labelSubtext = [NSString stringWithFormat:@"which offset the equivalent of %@ acres", [environmentalsData getCarbonOffsetAcresSavings]];
            NSMutableAttributedString *subtextAttributedString = [[NSMutableAttributedString alloc] initWithString:labelSubtext];
            NSRange newRange = [labelSubtext rangeOfString:[environmentalsData getCarbonOffsetAcresSavings]];
            [subtextAttributedString addAttribute:NSStrokeWidthAttributeName value:[NSNumber numberWithFloat:-3.0] range:newRange];
            [subtextAttributedString addAttribute:NSStrokeColorAttributeName value:[UIColor greenColor] range:newRange];
            subSavings.attributedText = subtextAttributedString;
            savings.frame = CGRectMake(100, 45, theView.bounds.size.width, 30);
            subSavings.frame = CGRectMake(100, 65, theView.bounds.size.width, 30);
        
            [theView addSubview:savings];
            [theView addSubview:subSavings];
            [theView addSubview:l];
            [theView addSubview:icon];
        
            return theView;
        }
    } else {}
    
    return theView;
}

- (void)didClickPage:(XLCycleScrollView *)csView atIndex:(NSInteger)index
{
    //UIViewController *view = [[UIViewController alloc] init];
    //[self.navigationController pushViewController:view animated:YES];
    //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"Clicked %d",index] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
    
    //[alert show];
}


static NSArray * object1;

+ (NSArray *) setArrayOne
{
    return object1;
}

+ (void)setArrayOne:(NSArray *)objectSetter
{
    if (object1 != objectSetter)
    {
        object1 = [objectSetter copy];
    }
}


static NSArray * object2;

+ (NSArray *) setArrayTwo
{
    return object2;
}

+ (void)setArrayTwo:(NSArray *)objectSetter
{
    if (object2 != objectSetter)
    {
        object2 = [objectSetter copy];
    }
}


@end
