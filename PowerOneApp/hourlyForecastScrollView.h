//
//  hourlyForecastScrollView.h
//  PowerOneApp
//
//  Created by John Setting on 6/24/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullEasyViewData.h"

@interface hourlyForecastScrollView : UIScrollView
{
    PullEasyViewData *hourlyForecast;
}

@property (nonatomic, strong) UIImage *icon;
@property (nonatomic, strong) UILabel *timeTemp;
@property (nonatomic, strong) UILabel *dateTime;


- (id)init:(NSString*)city state:(NSString *)state icons:(NSArray *)icons times:(NSArray *)times temps:(NSArray *)temps;

@property (nonatomic, strong) NSArray *times;
@property (nonatomic, strong) NSArray *temps;
@property (nonatomic, strong) NSArray *icons;

@end
