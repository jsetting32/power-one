//
//  CDDConfigViewController.h
//  PowerOneApp
//
//  Created by John Setting on 7/19/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDDConfigNetworkViewController.h"
#import "TBXML.h"
#import "ASIHTTPRequest.h"
#import <CoreLocation/CoreLocation.h>
#import "CDDConfigSegments.h"

@interface CDDConfigPlantViewController : UIViewController <UITextFieldDelegate, CLLocationManagerDelegate, UIAccelerometerDelegate>
{
    
    UITextField *_PlantName;
    UITextField *_Location;
    UITextField *_Address;
    UITextField *_Latitude;
    UITextField *_Longitude;
    
    UIAccelerometer * theAccelerometer;
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    
    @public
    NSString *ipAddress;
    NSString *encode;

}


@property (strong, nonatomic) IBOutlet UITextField *PlantName;
@property (strong, nonatomic) IBOutlet UITextField *Location;
@property (strong, nonatomic) IBOutlet UITextField *Address;
@property (strong, nonatomic) IBOutlet UITextField *Latitude;
@property (strong, nonatomic) IBOutlet UITextField *Longitude;

@property (strong, nonatomic) IBOutlet UIButton *ConfirmButton;
- (IBAction)ConfirmAction:(id)sender;
@end
