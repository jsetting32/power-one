//
//  FranksViewController.m
//  PowerOneApp
//
//  Created by John Setting on 6/10/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "FranksVC.h"


#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0) //1



@implementation FranksVC

@synthesize mainView, canvas;


-(id) init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)showLoginViewAnimated:(BOOL)animated {
    NSLog(@"[MainViewController] Show login view controller");

    PowerOneLoginTabBar *loginVC = [[PowerOneLoginTabBar alloc] init];
    [self presentViewController:loginVC animated:animated completion:nil];
}

- (void)logoutHandler:(NSNotification *)notification {
    NSLog(@"[MainViewController] Logout handler");
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"userLoggedIn"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self showLoginViewAnimated:YES];
}

- (void) clearCoreData
{
    NSError * error = nil;
    // retrieve the store URL
    NSURL * storeURL = [[self.managedObjectContext persistentStoreCoordinator] URLForPersistentStore:[[[self.managedObjectContext persistentStoreCoordinator] persistentStores] lastObject]];
    // lock the current context
    [self.managedObjectContext lock];
    [self.managedObjectContext reset];//to drop pending changes
    //delete the store from the current managedObjectContext
    if ([[self.managedObjectContext persistentStoreCoordinator] removePersistentStore:[[[self.managedObjectContext persistentStoreCoordinator] persistentStores] lastObject] error:&error])
    {
        // remove the file containing the data
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:&error];
        //recreate the store like in the  appDelegate method
        [[self.managedObjectContext persistentStoreCoordinator] addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error];//recreates the persistent store
    }
    [self.managedObjectContext unlock];
}

#pragma mark - View Life Cycle

- (void) viewDidLoad {
    
    loadMainView = @"NO";
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self clearCoreData];

    BOOL isUserLoggedIn = [[NSUserDefaults standardUserDefaults] boolForKey:@"userLoggedIn"];
    if( !isUserLoggedIn || (!setAuthenticationKey || [setAuthenticationKey isKindOfClass:[NSNull class]]))
    {
        NSLog(@"View Did Load Checker for user is logged in and they aren't");
        [self showLoginViewAnimated:NO];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logoutHandler:) name:LOGOUT_NOTIFICATION object:self.view];

    // Create a PullEasyViewData object to access it's instance (-) methods
    
    // ------------------------- Scroll View ------------------------------//
    scroller = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    scroller.delegate = self;
    scroller.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"1-carbon-fiber-standard-weave-iphone-background.jpg"]];
    scroller.scrollEnabled = YES;
    scroller.pagingEnabled = YES;
    scroller.showsVerticalScrollIndicator = NO;
    scroller.contentSize = CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height + 44);
    [self.view addSubview:scroller];
    
    UIImageView *iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"easyview-logo.png"]];
    self.navigationItem.titleView = iv;
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    // ------------------------- Scroll View ------------------------------//
    
    
    SWRevealViewController *revealController = [self revealViewController];
    
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    
    UIButton *leftRevealButtonItem = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 40)];
    [leftRevealButtonItem setImage:[UIImage imageNamed:@"reveal-icon.png"] forState:UIControlStateNormal];
    [leftRevealButtonItem addTarget:revealController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftRevealButtonItem];
    
    UIButton *rightRevealButtonItem = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 40)];
    [rightRevealButtonItem setImage:[UIImage imageNamed:@"Inverter Icon.png"] forState:UIControlStateNormal];
    [rightRevealButtonItem addTarget:revealController action:@selector(rightRevealToggle:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightRevealButtonItem];

    
    if (_refreshHeaderView == nil) {
        EGORefreshTableHeaderView *refreshview = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - scroller.bounds.size.height, scroller.frame.size.width, scroller.bounds.size.height)];
        refreshview.delegate = self;
        [scroller addSubview:refreshview];
        _refreshHeaderView = refreshview;
    }
    
    //  update the last update date
    [_refreshHeaderView refreshLastUpdatedDate];
}



- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadMainViewCallbacks:) name:@"AssetInfo" object:nil];
    // if (notification was made, don't execute)
    NSLog(@"View Did Appear: %@", loadMainView);
    if (![loadMainView isEqualToString:@"YES"])
    {
        HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        HUD.dimBackground = YES;
        HUD.labelText = @"Loading";
        HUD.detailsLabelText = @"Fetching data";
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            pulledData = [PullEasyViewData sharedManager];
            [self makeCallbacks];
            //authData = [AuthenticationData sharedManager];
            //[self getAuthData];
        });
    }
}


- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

#pragma mark - Authentication Callbacks
- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    BOOL isUserLoggedIn = [[NSUserDefaults standardUserDefaults] boolForKey:@"userLoggedIn"];
    if (isUserLoggedIn) { NSLog(@"Logged In"); }
    else { NSLog(@"Not Logged In"); }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(yourNotificationHandler:) name:@"AssetInfo" object:nil];
    
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
}


//Now create yourNotificationHandler: like this in parent class
-(void)yourNotificationHandler:(NSNotification *)notice {
    _entityID = [[notice object] objectForKey:@"entityId"];
    NSLog(@"yourNotificationHandler Method: %@", _entityID);
    _uniqueSiteName = [[[notice object] objectForKey:@"meta"] objectAtIndex:0];
    _siteSystemSize = [[[notice object] objectForKey:@"installedCapacities"] objectAtIndex:1];
    
    NSLog(@"%@", [_systemSize objectAtIndex:0]);
    NSLog(@"%@", _uniqueSiteName);
}

-(void)loadMainViewCallbacks:(NSNotification *)notice {
    loadMainView = [notice object];
    NSLog(@"loadMainViewCallback Method: %@", loadMainView);
}

- (void)toggleMenu{
    if (_menu.isOpen)
        return [_menu close];
    [_menu showInView:scroller];
}


- (void) getAuthData
{
    
    loadMainView = @"YES";
    //[authData ConfirmAction:@"https://easyview.idev.auroravision.net/easyview/services/gai/plants.json?entityId=210298"];
    //[authData ConfirmAction:@"https://www.auroravision.net/dash/services/gai/plants?entityId=1129945&api_key=special-key"];
    // ---------------------------------------------------------------------------------------------- Get Energy Generated Data ----------------------------------------------------------------------------------------- //
    
    [authData fetchPlantData:[NSString stringWithFormat:@"https://platform.fatspaniel.net/rest/v1/feed/%@?field=PVPower&type=windowRequest&function=diff&startrelative=now&endrelative=now&field.1=PVEnergy&field.1.type=windowRequest&field.1.function=diff&field.1.startrelative=start-of-0-days-ago&endrelative=now&field.2=PVEnergy&field.2.type=windowRequest&field.2.function=diff&field.2.startrelative=start-of-week&endrelative=now&field.3=PVEnergy&field.3.type=windowRequest&field.3.function=diff&field.3.startrelative=start-of-month&endrelative=now&field.4=PVEnergy", _entityID] authKey:setAuthenticationKey];
    // ---------------------------------------------------------------------------------------------- Get Energy Generated Data ----------------------------------------------------------------------------------------- //
    
    // ---------------------------------------------------------------------------------------------------- Get Graph Data ----------------------------------------------------------------------------------------------- //
    [authData fetchPVEnergy_PVPowerDataYTD:[NSString stringWithFormat:@"https://platform.fatspaniel.net/rest/v1/info/%@?field=PVEnergy&type=binsRequest&binsize=month&function=diff&startrelative=start-of-year&endrelative=now", _entityID] authKey:setAuthenticationKey];
    [authData fetchPVEnergy_PVPowerDataMTD:[NSString stringWithFormat:@"https://platform.fatspaniel.net/rest/v1/info/%@?field=PVEnergy&type=binsRequest&binsize=day&function=diff&startrelative=start-of-month&endrelative=now", _entityID] authKey:setAuthenticationKey];
    [authData fetchPVEnergy_PVPowerDataWTD:[NSString stringWithFormat:@"https://platform.fatspaniel.net/rest/v1/info/%@?field=PVPower&type=binsRequest&binsize=hour&function=avg&startrelative=start-of-week&endrelative=now", _entityID] authKey:setAuthenticationKey];
    [authData fetchPVEnergy_PVPowerData365D:[NSString stringWithFormat:@"https://platform.fatspaniel.net/rest/v1/info/%@?field=PVEnergy&type=binsRequest&binsize=month&function=diff&startrelative=start-of-364-days-ago&endrelative=now", _entityID] authKey:setAuthenticationKey];
    [authData fetchPVEnergy_PVPowerData30D:[NSString stringWithFormat:@"https://platform.fatspaniel.net/rest/v1/info/%@?field=PVEnergy&type=binsRequest&binsize=day&function=diff&startrelative=start-of-29-days-ago&endrelative=now", _entityID] authKey:setAuthenticationKey];
    [authData fetchPVEnergy_PVPowerData7D:[NSString stringWithFormat:@"https://platform.fatspaniel.net/rest/v1/info/%@?field=PVPower&type=binsRequest&binsize=hour&function=avg&startrelative=start-of-6-days-ago&endrelative=now", _entityID] authKey:setAuthenticationKey];
    [authData fetchPVEnergy_PVPowerDataDay:[NSString stringWithFormat:@"https://platform.fatspaniel.net/rest/v1/info/%@?field=PVPower&type=binsRequest&binsize=min15&function=avg&startrelative=start-of-0-days-ago&endrelative=now", _entityID] authKey:setAuthenticationKey];
    // ---------------------------------------------------------------------------------------------------- Get Graph Data ----------------------------------------------------------------------------------------------- //
    
    // ------------------------------------------------------------------------------------------------- Get Module Status Data -------------------------------------------------------------------------------------------- //
    [authData fetchWeatherStation:[NSString stringWithFormat:@"https://platform.fatspaniel.net/rest/v1/info/%@?field=CumIrradiance&field.1=Irradiance&field.2=CellTemp&field.3=AmbientTemp", _entityID] authKey:setAuthenticationKey];
    // ------------------------------------------------------------------------------------------------- Get Module Status Data -------------------------------------------------------------------------------------------- //
    
    // ---------------------------------------------------------------------------------------------------- Get Device IDs ----------------------------------------------------------------------------------------------- //
    [authData fetchLoggerIDs:[NSString stringWithFormat:@"https://platform.fatspaniel.net/rest/v1/asset?query=entityId=%@&resultQuery=Device[detailLevel('Limited')]", _entityID] authKey:setAuthenticationKey];
    // ---------------------------------------------------------------------------------------------------- Get Device IDs ----------------------------------------------------------------------------------------------- //
    
    // --------------------------------------------------------------------------------------------------- Get Device Data ----------------------------------------------------------------------------------------------- //
    for (int i = 0; i < [[authData getLoggerIDs] count]; i++)
        [authData fetchInverterData:[NSString stringWithFormat:@"https://platform.fatspaniel.net/rest/v1/info/%@?field=PVPower&type=binsRequest&binsize=min15&function=avg&startrelative=start-of-0-days-ago", [[authData getLoggerIDs] objectAtIndex:i]] authKey:setAuthenticationKey];
    // --------------------------------------------------------------------------------------------------- Get Device Data ----------------------------------------------------------------------------------------------- //

    [self returnSubViews];
    
    // Another asyncronous call to allow for the instance methods to finish their job in getting information
    // Once the information is fetched ... the UILabel's text field is set to the respective text.
    dispatch_async(dispatch_get_main_queue(), ^{
        _SystemName.text = _uniqueSiteName;
        _PowerNow.text = [authData getNowValue];
        _siteAddress.text = [pulledData getSiteAddress];
        _SiteSystemSizeLabel.text = [NSString stringWithFormat: @"System Size: %@ kW", _systemSize];
        _PerformanceNow.text = [NSString stringWithFormat:@"%@%%", [pulledData getPerformanceNowPercentage]];
        
        
        if ([pulledData percentageChecker]) {
            _PerformanceIcon.image = [UIImage imageNamed:@"thumbs-up-icon-green2.png"];
        } else {
            _PerformanceIcon.image = [UIImage imageNamed:@"thumbsdown.png"];
        }
        
        if ([pulledData getSiteZipCode] == nil) {
            [[[UIAlertView alloc] initWithTitle:@"Cannot Receive Weather Data" message:@"Weather Data is currently Unavailable" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
        } else {
            _weather = [YWHelper getWeather:[[pulledData getSiteZipCode] intValue]];
            _WeatherNow.text = [NSString stringWithFormat:@"%d°", _weather.currentTemp];
            _WeatherLow.text = [NSString stringWithFormat:@"%d", _weather.todayLow];
            _WeatherHigh.text = [NSString stringWithFormat:@"%d", _weather.todayHigh];
            _WeatherIcon.image = [UIImage imageWithData:[YWHelper getWeatherIcon:_weather.currentCode]];
            sunset = [[NSString alloc] initWithString:_weather.sunSet];
            sunrise = [[NSString alloc] initWithString:_weather.sunRise];
            [WeatherViewController setsunRiseandSetTimes:[NSArray arrayWithObjects:_weather.sunRise, _weather.sunSet, nil]];
            [WeatherViewController setWindSpeedAndDirection:[NSArray arrayWithObjects:_weather.windDirection, _weather.windSpeed, nil]];
            [WeatherViewController setAtmosphereData:[NSArray arrayWithObjects:_weather.humidity,_weather.visibility, _weather.pressure, nil]];

        }
        
        [_SystemStatusIconButton setBackgroundImage:[pulledData getSystemStatusIcon] forState:UIControlStateNormal];
        if (![[pulledData getSystemStatus] isEqual: @"Normal"]) {
            _SystemStatusIconButton.userInteractionEnabled = YES;
            CustomBadge *customBadge1 = [CustomBadge customBadgeWithString:[NSString stringWithFormat:@"%i", [pulledData.systemStatusArrayAlerts count]] withStringColor:[UIColor whiteColor] withInsetColor:[UIColor redColor] withBadgeFrame:YES withBadgeFrameColor:[UIColor whiteColor] withScale:0.6 withShining:YES];
            [customBadge1 setFrame:CGRectMake(23,-5,15,15)];
            [_SystemStatusIconButton addSubview:customBadge1];
        } else if ([[pulledData getSystemStatus] isKindOfClass:[NSNull class]]) {
            [_SystemStatusIconButton setBackgroundImage:[UIImage imageNamed:@"info.png"] forState:UIControlStateNormal];
        }
        
        _EnvironmentalsView.getComputerSavings = [pulledData getComputerSavings];
        _EnvironmentalsView.getCarSavings = [pulledData getCarSavings];
        _EnvironmentalsView.getTVSavings = [pulledData getTVSavings];
        _EnvironmentalsView.getCO2Savings = [pulledData getCO2Savings];
        _EnvironmentalsView.getNOXSavings = [pulledData getNOXSavings];
        _EnvironmentalsView.getSO2Savings = [pulledData getSO2Savings];
        _EnvironmentalsView.getCarbonOffsetMetricTonsSavings = [pulledData getCarbonOffsetMetricTonsSavings];
        _EnvironmentalsView.getCarbonOffsetAcresSavings = [pulledData getCarbonOffsetAcresSavings];
        
    });
    
    [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
}






NSString *const FlickrAPIKey = @"a726ae456df8826108f88598b54b95bf";


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
        
    // Store incoming data into a string
    NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
    NSLog(@"CALLING:%@", jsonString);
    
    NSError *error = nil;
    // Create a dictionary from the JSON string
    NSDictionary *results = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    NSLog(@"%@", results);
    
    // Build an array from the dictionary for easy access to each entry
    NSArray *photos = [[results objectForKey:@"photos"] objectForKey:@"photo"];
    // Loop through each entry in the dictionary...
    
    NSMutableArray *mArray = [[NSMutableArray alloc] init];
    int i = 0;
    for (NSDictionary *photo in photos)
    {
        // Get title of the image
        NSString *title = [photo objectForKey:@"title"];
        
        // Save the title to the photo titles array
        [photoTitles addObject:(title.length > 0 ? title : @"Untitled")];
        
        // Build the URL to where the image is stored (see the Flickr API)
        // In the format http://farmX.static.flickr.com/server/id/secret
        // Notice the "_s" which requests a "small" image 75 x 75 pixels
        //NSString *photoURLString = [NSString stringWithFormat:@"http://farm%@.static.flickr.com/%@/%@_%@_s.jpg", [photo objectForKey:@"farm"], [photo objectForKey:@"server"], [photo objectForKey:@"id"], [photo objectForKey:@"secret"]];
        
        // The performance (scrolling) of the table will be much better if we
        // build an array of the image data here, and then add this data as
        // the cell.image value (see cellForRowAtIndexPath:)
        //[photoSmallImageData addObject:[NSData dataWithContentsOfURL:[NSURL URLWithString:photoURLString]]];
        
        // Build and save the URL to the large image so we can zoom
        // in on the image if requested
        NSString *photoURLString = [NSString stringWithFormat:@"http://farm%@.static.flickr.com/%@/%@_%@_b.jpg", [photo objectForKey:@"farm"], [photo objectForKey:@"server"], [photo objectForKey:@"id"], [photo objectForKey:@"secret"]];
        [photoURLsLargeImage addObject:[NSURL URLWithString:photoURLString]];
        
        UIImage *croppedImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:photoURLString]]];
        [mArray addObject:[FranksVC imageWithImage:croppedImage scaledToSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height)]];
        
        
        i++;
    }
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


-(void)searchFlickrPhotos:(NSString *)text
{
    // Build the string to call the Flickr API
    
    NSString *urlString = [NSString stringWithFormat:@"http://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=%@&tags=%@&per_page=15&format=json&nojsoncallback=1", FlickrAPIKey, text];
    
    // Create NSURL string from formatted string, by calling the Flickr API
    NSURL *url = [NSURL URLWithString:urlString];
    NSData *data = [[NSData alloc] initWithContentsOfURL:url];
    
    NSError *error;
    // Create a dictionary from the JSON string
    NSDictionary *results = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
    // Build an array from the dictionary for easy access to each entry
    NSArray *photos = [[results objectForKey:@"photos"] objectForKey:@"photo"];
    // Loop through each entry in the dictionary...
    
    NSMutableArray *mArray = [[NSMutableArray alloc] init];
    int i = 0;
    for (NSDictionary *photo in photos)
    {
        // Get title of the image
        NSString *title = [photo objectForKey:@"title"];
        
        // Save the title to the photo titles array
        [photoTitles addObject:(title.length > 0 ? title : @"Untitled")];
        
        // Build the URL to where the image is stored (see the Flickr API)
        // In the format http://farmX.static.flickr.com/server/id/secret
        // Notice the "_s" which requests a "small" image 75 x 75 pixels
        //NSString *photoURLString = [NSString stringWithFormat:@"http://farm%@.static.flickr.com/%@/%@_%@_s.jpg", [photo objectForKey:@"farm"], [photo objectForKey:@"server"], [photo objectForKey:@"id"], [photo objectForKey:@"secret"]];
        
        // The performance (scrolling) of the table will be much better if we
        // build an array of the image data here, and then add this data as
        // the cell.image value (see cellForRowAtIndexPath:)
        //[photoSmallImageData addObject:[NSData dataWithContentsOfURL:[NSURL URLWithString:photoURLString]]];
        
        // Build and save the URL to the large image so we can zoom
        // in on the image if requested
        NSString *photoURLString = [NSString stringWithFormat:@"http://farm%@.static.flickr.com/%@/%@_%@_b.jpg", [photo objectForKey:@"farm"], [photo objectForKey:@"server"], [photo objectForKey:@"id"], [photo objectForKey:@"secret"]];
        [photoURLsLargeImage addObject:[NSURL URLWithString:photoURLString]];
        
        UIImage *croppedImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:photoURLString]]];
        [mArray addObject:[FranksVC imageWithImage:croppedImage scaledToSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height)]];
        
        //NSLog(@"%@", [mArray objectAtIndex:i]);
        
        i++;
    }
    
    [WeatherViewController setImages:mArray];
    
}



- (void) makeCallbacks
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"makeCallbacks" object:loadMainView];
    
    //[self searchFlickrPhotos:@"Weather"];

    loadMainView = @"YES";

    // Instantiate and allocate memory to prepare for an alert popup if any connections fail
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Status" message:@"Some connections to your site are down. In result, some data will not present. Please pull to refresh to try again!" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
    
    // An asyncronous call on the main queue/thread of the application
    // Purpose: Used to allow the application to load successfully with allocated information
    // NSData objects are created to hold the JSON objects of the URLs (faster processing speeds
    // than storing as regular JSON objects and several class methods are called from "PullEasyViewData.h/.m"
    // to perform the methods implementation of parsing the JSON obejcts from NSData objects
    dispatch_async(kBgQueue, ^{



        
        
        // Briefing of whats going on
        // 1: First, an if statement is executed to check if the URL does contain DATA, then
        // 2: "pulled Data" is the object we are performing the current METHOD from on the main queue/thread
        // and we call a selector method "fetchEasyViewData" defined in pulledData whose object is the NSData
        // object defined on the previous line, we also wait until the method is done to move onto the next
        // line of code the compiler will execute
        
        // -------------------- Get JSON Object from easyView -------------------------- //
        NSData* easyViewNSData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://easyview.auroravision.net/easyview/services/gmi/summary/PlantEnergy.json?eids=%@&tz=US%%2FMountain&hasUsage=false&dateLabel=E+MMM+d%%2C+yyyy+h%%3Amm%%3Ass+a+z&locale=en&v=1.4.7", _entityID]]];
        //NSData* easyViewNSData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"asdkjalskdja"]];
        if (easyViewNSData == nil) {
            NSLog(@"EasyView Data is nil");
            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        } else {
            [pulledData performSelectorOnMainThread:@selector(fetchEasyViewData:) withObject:easyViewNSData waitUntilDone:YES];
            //[pulledData performSelectorInBackground:@selector(fetchEasyViewData:) withObject:easyViewNSData];
        }
        // -------------------- Get JSON Object from easyView -------------------------- //
        
        
        
        
        // -------------------- Get JSON Object from weatherStation -------------------------- //
        NSData* weatherStationNSData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://easyview.auroravision.net/easyview/services/gmi/summary/WeatherStation.json?eids=%@&fields=Irradiance%%2CCumIrradiance%%2CCellTemp%%2CAmbientTemp&locale=&tz=US%%2FMountain&v=1.4.7", _entityID]]];
        if (weatherStationNSData == nil) {
            NSLog(@"Weather Station Data is nil");
            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        } else {
            [pulledData performSelectorOnMainThread:@selector(fetchWeatherStationRequestData:) withObject:weatherStationNSData waitUntilDone:YES];
            //[pulledData performSelectorInBackground:@selector(fetchWeatherStationRequestData:) withObject:weatherStationNSData];
        }
        // -------------------- Get JSON Object from weatherStation -------------------------- //
        
        
        
        
        // -------------------- Get JSON Object from servicesSummary -------------------------- //
        NSData* servicesSummaryNSData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://easyview.auroravision.net/easyview/services/gmi/summary/PlantEnergy.json?eids=%@&tz=US%%2FMountain&hasUsage=false&dateLabel=E+MMM+d%%2C+yyyy+h%%3Amm%%3Ass+a+z&locale=en&v=1.4.7", _entityID]]];
        if (servicesSummaryNSData == nil) {
            NSLog(@"Services Summary Data is nil");
            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        } else {
            [pulledData performSelectorOnMainThread:@selector(fetchServicesSummaryRequestData:) withObject:servicesSummaryNSData waitUntilDone:YES];
            //[pulledData performSelectorInBackground:@selector(fetchServicesSummaryRequestData:) withObject:servicesSummaryNSData];
        }
        // -------------------- Get JSON Object from servicesSummary -------------------------- //
        
        
        
        
        // -------------------- Get JSON Object from servicesDocument -------------------------- //
        NSData* servicesDocumentNSData = [NSData dataWithContentsOfURL: [NSURL URLWithString: [NSString stringWithFormat:@"https://easyview.auroravision.net/easyview/services/document/%@/plant_layout_canvas_config?version=1&v=1.3.2&_=1370148563070", _entityID]]];
        //[alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        if (servicesDocumentNSData == nil) {
            NSLog(@"Services Documents Data is nil");
            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        } else {
            [pulledData performSelectorOnMainThread:@selector(fetchServicesDocumentRequestData:) withObject:servicesDocumentNSData waitUntilDone:YES];
            //[pulledData performSelectorInBackground:@selector(fetchServicesDocumentRequestData:) withObject:servicesDocumentNSData];
        }
        // -------------------- Get JSON Object from servicesDocument -------------------------- //
        
        
        
        
        // -------------------- Get JSON Object from easyViewStatus -------------------------- //
        NSData* easyViewStatusNSData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://easyview.auroravision.net/easyview/services/events/status/plant/easyview.json?plantEId=%@&tz=US%%2FMountain&v=1.4.7", _entityID]]];
        if (easyViewStatusNSData == nil) {
            NSLog(@"EasyView Status Data is nil");
            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        } else {
            [pulledData performSelectorOnMainThread:@selector(fetchEasyViewStatus:) withObject:easyViewStatusNSData waitUntilDone:YES];
            //[pulledData performSelectorInBackground:@selector(fetchEasyViewStatus:) withObject:easyViewStatusNSData];
        }
        // -------------------- Get JSON Object from easyViewStatus -------------------------- //
        

        
        // -------------------- Get JSON Object from hierarchyData -------------------------- //
        NSData* hierarchyNSData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://easyview.auroravision.net/easyview/services/gai/plant/hierarchy.json?entityId=%@&v=1.4.7", _entityID]]];
        if (hierarchyNSData == nil) {
            NSLog(@"Hierarchy Data is nil");
            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        } else {
            [pulledData performSelectorOnMainThread:@selector(fetchHierarchyRequestData:) withObject:hierarchyNSData waitUntilDone:YES];
            //[pulledData performSelectorInBackground:@selector(fetchHierarchyRequestData:) withObject:hierarchyNSData];
        }
        // -------------------- Get JSON Object from installDate -------------------------- //

        

        
        // -------------------- Get JSON Object from installDate -------------------------- //
        NSData* installDateNSData = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://easyview.auroravision.net/easyview/services/gai/plant.json?entityId=%@&v=1.3.2", _entityID]]];
        if (installDateNSData == nil) {
            NSLog(@"Install Date Data is nil");
            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        } else {
            [pulledData performSelectorOnMainThread:@selector(fetchInstallDateData:) withObject:installDateNSData waitUntilDone:YES];
            //[pulledData performSelectorInBackground:@selector(fetchInstallDateData:) withObject:installDateNSData];
        }
        // -------------------- Get JSON Object from installDate -------------------------- //
        
        
        // -------------------- Get JSON Object from hierarchyData -------------------------- //
        NSDate *now = [NSDate date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyyMMdd"];
        NSDate *tomorrow = [NSDate dateWithTimeIntervalSinceNow: (60.0f*60.0f*24.0f)];
        //NSDate *yesterday = [NSDate dateWithTimeIntervalSinceNow: -(60.0f*60.0f*24.0f)];
        //NSDate *lastweek = [NSDate dateWithTimeIntervalSinceNow: -(360.0f*60.0f*24.0f)];
        
        NSString *tomorrowDate = [dateFormat stringFromDate:tomorrow];
        NSString *todayDate = [dateFormat stringFromDate:now];
        //NSString *yesterdayDate = [dateFormat stringFromDate:yesterday];
        //NSString *lastweekDate = [dateFormat stringFromDate:lastweek];
        
        //NSLog(@"%@", todayDate);
        //NSLog(@"%@", tomorrowDate);
        //NSLog(@"%@", yesterdayDate);
        //NSLog(@"%@", lastweekDate);
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://easyview.auroravision.net/easyview/services/gmi/summary/GenerationPower.json?eids=1168868%%2C1168839%%2C1168810%%2C1169042%%2C1169071%%2C1168984%%2C1169013%%2C1168897%%2C1168926%%2C1168955&tz=US%%2FMountain&bins=true&start=%@&end=%@&range=1D&hasUsage=false&summary=false&binSize=Min15&v=1.5.3",todayDate, tomorrowDate]];
        
        NSData* inverterServicesNSData = [NSData dataWithContentsOfURL:url];
        if (inverterServicesNSData == nil) {
            NSLog(@"Inverter Services Data is nil");
            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        } else {
            [pulledData performSelectorOnMainThread:@selector(fetchInverterServicesSummaryData:) withObject:inverterServicesNSData waitUntilDone:YES];
            //[pulledData performSelectorInBackground:@selector(fetchInverterServicesSummaryData:) withObject:inverterServicesNSData];
        }
        // -------------------- Get JSON Object from InverterServices -------------------------- //
        
        
        
        // -------------------- Get JSON Object from InverterPositions -------------------------- //
        NSData* inverterPositionsNSData = [NSData dataWithContentsOfURL:[NSURL URLWithString: @"https://easyview.auroravision.net/easyview/services/meta/get/1167815.json?path=%2F1167815%2F&propertyName=inverterPosition&v=1.4.7"]];
        if (inverterPositionsNSData == nil) {
            NSLog(@"Inverter Positions Data is nil");
            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        } else {
            [pulledData performSelectorOnMainThread:@selector(fetchInverterPositions:) withObject:inverterPositionsNSData waitUntilDone:YES];
            //[pulledData performSelectorInBackground:@selector(fetchInverterPositions:) withObject:inverterPositionsNSData];
        }
        // -------------------- Get JSON Object from InverterPositions -------------------------- //
        
        
        
        
        // -------------------- Get JSON Object from WUnderground (hourly Forecast) -------------------------- //
        if ([[setRegionAndCity objectAtIndex:0] isKindOfClass:[NSNull class]] || [[setRegionAndCity objectAtIndex:1] isKindOfClass:[NSNull class]]){
            NSLog(@"Failed to receive Hierarchy Data");
        } else {
            NSURL *weatherURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.wunderground.com/api/9715df1732b90be6/hourly/q/%@/%@.json", [setRegionAndCity objectAtIndex:0], [setRegionAndCity objectAtIndex:1]]];
            NSData* weatherDataURL = [NSData dataWithContentsOfURL:weatherURL];
            if (weatherDataURL == nil) {
                NSLog(@"Hourly Forecast Data is nil");
            } else {
                //[pulledData performSelectorInBackground:@selector(fetchHourlyForecast:) withObject:weatherDataURL];
                [pulledData performSelectorOnMainThread:@selector(fetchHourlyForecast:) withObject:weatherDataURL waitUntilDone:YES];
            }
        }
        // -------------------- Get JSON Object from WUnderground (hourly Forecast) -------------------------- //
        
        
        
        /*
        // -------------------- Get JSON Object from WUnderground (Astronomy Forecast) -------------------------- //
        if ([[setRegionAndCity objectAtIndex:0] isKindOfClass:[NSNull class]] || [[setRegionAndCity objectAtIndex:1] isKindOfClass:[NSNull class]]){
            NSLog(@"Failed to receive Hierarchy Data");
        } else {
            NSURL *astonomyURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.wunderground.com/api/9715df1732b90be6/astronomy/q/%@/%@.json", [setRegionAndCity objectAtIndex:0], [setRegionAndCity objectAtIndex:1]]];            
            NSData* astronomyData = [NSData dataWithContentsOfURL:astonomyURL];
            if (astronomyData == nil) {
                NSLog(@"Forecast Astronomy Data is nil");
            } else {
                [pulledData performSelectorInBackground:@selector(fetchSunriseAndSunsetTimes:) withObject:astronomyData];
                //[pulledData performSelectorOnMainThread:@selector(fetchSunriseAndSunsetTimes:) withObject:astronomyData waitUntilDone:YES];
            }
        }
        // -------------------- Get JSON Object from WUnderground (Astronomy Forecast) -------------------------- //
        
        */
        
        
        // -------------------- Get JSON Object from WUnderground (Full Forecast) -------------------------- //
        if ([[setRegionAndCity objectAtIndex:0] isKindOfClass:[NSNull class]] || [[setRegionAndCity objectAtIndex:1] isKindOfClass:[NSNull class]]){
            NSLog(@"Failed to receive Hierarchy Data");
        } else {
            NSURL *FullForecastURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.wunderground.com/api/9715df1732b90be6/forecast/q/%@/%@.json", [setRegionAndCity objectAtIndex:0], [setRegionAndCity objectAtIndex:1]]];
            NSData* fullForecastData = [NSData dataWithContentsOfURL:FullForecastURL];
            if (fullForecastData == nil) {
                NSLog(@"Full Forecast Data is nil");
            } else {
                //[pulledData performSelectorInBackground:@selector(fetchFullForeCast:) withObject:fullForecastData];
                [pulledData performSelectorOnMainThread:@selector(fetchFullForeCast:) withObject:fullForecastData waitUntilDone:YES];
            }
        }
        // -------------------- Get JSON Object from WUnderground (Full Forecast) -------------------------- //
        
        
        
        
        // -------------------- Get JSON Object from WUnderground (conditions Forecast) -------------------------- //
        if ([[setRegionAndCity objectAtIndex:0] isKindOfClass:[NSNull class]] || [[setRegionAndCity objectAtIndex:1] isKindOfClass:[NSNull class]]){
            NSLog(@"Failed to receive Hierarchy Data");
        } else {
            NSURL *conditionsURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.wunderground.com/api/9715df1732b90be6/conditions/q/%@/%@.json", [setRegionAndCity objectAtIndex:0], [setRegionAndCity objectAtIndex:1]]];
            NSData* conditionsData = [NSData dataWithContentsOfURL:conditionsURL];
            if (conditionsData == nil) {
                NSLog(@"Forecast Conditions Data is nil");
            } else {
                //[pulledData performSelectorInBackground:@selector(fetchForecastConditions:) withObject:conditionsData];
                [pulledData performSelectorOnMainThread:@selector(fetchForecastConditions:) withObject:conditionsData waitUntilDone:YES];
            }
        }
        // -------------------- Get JSON Object from WUnderground (conditions Forecast) -------------------------- //
        
        
        
        
        // -------------------- Get JSON Object from EIA (Energy Costs) -------------------------- //
        NSURL *energyCostsURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.eia.gov/series/data/?api_key=FF691F5BE67A113AC55F3454BAD7F59F&series_id=ELEC.PRICE.%@-RES.M", [setRegionAndCity objectAtIndex:0]]];
        NSData* energyCostsData = [NSData dataWithContentsOfURL:energyCostsURL];
        if (energyCostsData == nil) {
            NSLog(@"Energy Costs Data is nil");
        } else {
            [pulledData performSelectorOnMainThread:@selector(fetchEnergyCosts:) withObject:energyCostsData waitUntilDone:YES];
            //[pulledData performSelectorInBackground:@selector(fetchEnergyCosts:) withObject:energyCostsData];
        }

        // -------------------- Get JSON Object from EIA (Energy Costs) -------------------------- //
        
        [EnvironmentalsSubView setArrayOne:[[NSArray alloc] initWithObjects:[pulledData getTVSavings], [pulledData getComputerSavings], nil]];
        [EnvironmentalsSubView setArrayTwo:[[NSArray alloc] initWithObjects:[pulledData getCarSavings] , [pulledData getCO2Savings], [pulledData getNOXSavings], [pulledData getSO2Savings], [pulledData getCarbonOffsetAcresSavings], [pulledData getCarbonOffsetMetricTonsSavings], nil]];
        
        
        [self returnSubViews];

        // Another asyncronous call to allow for the instance methods to finish their job in getting information
        // Once the information is fetched ... the UILabel's text field is set to the respective text.
        dispatch_async(dispatch_get_main_queue(), ^{
            _SystemName.text = [pulledData getEntityName];
            _PowerNow.text = [pulledData getNowValue];
            _siteAddress.text = [pulledData getSiteAddress];
            _SiteSystemSizeLabel.text = [NSString stringWithFormat: @"System Size: %@ kW", [pulledData getSystemSizeValue]];
            _PerformanceNow.text = [NSString stringWithFormat:@"%@%%", [pulledData getPerformanceNowPercentage]];
            
            
            if ([pulledData percentageChecker]) {
                _PerformanceIcon.image = [UIImage imageNamed:@"thumbs-up-icon-green2.png"];
            } else {
                _PerformanceIcon.image = [UIImage imageNamed:@"thumbsdown.png"];
            }
            
            if ([pulledData getSiteZipCode] == nil) {
                [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
            } else {
                _weather = [YWHelper getWeather:[[pulledData getSiteZipCode] intValue]];
                _WeatherNow.text = [NSString stringWithFormat:@"%d°", _weather.currentTemp];
                _WeatherLow.text = [NSString stringWithFormat:@"%d", _weather.todayLow];
                _WeatherHigh.text = [NSString stringWithFormat:@"%d", _weather.todayHigh];
                _WeatherIcon.image = [UIImage imageWithData:[YWHelper getWeatherIcon:_weather.currentCode]];
                [DayGraphViewController getTimes:[NSArray arrayWithObjects:_weather.sunRise, _weather.sunSet, nil]];
                
                [WeatherViewController setsunRiseandSetTimes:[NSArray arrayWithObjects:_weather.sunRise, _weather.sunSet, nil]];
                [WeatherViewController setWindSpeedAndDirection:[NSArray arrayWithObjects:_weather.windDirection, _weather.windSpeed, nil]];
                [WeatherViewController setAtmosphereData:[NSArray arrayWithObjects:_weather.humidity,_weather.visibility, _weather.pressure, nil]];
                //view.sunriseTimeString = [pulledData getSunriseTime];
                //view.sunsetTimeString = [pulledData getSunsetTime];
                //NSLog(@"%@ : %@", _weather.sunRise, _weather.sunSet);
            }
                        
            [_SystemStatusIconButton setBackgroundImage:[pulledData getSystemStatusIcon] forState:UIControlStateNormal];
            //NSLog(@"%@", [pulledData getSystemStatus]);
            if (![[pulledData getSystemStatus] isEqual: @"Normal"]) {
                _SystemStatusIconButton.userInteractionEnabled = YES;
                CustomBadge *customBadge1 = [CustomBadge customBadgeWithString:[NSString stringWithFormat:@"%i", [pulledData.systemStatusArrayAlerts count]] withStringColor:[UIColor whiteColor] withInsetColor:[UIColor redColor] withBadgeFrame:YES withBadgeFrameColor:[UIColor whiteColor] withScale:0.6 withShining:YES];
                [customBadge1 setFrame:CGRectMake(23,-5,15,15)];
                [_SystemStatusIconButton addSubview:customBadge1];
            } else if ([[pulledData getSystemStatus] isKindOfClass:[NSNull class]]) {
                [_SystemStatusIconButton setBackgroundImage:[UIImage imageNamed:@"info.png"] forState:UIControlStateNormal];
            }
            
            _EnvironmentalsView.getComputerSavings = [pulledData getComputerSavings];
            _EnvironmentalsView.getCarSavings = [pulledData getCarSavings];
            _EnvironmentalsView.getTVSavings = [pulledData getTVSavings];
            _EnvironmentalsView.getCO2Savings = [pulledData getCO2Savings];
            _EnvironmentalsView.getNOXSavings = [pulledData getNOXSavings];
            _EnvironmentalsView.getSO2Savings = [pulledData getSO2Savings];
            _EnvironmentalsView.getCarbonOffsetMetricTonsSavings = [pulledData getCarbonOffsetMetricTonsSavings];
            _EnvironmentalsView.getCarbonOffsetAcresSavings = [pulledData getCarbonOffsetAcresSavings];
            
        });

        [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        
    });


}






-(BOOL)reachable {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}

- (void) Events
{
    NotificationsViewController *view = [[NotificationsViewController alloc] init];
    view.title = @"Events";
    [self.navigationController pushViewController:view animated:YES];
}

- (void) PowerNowController
{
    PowerViewController *view = [[PowerViewController alloc] initWithNibName:@"PowerViewController" bundle:nil];
    view.title = @"Power Now";
    
    view.powerLifetimeValue = [pulledData getLifeValue];
    view.powerMonthValue = [pulledData getMonthValue];
    view.powerWeekValue = [pulledData getWeekValue];
    view.powerTodayValue = [pulledData getTodayValue];
    
    view.costPerKWH = [pulledData getEnergyCosts];
    view.costPerKWHDate = [NSString stringWithFormat:@"%@", [pulledData getEnergyCostsDate]];
    
    [UIView  transitionWithView:self.navigationController.view duration:0.8  options:UIViewAnimationOptionTransitionFlipFromRight
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         [self.navigationController pushViewController:view animated:YES];
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
    
    
    
    //[self.navigationController pushViewController:view animated:YES];
}

- (void)PowerNowPopUpView:(id)sender{
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 200)];
    
    NSNumberFormatter *nFormatter = [[NSNumberFormatter alloc] init];
    [nFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [nFormatter setRoundingMode:NSNumberFormatterRoundHalfUp];
    [nFormatter setMaximumFractionDigits:2];
    [nFormatter setMinimumFractionDigits:2];
    
    NSNumber *number = [[NSNumber alloc] init];
    NSString *formattedNumber = [[NSString alloc] init];
    
    CGRect welcomeLabelRect = contentView.bounds;
    welcomeLabelRect.origin.y = 20;
    welcomeLabelRect.size.height = 20;
    UIFont *welcomeLabelFont = [UIFont boldSystemFontOfSize:14];
    UILabel *welcomeLabel = [[UILabel alloc] initWithFrame:welcomeLabelRect];
    welcomeLabel.text = @"kWh Accumulated : Money Saved";
    welcomeLabel.font = welcomeLabelFont;
    welcomeLabel.textColor = [UIColor whiteColor];
    welcomeLabel.textAlignment = NSTextAlignmentCenter;
    welcomeLabel.backgroundColor = [UIColor clearColor];
    welcomeLabel.shadowColor = [UIColor blackColor];
    welcomeLabel.shadowOffset = CGSizeMake(0, 1);
    [contentView addSubview:welcomeLabel];
    
    CGRect todayInfoLabelRect = CGRectInset(contentView.bounds, 5, 5);
    todayInfoLabelRect.origin.y = CGRectGetMaxY(welcomeLabelRect)-100;
    todayInfoLabelRect.size.height -= CGRectGetMinY(todayInfoLabelRect);
    UILabel *todayInfoLabel = [[UILabel alloc] initWithFrame:todayInfoLabelRect];
    number = [NSNumber numberWithFloat:([[pulledData getEnergyCosts] floatValue] * .01) * ([[pulledData getTodayValue] floatValue])];
    formattedNumber = [nFormatter stringFromNumber:number];
    //todayInfoLabel.text = [NSString stringWithFormat:@"Today: %@ kWh : $%@", [authData getTodayValue], formattedNumber];
    todayInfoLabel.text = [NSString stringWithFormat:@"Today: %@ kWh : $%@", [pulledData getTodayValue], formattedNumber];
    todayInfoLabel.font = [UIFont systemFontOfSize:12.0];
    todayInfoLabel.numberOfLines = 6;
    todayInfoLabel.textColor = [UIColor whiteColor];
    todayInfoLabel.textAlignment = NSTextAlignmentCenter;
    todayInfoLabel.backgroundColor = [UIColor clearColor];
    todayInfoLabel.shadowColor = [UIColor blackColor];
    todayInfoLabel.shadowOffset = CGSizeMake(0, 1);
    [contentView addSubview:todayInfoLabel];
    
    
    CGRect weekInfoLabelRect = CGRectInset(contentView.bounds, 5, 5);
    weekInfoLabelRect.origin.y = CGRectGetMaxY(welcomeLabelRect)-50;
    weekInfoLabelRect.size.height -= CGRectGetMinY(weekInfoLabelRect);
    UILabel *weekInfoLabel = [[UILabel alloc] initWithFrame:weekInfoLabelRect];
    number = [NSNumber numberWithFloat:([[pulledData getEnergyCosts] floatValue] * .01) * ([[pulledData getWeekValue] floatValue])];
    formattedNumber = [nFormatter stringFromNumber:number];
    //weekInfoLabel.text = [NSString stringWithFormat:@"Week: %@ kWh : $%@", [authData getWeekValue], formattedNumber];
    weekInfoLabel.text = [NSString stringWithFormat:@"Week: %@ kWh : $%@", [pulledData getWeekValue], formattedNumber];
    weekInfoLabel.font = [UIFont systemFontOfSize:12.0];
    weekInfoLabel.numberOfLines = 6;
    weekInfoLabel.textColor = [UIColor whiteColor];
    weekInfoLabel.textAlignment = NSTextAlignmentCenter;
    weekInfoLabel.backgroundColor = [UIColor clearColor];
    weekInfoLabel.shadowColor = [UIColor blackColor];
    weekInfoLabel.shadowOffset = CGSizeMake(0, 1);
    [contentView addSubview:weekInfoLabel];
    
    CGRect monthInfoLabelRect = CGRectInset(contentView.bounds, 5, 5);
    monthInfoLabelRect.origin.y = CGRectGetMaxY(welcomeLabelRect)+0;
    monthInfoLabelRect.size.height -= CGRectGetMinY(monthInfoLabelRect);
    UILabel *monthInfoLabel = [[UILabel alloc] initWithFrame:monthInfoLabelRect];
    number = [NSNumber numberWithFloat:([[pulledData getEnergyCosts] floatValue] * .01) * ([[pulledData getMonthValue] floatValue])];
    formattedNumber = [nFormatter stringFromNumber:number];
    //monthInfoLabel.text = [NSString stringWithFormat:@"Month: %@ kWh : $%@", [authData getMonthValue], formattedNumber];
    monthInfoLabel.text = [NSString stringWithFormat:@"Month: %@ kWh : $%@", [pulledData getMonthValue], formattedNumber];
    monthInfoLabel.font = [UIFont systemFontOfSize:12.0];
    monthInfoLabel.numberOfLines = 6;
    monthInfoLabel.textColor = [UIColor whiteColor];
    monthInfoLabel.textAlignment = NSTextAlignmentCenter;
    monthInfoLabel.backgroundColor = [UIColor clearColor];
    monthInfoLabel.shadowColor = [UIColor blackColor];
    monthInfoLabel.shadowOffset = CGSizeMake(0, 1);
    [contentView addSubview:monthInfoLabel];
    
    NSString *newString = [[pulledData getLifeValue] stringByReplacingOccurrencesOfString:@"," withString:@""];
    
    CGRect lifetimeInfoLabelRect = CGRectInset(contentView.bounds, 5, 5);
    lifetimeInfoLabelRect.origin.y = CGRectGetMaxY(welcomeLabelRect)+50;
    lifetimeInfoLabelRect.size.height -= CGRectGetMinY(lifetimeInfoLabelRect);
    UILabel *lifetimeInfoLabel = [[UILabel alloc] initWithFrame:lifetimeInfoLabelRect];
    number = [NSNumber numberWithFloat:([[pulledData getEnergyCosts] floatValue] * .01) * ([newString floatValue])];
    formattedNumber = [nFormatter stringFromNumber:number];
    //lifetimeInfoLabel.text = [NSString stringWithFormat:@"Lifetime: %@ kWh : $%@", [authData getLifeTimeValue], formattedNumber];
    lifetimeInfoLabel.text = [NSString stringWithFormat:@"Lifetime: %@ kWh : $%@", [pulledData getLifeValue], formattedNumber];
    lifetimeInfoLabel.font = [UIFont systemFontOfSize:12.0];
    lifetimeInfoLabel.numberOfLines = 6;
    lifetimeInfoLabel.textColor = [UIColor whiteColor];
    lifetimeInfoLabel.textAlignment = NSTextAlignmentCenter;
    lifetimeInfoLabel.backgroundColor = [UIColor clearColor];
    lifetimeInfoLabel.shadowColor = [UIColor blackColor];
    lifetimeInfoLabel.shadowOffset = CGSizeMake(0, 1);
    [contentView addSubview:lifetimeInfoLabel];
    
    CGRect astriskLabelRect = CGRectInset(contentView.bounds, 5, 5);
    astriskLabelRect.origin.y = CGRectGetMaxY(welcomeLabelRect)+110;
    astriskLabelRect.size.height -= CGRectGetMinY(astriskLabelRect);
    UILabel *astriskLabel = [[UILabel alloc] initWithFrame:astriskLabelRect];
    astriskLabel.text = @"* The cost per kiloWatt/hour is assumed to be at the lowest Tier\nconsumption. If you want the saved amount to be as accurate as possible,\nplease enter your monthly electricity bill in account settings.";
    astriskLabel.font = [UIFont systemFontOfSize:8.0];
    astriskLabel.numberOfLines = 4;
    astriskLabel.textColor = [UIColor whiteColor];
    astriskLabel.textAlignment = NSTextAlignmentCenter;
    astriskLabel.backgroundColor = [UIColor clearColor];
    astriskLabel.shadowColor = [UIColor blackColor];
    astriskLabel.shadowOffset = CGSizeMake(0, 1);
    [contentView addSubview:astriskLabel];

    
    [[KGModal sharedInstance] showWithContentView:contentView andAnimated:YES];
}

- (void) PerformanceNowController
{
    PerformanceViewController *view = [[PerformanceViewController alloc] initWithNibName:@"PerformanceViewController" bundle:nil];
    //view.title = @"Performance Now";
    view.inverterValuesAndNames = [pulledData getInverterNamesAndValues];
    view.inverterDates = [pulledData getInverterDates];
    //[self.navigationController pushViewController:view animated:YES];
    
    [UIView  transitionWithView:self.navigationController.view duration:0.4  options:UIViewAnimationOptionTransitionFlipFromRight
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         [self.navigationController pushViewController:view animated:YES];
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
}



- (void) WeatherNowController
{
    WeatherViewController *view = [[WeatherViewController alloc] initWithNibName:@"WeatherViewController" bundle:nil];
    //view.title = @"Weather Now";
    view.zipCode = [pulledData getSiteZipCode];
    view.city = [pulledData getSiteCity];
    view.state = [pulledData getSiteRegionCode];
    
    //view.IrradianceValueText = [authData getIrradiance];
    //view.CellTempValueText = [authData getCellTemp];
    //view.AmbientValueText = [authData getAmbientTemp];
    //view.InsolationValueText  = [authData getInsolation];
    view.IrradianceValueText = [pulledData getIrradiance];
    view.CellTempValueText = [pulledData getCellTemp];
    view.AmbientValueText = [pulledData getAmbientTemp];
    view.InsolationValueText  = [pulledData getInsolation];
    
    view.times = [pulledData getHourlyTimes];
    view.temps = [pulledData getHourlyTemps];
    view.icons = [pulledData getHourlyIcons];
    view.feelsLikeString = [pulledData getFeelsLike];
    view.tonightDescriptionString = [pulledData tonightDetails];
    view.todayDescriptionString = [pulledData todayDetails];
    view.todayDetailIcon = [pulledData currentIcon];
    
    
    [UIView  transitionWithView:self.navigationController.view duration:0.4  options:UIViewAnimationOptionTransitionFlipFromLeft
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         [self.navigationController pushViewController:view animated:YES];
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
}

- (void) pushToAlerts
{
    NotificationsViewController *view = [[NotificationsViewController alloc] initWithNibName:@"NotificationsViewController" bundle:nil];
    [self.navigationController pushViewController:view animated:YES];
}

- (void)share:(UIButton *)share
{
    SharingActivityProvider *sharing = [[SharingActivityProvider alloc] init];
    
    sharing.EntityName = [pulledData getEntityName];
    sharing.CarSavings = [pulledData getCarSavings];
    sharing.ComputerSavings = [pulledData getComputerSavings];
    sharing.TVSavings = [pulledData getTVSavings];
    sharing.CO2Savings = [pulledData getCO2Savings];
    sharing.NOXSavings = [pulledData getNOXSavings];
    sharing.SO2Savings = [pulledData getSO2Savings];
    sharing.CarbonOffsetAcresSavings = [pulledData getCarbonOffsetAcresSavings];
    sharing.CarbonOffsetMetricTonsSavings = [pulledData getCarbonOffsetMetricTonsSavings];
    
    NSArray *array = @[sharing];
    
    UIActivityViewController *activity = [[UIActivityViewController alloc] initWithActivityItems:array applicationActivities:nil];
    
    activity.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePostToWeibo];
    
    activity.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:activity animated:YES completion:nil];
    
}

- (void) returnSubViews
{
    // ------------------------- Add SubViews ------------------------------//
    [scroller addSubview:[self returnSystemInfoView]];
    [scroller addSubview:[self returnPowerNowView]];
    [scroller addSubview:[self returnPerformanceNowView]];
    [scroller addSubview:[self returnWeatherNowView]];
    
    // Crashes OCCASIONALLY when internet fails
    // Error is given with Hex code ---- Assembly BULLSHIT----//
    [scroller addSubview:[self returnSliderGraphsView]];
    // Crashes OCCASIONALLY when internet fails
    
    
    [scroller addSubview:[self returnEnvironmentalsView]];
    [scroller addSubview:[self returnEnvironmentalsGradientLabel]];
    [scroller addSubview:[self returnFooterView]];
    // ------------------------- Add SubViews ------------------------------//
}

- (UIView *) returnSystemInfoView
{
    _SystemInfoView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y , self.view.frame.size.width, 40)];
    _SystemInfoView.backgroundColor = [UIColor clearColor];
    [scroller addSubview:_SystemInfoView];
    
    _SystemName = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 265, 25)];
    _SystemName.backgroundColor = [UIColor clearColor];
    _SystemName.textColor = [UIColor whiteColor];
    [_SystemName setFont:[UIFont fontWithName:@"Arial-BoldMT" size:20.0f]];
    [_SystemInfoView addSubview:_SystemName];
    
    _SystemStatusIconButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _SystemStatusIconButton.frame = CGRectMake(275, 5, 35, 30);
    _SystemStatusIconButton.backgroundColor = [UIColor clearColor];
    [_SystemStatusIconButton addTarget:self action:@selector(Events) forControlEvents:UIControlEventTouchUpInside];
    _SystemStatusIconButton.userInteractionEnabled = NO;
    [_SystemInfoView addSubview:_SystemStatusIconButton];

    return _SystemInfoView;
}

- (UIButton *) returnPowerNowView
{
    _PowerNowView = [UIButton buttonWithType:UIButtonTypeCustom];
    _PowerNowView.frame = CGRectMake(5, 40, 100, 80);
    _PowerNowView.backgroundColor = [UIColor blackColor];
    _PowerNowView.layer.borderColor = [[UIColor grayColor] CGColor];
    _PowerNowView.layer.borderWidth = 1.0f;
    _PowerNowView.layer.cornerRadius = 10.0f;
    [_PowerNowView setBackgroundImage:[UIImage imageNamed:@"1280x -Slate Gradient.jpg"] forState:UIControlStateNormal];
    _PowerNowView.layer.masksToBounds = YES;
    [_PowerNowView addTarget:self action:@selector(PowerNowPopUpView:) forControlEvents:UIControlEventTouchUpInside];
    
    _StaticPowerNowLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
    _StaticPowerNowLabel.text = @"POWER NOW";
    _StaticPowerNowLabel.backgroundColor = [UIColor clearColor];
    _StaticPowerNowLabel.textAlignment = NSTextAlignmentCenter;
    [_StaticPowerNowLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:8.0f]];
    _StaticPowerNowLabel.textColor = [UIColor grayColor];
    [_PowerNowView addSubview:_StaticPowerNowLabel];
    
    _PowerNow = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 100, 40)];
    _PowerNow.backgroundColor = [UIColor clearColor];
    _PowerNow.textAlignment = NSTextAlignmentCenter;
    [_PowerNow setFont:[UIFont fontWithName:@"Arial-BoldMT" size:20.0f]];
    _PowerNow.textColor = [UIColor whiteColor];
    [_PowerNowView addSubview:_PowerNow];
    
    _PowerNowUnits = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, 100, 10)];
    _PowerNowUnits.text = @"kW";
    _PowerNowUnits.backgroundColor = [UIColor clearColor];
    _PowerNowUnits.textAlignment = NSTextAlignmentCenter;
    [_PowerNowUnits setFont:[UIFont fontWithName:@"Arial-BoldMT" size:7.0f]];
    _PowerNowUnits.textColor = [UIColor grayColor];
    [_PowerNowView addSubview:_PowerNowUnits];
    
    
    _MeterBar = [[UIImageView alloc] initWithFrame:CGRectMake(25, 40, 50, 40)];
    _MeterBar.image = [UIImage imageNamed:@"battery-empty-icon.png"];
    _MeterBar.backgroundColor = [UIColor clearColor];
    [_PowerNowView addSubview:_MeterBar];

    dispatch_async(dispatch_get_main_queue(), ^{
        UIImageView *newBar = [[UIImageView alloc] initWithFrame:CGRectMake(10, 61, 38, 14)];
        newBar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"michigan_vacation_rentals_green_bar-997x301.jpg"]];
        newBar.layer.borderColor = [[UIColor clearColor] CGColor];
        newBar.layer.borderWidth = 0.0f;
        newBar.layer.cornerRadius = 2.0f;
        CABasicAnimation *scaleToValue = [CABasicAnimation animationWithKeyPath:@"transform.scale.x"];
        scaleToValue.fromValue = [NSNumber numberWithFloat:0.0f];
        scaleToValue.duration = 3.0f;
        scaleToValue.delegate = self;
        newBar.layer.anchorPoint = CGPointMake(0.0, 1.0);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //NSLog(@"Real Guage Value: %f", [pulledData getRealGuageValue]);
            scaleToValue.toValue = [NSNumber numberWithFloat:[pulledData getRealGuageValue]];
            [newBar.layer addAnimation:scaleToValue forKey:@"scaleRight"];
            CGAffineTransform scaleTo = CGAffineTransformMakeScale([pulledData getRealGuageValue], 1.0f );
            newBar.transform = scaleTo;
            [_PowerNowView addSubview:newBar];
        });
    });
        
    

    

    return _PowerNowView;
}

- (UIButton *) returnPerformanceNowView
{
    _PerformanceNowView = [UIButton buttonWithType:UIButtonTypeCustom];
    _PerformanceNowView.frame = CGRectMake(110, 40, 100, 80);
    _PerformanceNowView.backgroundColor = [UIColor blackColor];
    _PerformanceNowView.layer.borderColor = [[UIColor grayColor] CGColor];
    _PerformanceNowView.layer.borderWidth = 1.0f;
    _PerformanceNowView.layer.cornerRadius = 10.0f;
    [_PerformanceNowView setBackgroundImage:[UIImage imageNamed:@"1280x -Slate Gradient.jpg"] forState:UIControlStateNormal];
    _PerformanceNowView.layer.masksToBounds = YES;
    [_PerformanceNowView addTarget:self action:@selector(PerformanceNowController) forControlEvents:UIControlEventTouchUpInside];
    
    _StaticPerformanceNowLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
    _StaticPerformanceNowLabel.text = @"PERFORMANCE NOW";
    _StaticPerformanceNowLabel.backgroundColor = [UIColor clearColor];
    _StaticPerformanceNowLabel.textAlignment = NSTextAlignmentCenter;
    [_StaticPerformanceNowLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:8.0f]];
    _StaticPerformanceNowLabel.textColor = [UIColor grayColor];
    [_PerformanceNowView addSubview:_StaticPerformanceNowLabel];
    
    _PerformanceNow = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 100, 40)];
    _PerformanceNow.backgroundColor = [UIColor clearColor];
    _PerformanceNow.textAlignment = NSTextAlignmentCenter;
    [_PerformanceNow setFont:[UIFont fontWithName:@"Arial-BoldMT" size:20.0f]];
    _PerformanceNow.textColor = [UIColor whiteColor];
    [_PerformanceNowView addSubview:_PerformanceNow];
    
    _PerformanceIcon = [[UIImageView alloc] initWithFrame:CGRectMake(38, 50, 25, 25)];
    _PerformanceIcon.backgroundColor = [UIColor clearColor];
    [_PerformanceNowView addSubview:_PerformanceIcon];

    return _PerformanceNowView;
}

- (UIButton *) returnWeatherNowView
{
    _WeatherNowView = [UIButton buttonWithType:UIButtonTypeCustom];
    _WeatherNowView.frame = CGRectMake(215, 40, 100, 80);
    _WeatherNowView.backgroundColor = [UIColor blackColor];
    _WeatherNowView.layer.borderColor = [[UIColor grayColor] CGColor];
    _WeatherNowView.layer.borderWidth = 1.0f;
    _WeatherNowView.layer.cornerRadius = 10.0f;
    [_WeatherNowView setBackgroundImage:[UIImage imageNamed:@"1280x -Slate Gradient.jpg"] forState:UIControlStateNormal];
    _WeatherNowView.layer.masksToBounds = YES;
    [_WeatherNowView addTarget:self action:@selector(WeatherNowController) forControlEvents:UIControlEventTouchUpInside];
    
    _StaticWeatherNowLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
    _StaticWeatherNowLabel.text = @"WEATHER";
    _StaticWeatherNowLabel.backgroundColor = [UIColor clearColor];
    _StaticWeatherNowLabel.textAlignment = NSTextAlignmentCenter;
    [_StaticWeatherNowLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:8.0f]];
    _StaticWeatherNowLabel.textColor = [UIColor grayColor];
    [_WeatherNowView addSubview:_StaticWeatherNowLabel];
    
    _WeatherNow = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 100, 40)];
    _WeatherNow.backgroundColor = [UIColor clearColor];
    _WeatherNow.textAlignment = NSTextAlignmentCenter;
    [_WeatherNow setFont:[UIFont fontWithName:@"Arial-BoldMT" size:20.0f]];
    _WeatherNow.textColor = [UIColor whiteColor];
    [_WeatherNowView addSubview:_WeatherNow];
    
    _WeatherLow = [[UILabel alloc] initWithFrame:CGRectMake(5, 25, 95, 10)];
    _WeatherLow.backgroundColor = [UIColor clearColor];
    _WeatherLow.textAlignment = NSTextAlignmentLeft;
    [_WeatherLow setFont:[UIFont fontWithName:@"Arial" size:10.0f]];
    _WeatherLow.textColor = [UIColor whiteColor];
    [_WeatherNowView addSubview:_WeatherLow];
    
    _WeatherHigh = [[UILabel alloc] initWithFrame:CGRectMake(5, 25, 90, 10)];
    _WeatherHigh.backgroundColor = [UIColor clearColor];
    _WeatherHigh.textAlignment = NSTextAlignmentRight;
    [_WeatherHigh setFont:[UIFont fontWithName:@"Arial" size:10.0f]];
    _WeatherHigh.textColor = [UIColor whiteColor];
    [_WeatherNowView addSubview:_WeatherHigh];
    
    _WeatherIcon = [[UIImageView alloc] initWithFrame:CGRectMake(25, 45, 50, 30)];
    _WeatherIcon.backgroundColor = [UIColor clearColor];
    [_WeatherNowView addSubview:_WeatherIcon];
    
    return _WeatherNowView;
}

- (SliderGraphsView *) returnSliderGraphsView
{
    _sliderGraphsView = [[SliderGraphsView alloc] initWithFrame:CGRectMake(5, 125, 310, 170)];
    _sliderGraphsView.authToken = setAuthenticationKey;
    _sliderGraphsView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"1280x -Slate Gradient.jpg"]];
    _sliderGraphsView.layer.borderColor = [[UIColor grayColor] CGColor];
    _sliderGraphsView.layer.borderWidth = 1.0f;
    _sliderGraphsView.layer.cornerRadius = 10.0f;
    _sliderGraphsView.layer.masksToBounds = YES;
    
    return _sliderGraphsView;
}



- (UIView *) returnEnvironmentalsGradientLabel
{
    _EnvironmentalsGradientLabel = [[UIView alloc] initWithFrame:CGRectMake(5, 300, 310, 30)];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = _EnvironmentalsGradientLabel.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor darkGrayColor]CGColor], (id)[[UIColor blackColor]CGColor], nil];
    [_EnvironmentalsGradientLabel.layer insertSublayer:gradient atIndex:0];
    [scroller addSubview:_EnvironmentalsGradientLabel];
    
    UILabel *EnvironmentalsLabelText = [[UILabel alloc] initWithFrame:_EnvironmentalsGradientLabel.bounds];
    [EnvironmentalsLabelText setFont:[UIFont fontWithName:@"Arial-BoldMT" size:12.0f]];
    EnvironmentalsLabelText.textAlignment = NSTextAlignmentCenter;
    EnvironmentalsLabelText.backgroundColor = [UIColor clearColor];
    EnvironmentalsLabelText.textColor = [UIColor lightGrayColor];
    EnvironmentalsLabelText.text = @"Environmental Benefits";
    [_EnvironmentalsGradientLabel addSubview:EnvironmentalsLabelText];
    
    UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    shareButton.frame = CGRectMake(270, 2, 35, 24);
    [shareButton setBackgroundImage:[UIImage imageNamed:@"ActionButton.png"] forState:UIControlStateNormal];
    [shareButton addTarget:self action:@selector(share:) forControlEvents:UIControlEventTouchUpInside];
    [_EnvironmentalsGradientLabel addSubview:shareButton];
    
    return _EnvironmentalsGradientLabel;
}

- (EnvironmentalsSubView *) returnEnvironmentalsView
{
    _EnvironmentalsView = [[EnvironmentalsSubView alloc] initWithFrame:CGRectMake(5, 300, 310, 96)];
    _EnvironmentalsView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"1280x -Slate Gradient.jpg"]];
    _EnvironmentalsView.layer.borderColor = [[UIColor grayColor] CGColor];
    _EnvironmentalsView.layer.borderWidth = 1.0f;
    _EnvironmentalsView.layer.cornerRadius = 10.0f;
    _EnvironmentalsView.layer.masksToBounds = YES;

    return _EnvironmentalsView;
}

- (UIView *) returnFooterView
{
    _FooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 396, 320, 20)];
    _FooterView.backgroundColor = [UIColor clearColor];
    
    _siteAddress = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, 220, 10)];
    _siteAddress.textAlignment = NSTextAlignmentLeft;
    _siteAddress.textColor = [UIColor whiteColor];
    _siteAddress.backgroundColor = [UIColor clearColor];
    [_siteAddress setFont:[UIFont fontWithName:@"Arial-BoldMT" size:8.0f]];
    [_FooterView addSubview:_siteAddress];
    
    _SiteSystemSizeLabel = [[UILabel alloc] initWithFrame:CGRectMake(220, 5, 100, 10)];
    _SiteSystemSizeLabel.textAlignment = NSTextAlignmentRight;
    _SiteSystemSizeLabel.textColor = [UIColor whiteColor];
    _SiteSystemSizeLabel.backgroundColor = [UIColor clearColor];
    [_SiteSystemSizeLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:8.0f]];
    [_FooterView addSubview:_SiteSystemSizeLabel];
    
    return _FooterView;
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource {
    
	//  should be calling your tableviews data source model to reload
	//  put here just for demo
	_reloading = YES;
}

- (void)doneLoadingTableViewData {
    
	//  model should call this when its done loading
	_reloading = NO;
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Loading";
    HUD.detailsLabelText = @"Fetching data";
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [self makeCallbacks];
        //authData = [AuthenticationData sharedManager];
        //[self getAuthData];
    });
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:scroller];
}


#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (scrollView.contentOffset.y > 0) {
        [scrollView setContentOffset:CGPointMake(0, 0)];
    }
    //NSLog(@"Scroll View Did Scroll");
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    //NSLog(@"Scroll View Ended Scroll");
    
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    
}


#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view
{
    [self reloadTableViewDataSource];
    [self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:3.0];
    
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
    
	return _reloading; // should return if data source model is reloading
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
	
    //NSLog(@"Refreshing Date String");
    
	return [NSDate date]; // should return date data source was last changed
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    [[[UIAlertView alloc] initWithTitle:@"Memory Warning" message:@"Power One App is currently storing too much memory in the iPhone!" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
}


static NSString * setAuthenticationKey;

#pragma mark - Class methods
+ (NSString*)setAuthenticationKey
{
    return setAuthenticationKey;
}

+ (void)setAuthenticationKey:(NSString*)withAuthKey
{
    if (setAuthenticationKey != withAuthKey) {
    	//[str release];
    	setAuthenticationKey = [withAuthKey copy];
    }
}


static NSArray * setRegionAndCity;

#pragma mark - Class methods
+ (NSArray*)setRegionAndCity{
    return setRegionAndCity;
}

+ (void)setRegionAndCity:(NSArray*)withRegionAndCity{
    if (setRegionAndCity != withRegionAndCity) {
    	setRegionAndCity = [withRegionAndCity copy];
    }
}

static NSString * setInstallDate;

#pragma mark - Class methods
+ (NSString*)setInstallDate{
    return setInstallDate;
}

+ (void)setInstallDate:(NSString*)withInstallDate {
    if (setInstallDate != withInstallDate) {
    	setInstallDate = [withInstallDate copy];
    }
}

@end
