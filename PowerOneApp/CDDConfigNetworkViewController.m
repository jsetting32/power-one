//
//  CDDConfigNetworkViewController.m
//  PowerOneApp
//
//  Created by John Setting on 7/22/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "CDDConfigNetworkViewController.h"

@interface CDDConfigNetworkViewController ()

@end

@implementation CDDConfigNetworkViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Network";
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)boxCheck:(id)sender {
    if (self.avmethodCheckbox.checked) {
        NSLog(@"avmethodCheckbox Button was Checked");
    } else if (!self.avmethodCheckbox.checked) {
        NSLog(@"avmethodCheckbox Button was Unchecked");
    }
    
    if (self.dhcpcheckbox.checked) {
        NSLog(@"dhcpcheckbox Button was Checked");
    } else if (!self.dhcpcheckbox.checked) {
        NSLog(@"dhcpcheckbox Button was Unchecked");
    }
    
    if (self.senddatatoportalCheckbox.checked) {
        NSLog(@"senddatatoportalCheckbox Button was Checked");
    } else if (!self.senddatatoportalCheckbox.checked) {
        NSLog(@"senddatatoportalCheckbox Button was Unchecked");
    }
    
    if (self.sendeventstoportalCheckbox.checked) {
        NSLog(@"sendeventstoportalCheckbox Button was Checked");
    } else if (!self.sendeventstoportalCheckbox.checked) {
        NSLog(@"sendeventstoportalCheckbox Button was Unchecked");
    }
    
    if (self.checkautomaticupdatesCheckbox.checked) {
        NSLog(@"checkautomaticupdatesCheckbox Button was Checked");
    } else if (!self.checkautomaticupdatesCheckbox.checked) {
        NSLog(@"checkautomaticupdatesCheckbox Button was Unchecked");
    }
    
    
    NSLog(@"RememberMe Button was Checked");
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)nextViewAction:(id)sender {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/protect/config.htm", ipAddress]]];
    
    NSLog(@"%@", encode);
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
    [request setValue:@"gzip, deflate" forHTTPHeaderField:@"Accept-Encoding"];
    [request setValue:[NSString stringWithFormat:@"Basic %@", encode] forHTTPHeaderField:@"Authorization"];
    [request setValue:@"keep-alive" forHTTPHeaderField:@"Connection"];
    [request setValue:ipAddress forHTTPHeaderField:@"Host"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    NSString* astr = @"mac=0%3A04%3AA3%3AB0%3A47%3AF1&host=P1+++++++++++++&dhcp=1&ipdb=0.0.0.0&prdb=80&ipp=63.236.63.180&cmp=1&edf=1&eaf=1&eupd=1&sdat=300&ipup=151.22.100.235";
    
    [request setHTTPBody:[astr dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSError *error;
    NSURLResponse *response;
    
    NSLog(@"%@", response);
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString* another = [NSString stringWithUTF8String:[data bytes]];
    
    NSLog(@"%@ : %@", newStr, another);
    NSLog(@"Error: %@", error);

    CDDConfigAcquisitionWizardViewController *acquisition = [[CDDConfigAcquisitionWizardViewController alloc] initWithNibName:@"CDDConfigAcquisitionWizardViewController" bundle:NULL];
    acquisition->ipAddress = ipAddress;
    acquisition->encode = encode;
    [self.navigationController pushViewController:acquisition animated:YES];

}
@end
