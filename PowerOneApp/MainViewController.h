//
//  ViewController.h
//  PowerOneAuroraApp
//
//  Created by John Setting on 5/21/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <CoreLocation/CoreLocation.h>

#import "PowerOneLogin.h"

#import "DayGraphViewController.h"
#import "30DGraphViewController.h"

//#import "YWHelper.h"

#import "CustomBadge.h"
#import "FPPopoverController.h"
 
#import "SharingActivityProvider.h"

#import "PullEasyViewData.h"

@interface MainViewController : UIViewController <UIActionSheetDelegate,CLLocationManagerDelegate, FPPopoverControllerDelegate> {

    // Keys
    NSString *solarStatus;
    NSString *fields;
    
    // Key Elements
    NSString *nowType;
    NSString *nowField;
    NSString *nowLabel;
    NSString *nowEntityID;
    NSString *nowEntityName;
    NSString *nowTimeZone;
    NSString *nowUnits;
    NSString *nowParameters;
    
    // Key Elements (parameter) Elements
    NSString *nowParam_Value;
    NSString *nowParam_Name;
    
    NSString *nowStart;
    NSString *nowStartLabel;
    NSString *nowEnd;
    NSString *nowEndLabel;
    NSString *nowSampleStart;
    NSString *nowSampleStartLabel;
    NSString *nowSampleEnd;
    NSString *nowSampleEndLabel;
    
    NSString *nowValue;
    NSString *nowValueFormatted;
    
    NSString *todayValue;
    NSString *todayValueFormatted;
    
    NSString *weekValue;
    NSString *weekValueFormatted;
    
    NSString *monthValue;
    NSString *monthValueFormatted;
    
    NSString *lifeValue;
    NSString *lifeValueFormatted;
    
    NSArray *graphStartDateDAY;
    NSArray *graphValuesDAY;
    
    UIButton *inverters;
    UIButton *modules;
    UIButton *events;
    
    UIButton *navigationButton;
    UIButton *shareButton;
    
    UINavigationBar *navBar;
    
    FPPopoverController *popover;
    
    UIView *statusView;
    UIView *dateView;
    UIView *gagueView;
    UIView *environmentalsView;
    UIView *powerGenerationView;
    UIView *weatherView;
    UIView *graphView;
    
	//CALayer *tachLayer;
	//CALayer *pinLayer;
    
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    
}

//@property (strong, nonatomic) PowerOneLogin *loginView;

@property (strong, nonatomic) UILabel *entityName;

//- (void)selectedInverters: (NSUInteger) rowNumber;
//- (void)selectedModules:(NSUInteger) rowNumber;
//- (void)selectedNotifications:(NSUInteger) rowNumber;



@property (strong, nonatomic) UIView *view1;
@property (strong, nonatomic) UILabel *StaticSystemStatus;
@property (strong, nonatomic) UIImageView *SystemStatusIcon;
@property (strong, nonatomic) UILabel *SystemStatus;

@property (strong, nonatomic) UIView *view2;
@property (strong, nonatomic) UILabel *StaticPowerText;
@property (strong, nonatomic) UILabel *UpdatedForCurrentPowerTime;
@property (strong, nonatomic) UILabel *PowerUnitsInKiloWattHours;
@property (strong, nonatomic) UIImageView *CanvasMonitorImage;

@property (strong, nonatomic) UIView *view3;
@property (strong, nonatomic) UILabel *StaticCarbonOffsetText;
@property (strong, nonatomic) UILabel *CarbonOffsetMetricTonsUnits;
@property (strong, nonatomic) UIImageView *CarbonTreeImage;
@property (strong, nonatomic) UILabel *CarbonOffsetAcresUnits;

@property (strong, nonatomic) UIView *view4;
@property (strong, nonatomic) UILabel *CurrentDateText;
@property (strong, nonatomic) UILabel *StaticEnergyText;
@property (strong, nonatomic) UIImageView *EnergyIcon;
@property (strong, nonatomic) UILabel *KilowattHoursUnitsText;
@property (strong, nonatomic) UILabel *StaticKilowattHoursUnit;

@property (strong, nonatomic) UIView *view5;
@property (strong, nonatomic) UILabel *StaticEnvironmentalsFromWeatherStationText;
@property (strong, nonatomic) UILabel *UpdatedForCurrentEnvironmentsTime;
@property (strong, nonatomic) UIImageView *sunIcon;
@property (strong, nonatomic) UILabel *IrradianceText;
@property (strong, nonatomic) UILabel *IrradianceValue;
@property (strong, nonatomic) UILabel *InsolationText;
@property (strong, nonatomic) UILabel *InsolationValue;
@property (strong, nonatomic) UILabel *CellTempText;
@property (strong, nonatomic) UILabel *CellTempValue;
@property (strong, nonatomic) UILabel *AmbientTempText;
@property (strong, nonatomic) UILabel *AmbientTempValue;
@property (strong, nonatomic) UIImageView *thermometerIcon;

@property(strong, nonatomic) UILabel *energyGeneratedToday;
@property(strong, nonatomic) UILabel *energyGeneratedWeek;
@property(strong, nonatomic) UILabel *energyGeneratedMonth;
@property(strong, nonatomic) UILabel *energyGeneratedLifetime;
@property(strong, nonatomic) UILabel *energyGeneratedTodayData;
@property(strong, nonatomic) UILabel *energyGeneratedWeekData;
@property(strong, nonatomic) UILabel *energyGeneratedMonthData;
@property(strong, nonatomic) UILabel *energyGeneratedLifetimeData;

@property(strong, nonatomic) UILabel *weatherCurrentDate;
@property(strong, nonatomic) UILabel *weatherCurrentCode;
@property(strong, nonatomic) UILabel *weatherCurrentTemp;
@property(strong, nonatomic) UILabel *weatherCurrentCondition;
@property(strong, nonatomic) UIImageView *weatherIconImageView;
@property(strong, nonatomic) UILabel *weatherTodaysHigh;
@property(strong, nonatomic) UILabel *weatherTodaysLow;

@property(strong, nonatomic) UILabel *weatherCurrentState;
@property(strong, nonatomic) UILabel *weatherCurrentCity;



@property(strong, nonatomic) UILabel *currentDayStringText;
@property(strong, nonatomic) UILabel *currentDayNumberText;
@property(strong, nonatomic) UILabel *currentMonthText;


@property (strong, nonatomic) UIView *view6;
@property (strong, nonatomic) UILabel *WeMoDevices;
@property (strong, nonatomic) UIImageView *WeMoImage;

@property (strong, nonatomic) NSString *num;

@property (strong,nonatomic) NSString *solarStatus;
@property (strong,nonatomic) NSString *fields;

@property (strong,nonatomic) NSString *nowType;
@property (strong,nonatomic) NSString *nowField;
@property (strong,nonatomic) NSString *nowLabel;
@property (strong,nonatomic) NSString *nowEntityID;
@property (strong,nonatomic) NSString *nowEntityName;
@property (strong,nonatomic) NSString *nowTimeZone;
@property (strong,nonatomic) NSString *nowUnits;
@property (strong,nonatomic) NSString *nowParameters;
@property (strong,nonatomic) NSString *nowParam_Value;
@property (strong,nonatomic) NSString *nowParam_Name;
@property (strong,nonatomic) NSString *nowStart;
@property (strong,nonatomic) NSString *nowStartLabel;
@property (strong,nonatomic) NSString *nowEnd;
@property (strong,nonatomic) NSString *nowEndLabel;
@property (strong,nonatomic) NSString *nowSampleStart;
@property (strong,nonatomic) NSString *nowSampleStartLabel;
@property (strong,nonatomic) NSString *nowSampleEnd;
@property (strong,nonatomic) NSString *nowSampleEndLabel;
@property (strong,nonatomic) NSString *nowValue;

@property (strong, nonatomic) NSArray *maxGaugeValue;
@property (nonatomic) float currentGaugeValue;
@property (strong, atomic) NSString *zipCode;

+(NSArray *)getMonthData;

@end
