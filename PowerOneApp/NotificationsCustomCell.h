//
//  SearchTableCell.h
//  ourApplication
//
//  Created by John Setting on 4/21/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationsCustomCell : UITableViewCell <UIActionSheetDelegate>
{
}
@property (strong, nonatomic) IBOutlet UILabel *EventTitle;
@property (strong, nonatomic) IBOutlet UILabel *EventDescription;
@property (strong, nonatomic) IBOutlet UILabel *EventStartTime;
@property (strong, nonatomic) IBOutlet UIImageView *avatarImage;
@property (strong, nonatomic) IBOutlet UILabel *avatarName;

- (IBAction)facebookUpload:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *facebookOutlet;

@property (weak, nonatomic) IBOutlet UITableViewHeaderFooterView *footerView;

@property (weak, nonatomic) IBOutlet UIButton *twitterOutlet;
- (IBAction)twitterUpload:(id)sender;


@end
