//
//  BarGraphViewController.m
//  PowerOneAuroraApp
//
//  Created by John Setting on 6/1/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "MTDGraphViewController.h"

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0) //1

@implementation MTDGraphViewController

@synthesize hostView = hostView_;

CGFloat const MTDBarWidth = 0.25f;
CGFloat const MTDBarInitialX = 0.25f;

@synthesize priceAnnotation = priceAnnotation_;


-(void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - UIViewController lifecycle methods
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
        
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    graphData = [[PullEasyViewData alloc] init];
    
    dispatch_async(kBgQueue, ^{
        
        NSDate *now = [NSDate date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyyMMdd"];
        
        NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *comp = [gregorian components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:[NSDate date]];
        [comp setDay:1];
        NSDate *firstDayOfMonthDate = [gregorian dateFromComponents:comp];
        
        int daysToAdd = 1;
        NSDate *tomorrow = [now dateByAddingTimeInterval:60*60*24*daysToAdd];
        
        NSString *firstDayofMonthFormatted = [dateFormat stringFromDate:firstDayOfMonthDate];
        NSString *tomorrowFormatted = [dateFormat stringFromDate:tomorrow];
        
        // Instantiate and allocate memory to prepare for an alert popup if any connections fail
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Status" message:@"Some connections to your site are down. In result, some data will not present. Please pull to refresh to try again!" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        
        NSData* data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://easyview.auroravision.net/easyview/services/gmi/summary/GenerationEnergy.json?type=GenerationEnergy&eids=1167815&tz=US%%2FMountain&start=%@&end=%@&range=MTD&hasUsage=false&label=MTD&dataProperty=chartData&binSize=Min15&bins=true&plantPowerNow=false&v=1.4.7", firstDayofMonthFormatted, tomorrowFormatted]]];
        if (data == nil) {
            NSLog(@"MTD Graph Data is nil");
            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        } else {
            [graphData performSelectorOnMainThread:@selector(fetchGraphDataRequestDataMTD:) withObject:data waitUntilDone:YES];
            //[graphData performSelectorInBackground:@selector(fetchGraphDataRequestDataMTD:) withObject:data];
            
            //dispatch_async(kBgQueue, ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self initPlot];
            });
        }
        
        
        
    });
    
    //data = [AuthenticationData sharedManager];
    //dispatch_async(kBgQueue, ^{ dispatch_async(dispatch_get_main_queue(), ^{ [self initPlot]; }); });
}

#pragma mark - Rotation
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
}

#pragma mark - Chart behavior
-(void)initPlot {
    //self.hostView.allowPinchScaling = NO;
    [self configureHost];
    [self configureGraph];
    [self configurePlots];
    [self configureAxes];
}

-(void)configureHost {
    self.hostView = [(CPTGraphHostingView *) [CPTGraphHostingView alloc] initWithFrame:CGRectMake(-5, -35, 320, 190)];
    //self.hostView = [(CPTGraphHostingView *) [CPTGraphHostingView alloc] initWithFrame:self.view.bounds];
    self.hostView.backgroundColor = [UIColor clearColor];
    self.hostView.allowPinchScaling = NO;
	[self.view addSubview:self.hostView];
}

-(void)configureGraph {
	// 1 - Create the graph
	CPTGraph *graph = [[CPTXYGraph alloc] initWithFrame:self.hostView.bounds];
	graph.plotAreaFrame.masksToBorder = NO;
	self.hostView.hostedGraph = graph;
    [graph applyTheme:[CPTTheme themeNamed:kCPTDarkGradientTheme]];
    
	// 2 - Create and set text style
	CPTMutableTextStyle *titleStyle = [CPTMutableTextStyle textStyle];
	titleStyle.color = [CPTColor whiteColor];
	titleStyle.fontName = @"Helvetica-Bold";
	titleStyle.fontSize = 12.0f;
	graph.titleTextStyle = titleStyle;
	graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
	graph.titleDisplacement = CGPointMake(0.0f, 10.0f);
    
    
	// 3 - Set padding for plot area
	[graph.plotAreaFrame setPaddingLeft:30.0f];
	[graph.plotAreaFrame setPaddingBottom:15.0f];
    
	// 4 - Set up plot space
	CGFloat xMin = 0.0f;
	CGFloat xMax = [[graphData getGraphDatesMTD] count];
	CGFloat yMin = 0.0f;
	CGFloat yMax = [[graphData getMaxMTDValue] floatValue] * 1.1;  // should determine dynamically based on max price
	CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    plotSpace.allowsUserInteraction = YES;
	plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(xMin) length:CPTDecimalFromFloat(xMax)];
	plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(yMin) length:CPTDecimalFromFloat(yMax)];
}

-(void)configurePlots {
    
	// 1 - Set up the three plots
	CPTBarPlot *aaplPlot = [CPTBarPlot tubularBarPlotWithColor:[CPTColor yellowColor] horizontalBars:NO];
	aaplPlot.identifier = @"APPL";
    
	// 2 - Set up line style
	CPTMutableLineStyle *barLineStyle = [[CPTMutableLineStyle alloc] init];
	barLineStyle.lineColor = [CPTColor whiteColor];
	barLineStyle.lineWidth = 0.5;
    
    CPTFill *filler = [CPTFill fillWithColor:[CPTColor yellowColor]];
    aaplPlot.fill      = filler;
    
	// 3 - Add plots to graph
	CPTGraph *graph = self.hostView.hostedGraph;
	CGFloat barX = MTDBarInitialX;
    
    // 4 - Set the Datasource, Delegate of the plot (RETAINING THE BARPLOT DURING THE LIFETIME OF THE GRAPH IS A MUST!!!)
    aaplPlot.dataSource = self;
    [aaplPlot.dataSource retain];
    aaplPlot.delegate = self;
    
    aaplPlot.barWidth = CPTDecimalFromDouble(MTDBarWidth);
    aaplPlot.barOffset = CPTDecimalFromDouble(barX);
    aaplPlot.lineStyle = barLineStyle;
    [graph addPlot:aaplPlot toPlotSpace:graph.defaultPlotSpace];
    barX += MTDBarWidth;
}

-(void)configureAxes {
    
	// 1 - Configure styles
	CPTMutableTextStyle *axisTitleStyle = [CPTMutableTextStyle textStyle];
	axisTitleStyle.color = [CPTColor whiteColor];
	axisTitleStyle.fontName = @"Helvetica-Bold";
	axisTitleStyle.fontSize = 8.0f;
	CPTMutableLineStyle *axisLineStyle = [CPTMutableLineStyle lineStyle];
	axisLineStyle.lineWidth = 0.5f;
	axisLineStyle.lineColor = [[CPTColor whiteColor] colorWithAlphaComponent:1];
    
	// 2 - Get the graph's axis set
	CPTXYAxisSet *axisSet = (CPTXYAxisSet *) self.hostView.hostedGraph.axisSet;
    
	// 3 - Configure the x-axis
	axisSet.xAxis.labelingPolicy = CPTAxisLabelingPolicyNone;
	//axisSet.xAxis.title = @"Days of Week (Mon - Fri)";
	axisSet.xAxis.titleTextStyle = axisTitleStyle;
	axisSet.xAxis.titleOffset = 10.0f;
	axisSet.xAxis.axisLineStyle = axisLineStyle;
    
    CPTAxis *x = axisSet.xAxis;
	//x.title = @"";
	x.titleTextStyle = axisTitleStyle;
	x.titleOffset = 10.0f;
	x.axisLineStyle = axisLineStyle;
	x.labelingPolicy = CPTAxisLabelingPolicyNone;
	x.labelTextStyle = axisTitleStyle;
	x.majorTickLineStyle = axisLineStyle;
	x.majorTickLength = 2.0f;
	x.tickDirection = CPTSignNegative;
    
    // Define some custom labels for the data elements
    //x.labelRotation  = M_PI / 4;
    //x.labelingPolicy = CPTAxisLabelingPolicyNone;
    
    CGFloat dateCount = [[graphData getGraphDatesMTD] count];
    
	NSMutableSet *xLabels = [NSMutableSet setWithCapacity:dateCount];
	NSMutableSet *xLocations = [NSMutableSet setWithCapacity:dateCount];
	NSInteger i;
	
    NSString *date;
    for (i = [[graphData getGraphDatesMTD] count]/12; i < [[graphData getGraphDatesMTD] count]; i=i+6)
    {
        //for (NSString *date in [graphData getGraphDatesDay])
        //{
        date = [[graphData getGraphDatesMTD] objectAtIndex:i];
		CPTAxisLabel *label = [[CPTAxisLabel alloc] initWithText:date  textStyle:x.labelTextStyle];
		CGFloat location = i;
		label.tickLocation = CPTDecimalFromCGFloat(location);
		label.offset = x.majorTickLength;
		
        if (label)
        {
			[xLabels addObject:label];
			[xLocations addObject:[NSNumber numberWithFloat:location]];
		}
	}
	x.axisLabels = xLabels;
	x.majorTickLocations = xLocations;
    
    /*
	// 4 - Configure the y-axis
	axisSet.yAxis.labelingPolicy = CPTAxisLabelingPolicyNone;
	axisSet.yAxis.title = @"kWh";
	axisSet.yAxis.titleTextStyle = axisTitleStyle;
	axisSet.yAxis.titleOffset = 5.0f;
	axisSet.yAxis.axisLineStyle = axisLineStyle;
    */
    
    CPTMutableTextStyle *axisTextStyle = [[CPTMutableTextStyle alloc] init];
	axisTextStyle.color = [CPTColor whiteColor];
	axisTextStyle.fontName = @"Helvetica-Bold";
	axisTextStyle.fontSize = 7.0f;
    
    CPTMutableLineStyle *tickLineStyle = [CPTMutableLineStyle lineStyle];
	tickLineStyle.lineColor = [CPTColor whiteColor];
	tickLineStyle.lineWidth = 0.5f;
	
    CPTMutableLineStyle *gridLineStyle = [CPTMutableLineStyle lineStyle];
	tickLineStyle.lineColor = [CPTColor blackColor];
	tickLineStyle.lineWidth = 0.25f;
    
    // 4 - Configure y-axis
	CPTAxis *y = axisSet.yAxis;
	y.title = @"kWh";
	y.titleTextStyle = axisTitleStyle;
	y.titleOffset = -40.0f;
	y.axisLineStyle = axisLineStyle;
	y.majorGridLineStyle = gridLineStyle;
	y.labelingPolicy = CPTAxisLabelingPolicyNone;
	y.labelTextStyle = axisTextStyle;
	y.labelOffset = 16.0f;
	y.majorTickLineStyle = axisLineStyle;
	y.majorTickLength = 2.0f;
	y.minorTickLength = 1.0f;
	y.tickDirection = CPTSignPositive;
	
    NSInteger majorIncrement = [[graphData getMaxMTDValue] integerValue] * .2;
    NSInteger minorIncrement = [[graphData getMaxMTDValue] integerValue] * .2;
    
    //NSInteger majorIncrement = 5;
    //NSInteger minorIncrement = 5;
	CGFloat yMax = [[graphData getMaxMTDValue] floatValue] * 1.1;  // should determine dynamically based on max price
    //NSLog(@"%ld : %ld", (long)majorIncrement, (long)minorIncrement);

    NSMutableSet *yLabels = [NSMutableSet set];
	NSMutableSet *yMajorLocations = [NSMutableSet set];
	NSMutableSet *yMinorLocations = [NSMutableSet set];
	
    NSUInteger mod = 0;
    for (NSInteger j = minorIncrement; j <= yMax; j += minorIncrement)
    {
        if (majorIncrement == 0) {
            mod = 0;
        } else {
            mod = j % majorIncrement;
        }
        if (mod == 0) {
			CPTAxisLabel *label = [[CPTAxisLabel alloc] initWithText:[NSString stringWithFormat:@"%i", j] textStyle:y.labelTextStyle];
			NSDecimal location = CPTDecimalFromInteger(j);
			label.tickLocation = location;
			label.offset = -y.majorTickLength - y.labelOffset;
			
            if (label)
            {
				[yLabels addObject:label];
			}
            
			[yMajorLocations addObject:[NSDecimalNumber decimalNumberWithDecimal:location]];
		} else {
			[yMinorLocations addObject:[NSDecimalNumber decimalNumberWithDecimal:CPTDecimalFromInteger(j)]];
		}
	}
    
	y.axisLabels = yLabels;
	y.majorTickLocations = yMajorLocations;
	y.minorTickLocations = yMinorLocations;
}

-(void)hideAnnotation:(CPTGraph *)graph {
	if ((graph.plotAreaFrame.plotArea) && (self.priceAnnotation)) {
		[graph.plotAreaFrame.plotArea removeAnnotation:self.priceAnnotation];
		self.priceAnnotation = nil;
	}
}

#pragma mark - CPTPlotDataSource methods
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    return [[graphData getGraphDatesMTD] count];
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index {
    if ((fieldEnum == CPTBarPlotFieldBarTip) && (index < [[graphData getGraphDatesMTD] count])) {
        if ([plot.identifier isEqual:@"APPL"])
			return [[graphData getGraphValuesMTD] objectAtIndex:index];
	}
	return [NSDecimalNumber numberWithUnsignedInteger:index];
}

#pragma mark - CPTBarPlotDelegate methods
-(void)barPlot:(CPTBarPlot *)plot barWasSelectedAtRecordIndex:(NSUInteger)index {
    
	// 1 - Is the plot hidden?
	if (plot.isHidden == YES) {
		return;
	}
    
	// 2 - Create style, if necessary
	static CPTMutableTextStyle *style = nil;
	if (!style) {
		style = [CPTMutableTextStyle textStyle];
		style.color= [CPTColor yellowColor];
		style.fontSize = 16.0f;
		style.fontName = @"Helvetica-Bold";
	}
    
	// 3 - Create annotation, if necessary
	NSNumber *price = [self numberForPlot:plot field:CPTBarPlotFieldBarTip recordIndex:index];
	if (!self.priceAnnotation) {
		NSNumber *x = [NSNumber numberWithInt:0];
		NSNumber *y = [NSNumber numberWithInt:0];
		NSArray *anchorPoint = [NSArray arrayWithObjects:x, y, nil];
		self.priceAnnotation = [[CPTPlotSpaceAnnotation alloc] initWithPlotSpace:plot.plotSpace anchorPlotPoint:anchorPoint];
	}
    
	// 4 - Create number formatter, if needed
	static NSNumberFormatter *formatter = nil;
	if (!formatter) {
		formatter = [[NSNumberFormatter alloc] init];
		[formatter setMaximumFractionDigits:2];
	}
    
	// 5 - Create text layer for annotation
	NSString *priceValue = [formatter stringFromNumber:price];
	CPTTextLayer *textLayer = [[CPTTextLayer alloc] initWithText:priceValue style:style];
	self.priceAnnotation.contentLayer = textLayer;
    
	// 6 - Get plot index based on identifier
	NSInteger plotIndex = 0;
	if ([plot.identifier isEqual:@"APPL"] == YES) {
		plotIndex = 0;
	} 
    
	// 7 - Get the anchor point for annotation
	CGFloat x = index + MTDBarInitialX + (plotIndex * MTDBarWidth);
	NSNumber *anchorX = [NSNumber numberWithFloat:x];
	CGFloat y = [price floatValue] + 40.0f;
	NSNumber *anchorY = [NSNumber numberWithFloat:y];
	self.priceAnnotation.anchorPlotPoint = [NSArray arrayWithObjects:anchorX, anchorY, nil];
    
	// 8 - Add the annotation
	[plot.graph.plotAreaFrame.plotArea addAnnotation:self.priceAnnotation];
}


@end
