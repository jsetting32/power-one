//
//  CDDConfigDateTimeViewController.h
//  PowerOneApp
//
//  Created by John Setting on 7/22/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDDConfigCDDCloneViewController.h"

@interface CDDConfigDateTimeViewController : UIViewController
{
    @public
    NSString *ipAddress;
    NSString *encode;
}
- (IBAction)nextViewAction:(id)sender;
@end
