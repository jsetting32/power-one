//
//  CDDConfigAcquisitionWizardViewController.m
//  PowerOneApp
//
//  Created by John Setting on 7/22/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "CDDConfigAcquisitionWizardViewController.h"

@interface CDDConfigAcquisitionWizardViewController ()

@end

@implementation CDDConfigAcquisitionWizardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Acquisition";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)startAcquisitionAction:(id)sender {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/protect/acquisition.htm", ipAddress]]];
    

    
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"Basic %@", encode] forHTTPHeaderField:@"Authorization"];
    [request setValue:ipAddress forHTTPHeaderField:@"Host"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    NSString* astr = @"st=start";
    [request setHTTPBody:[astr dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSError *error;
    NSURLResponse *response;
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString* another = [NSString stringWithUTF8String:[data bytes]];
    NSLog(@"%@ : %@", newStr, another);
    NSLog(@"Error: %@", error);

    //NSLog(@"%@ : %@", newStr, another);
}

- (IBAction)nextViewAction:(id)sender {
    
    CDDConfigDateTimeViewController *dateTime = [[CDDConfigDateTimeViewController alloc] initWithNibName:@"CDDConfigDateTimeViewController" bundle:NULL];
    dateTime->ipAddress = ipAddress;
    dateTime->encode = encode;
    [self.navigationController pushViewController:dateTime animated:YES];
    
}
@end
