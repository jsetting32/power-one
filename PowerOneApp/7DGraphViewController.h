//
//  7DGraphViewController.h
//  PowerOneApp
//
//  Created by John Setting on 6/12/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"
#import "PullEasyViewData.h"
#import "AuthenticationData.h"
#import "PullEasyViewData.h"
@interface _DGraphViewController : UIViewController <CPTPlotDataSource, CPTScatterPlotDelegate, CPTScatterPlotDataSource, CPTPlotSpaceDelegate>
{
    PullEasyViewData *graphData;
    AuthenticationData * authData;
}
@property (nonatomic, strong) CPTGraphHostingView *hostView;


@property (nonatomic, strong) NSString *authToken;

@end
