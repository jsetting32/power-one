//
//  HelpCenterViewController.h
//  PowerOneApp
//
//  Created by John Setting on 6/21/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "TDBadgedCell.h"

@interface HelpCenterViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>


@property (retain, nonatomic) IBOutlet UITableView *helpCenterTableView;

@end
