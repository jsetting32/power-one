//
//  CDDDetailsViewController.m
//  PowerOneApp
//
//  Created by John Setting on 7/16/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "CDDDetailsViewController.h"
#define LABEL_TAG 1000
#define IMAGE_TAG 500
#define PERCENTAGE_TAG 250
@interface CDDDetailsViewController ()

@end

@implementation CDDDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //NSLog(@"%@", [_detailsArray objectAtIndex:1]);
    UIView *view;
    UILabel *label;
    UILabel *powerOutput;
    
    for (int i = 1; i < [[_detailsArray objectAtIndex:1] count]; i++)
    {
        view = [[UIView alloc] initWithFrame:CGRectMake((i-1)*100 + 5, 10, 90, 100)];
        view.backgroundColor = [UIColor blackColor];
        
        label = [[UILabel alloc] init];
        //label.frame = CGRectMake(view.frame.origin.x + 5, view.frame.origin.y + 2, view.frame.size.width - 10, 20);
        label.frame = CGRectMake(10, view.frame.origin.y + 2, view.frame.size.width - 10, 20);
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont systemFontOfSize:10];
        label.textColor = [UIColor whiteColor];
        powerOutput.textAlignment = NSTextAlignmentCenter;
        label.text = [NSString stringWithFormat:@"SN %@", [[[_detailsArray objectAtIndex:1] objectAtIndex:i] objectForKey:@"sn"]];
        //[powerOutput setTag:LABEL_TAG+i];
        NSLog(@"%@", label.text);
        [view addSubview:label];
        
        powerOutput = [[UILabel alloc] init];
        //powerOutput.frame = CGRectMake(view.frame.origin.x + 5, view.frame.origin.y + 50, view.frame.size.width - 10, 20);
        powerOutput.frame = CGRectMake(10, view.frame.origin.y + 50, view.frame.size.width - 10, 20);
        powerOutput.backgroundColor = [UIColor clearColor];
        powerOutput.font = [UIFont systemFontOfSize:10];
        powerOutput.textColor = [UIColor whiteColor];
        powerOutput.textAlignment = NSTextAlignmentCenter;
        powerOutput.text = [NSString stringWithFormat:@"%@W", [[[_detailsArray objectAtIndex:1] objectAtIndex:i] objectForKey:@"Pout_W"]];
        //[powerOutput setTag:IMAGE_TAG+i];
        NSLog(@"%@", powerOutput.text);
        [view addSubview:powerOutput];
        
        [self.view addSubview:view];

        
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
