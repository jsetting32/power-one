//
//  WeatherViewController.h
//  PowerOneApp
//
//  Created by John Setting on 6/21/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YWWeeklyHelper.h"
#import "XLCycleScrollView.h"
#import "PullEasyViewData.h"
#import "hourlyForecastScrollView.h"



@interface WeatherViewController : UIViewController <UIScrollViewDelegate, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate>
{
    PullEasyViewData *weatherData;
    
    //UITableView     *theTableView;

}
@property (nonatomic, strong) UIImageView *image;
+ (NSArray *)getImages;
+ (void)setImages:(NSArray*)newImages;
+ (NSArray*)setsunRiseandSetTimes;
+ (void)setsunRiseandSetTimes:(NSArray*)withSunRiseandSetTimes;
+ (NSArray*)setWindSpeedAndDirection;
+ (void)setWindSpeedAndDirection:(NSArray*)windSpeedAndDirectionSetter;
+ (NSArray*)setAtmosphereData;
+ (void)setAtmosphereData:(NSArray*)atmosphereDataSetter;


@property (nonatomic, strong) NSArray *times;
@property (nonatomic, strong) NSArray *temps;
@property (nonatomic, strong) NSArray *icons;

@property (strong, nonatomic) NSString* zipCode;
@property (strong, nonatomic) NSString* city;
@property (strong, nonatomic) NSString* state;

@property (strong, nonatomic) UIScrollView *weatherView;

@property (strong, nonatomic) UIView *weatherNow;
@property (strong, nonatomic) UILabel *locationLabel;
@property (strong, nonatomic) UILabel *todayHighs;
@property (strong, nonatomic) UILabel *todayLows;
@property (strong, nonatomic) UILabel *todayNowTemp;
@property (strong, nonatomic) UIImageView *NowImage;
@property (strong, nonatomic) UILabel *nowImageDescription;

@property (strong, nonatomic) UIView *ForecastView;
@property (strong, nonatomic) hourlyForecastScrollView *hourlyForecast;

@property (strong, nonatomic) UIView *plantDetailsView;
@property (strong, nonatomic) UILabel *IrradianceText;
@property (strong, nonatomic) UILabel *IrradianceValue;
@property (strong, nonatomic) NSString *IrradianceValueText;
@property (strong, nonatomic) UILabel *InsolationText;
@property (strong, nonatomic) UILabel *InsolationValue;
@property (strong, nonatomic) NSString *InsolationValueText;
@property (strong, nonatomic) UILabel *CellTempText;
@property (strong, nonatomic) UILabel *CellTempValue;
@property (strong, nonatomic) NSString *CellTempValueText;

@property (strong, nonatomic) UILabel *AmbientTempText;
@property (strong, nonatomic) UILabel *AmbientTempValue;
@property (strong, nonatomic) NSString *AmbientValueText;
@property (strong, nonatomic) UIView *todayOverallForecast;
@property (strong, nonatomic) UILabel *todayDayTitle;
@property (strong, nonatomic) UIImageView *todayDayImage;
@property (strong, nonatomic) UILabel *todayDayHigh;
@property (strong, nonatomic) UILabel *todayDayLow;

@property (strong, nonatomic) UIView *nextDayOverallForecast;
@property (strong, nonatomic) UILabel *nextDayTitle;
@property (strong, nonatomic) UIImageView *nextDayImage;
@property (strong, nonatomic) UILabel *nextDayHigh;
@property (strong, nonatomic) UILabel *nextDayLow;

@property (strong, nonatomic) UIView *nextnextDayOverallForecast;
@property (strong, nonatomic) UILabel *nextnextDayTitle;
@property (strong, nonatomic) UIImageView *nextnextDayImage;
@property (strong, nonatomic) UILabel *nextnextDayHigh;
@property (strong, nonatomic) UILabel *nextnextDayLow;

@property (strong, nonatomic) UIView *nextnextnextDayOverallForecast;
@property (strong, nonatomic) UILabel *nextnextnextDayTitle;
@property (strong, nonatomic) UIImageView *nextnextnextDayImage;
@property (strong, nonatomic) UILabel *nextnextnextDayHigh;
@property (strong, nonatomic) UILabel *nextnextnextDayLow;

@property (strong, nonatomic) UIView *nextnextnextnextDayOverallForecast;
@property (strong, nonatomic) UILabel *nextnextnextnextDayTitle;
@property (strong, nonatomic) UIImageView *nextnextnextnextDayImage;
@property (strong, nonatomic) UILabel *nextnextnextnextDayHigh;
@property (strong, nonatomic) UILabel *nextnextnextnextDayLow;


@property (strong, nonatomic) UIView *forecastDetailView;
@property (strong, nonatomic) UIImage *todayDetailIcon;

@property (strong, nonatomic) UILabel *feelsLikeLabel;
@property (strong, nonatomic) NSString *feelsLikeString;

@property (strong, nonatomic) UILabel *humidityLabel;
@property (strong, nonatomic) NSString *humidityString;

@property (strong, nonatomic) UILabel *visibilityLabel;
@property (strong, nonatomic) NSString *visibilityString;

@property (strong, nonatomic) UILabel *todayDescription;
@property (strong, nonatomic) NSString *todayDescriptionString;

@property (strong, nonatomic) UILabel *tonightDescription;
@property (strong, nonatomic) NSString *tonightDescriptionString;


@property (strong, nonatomic) UIView *mapView;

@property (strong, nonatomic) UIView *precipitationView;

@property (strong, nonatomic) UIView *windAndPressureView;
@property (strong, nonatomic) UILabel *windPowerLabel;
@property (strong, nonatomic) NSString *windPowerString;
@property (strong, nonatomic) UILabel *barometerLabel;
@property (strong, nonatomic) NSString *barometerString;
@property (strong, nonatomic) UILabel *windDirectionLabel;


@property (strong, nonatomic) UIView *sunriseAndSunsetView;
@property (strong, nonatomic) UILabel *sunriseTime;
@property (strong, nonatomic) NSString *sunriseTimeString;
@property (strong, nonatomic) UILabel *sunsetTime;
@property (strong, nonatomic) NSString *sunsetTimeString;


@end
