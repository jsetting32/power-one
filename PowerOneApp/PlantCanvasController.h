//
//  PlantCanvasController.h
//  PowerOneApp
//
//  Created by John Setting on 6/10/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface PlantCanvasController : UIViewController <UIScrollViewDelegate>
{
    UINavigationBar *navbar;
    
    NSMutableArray *numbers;
    UISlider *slider;
}
@end
