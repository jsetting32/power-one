//
//  PowerViewController.h
//  PowerOneApp
//
//  Created by John Setting on 6/21/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullEasyViewData.h"

@interface PowerViewController : UIViewController
{
    NSString *_powerTodayValue;
    NSString *_powerWeekValue;
    NSString *_powerMonthValue;
    NSString *_powerLifetimeValue;
}

@property (nonatomic, weak) IBOutlet UILabel *TodayValue;
@property (nonatomic, weak) IBOutlet UILabel *WeekValue;
@property (nonatomic, weak) IBOutlet UILabel *MonthValue;
@property (nonatomic, weak) IBOutlet UILabel *LifetimeValue;

@property (nonatomic, copy) NSString *powerTodayValue;
@property (nonatomic, copy) NSString *powerWeekValue;
@property (nonatomic, copy) NSString *powerMonthValue;
@property (nonatomic, copy) NSString *powerLifetimeValue;


@property (weak, nonatomic) IBOutlet UILabel *todaySavings;
@property (weak, nonatomic) IBOutlet UILabel *weeklySavings;
@property (weak, nonatomic) IBOutlet UILabel *monthlySavings;
@property (weak, nonatomic) IBOutlet UILabel *LifetimeSavings;

@property (strong, nonatomic) NSString *costPerKWH;
@property (strong, nonatomic) NSString *costPerKWHDate;


@end
