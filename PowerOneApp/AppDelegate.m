//
//  AppDelegate.m
//  PowerOneApp
//
//  Created by John Setting on 6/7/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "AppDelegate.h"

#import "CDDTabBarViewController.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    
    /*
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        self.login = [[PowerOneLoginTabBar alloc] init];
    } else {
        self.login = [[PowerOneLoginTabBar alloc] init];
    }
    self.window.rootViewController = self.login;
    [self.window makeKeyAndVisible];
    */
    
    // Let the device know we want to receive push notifications
	//[[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    
    //UIRemoteNotificationType enabledTypes = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
    
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
     // Override point for customization after application launch.

    FranksVC *front = [[FranksVC alloc] initWithNibName:@"FranksVC" bundle:nil];
    RearViewController *rear = [[RearViewController alloc] init];
    
    UINavigationController *navigator = [[UINavigationController alloc] initWithRootViewController:front];
    
    SWRevealViewController *reveal = [[SWRevealViewController alloc] initWithRearViewController:rear frontViewController:navigator];
    reveal.delegate = self;
    
    InvertersTableView *right = [[InvertersTableView alloc] initWithNibName:@"InvertersTableView" bundle:nil];
    right.view.backgroundColor = [UIColor blackColor];
    
    reveal.rightViewController = right;
    
    SWRevealViewController *sideBar = [[SWRevealViewController alloc] init];
    sideBar = reveal;
    
    self.window.rootViewController = sideBar;    
    [self.window makeKeyAndVisible];
    
    
    /*
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen]bounds]];
    // Override point for customization after application launch.
    FranksVC *front = [[FranksVC alloc] initWithNibName:@"FranksVC" bundle:nil];
    RearViewController *rear = [[RearViewController alloc] init];
    UINavigationController *navigator = [[UINavigationController alloc] initWithRootViewController:front];
    SWRevealViewController *reveal = [[SWRevealViewController alloc] initWithRearViewController:rear frontViewController:navigator];
    reveal.delegate = self;
    InvertersTableView *right = [[InvertersTableView alloc] init];
    right.view.backgroundColor = [UIColor blackColor];
    reveal.rightViewController = right;
    SWRevealViewController *sideBar = [[SWRevealViewController alloc] init];
    sideBar = reveal;
    
    PlantCanvasController *canvas = [[PlantCanvasController alloc] initWithNibName:@"PlantCanvasController" bundle:nil];
    UINavigationController *canvasNavigation = [[UINavigationController alloc] initWithRootViewController:canvas];
    
    //SDOrientationController *mainView = [[SDOrientationController alloc] initWithPortraitViewController:sideBar landscapeViewController:canvasNavigation];
    
    self.window.rootViewController = sideBar;
    [self.window makeKeyAndVisible];
    */
    
    /*
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen]bounds]];
    // Override point for customization after application launch.
    FranksVC *front = [[FranksVC alloc] initWithNibName:@"FranksVC" bundle:nil];
    UINavigationController *navigator = [[UINavigationController alloc] initWithRootViewController:front];

    PlantCanvasController *canvas = [[PlantCanvasController alloc] initWithNibName:@"PlantCanvasController" bundle:nil];
    SDOrientationController *mainView = [[SDOrientationController alloc] initWithPortraitViewController:navigator landscapeViewController:canvas];
    
    self.window.rootViewController = mainView;
    [self.window makeKeyAndVisible];
    */
    
    
    /*
    AccelerometerSolarInstallationViewController *solar = [[AccelerometerSolarInstallationViewController alloc] initWithNibName:@"AccelerometerSolarInstallationViewController" bundle:nil];
    
    //LoggerLoginViewController *logger = [[LoggerLoginViewController alloc] initWithNibName:@"LoggerLoginViewController" bundle:nil];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.

    self.window.rootViewController = solar;
    [self.window makeKeyAndVisible];
    */
    
    /*
    CDDLoginViewController *cdd = [[CDDLoginViewController alloc] initWithNibName:@"CDDLoginViewController" bundle:nil];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = cdd;
    [self.window makeKeyAndVisible];
    */
     
     
    return YES;
}



- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
	NSLog(@"My token is: %@", deviceToken);
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"coreDataApp" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"coreDataApp.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
