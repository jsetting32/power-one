//
//  PowerOneLogin.m
//  PowerOneAuroraApp
//
//  Created by John Setting on 5/29/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "PowerOneLogin.h"

@implementation PowerOneLogin

@synthesize rememberMe = _rememberMe;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.view.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"Yablon-solar1.jpg"]];
    }
    return self;
}

- (void)viewDidLoad
{

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(authenticateHandler:) name:AUTHENTICATE_NOTIFICATION object:self];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _userName.delegate = self;
    _passWord.delegate = self;
    _entityId.delegate = self;

    [_userName setText:[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"usernameString"]];
    [_passWord setText:[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"passwordString"]];
    [_entityId setText:[[PDKeychainBindings sharedKeychainBindings] objectForKey:@"entityIdString"]];
    
    self.loginButton.layer.cornerRadius = 5;
    self.loginButton.layer.borderWidth = 2;
    self.loginButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.loginButton.layer.masksToBounds = YES;

    self.demoButton.layer.cornerRadius = 5;
    self.demoButton.layer.borderWidth = 2;
    self.demoButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.demoButton.layer.masksToBounds = YES;

    NSTimer *time = [NSTimer timerWithTimeInterval:0.25 target:self selector:@selector(changeImage) userInfo:nil repeats:YES];
    [time fire];
    
    self.rememberMe.checked = FALSE;
    self.rememberMe.disabled = FALSE;
    
    self.topBar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"graybackground.jpg"]];
    
}

- (void) changeImage {
    UIImage *holder = self.image1.image;
    self.image1.image = self.image2.image;
    self.image2.image = holder;
    
    holder = self.image2.image;
    self.image2.image = self.image3.image;
    self.image3.image = holder;
    
    holder = self.image3.image;
    self.image3.image = self.image1.image;
    self.image1.image = holder;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if (textField == _userName) _userName.text = textField.text;
    else if(textField == _passWord) _passWord.text = textField.text;
    else if(textField == _entityId) _entityId.text = textField.text;
    
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    PDKeychainBindings *bindings=[PDKeychainBindings sharedKeychainBindings];
    
    if (textField == _userName) {
        [bindings setObject:[textField.text stringByReplacingCharactersInRange:range withString:string] forKey:@"usernameString"];
    } else if (textField == _passWord) {
        [bindings setObject:[textField.text stringByReplacingCharactersInRange:range withString:string] forKey:@"passwordString"];
    } else if (textField == _entityId) {
        [bindings setObject:[textField.text stringByReplacingCharactersInRange:range withString:string] forKey:@"entityIdString"];
    }
    
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqual:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    [textView resignFirstResponder];
    return YES;
}


#define api_Key @"c302d52a-ef4c-4c35-8312-d187ee0a4c49-00a1"
- (void)authenticateHandler:(NSNotification *)notification {
    NSLog(@"[LoginViewController] Authenticate handler");
    
    NSDictionary *userDict = [notification userInfo];
    
    BOOL isUserAuthenticated = [api_Key isEqualToString:[userDict objectForKey:@"apiKey"]] &&
    ([_auth isEqualToString:[userDict objectForKey:@"authKey"]]);
    
    [[NSUserDefaults standardUserDefaults] setBool:isUserAuthenticated forKey:@"userLoggedIn"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    if( isUserAuthenticated ){
        NSLog(@"Authenticated");
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        NSLog(@"Not Authenticated");
        
        if ([self reachable]) {
            [[[UIAlertView alloc] initWithTitle:@"Woops..." message:@"Username or password was not correct" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
        } else {
            [[[UIAlertView alloc] initWithTitle:@"Connection Status" message:@"Please check your internet connection!" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
        }
    }
}

#pragma mark - DemoButton Action
- (IBAction)demoAction:(id)sender
{

}

#pragma mark - Demo Tour
- (void)Demo
{

}

#pragma mark - LoginButton Action
- (IBAction)loginButtonAction:(id)sender
{
    HUD = [MBProgressHUD showHUDAddedTo:self.tabBarController.view animated:YES];
    HUD.dimBackground = YES;
    HUD.labelText = @"Loading";
    HUD.detailsLabelText = @"Fetching Asset Info";
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        if ([_userName.text isEqual: @""] || [_passWord.text isEqual: @""]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.tabBarController.view animated:YES];
                [[[UIAlertView alloc] initWithTitle:@"Login Credentials" message:@"Please fill in both the username and password text fields to login" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
            });
        } else {
            AuthenticationData *authData = [AuthenticationData sharedManager];
            _auth = [authData fetchAuthToken:@"https://platform.fatspaniel.net/accounts/ClientLogin" username:_userName.text password:_passWord.text apiKey:api_Key];
            
            if (([_auth isEqualToString:@"Error=BadAuthentication"]) || ([_auth length] != 126)) {
                if ([self reachable])
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:self.tabBarController.view animated:YES];
                        [[[UIAlertView alloc] initWithTitle:@"Woops..." message:@"Username or password was not correct" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
                    });
                else
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:self.tabBarController.view animated:YES];
                        [[[UIAlertView alloc] initWithTitle:@"Connection Status" message:@"Please check your internet connection!" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
                    });
            } else {
                _auth = [[_auth substringFromIndex:5] substringToIndex:[[_auth substringFromIndex:5] length] - 1];
                
                [FranksVC setAuthenticationKey:_auth];
                // --------------------------------- Get User Info ------------------------------------------ //
                [authData fetchUserServiceData:@"https://platform.fatspaniel.net/rest/v1/user" authKey:_auth];
                // --------------------------------- Get User Info ------------------------------------------ //
                
                // ------------------------------------------------------------------------------------------------- Get Portfolio Data ----------------------------------------------------------------------------------------- //
                if ([authData fetchAssetServiceData:[NSString stringWithFormat:@"https://platform.fatspaniel.net/rest/v1/asset?query=entityId=%@&resultQuery=Site[detailLevel('All')]", [authData getSiteListEid]] authKey:_auth]) {
                // ------------------------------------------------------------------------------------------------- Get Portfolio Data ----------------------------------------------------------------------------------------- //
                    if ([[authData getAssetDictionary] count] == 1 ) {
                        if ([self reachable])
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self Login];
                                [MBProgressHUD hideHUDForView:self.tabBarController.view animated:YES];
                            });
                        else
                            [[[UIAlertView alloc] initWithTitle:@"Connection Status" message:@"Please check your internet connection!" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
                    } else if ([[authData getAssetDictionary] count] > 1) {
                        if ([self reachable]) {
                            PortfolioViewController *view = [[PortfolioViewController alloc] initWithNibName:@"PortfolioViewController" bundle:nil];
                            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:view];
                            view.username = self.userName.text;
                            view.password = self.passWord.text;
                            view.apiKey = api_Key;
                            view.authKey = self.auth;
                            view.portfolioArray = [authData getAssetDictionary];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self presentViewController:nav animated:YES completion:nil];
                                [MBProgressHUD hideHUDForView:self.tabBarController.view animated:YES];
                            });
                        } else
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [MBProgressHUD hideHUDForView:self.tabBarController.view animated:YES];
                                [[[UIAlertView alloc] initWithTitle:@"Connection Status" message:@"Please check your internet connection!" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
                            });
                    } else
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [MBProgressHUD hideHUDForView:self.tabBarController.view animated:YES];
                            [[[UIAlertView alloc] initWithTitle:@"No Site Listings" message:@"You currently have no sites in your portfolio. Either signup and register your site or contact product management for more information." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil] show];
                        });
                } else {}
            }
        }
    });
}


#pragma mark - Login
- (void)Login
{
    NSDictionary *loginDict = [NSDictionary dictionaryWithObjectsAndKeys:_userName.text, @"username", _passWord.text, @"password", api_Key, @"apiKey", _auth , @"authKey", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:AUTHENTICATE_NOTIFICATION object:self userInfo:loginDict];
}

#pragma mark - Check Internet Connection
-(BOOL)reachable {
    Reachability *r = [Reachability reachabilityWithHostname:@"google.com"];
    NetworkStatus internetStatus = [r currentReachabilityStatus];
    if(internetStatus == NotReachable) {
        return NO;
    }
    return YES;
}

#pragma mark - UICheckBox Method
- (IBAction)checked:(id)sender {
    if (self.rememberMe.checked) {
        NSLog(@"RememberMe Button was Checked");
    } else if (!self.rememberMe.checked) {
        NSLog(@"RememberMe Button was Unchecked");
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    [[[UIAlertView alloc] initWithTitle:@"Memory Warning" message:@"The application is storing alot of memory and the cache needs to be flushed!" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
}

@end
