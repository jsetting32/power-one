//
//  AuthenticationData.m
//  PowerOneApp
//
//  Created by John Setting on 7/16/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "AuthenticationData.h"

# pragma mark - JSON Interface
@interface NSDictionary(JSONCategories)

+(NSDictionary*)dictionaryWithContentsOfJSONURLString:(NSString*)urlAddress;
-(NSData*)toJSON;

@end

# pragma mark - JSON Implementation
@implementation NSDictionary(JSONCategories)

+ (NSDictionary*)dictionaryWithContentsOfJSONURLString:(NSString*)urlAddress
{
    NSData* data = [NSData dataWithContentsOfURL: [NSURL URLWithString: urlAddress] ];
    __autoreleasing NSError* error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    if (error != nil) return nil;
    return result;
}

- (NSData*)toJSON
{
    NSError* error = nil;
    id result = [NSJSONSerialization dataWithJSONObject:self options:kNilOptions error:&error];
    if (error != nil) return nil;
    return result;
}

@end

@interface NSURLRequest (DummyInterface)
+ (void)setAllowsAnyHTTPSCertificate:(BOOL)allow forHost:(NSString*)host;
@end

@implementation NSURLRequest (IgnoreSSL)

+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString*)host
{
	// ignore certificate errors only for this domain
	if ([host hasSuffix:@"drobnik.com"]) { return YES; }
    else { return NO; }
}

@end

@implementation AuthenticationData

+ (id)sharedManager {
    static AuthenticationData *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}


#pragma mark - Fetch Login Credentials and Information
- (NSString *)fetchAuthToken:(NSString*)url username:(NSString*)username password:(NSString*)password apiKey:(NSString*)apiKey {
    
    NSLog(@"Fetching Auth Token");
    // -------------------- Login Logic -------------------- //
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];

    //NSString *string = [[NSString alloc] initWithFormat:@"userId=%@&password=%@&fspApiKey=%@", _userName.text, _passWord.text, apiKey];
    NSString *string = [[NSString alloc] initWithFormat:@"userId=%@&password=%@&fspApiKey=%@", username, password, apiKey];

    [request setHTTPBody:[string dataUsingEncoding:NSUTF8StringEncoding]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];

    NSError *error;
    NSURLResponse *response;
    NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    return [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
}

- (void)fetchUserServiceData:(NSString*)url authKey:(NSString *)authKey {
    
    
    NSLog(@"Fetching User Services Data");
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //[request setValue:@"text/javascript" forHTTPHeaderField:@"Accept"];
    [request setValue:[NSString stringWithFormat:@"ClientLogin auth=%@", authKey] forHTTPHeaderField:@"Authorization"];
    //[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:&error];
    
    firstName = [json valueForKeyPath:@"user.firstName"];
    lastName = [json valueForKeyPath:@"user.lastName"];
    userId = [json valueForKeyPath:@"user.userId"];
    email = [json valueForKeyPath:@"user.email"];
    siteListEid = [json valueForKeyPath:@"user.siteListEid"];
}

- (BOOL)fetchAssetServiceData:(NSString*)url authKey:(NSString *)authKey
{
    NSLog(@"Fetching Asset Data");

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:[NSString stringWithFormat:@"ClientLogin auth=%@", authKey] forHTTPHeaderField:@"Authorization"];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if ([urlData isKindOfClass:[NSNull class]] || !urlData) {
        [[[UIAlertView alloc] initWithTitle:@"Connection Issue" message:@"There was an issue getting Asset Information. Please try and login again!" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil] show];
        return NO;
    } else {
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:&error];
            
        NSArray *assetArray = [json valueForKeyPath:@"getAssetInfoResponse.assets"];
        assetDictionary = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < [assetArray count]; i++)
        {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            if ([[assetArray objectAtIndex:i] valueForKeyPath:@"address"])
            {
                NSArray *array = [[NSArray alloc] initWithObjects:[[assetArray objectAtIndex:i] valueForKeyPath:@"address.city"], [[assetArray objectAtIndex:i] valueForKeyPath:@"address.country"], [[assetArray objectAtIndex:i] valueForKeyPath:@"address.countryCode"], [[assetArray objectAtIndex:i] valueForKeyPath:@"address.postalCode"], [[assetArray objectAtIndex:i] valueForKeyPath:@"address.region"], [[assetArray objectAtIndex:i] valueForKeyPath:@"address.regionCode"], [[assetArray objectAtIndex:i] valueForKeyPath:@"address.street1"], nil];
                [dict setValue:array forKeyPath:@"address"];
            }
            
            if ([[assetArray objectAtIndex:i]  valueForKeyPath:@"location"])
            {
                NSArray *array = [[NSArray alloc] initWithObjects:[[assetArray objectAtIndex:i] valueForKeyPath:@"location.latitude"], [[assetArray objectAtIndex:i] valueForKeyPath:@"location.longitude"], nil];
                [dict setValue:array forKeyPath:@"location"];
            }
            
            if ([[assetArray objectAtIndex:i]  valueForKeyPath:@"installDate"])
            {
                NSArray *array = [[NSArray alloc] initWithObjects:[[assetArray objectAtIndex:i] valueForKeyPath:@"installDate"], nil];
                [dict setValue:array forKeyPath:@"installDate"];
            }
            
            if ([[assetArray objectAtIndex:i]  valueForKeyPath:@"installedCapacities"])
            {
                NSArray *array = [[NSArray alloc] initWithObjects:[[assetArray objectAtIndex:i] valueForKeyPath:@"installedCapacities.unit"], [[assetArray objectAtIndex:i] valueForKeyPath:@"installedCapacities.value"], nil];
                [dict setValue:array forKeyPath:@"installedCapacities"];
            }
            
            if ([[assetArray objectAtIndex:i]  valueForKeyPath:@"entityId"])
            {
                [dict setValue:[[assetArray objectAtIndex:i] valueForKeyPath:@"entityId"] forKeyPath:@"entityId"];
            }
            
            if ([[assetArray objectAtIndex:i]  valueForKeyPath:@"siteId"])
            {
                [dict setValue:[[assetArray objectAtIndex:i] valueForKeyPath:@"siteId"] forKeyPath:@"siteId"];
            }
            
            if ([[assetArray objectAtIndex:i]  valueForKeyPath:@"timeZone"])
            {
                [dict setValue:[[assetArray objectAtIndex:i] valueForKeyPath:@"timeZone"] forKeyPath:@"timeZone"];
            }
            
            if ([[assetArray objectAtIndex:i]  valueForKeyPath:@"meta"])
            {
                NSArray *array = [[NSArray alloc] initWithObjects:[[assetArray objectAtIndex:i] valueForKeyPath:@"meta.displayName"], [[assetArray objectAtIndex:i] valueForKeyPath:@"meta.description"], nil];
                [dict setValue:array forKeyPath:@"meta"];
            }
            
            if (!(![[assetArray objectAtIndex:i] valueForKeyPath:@"configuredFunctions.category"] || [[[assetArray objectAtIndex:i] valueForKeyPath:@"configuredFunctions.category"] isKindOfClass:[NSNull class]]))
            {
                
                NSArray *moreData = [[assetArray objectAtIndex:i] objectForKey:@"configuredFunctions"];
                NSString *number;
                NSMutableArray *array = [[NSMutableArray alloc] init];
                for (int i = 0; i < [moreData count]; i++) {
                    if ([[[moreData objectAtIndex:i] valueForKeyPath:@"category"] isEqualToString:@"Benefit"]) {
                        NSArray *moremoreData = [moreData objectAtIndex:i];
                        number = [moremoreData valueForKeyPath:@"parameters.$"];
                        [array addObject:number];
                    }
                }
                [dict setValue:array forKeyPath:@"multipliers"];
            }
            
            [assetDictionary addObject:dict];
        }
        return YES;
    }
}


#pragma mark - Fetch Plant Data
- (void)fetchPlantData:(NSString*)url authKey:(NSString *)authKey
{
    NSLog(@"Fetching Plant Data");

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:[NSString stringWithFormat:@"ClientLogin auth=%@", authKey] forHTTPHeaderField:@"Authorization"];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:&error];
    
    NSArray *plantData = json[@"getMonitoredInfoResponse"][@"fields"];
    
    NSString *UNowValue = [[plantData objectAtIndex:0] valueForKeyPath:@"value"];
    NSString *UTodayValue = [[plantData objectAtIndex:1] valueForKeyPath:@"value"];
    NSString *UWeekValue = [[plantData objectAtIndex:2] valueForKeyPath:@"value"];
    NSString *UMonthValue = [[plantData objectAtIndex:3] valueForKeyPath:@"value"];
    NSString *ULifeTimeValue = [[plantData objectAtIndex:4] valueForKeyPath:@"value"];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    [f setMaximumFractionDigits:2];
    [f setMinimumFractionDigits:2];
    [f setRoundingMode:NSNumberFormatterRoundUp];

    NSNumber *roundedNowValue = [f numberFromString:[NSString stringWithFormat:@"%f", [UNowValue floatValue]]];
    NSNumber *roundedTodayValue = [f numberFromString:[NSString stringWithFormat:@"%f", [UTodayValue floatValue]]];
    NSNumber *roundedWeekValue = [f numberFromString:[NSString stringWithFormat:@"%f", [UWeekValue floatValue]]];
    NSNumber *roundedMonthValue = [f numberFromString:[NSString stringWithFormat:@"%f", [UMonthValue floatValue]]];
    NSNumber *roundedLifetimeValue = [f numberFromString:[NSString stringWithFormat:@"%f", [ULifeTimeValue floatValue]]];
    
    NowValue = [f stringFromNumber:roundedNowValue];
    TodayValue = [f stringFromNumber:roundedTodayValue];
    WeekValue = [f stringFromNumber:roundedWeekValue];
    MonthValue = [f stringFromNumber:roundedMonthValue];
    LifeTimeValue = [f stringFromNumber:roundedLifetimeValue];
    
    
}

#pragma mark - Fetch Weather Station Data
- (void)fetchWeatherStation:(NSString*)url authKey:(NSString *)authKey
{
    NSLog(@"Fetching WeatherStation Data");
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:[NSString stringWithFormat:@"ClientLogin auth=%@", authKey] forHTTPHeaderField:@"Authorization"];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:&error];
    
    NSArray* data = json[@"getMonitoredInfoResponse"][@"fields"];
    
    /* ----------------Set Precision of values to 2 decimal points ------------------ */
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setRoundingMode:NSNumberFormatterRoundHalfUp];
    [formatter setMaximumFractionDigits:2];
    [formatter setMinimumFractionDigits:2];
    
    //if ([[[data objectAtIndex:0] objectForKey:@"value"] isKindOfClass:[NSNull class]] || [[[data objectAtIndex:1] objectForKey:@"value"] isKindOfClass:[NSNull class]] || [[[data objectAtIndex:2] objectForKey:@"value"] isKindOfClass:[NSNull class]] || [[[data objectAtIndex:3] objectForKey:@"value"] isKindOfClass:[NSNull class]] || ![[data objectAtIndex:0] objectForKey:@"value"] || ![[data objectAtIndex:1] objectForKey:@"value"] || ![[data objectAtIndex:2] objectForKey:@"value"] || ![[data objectAtIndex:3] objectForKey:@"value"]) {
    //    NSString *checker = @"NO";
    //    [[NSNotificationCenter defaultCenter] postNotificationName:@"WeatherStation" object:checker];
    //} else {
    
    NSNumber *insolationNumber = [formatter numberFromString:[[data objectAtIndex:0] objectForKey:@"value"]];    
    NSNumber *irradianceNumber = [formatter numberFromString:[[data objectAtIndex:1] objectForKey:@"value"]];
    NSNumber *cellTempNumber = [formatter numberFromString:[[data objectAtIndex:2] objectForKey:@"value"]];
    NSNumber *ambientTempNumber = [formatter numberFromString:[[data objectAtIndex:3] objectForKey:@"value"]];
    
    insolation = [formatter stringForObjectValue:insolationNumber];
    irradiance = [formatter stringForObjectValue:irradianceNumber];
    cellTemp = [formatter stringForObjectValue:cellTempNumber];
    ambientTemp = [formatter stringForObjectValue:ambientTempNumber];
        
}

#pragma mark - Fetch Logger Data
- (void)fetchLoggerIDs:(NSString*)url authKey:(NSString *)authKey
{
    NSLog(@"Fetching Logger ID");
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:[NSString stringWithFormat:@"ClientLogin auth=%@", authKey] forHTTPHeaderField:@"Authorization"];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:&error];
    loggerIDs = [[NSArray alloc] initWithArray:[json valueForKeyPath:@"getAssetInfoResponse.assets.entityId"]];

}

- (void)fetchLoggerData:(NSString*)url authKey:(NSString *)authKey
{
    NSLog(@"Fetching Logger Data");

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:[NSString stringWithFormat:@"ClientLogin auth=%@", authKey] forHTTPHeaderField:@"Authorization"];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:&error];
    //NSLog(@"%@", json);
}

#pragma mark - Fetch Inverter Data
- (void)fetchInverterData:(NSString*)url authKey:(NSString *)authKey
{
    NSLog(@"Fetching Inverter Data");
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:[NSString stringWithFormat:@"ClientLogin auth=%@", authKey] forHTTPHeaderField:@"Authorization"];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:&error];    
    NSArray *array = [json valueForKeyPath:@"getMonitoredInfoResponse.fields.values"];
    
    for (int i = 0; i < [[array objectAtIndex:0] count]; i++)
    {
        //NSLog(@"%@", [[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@start"]);
        
        NSTimeInterval _interval= [[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@start"] doubleValue];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
        NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
        [_formatter setLocale:[NSLocale currentLocale]];
        [_formatter setDateFormat:@"dd.MM.yyyy hh:mm:ss"];
        //NSLog(@"%@",[_formatter stringFromDate:date]);
        
        //NSLog(@"%@", [[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@value"]);
    }
    //NSLog(@"%@", array);
}

#pragma mark - Fetch Graphs Data Methods
- (void)fetchPVEnergy_PVPowerDataDay:(NSString*)url authKey:(NSString *)authKey
{
    NSLog(@"Fetching Day Data");
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:[NSString stringWithFormat:@"ClientLogin auth=%@", authKey] forHTTPHeaderField:@"Authorization"];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:&error];
    //NSLog(@"%@", json);
    
    NSArray *array = [json valueForKeyPath:@"getMonitoredInfoResponse.fields.values"];
    
    NSMutableArray *graphValues = [[NSMutableArray alloc] init];
    NSMutableArray *graphDates = [[NSMutableArray alloc] init];
    
    
    for (int i = 0; i < [[array objectAtIndex:0] count]; i++)
    {
        if ([[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@value"] isKindOfClass:[NSNull class]] || ![[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@value"])
        {
            
            NSTimeInterval _interval= [[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@start"] doubleValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
            NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
            [_formatter setLocale:[NSLocale currentLocale]];
            [_formatter setDateFormat:@"hh:mm a"];
            
            [graphDates addObject:[_formatter stringFromDate:date]];
            [graphValues addObject:@"0"];
        } else {
            NSTimeInterval _interval= [[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@start"] doubleValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
            NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
            [_formatter setLocale:[NSLocale currentLocale]];
            [_formatter setDateFormat:@"hh:mm a"];
            
            [graphDates addObject:[_formatter stringFromDate:date]];
            [graphValues addObject:[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@value"]];
        }
    }

    
    NSMutableArray *maxValueArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [graphValues count]; i++)
    {
        if ([graphValues objectAtIndex:i] != [NSNull null])
        {
            NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
            [f setNumberStyle:NSNumberFormatterDecimalStyle];
            NSNumber *myNumber = [f numberFromString:[graphValues objectAtIndex:i]];
            [maxValueArray addObject:myNumber];
        }
    }
    
    NSNumber *highestNumber = [NSNumber numberWithFloat:0.0];
    
    for (NSNumber *theNumber in maxValueArray)
    {
        NSComparisonResult Val;
        Val = [theNumber compare: highestNumber];
        
        if (Val == NSOrderedSame) {}
        else if (Val == NSOrderedAscending) {}
        else if (Val == NSOrderedDescending) { highestNumber = theNumber; }
    }
    maxDayValue = highestNumber;
    graphValuesDay = graphValues;
    graphStartDateDAY = graphDates;
    
}

- (void)fetchPVEnergy_PVPowerData7D:(NSString*)url authKey:(NSString *)authKey
{
    NSLog(@"Fetching 7D Data");
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:[NSString stringWithFormat:@"ClientLogin auth=%@", authKey] forHTTPHeaderField:@"Authorization"];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:&error];
    //NSLog(@"%@", json);
    
    NSArray *array = [json valueForKeyPath:@"getMonitoredInfoResponse.fields.values"];
    
    NSMutableArray *graphValues = [[NSMutableArray alloc] init];
    NSMutableArray *graphDates = [[NSMutableArray alloc] init];
    
    
    for (int i = 0; i < [[array objectAtIndex:0] count]; i++)
    {
        if ([[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@value"] isKindOfClass:[NSNull class]] || ![[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@value"])
        {
            
            NSTimeInterval _interval= [[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@start"] doubleValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
            NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
            [_formatter setLocale:[NSLocale currentLocale]];
            [_formatter setDateFormat:@"hh:mm a"];
            
            [graphDates addObject:[_formatter stringFromDate:date]];
            [graphValues addObject:@"0"];
        } else {
            NSTimeInterval _interval= [[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@start"] doubleValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
            NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
            [_formatter setLocale:[NSLocale currentLocale]];
            [_formatter setDateFormat:@"hh:mm a"];

            [graphDates addObject:[_formatter stringFromDate:date]];
            [graphValues addObject:[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@value"]];
        }
    }
    
    NSMutableArray *maxValueArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [graphValues count]; i++)
    {
        if ([graphValues objectAtIndex:i] != [NSNull null])
        {
            NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
            [f setNumberStyle:NSNumberFormatterDecimalStyle];
            NSNumber *myNumber = [f numberFromString:[graphValues objectAtIndex:i]];
            [maxValueArray addObject:myNumber];
        }
    }
    
    NSNumber *highestNumber = [NSNumber numberWithFloat:0.0];
    
    for (NSNumber *theNumber in maxValueArray)
    {
        NSComparisonResult Val;
        Val = [theNumber compare: highestNumber];
        
        if (Val == NSOrderedSame) {}
        else if (Val == NSOrderedAscending) {}
        else if (Val == NSOrderedDescending) { highestNumber = theNumber; }
    }
    max7DValue = highestNumber;
    graphValues7D = graphValues;
    graphStartDate7D = graphDates;
    

}

- (void)fetchPVEnergy_PVPowerData30D:(NSString*)url authKey:(NSString *)authKey
{
    NSLog(@"Fetching 30D Data");
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:[NSString stringWithFormat:@"ClientLogin auth=%@", authKey] forHTTPHeaderField:@"Authorization"];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:&error];
    //NSLog(@"%@", json);
    
    NSArray *array = [json valueForKeyPath:@"getMonitoredInfoResponse.fields.values"];
    
    NSMutableArray *graphValues = [[NSMutableArray alloc] init];
    NSMutableArray *graphDates = [[NSMutableArray alloc] init];
    
    
    for (int i = 0; i < [[array objectAtIndex:0] count]; i++)
    {
        if ([[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@value"] isKindOfClass:[NSNull class]] || ![[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@value"])
        {
            
            NSTimeInterval _interval= [[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@start"] doubleValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
            NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
            [_formatter setLocale:[NSLocale currentLocale]];
            [_formatter setDateFormat:@"hh:mm a"];
            
            [graphDates addObject:[_formatter stringFromDate:date]];
            [graphValues addObject:@"0"];
        } else {
            NSTimeInterval _interval= [[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@start"] doubleValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
            NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
            [_formatter setLocale:[NSLocale currentLocale]];
            [_formatter setDateFormat:@"hh:mm a"];
            
            [graphDates addObject:[_formatter stringFromDate:date]];
            [graphValues addObject:[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@value"]];
        }
    }
    
    NSMutableArray *maxValueArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [graphValues count]; i++)
    {
        if ([graphValues objectAtIndex:i] != [NSNull null])
        {
            NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
            [f setNumberStyle:NSNumberFormatterDecimalStyle];
            NSNumber *myNumber = [f numberFromString:[graphValues objectAtIndex:i]];
            [maxValueArray addObject:myNumber];
        }
    }
    
    NSNumber *highestNumber = [NSNumber numberWithFloat:0.0];
    
    for (NSNumber *theNumber in maxValueArray)
    {
        NSComparisonResult Val;
        Val = [theNumber compare: highestNumber];
        
        if (Val == NSOrderedSame) {}
        else if (Val == NSOrderedAscending) {}
        else if (Val == NSOrderedDescending) { highestNumber = theNumber; }
    }
    max30DValue = highestNumber;
    graphValues30D = graphValues;
    graphStartDate30D = graphDates;

}

- (void)fetchPVEnergy_PVPowerData365D:(NSString*)url authKey:(NSString *)authKey
{
    NSLog(@"Fetching 365D Data");
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:[NSString stringWithFormat:@"ClientLogin auth=%@", authKey] forHTTPHeaderField:@"Authorization"];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:&error];
    //NSLog(@"%@", json);
    
    NSArray *array = [json valueForKeyPath:@"getMonitoredInfoResponse.fields.values"];
    
    NSMutableArray *graphValues = [[NSMutableArray alloc] init];
    NSMutableArray *graphDates = [[NSMutableArray alloc] init];
    
    
    for (int i = 0; i < [[array objectAtIndex:0] count]; i++)
    {
        if ([[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@value"] isKindOfClass:[NSNull class]] || ![[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@value"])
        {
            
            NSTimeInterval _interval= [[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@start"] doubleValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
            NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
            [_formatter setLocale:[NSLocale currentLocale]];
            [_formatter setDateFormat:@"hh:mm a"];
            
            [graphDates addObject:[_formatter stringFromDate:date]];
            [graphValues addObject:@"0"];
        } else {
            NSTimeInterval _interval= [[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@start"] doubleValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
            NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
            [_formatter setLocale:[NSLocale currentLocale]];
            [_formatter setDateFormat:@"hh:mm a"];
            
            [graphDates addObject:[_formatter stringFromDate:date]];
            [graphValues addObject:[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@value"]];
        }
    }
    
    NSMutableArray *maxValueArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [graphValues count]; i++)
    {
        if ([graphValues objectAtIndex:i] != [NSNull null])
        {
            NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
            [f setNumberStyle:NSNumberFormatterDecimalStyle];
            NSNumber *myNumber = [f numberFromString:[graphValues objectAtIndex:i]];
            [maxValueArray addObject:myNumber];
        }
    }

    NSNumber *highestNumber = [NSNumber numberWithFloat:0.0];
    
    for (NSNumber *theNumber in maxValueArray)
    {
        NSComparisonResult Val;
        Val = [theNumber compare: highestNumber];
        
        if (Val == NSOrderedSame) {}
        else if (Val == NSOrderedAscending) {}
        else if (Val == NSOrderedDescending) { highestNumber = theNumber; }
    }
    max365DValue = highestNumber;
    graphValues365D = graphValues;
    graphStartDate365D = graphDates;
     
}

- (void)fetchPVEnergy_PVPowerDataWTD:(NSString*)url authKey:(NSString *)authKey
{
    NSLog(@"Fetching WTD Data");
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:[NSString stringWithFormat:@"ClientLogin auth=%@", authKey] forHTTPHeaderField:@"Authorization"];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:&error];
    //NSLog(@"%@", json);
    
    NSArray *array = [json valueForKeyPath:@"getMonitoredInfoResponse.fields.values"];
    
    NSMutableArray *graphValues = [[NSMutableArray alloc] init];
    NSMutableArray *graphDates = [[NSMutableArray alloc] init];
    
    
    for (int i = 0; i < [[array objectAtIndex:0] count]; i++)
    {
        if ([[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@value"] isKindOfClass:[NSNull class]] || ![[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@value"])
        {
            
            NSTimeInterval _interval= [[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@start"] doubleValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
            NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
            [_formatter setLocale:[NSLocale currentLocale]];
            [_formatter setDateFormat:@"hh:mm a"];
            
            [graphDates addObject:[_formatter stringFromDate:date]];
            [graphValues addObject:@"0"];
        } else {
            
            NSTimeInterval _interval= [[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@start"] doubleValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
            NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
            [_formatter setLocale:[NSLocale currentLocale]];
            [_formatter setDateFormat:@"hh:mm a"];
            
            [graphDates addObject:[_formatter stringFromDate:date]];
            [graphValues addObject:[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@value"]];
        }
    }
    
    NSMutableArray *maxValueArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [graphValues count]; i++)
    {
        if ([graphValues objectAtIndex:i] != [NSNull null])
        {
            NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
            [f setNumberStyle:NSNumberFormatterDecimalStyle];
            NSNumber *myNumber = [f numberFromString:[graphValues objectAtIndex:i]];
            [maxValueArray addObject:myNumber];
        }
    }
    
    NSNumber *highestNumber = [NSNumber numberWithFloat:0.0];
    
    for (NSNumber *theNumber in maxValueArray)
    {
        NSComparisonResult Val;
        Val = [theNumber compare: highestNumber];
        
        if (Val == NSOrderedSame) {}
        else if (Val == NSOrderedAscending) {}
        else if (Val == NSOrderedDescending) { highestNumber = theNumber; }
    }
    maxWTDValue = highestNumber;
    graphValuesWTD = graphValues;
    graphStartDateWTD = graphDates;
}

- (void)fetchPVEnergy_PVPowerDataMTD:(NSString*)url authKey:(NSString *)authKey
{
    
    NSLog(@"Fetching MTD Data");
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:[NSString stringWithFormat:@"ClientLogin auth=%@", authKey] forHTTPHeaderField:@"Authorization"];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:&error];
    //NSLog(@"%@", json);
    
    NSArray *array = [json valueForKeyPath:@"getMonitoredInfoResponse.fields.values"];
    
    NSMutableArray *graphValues = [[NSMutableArray alloc] init];
    NSMutableArray *graphDates = [[NSMutableArray alloc] init];
    
    
    for (int i = 0; i < [[array objectAtIndex:0] count]; i++)
    {
        if ([[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@value"] isKindOfClass:[NSNull class]] || ![[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@value"]) {
            
            NSTimeInterval _interval= [[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@start"] doubleValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
            NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
            [_formatter setLocale:[NSLocale currentLocale]];
            [_formatter setDateFormat:@"hh:mm a"];
            
            [graphDates addObject:[_formatter stringFromDate:date]];
            [graphValues addObject:@"0"];
        } else {
        
            NSTimeInterval _interval= [[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@start"] doubleValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
            NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
            [_formatter setLocale:[NSLocale currentLocale]];
            [_formatter setDateFormat:@"hh:mm a"];
            
            [graphDates addObject:[_formatter stringFromDate:date]];
            [graphValues addObject:[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@value"]];
        }
    }
    
    NSMutableArray *maxValueArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [graphValues count]; i++)
    {
        if ([graphValues objectAtIndex:i] != [NSNull null])
        {
            NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
            [f setNumberStyle:NSNumberFormatterDecimalStyle];
            NSNumber *myNumber = [f numberFromString:[graphValues objectAtIndex:i]];
            [maxValueArray addObject:myNumber];
        }
    }
    
    NSNumber *highestNumber = [NSNumber numberWithFloat:0.0];
    
    for (NSNumber *theNumber in maxValueArray)
    {
        NSComparisonResult Val;
        Val = [theNumber compare: highestNumber];
        
        if (Val == NSOrderedSame) {}
        else if (Val == NSOrderedAscending) {}
        else if (Val == NSOrderedDescending) { highestNumber = theNumber; }
    }
    maxMTDValue = highestNumber;
    graphValuesMTD = graphValues;
    graphStartDateMTD = graphDates;
}

- (void)fetchPVEnergy_PVPowerDataYTD:(NSString*)url authKey:(NSString *)authKey
{
    NSLog(@"Fetching YTD Data");
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:[NSString stringWithFormat:@"ClientLogin auth=%@", authKey] forHTTPHeaderField:@"Authorization"];
    NSError *error;
    NSURLResponse *response;
    NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:urlData options:kNilOptions error:&error];
    //NSLog(@"%@", json);
    
    NSArray *array = [json valueForKeyPath:@"getMonitoredInfoResponse.fields.values"];
    
    NSMutableArray *graphValues = [[NSMutableArray alloc] init];
    NSMutableArray *graphDates = [[NSMutableArray alloc] init];
    
    
    for (int i = 0; i < [[array objectAtIndex:0] count]; i++)
    {
        if ([[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@value"] isKindOfClass:[NSNull class]] || ![[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@value"])
        {
            
            NSTimeInterval _interval= [[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@start"] doubleValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
            NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
            [_formatter setLocale:[NSLocale currentLocale]];
            [_formatter setDateFormat:@"hh:mm a"];
            
            [graphDates addObject:[_formatter stringFromDate:date]];
            [graphValues addObject:@"0"];
        } else {
        
            NSTimeInterval _interval= [[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@start"] doubleValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
            NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
            [_formatter setLocale:[NSLocale currentLocale]];
            [_formatter setDateFormat:@"hh:mm a"];
            
            [graphDates addObject:[_formatter stringFromDate:date]];
            [graphValues addObject:[[[array objectAtIndex:0] objectAtIndex:i] objectForKey:@"@value"]];
        }
    }
    
    NSMutableArray *maxValueArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [graphValues count]; i++)
    {
        if ([graphValues objectAtIndex:i] != [NSNull null])
        {
            NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
            [f setNumberStyle:NSNumberFormatterDecimalStyle];
            NSNumber *myNumber = [f numberFromString:[graphValues objectAtIndex:i]];
            [maxValueArray addObject:myNumber];
        }
    }
    
    NSNumber *highestNumber = [NSNumber numberWithFloat:0.0];
    
    for (NSNumber *theNumber in maxValueArray)
    {
        NSComparisonResult Val;
        Val = [theNumber compare: highestNumber];
        
        if (Val == NSOrderedSame) {}
        else if (Val == NSOrderedAscending) {}
        else if (Val == NSOrderedDescending) { highestNumber = theNumber; }
    }
    maxYTDValue = highestNumber;
    graphValuesYTD = graphValues;
    graphStartDateYTD = graphDates;
    
}


#pragma mark - SSL Certs protocol methods
- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    NSLog(@"%@", challenge);
    NSLog(@"%@", connection);
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
        if ([@[@"easyview.idev.auroravision.net"] containsObject:challenge.protectionSpace.host])
            [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    NSLog(@"%@", connection);
    NSLog(@"%@", protectionSpace);
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)ConfirmAction:(NSString *)url
{
    NSLog(@"Confirm Action");
    
    //NSData* easyViewNSData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"https://easyview.auroravision.net/easyview/services/gai/plant/devices?entityId=1167815&api_key=c302d52a-ef4c-4c35-8312-d187ee0a4c49-00a1"]];
    //NSString *urlString = @"https://www.auroravision.net/dash/services/gai/plants?entityId=1129945&api_key=c302d52a-ef4c-4c35-8312-d187ee0a4c49-00a1";
    //NSURL *aURL = [[NSURL alloc] initWithString:@"https://www.auroravision.net/dash/services/gai/plants?entityId=1129945&api_key=c302d52a-ef4c-4c35-8312-d187ee0a4c49-00a1"];
    NSString *string = @"junit.test:password";
    NSString *encode = [string base64EncodedString];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"GET"];
    [request setValue:[NSString stringWithFormat:@"Basic %@", encode] forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //[request setValue:@"" forHTTPHeaderField:@"Accept-Language"];
    //[request setValue:@"" forHTTPHeaderField:@"Connection"];
    //[request setValue:@"" forHTTPHeaderField:@"User-Agent"];
    //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:@"easyview.idev.auroravision.net"];
    NSError *error;
    NSURLResponse *response;
    [NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:[[NSURL URLWithString:url] host]];
    NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString *myString = [[NSString alloc] initWithData:urlData encoding:NSUTF8StringEncoding];
    //NSDictionary* json = [NSJSONSerialization JSONObjectWithData:easyViewNSData options:kNilOptions error:&error];
    //NSLog(@"%@", json);
}



-(NSData *)formatURL
{
    NSData *urlData;
    
    return urlData;
}

#pragma mark - Getter Methods
- (NSString *)getFirstName { return firstName; }
- (NSString *)getLastName { return lastName; }
- (NSString *)getUserId { return userId; }
- (NSString *)getEmail { return email; }
- (NSString *)getSiteListEid { return siteListEid; }
- (NSMutableArray *)getAssetDictionary { return assetDictionary; }

- (NSString *)getUniqueSiteName { return uniqueSiteName; }

- (NSString *)getNowValue{ return NowValue; }
- (NSString *)getTodayValue{ return TodayValue; }
- (NSString *)getWeekValue{ return WeekValue; }
- (NSString *)getMonthValue{ return MonthValue; }
- (NSString *)getLifeTimeValue{ return LifeTimeValue; }

- (NSArray*) getGraphValuesDay { return graphValuesDay; }
- (NSArray*) getGraphDatesDay { return graphStartDateDAY; }
- (NSNumber*) getMaxDayValue { return maxDayValue; }

- (NSArray*) getGraphValues7D { return graphValues7D; }
- (NSArray*) getGraphDates7D { return graphStartDate7D; }
- (NSNumber*) getMax7DValue { return max7DValue; }

- (NSArray*) getGraphValues30D { return graphValues30D; }
- (NSArray*) getGraphDates30D{ return graphStartDate30D; }
- (NSNumber*) getMax30DValue { return max30DValue; }

- (NSArray*) getGraphValues365D{ return graphValues365D; }
- (NSArray*) getGraphDates365D{ return graphStartDate365D; }
- (NSNumber*) getMax365DValue { return max365DValue; }

- (NSArray*) getGraphValuesWTD{ return graphValuesWTD; }
- (NSArray*) getGraphDatesWTD{ return graphStartDateWTD; }
- (NSNumber*) getMaxWTDValue { return maxWTDValue; }

- (NSArray*) getGraphValuesMTD { return graphValuesMTD; }
- (NSArray*) getGraphDatesMTD { return graphStartDateMTD; }
- (NSNumber*) getMaxMTDValue { return maxMTDValue; }

- (NSArray*) getGraphValuesYTD { return graphValuesYTD; }
- (NSArray*) getGraphDatesYTD { return graphStartDateYTD; }
- (NSNumber*) getMaxYTDValue { return maxYTDValue; }

- (NSString*) getIrradiance { return irradiance; }
- (NSString*) getInsolation { return insolation; }
- (NSString*) getCellTemp { return cellTemp; }
- (NSString*) getAmbientTemp { return ambientTemp; }

- (NSArray*) getLoggerIDs {return loggerIDs; }

@end
