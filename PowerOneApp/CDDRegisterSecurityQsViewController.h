//
//  CDDRegisterSecurityQsViewController.h
//  PowerOneApp
//
//  Created by John Setting on 7/23/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"
@class UICheckbox;

@interface CDDRegisterSecurityQsViewController : UIViewController <UIWebViewDelegate, TTTAttributedLabelDelegate, UIAlertViewDelegate, UITabBarControllerDelegate>
{
    UIViewController *webViewController;
    UIWebView *webview;
}

@property (nonatomic) BOOL isAnimating;

@property (weak, nonatomic) IBOutlet UICheckbox *termsAndPolicyCheckbox;
- (IBAction)termsAndPolicyAction:(id)sender;

@property (strong, nonatomic) IBOutlet TTTAttributedLabel *termsAndPolicyLabel;

- (IBAction)submitContactFormActionAndProceed:(id)sender;

@end
