//
//  InstallersViewController.m
//  PowerOneApp
//
//  Created by John Setting on 6/14/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "InstallersViewController.h"

@interface InstallersViewController ()

@end

@implementation InstallersViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    self.title = @"Installers Guide";
    
    SWRevealViewController *revealController = [self revealViewController];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];

    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"]
                                                                         style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    UIBarButtonItem *rightRevealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"19-gear.png"] style:UIBarButtonItemStyleBordered target:revealController action:@selector(rightRevealToggle:)];
    self.navigationItem.rightBarButtonItem = rightRevealButtonItem;
    
    
    
    self.view.frame = CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, 10000);
    //self.scrollView = [[UIScrollView alloc] init];
    //scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.bounds.size.width, self.view.bounds.size.height)];
    //scrollView.scrollEnabled = YES;
    //scrollView.pagingEnabled = YES;
    //scrollView.showsVerticalScrollIndicator = YES;
    // self scrollView
    //scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, 10000);
    //CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
    //NSLog(@"%f", scrollView.contentSize.height);
    //[scrollView setContentOffset:bottomOffset animated:YES];
    //[self.view addSubview:self.scrollView];
    
    [scroller setScrollEnabled:NO];
    [scroller setContentSize:CGSizeMake(320, 10000)];
    
    [super viewDidLoad];
    
    
    MBProgressHUD *HUD;
	HUD = [[MBProgressHUD alloc] initWithView:self.view];
	[self.view addSubview:HUD];
	HUD.delegate = self;
	[HUD showWhileExecuting:@selector(performFetchOnMainThread) onTarget:self withObject:nil animated:YES];
    


    // Do any additional setup after loading the view from its nib.
}

- (void) getTerms {
    NSURL *termAndPoliciesURL = [NSURL URLWithString:@"http://www.power-one.com/renewable-energy/installers"];
    UIWebView *webview = [[UIWebView alloc] initWithFrame:self.view.bounds];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:termAndPoliciesURL];
    [webview loadRequest:requestObj];
    webview.delegate=self;
    webview.scalesPageToFit=YES;
    [self.view addSubview:webview];
}

-(void) performFetchOnMainThread    {
    [self performSelectorOnMainThread:@selector(getTerms) withObject:nil waitUntilDone:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
