//
//  MTDGraphViewController.h
//  PowerOneApp
//
//  Created by John Setting on 6/12/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"
#import "PullEasyViewData.h"
#import "AuthenticationData.h"

@interface MTDGraphViewController : UIViewController <CPTBarPlotDataSource,CPTPlotDataSource, CPTBarPlotDelegate, CPTPlotSpaceDelegate>
{
    PullEasyViewData *graphData;
    AuthenticationData *authData;

}
@property (nonatomic, strong) CPTGraphHostingView *hostView;
@property (nonatomic, strong) NSString *authToken;

@property (nonatomic, strong) CPTPlotSpaceAnnotation *priceAnnotation;
@end
