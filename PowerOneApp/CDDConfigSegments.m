//
//  CDDConfigSegments.m
//  PowerOneApp
//
//  Created by John Setting on 7/22/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "CDDConfigSegments.h"

@interface CDDConfigSegments ()

@end

@implementation CDDConfigSegments

@synthesize segmentedViewControllers, segmentedControl, activeViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    /* This bunch of code creates the segmentedControllerButtons in the nav bar */
    self.segmentedViewControllers = [self segmentedViewControllerContent];
    
    NSArray * segmentTitles = @[@"Plant", @"Net", @"Wiz", @"Date", @"Clone", @"GF/E"];
    
    self.segmentedControl = [[UISegmentedControl alloc]initWithItems:segmentTitles];
    self.segmentedControl.selectedSegmentIndex = 0;
    self.segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
    
    [self.segmentedControl addTarget:self action:@selector(didChangeSegmentControl:) forControlEvents:UIControlEventValueChanged];
    
    self.navigationItem.titleView = self.segmentedControl;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.segmentedControl.tintColor = [UIColor redColor];
    [self didChangeSegmentControl:self.segmentedControl]; // kick everything off
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSArray *)segmentedViewControllerContent {
    
    CDDConfigPlantViewController *plant = [[CDDConfigPlantViewController alloc] initWithNibName:@"CDDConfigPlantViewController" bundle:nil];
    plant->ipAddress = ipAddress;
    plant->encode = encodedInfo;
    CDDConfigNetworkViewController *network = [[CDDConfigNetworkViewController alloc] initWithNibName:@"CDDConfigNetworkViewController" bundle:nil];
    network->ipAddress = ipAddress;
    network->encode = encodedInfo;
    CDDConfigAcquisitionWizardViewController *acquisition_wizard = [[CDDConfigAcquisitionWizardViewController alloc] initWithNibName:@"CDDConfigAcquisitionWizardViewController" bundle:nil];
    acquisition_wizard->ipAddress = ipAddress;
    acquisition_wizard->encode = encodedInfo;
    CDDConfigDateTimeViewController *date_time = [[CDDConfigDateTimeViewController alloc] initWithNibName:@"CDDConfigDateTimeViewController" bundle:nil];
    date_time->ipAddress = ipAddress;
    date_time->encode = encodedInfo;
    CDDConfigCDDCloneViewController *cdd_clone = [[CDDConfigCDDCloneViewController alloc] initWithNibName:@"CDDConfigCDDCloneViewController" bundle:nil];
    cdd_clone->ipAddress = ipAddress;
    cdd_clone->encode = encodedInfo;
    CDDConfigGroundfaultEnergyViewController *groundfault_energy = [[CDDConfigGroundfaultEnergyViewController alloc] initWithNibName:@"CDDConfigGroundfaultEnergyViewController" bundle:nil];
    groundfault_energy->ipAddress = ipAddress;
    groundfault_energy->encode = encodedInfo;
    
    NSArray * controllers = [NSArray arrayWithObjects:plant, network, acquisition_wizard, date_time, cdd_clone, groundfault_energy, nil];
    
    return controllers;
}



#pragma mark -
#pragma mark Segment control

- (void)didChangeSegmentControl:(UISegmentedControl *)control {
    if (self.activeViewController) {
        [self.activeViewController viewWillDisappear:NO];
        [self.activeViewController.view removeFromSuperview];
        [self.activeViewController viewDidDisappear:NO];
    }
    
    
    self.activeViewController = [self.segmentedViewControllers objectAtIndex:control.selectedSegmentIndex];
    
    
    [self.activeViewController viewWillAppear:YES];
    [self.view addSubview:self.activeViewController.view];
    [self.activeViewController viewDidAppear:YES];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.activeViewController viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.activeViewController viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.activeViewController viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.activeViewController viewDidDisappear:animated];
}

@end
