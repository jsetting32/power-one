//
//  SliderGraphsView.h
//  PowerOneApp
//
//  Created by John Setting on 6/19/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SliderPageControl.h"
#import "DayGraphViewController.h"
#import "7DGraphViewController.h"
#import "30DGraphViewController.h"
#import "365DGraphViewController.h"
#import "WTDGraphViewController.h"
#import "MTDGraphViewController.h"
#import "YTDGraphViewController.h"

@interface SliderGraphsView : UIView <SliderPageControlDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) SliderPageControl *sliderPageControl;
@property (nonatomic, strong) NSMutableArray *demoContent;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, assign) BOOL pageControlUsed;
@property (nonatomic, strong) NSString *authToken;

- (void)slideToCurrentPage:(bool)animated;
- (void)changeToPage:(int)page animated:(BOOL)animated;

+(NSArray *)getTimes;
+(void)getTimes:(NSArray *)setTimes;
@end
