//
//  ViewController.h
//  new
//
//  Created by John Setting on 6/1/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"
#import "AuthenticationData.h"
#import "PullEasyViewData.h"
@interface DayGraphViewController : UIViewController <CPTPlotDataSource, CPTScatterPlotDelegate, CPTScatterPlotDataSource, CPTPlotSpaceDelegate>
{
    NSMutableArray *times;
    NSMutableArray *datapoints;
    AuthenticationData *authData;
    PullEasyViewData *graphData;
}

@property (nonatomic, strong) NSMutableArray *times;
@property (nonatomic, strong) NSMutableArray *datapoints;

@property (nonatomic, strong) NSString *authToken;

@property (nonatomic, strong) CPTGraphHostingView *hostView;

+(NSArray *)getTimes;
+(void)getTimes:(NSArray *)setTimes;

@end
