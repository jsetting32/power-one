//
//  CDDConfigSegments.h
//  PowerOneApp
//
//  Created by John Setting on 7/22/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDDConfigPlantViewController.h"
#import "CDDConfigNetworkViewController.h"
#import "CDDConfigAcquisitionWizardViewController.h"
#import "CDDConfigDateTimeViewController.h"
#import "CDDConfigCDDCloneViewController.h"
#import "CDDConfigGroundfaultEnergyViewController.h"

@interface CDDConfigSegments : UIViewController
{
    UISegmentedControl    * segmentedControl;
    UIViewController      * activeViewController;
    NSArray               * segmentedViewControllers;
    
    
    @public
    NSString *ipAddress;
    NSString *encodedInfo;
}

@property (nonatomic) BOOL isAnimating;

@property (nonatomic, strong) UISegmentedControl * segmentedControl;
@property (nonatomic, strong) UIViewController   * activeViewController;
@property (nonatomic, strong) NSArray            * segmentedViewControllers;


@end
