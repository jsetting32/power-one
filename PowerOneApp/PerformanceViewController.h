//
//  PerformanceViewController.h
//  PowerOneApp
//
//  Created by John Setting on 6/21/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "KGModal.h"

@interface PerformanceViewController : UIViewController <UIGestureRecognizerDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    UISlider *slider;
    NSTimer *timer; // Doesn't Work
}

@property (weak, nonatomic) IBOutlet UIImageView *imageView;


@property (nonatomic, strong) NSMutableArray *views;

@property (nonatomic, strong) NSArray *inverterDates;
@property (nonatomic, strong) NSDictionary *inverterValuesAndNames;
@property (nonatomic, strong) NSArray *inverterValues;
//@property (nonatomic, strong) UILabel *label;

@property (nonatomic, retain) NSNumber *number;

@property (nonatomic, strong) UILabel *dates;

@property (nonatomic, weak) UIView *pieceForReset;


@end
