//
//  PowerOneTutorial.h
//  PowerOneApp
//
//  Created by John Setting on 7/6/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLCycleScrollView.h"

@interface PowerOneTutorial : UIViewController <XLCycleScrollViewDelegate, XLCycleScrollViewDatasource>
{
    
    XLCycleScrollView *csView;
}

@end
