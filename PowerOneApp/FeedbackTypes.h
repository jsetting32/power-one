//
//  FeedbackTypes.h
//  PowerOneApp
//
//  Created by John Setting on 6/26/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SendFeedBackViewController.h"

@protocol SendFeedBackDelegate;

@interface FeedbackTypes : UITableViewController
{
    id<SendFeedBackDelegate> delegate;
}

@property (nonatomic, assign) id<SendFeedBackDelegate> delegate;

@end
