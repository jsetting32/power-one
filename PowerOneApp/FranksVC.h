//
//  FranksVC.h
//  PowerOneApp
//
//  Created by John Setting on 6/10/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#import "SWRevealViewController.h"
#import "MBProgressHUD.h"
#import "CustomBadge.h"
#import "TDBadgedCell.h"
#import "PullEasyViewData.h"
#import "EGORefreshTableHeaderView.h"
#import "YWHelper.h"
#import "SliderGraphsView.h"
#import "EnvironmentalsSubView.h"
#import "SharingActivityProvider.h"

#import "PowerOneLoginTabBar.h"
#import "PowerOneLogin.h"


#import "PowerViewController.h"
#import "PerformanceViewController.h"
#import "WeatherViewController.h"
#import "NotificationsViewController.h"
#import "PowerCalculatorViewController.h"
#import "MainNotificationNames.h"

#import "KGModal.h"

#import "AuthenticationData.h"  
#import "REMenu.h"
#import "PortfolioViewController.h"


@interface FranksVC : UIViewController <MBProgressHUDDelegate, EGORefreshTableHeaderDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate>
{
    
    NSString *loadMainView;

    MBProgressHUD *HUD;
    
    NSString *sunrise;
    NSString *sunset;
    
    EGORefreshTableHeaderView *_refreshHeaderView;
    BOOL _reloading;
    UIScrollView *scroller;
    
    PullEasyViewData *pulledData;
    AuthenticationData *authData;
    
    FranksVC *mainView;
    PlantCanvasController *canvas;
    
    UIActivityIndicatorView *indicatorView;
    
    NSMutableArray  *photoTitles;         // Titles of images
    NSMutableArray  *photoSmallImageData; // Image data (thumbnail)
    NSMutableArray  *photoURLsLargeImage; // URL to larger image
    
    NSFetchedResultsController *fetchedResultsController_;
    NSManagedObjectContext *managedObjectContext_;
    
    @public
        NSString *subject;
    
}

@property (strong, readonly, nonatomic) REMenu *menu;

- (void)toggleMenu;

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, strong) NSString *entityID;
@property (nonatomic, strong) NSString *uniqueSiteName;
@property (nonatomic, strong) NSArray *systemSize;
@property (nonatomic, strong) NSArray *location;

@property (nonatomic, strong) FranksVC *mainView;
@property (nonatomic, strong) PlantCanvasController *canvas;

- (void) reloadTableViewDataSource;
- (void) doneLoadingTableViewData;

@property (nonatomic, strong) YWForecast * weather;

@property (nonatomic, strong) UIView *SystemInfoView;
@property (nonatomic, strong) UIImageView *SystemStatusIcon;
@property (nonatomic, strong) UIButton *SystemStatusIconButton;
@property (nonatomic, strong) UILabel *SystemName;

@property (nonatomic, strong) UIButton *PowerNowView;
@property (nonatomic, strong) UILabel *StaticPowerNowLabel;
@property (nonatomic, strong) UILabel *PowerNow;
@property (nonatomic, strong) UILabel *PowerNowUnits;
@property (nonatomic, strong) UIImageView *MeterBar;

@property (nonatomic, strong) UIButton *PerformanceNowView;
@property (nonatomic, strong) UILabel *StaticPerformanceNowLabel;
@property (nonatomic, strong) UILabel *PerformanceNow;
@property (nonatomic, strong) UIImageView *PerformanceIcon;

@property (nonatomic, strong) UIButton *WeatherNowView;
@property (nonatomic, strong) UILabel *StaticWeatherNowLabel;
@property (nonatomic, strong) UILabel *WeatherNow;
@property (nonatomic, strong) UILabel *WeatherHigh;
@property (nonatomic, strong) UILabel *WeatherLow;
@property (nonatomic, strong) UIImageView *WeatherIcon;

@property (nonatomic, strong) SliderGraphsView *sliderGraphsView;
@property (nonatomic, strong) SliderPageControl *sliderGraphView;

@property (nonatomic, strong) UIView *EnvironmentalsGradientLabel;
@property (nonatomic, strong) EnvironmentalsSubView *EnvironmentalsView;

@property (nonatomic, strong) UIView *FooterView;
@property (nonatomic, strong) UILabel *siteAddress;
@property (nonatomic, strong) UILabel *SiteSystemSizeLabel;
@property (nonatomic, strong) NSString *siteSystemSize;

- (UIView *) returnSystemInfoView;
- (UIButton *) returnPowerNowView;
- (UIButton *) returnPerformanceNowView;
- (UIButton *) returnWeatherNowView;

- (SliderGraphsView *) returnSliderGraphsView;

- (UIView *) returnEnvironmentalsGradientLabel;
- (EnvironmentalsSubView *) returnEnvironmentalsView;
- (UIView *) returnFooterView;

- (void) makeCallbacks;

+ (NSString*)setAuthenticationKey;
+ (void)setAuthenticationKey:(NSString*)withAuthKey;

+ (NSArray*)setRegionAndCity;
+ (void)setRegionAndCity:(NSArray*)withRegionAndCity;

+ (NSString*)setInstallDate;
+ (void)setInstallDate:(NSString*)withInstallDate;

@end