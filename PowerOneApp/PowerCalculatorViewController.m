//
//  PowerCalculatorViewController.m
//  PowerOneApp
//
//  Created by John Setting on 6/21/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "PowerCalculatorViewController.h"
#define hierarchyRequest [NSURL URLWithString: @"https://easyview.auroravision.net/easyview/services/gai/plant/hierarchy.json?entityId=1167815&v=1.4.7"]

@interface PowerCalculatorViewController ()

@end


@implementation PowerCalculatorViewController

@synthesize residentialCheckbox = _residentialCheckbox;
@synthesize commercialCheckbox = _commercialCheckbox;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    

    NSLog(@"%@", setInstallDateString);
    
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];

    _costOfInstallment.delegate = self;
    _zipCode.delegate = self;
    _monthlyEstimate.delegate = self;
    _kWhUsagePerMonth.delegate = self;
    
    _installDate.text = [NSString stringWithFormat:@"Install Date: %@",setInstallDateString];
    //NSLog(@"%@", [energyCosts getEntityInstallDate]);
    //self.title = @"Solar Electric Estimate";
    
    UILabel* tlabel=[[UILabel alloc] initWithFrame:CGRectMake(30,0, 260, 40)];
    tlabel.text = @"Solar Electric Estimate";
    tlabel.font = [UIFont systemFontOfSize:20.0f];
    tlabel.textAlignment = NSTextAlignmentCenter;
    tlabel.textColor=[UIColor whiteColor];
    tlabel.backgroundColor =[UIColor clearColor];
    //tlabel.adjustsFontSizeToFitWidth=YES;
    self.navigationItem.titleView = tlabel;
    
    
    SWRevealViewController *revealController = [self revealViewController];
    
    //[self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    //[self.view addGestureRecognizer:revealController.panGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"] style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    UIBarButtonItem *rightRevealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"] style:UIBarButtonItemStyleBordered target:revealController action:@selector(rightRevealToggle:)];
    
    self.navigationItem.rightBarButtonItem = rightRevealButtonItem;
    
    
    self.residentialCheckbox.checked = FALSE;
    self.residentialCheckbox.disabled = FALSE;
    
    self.commercialCheckbox.checked = FALSE;
    self.commercialCheckbox.disabled = FALSE;
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if (textField == _zipCode) _zipCode.text = textField.text;
    else if(textField == _monthlyEstimate) _monthlyEstimate.text = textField.text;
    else if (textField == _kWhUsagePerMonth) _kWhUsagePerMonth.text = textField.text;
    else if (textField == _costOfInstallment) _costOfInstallment.text = textField.text;
    
    return NO;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqual:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}


- (IBAction)GO {
    
    energyCosts = [[PullEasyViewData alloc] init];
    
    //NSLog(@"goButton pressed");
    
    if ([_zipCode.text isEqualToString:@""] || [_monthlyEstimate.text isEqualToString:@""]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"Please Fill in Your Information in the corresponding textfields" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
    } else if ((self.residentialCheckbox.checked = FALSE) && (self.commercialCheckbox.checked = FALSE)){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"Please Check a box (either residential or commercial" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [alert show];
    } else {
        //NSLog(@"%@ : %@", _zipCode.text, _monthlyEstimate.text);
        //NSLog(@"Residential Checker = %@", (self.residentialCheckbox.checked) ? @"YES" : @"NO");
        //NSLog(@"Commercial Checker = %@", (self.commercialCheckbox.checked) ? @"YES" : @"NO");
        
        // -------------------- Get JSON Object from EIA (Energy Costs) -------------------------- //
        _choice = @"RES";
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"Please Fill in Your Information in the corresponding textfields" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        
        // -------------------- Get JSON Object from hierarchyData -------------------------- //
        NSData* hierarchyNSData = [NSData dataWithContentsOfURL:hierarchyRequest];
        if (hierarchyNSData == nil) {
            NSLog(@"Hierarchy Data is nil");
            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        } else {
            //[pulledData performSelectorOnMainThread:@selector(fetchHierarchyRequestData:) withObject:hierarchyNSData waitUntilDone:NO];
            [energyCosts performSelectorOnMainThread:@selector(fetchHierarchyRequestData:) withObject:hierarchyNSData waitUntilDone:YES];
        }
        // -------------------- Get JSON Object from hierarchyData -------------------------- //
        
        
        // -------------------- Get JSON Object from EIA (Energy Costs) -------------------------- //
        NSURL *energyCostsURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.eia.gov/series/data/?api_key=FF691F5BE67A113AC55F3454BAD7F59F&series_id=ELEC.PRICE.%@-%@.M", [energyCosts getSiteRegionCode], _choice]];
        NSData* energyCostsData = [NSData dataWithContentsOfURL:energyCostsURL];
        if (energyCostsData == nil) {
            NSLog(@"Energy Costs Data is nil");
        } else {
            [energyCosts performSelectorOnMainThread:@selector(fetchEnergyCosts:) withObject:energyCostsData waitUntilDone:YES];
        }
        //NSLog(@"%@", [energyCosts getEnergyCosts]);
        //NSLog(@"%@", [NSString stringWithFormat:@"%@", [energyCosts getEnergyCostsDate]]);
        // -------------------- Get JSON Object from EIA (Energy Costs) -------------------------- //

        
        
        
        UIViewController *controller = [[UIViewController alloc] init];
        [self.navigationController pushViewController:controller animated:YES];
    }
}


- (IBAction)boxCheck:(id)sender {
    if (self.residentialCheckbox.checked) {
        self.commercialCheckbox.disabled = TRUE;
        self.commercialCheckbox.checked = FALSE;
    } else if (!self.residentialCheckbox.checked) {
        self.commercialCheckbox.disabled = FALSE;
    }
    
    if (self.commercialCheckbox.checked) {
        self.residentialCheckbox.disabled = TRUE;
        self.residentialCheckbox.checked = FALSE;
    } else if (!self.commercialCheckbox.checked) {
        self.residentialCheckbox.disabled = FALSE;
    }
}



static NSString* setInstallDateString;

#pragma mark - Class methods
+ (NSString*)installDateString
{
    return setInstallDateString;
}

+ (void)setInstallDateString:(NSString*)withNewInstallDateString
{
    if (setInstallDateString != withNewInstallDateString) {
    	//[str release];
    	setInstallDateString = [withNewInstallDateString copy];
    }
}


@end
