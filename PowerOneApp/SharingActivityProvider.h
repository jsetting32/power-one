//
//  SharingActivityProvider.h
//  PowerOneAuroraApp
//
//  Created by John Setting on 5/21/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface SharingActivityProvider : UIActivityItemProvider
{
}

@property (nonatomic, strong) NSString *CarbonOffsetAcresSavings;
@property (nonatomic, strong) NSString *CarbonOffsetMetricTonsSavings;
@property (nonatomic, strong) NSString *CO2Savings;
@property (nonatomic, strong) NSString *ComputerSavings;
@property (nonatomic, strong) NSString *EntityName;
@property (nonatomic, strong) NSString *SO2Savings;
@property (nonatomic, strong) NSString *TVSavings;
@property (nonatomic, strong) NSString *NOXSavings;
@property (nonatomic, strong) NSString *CarSavings;

@end