//
//  LoggerTableViewController.h
//  PowerOneApp
//
//  Created by John Setting on 7/10/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TBXML.h"
#import "ASIHTTPRequest.h"
#import "AccelerometerSolarInstallationViewController.h"

@interface LoggerTableViewController : UITableViewController < CLLocationManagerDelegate>
{
    UIActivityIndicatorView *scroller;
    NSMutableDictionary *configData;
    NSMutableDictionary *statusData;
    
    NSOperationQueue *_queue;
    NSArray *_feeds;
    
    NSMutableArray *_allEntries;
    
    NSMutableArray *_configEntries;
    NSMutableArray *_statusEntries;
    
    CLLocationManager *locationManager;
    float lat;
    float lon;
    
    
    @public
        NSString *ipAddress;

}

@property (retain) NSOperationQueue *queue;
@property (retain) NSArray *feeds;
@property (retain) NSMutableArray *configEntries;
@property (retain) NSMutableArray *statusEntries;
@property (retain) NSMutableArray *allEntries;

@property (nonatomic, strong) id aKey;
@property (nonatomic, strong) id keys;
@property (nonatomic, strong) id anObject;

@property (nonatomic, strong) NSMutableDictionary *configData;
@property (nonatomic, strong) NSMutableDictionary *statusData;

//+ (NSMutableDictionary *) getConfigInfo : (NSMutableDictionary *)configData;
//+ (NSMutableDictionary *) getStatusInfo : (NSMutableDictionary *)statusData;


@end
