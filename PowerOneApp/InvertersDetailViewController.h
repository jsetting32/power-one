//
//  InvertersDetailViewController.h
//  PowerOneApp
//
//  Created by John Setting on 6/18/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface InvertersDetailViewController : UIViewController
{
    NSString *_valuesString;
    NSString *_dateString;
    NSMutableArray *numbers;
    UISlider *slider;
}

@property (nonatomic, strong) NSArray *inverterDates;
@property (nonatomic, strong) NSArray *inverterValues;
@property (nonatomic, strong) UILabel *label;

@property (nonatomic, copy) NSString *valuesString;
@property (nonatomic, copy) NSString *dateString;
@property (nonatomic, weak) IBOutlet UILabel *myInverterValue;
@property (weak, nonatomic) IBOutlet UILabel *myInverterDates;

@end
