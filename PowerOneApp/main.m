
//
//  main.m
//  PowerOneApp
//
//  Created by John Setting on 6/7/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
