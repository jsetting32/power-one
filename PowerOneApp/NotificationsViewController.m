//
//  NotificationsViewController.m
//  PowerOneApp
//
//  Created by John Setting on 6/27/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "NotificationsViewController.h"

@interface NotificationsViewController ()

@end

@implementation NotificationsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    
    
    NSLog(@"%i", [setStatusData count]);
    
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [setStatusData count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    NotificationsCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NotificationsCustomCell" owner:nil options:nil];
        for(id currentObject in topLevelObjects)
        {
            if([currentObject isKindOfClass:[NotificationsCustomCell class]])
            {
                cell = (NotificationsCustomCell *)currentObject;
                break;
            }
        }
    }
    
    //NSArray *array = [setStatusData objectAtIndex:indexPath.row];
    //_keys = [[statusData getSystemStatusAlerts]  allKeys];
    //_aKey = [_keys objectAtIndex:indexPath.row];
    //_anObject = [[statusData getSystemStatusAlerts]  objectForKey:_aKey];
    
    //NSLog(@"%i", [[statusData getSystemStatusAlerts] count]);
    
    
    cell.EventTitle.text = [NSString stringWithFormat:@"%@",[[setStatusData objectAtIndex:indexPath.section] valueForKey:@"name"]];
    if ([[[setStatusData objectAtIndex:indexPath.section]  valueForKey:@"eventGroup"] isEqualToString:@"OEM"])
    { cell.avatarImage.image = [UIImage imageNamed:@"medium.png"]; }
    else { cell.avatarImage.image = [UIImage imageNamed:@"high.png"]; }
    cell.EventDescription.text = [NSString stringWithFormat:@"%@", [[setStatusData objectAtIndex:indexPath.section]  valueForKey:@"code"]];
    cell.EventStartTime.text = [NSString stringWithFormat:@"%@", [[setStatusData objectAtIndex:indexPath.section]  valueForKey:@"formattedStartTime"]];
    cell.avatarName.text = [NSString stringWithFormat:@"%@", [[setStatusData objectAtIndex:indexPath.section]  valueForKey:@"entityId"]];
    
    
    //cell.EventTitle.text = [NSString stringWithFormat:@"%@",_aKey];
    //cell.avatarImage.image = [statusData getSystemStatusIcon];
    //cell.EventDescription.text = [NSString stringWithFormat:@"%@", [_anObject objectAtIndex:1]];
    //cell.EventStartTime.text = [NSString stringWithFormat:@"%@", [_anObject objectAtIndex:2]];
    //cell.avatarName.text = [NSString stringWithFormat:@"%@", [_anObject objectAtIndex:0]];
    //cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [_anObject objectAtIndex:3]];
    //cell.textLabel.text = [statusData getSystemStatusAlerts]
    
    return cell;
}

- (NSString *)stringCleaner:(NSString *)yourString {
    
    NSScanner *theScanner;
    NSString *text = nil;
    
    theScanner = [NSScanner scannerWithString:yourString];
    
    while ([theScanner isAtEnd] == NO) {
        
        [theScanner scanUpToString:@"(" intoString:NULL] ;
        
        [theScanner scanUpToString:@")" intoString:&text] ;
        
        yourString = [yourString stringByReplacingOccurrencesOfString:
                      [NSString stringWithFormat:@"%@)", text]
                                                           withString:@""];
        
    }
    
    return yourString;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 108;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


static NSArray* setStatusData;

#pragma mark - Class methods
+ (NSArray*)getStatusData
{
    return setStatusData;
}

+ (void)setStatusData:(NSArray*)withStatusData
{
    if (setStatusData != withStatusData) {
    	//[str release];
    	setStatusData = [withStatusData copy];
    }
}




@end
