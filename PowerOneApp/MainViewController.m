//
//  MainViewController.m
//  PowerOneAuroraApp
//
//  Created by John Setting on 5/28/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "MainViewController.h"




#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) //1

#define easyViewData [NSURL URLWithString: @"https://easyview.auroravision.net/easyview/services/gmi/summary/PlantEnergy.json?eids=1167815&tz=US%2FMountain&hasUsage=false&dateLabel=E+MMM+d%2C+yyyy+h%3Amm%3Ass+a+z&locale=en&v=1.3.2"] //2

#define hierarchyRequest [NSURL URLWithString: @"https://easyview.auroravision.net/easyview/services/gai/plant/hierarchy.json?entityId=1167815&v=1.3.2"]

#define weatherStationRequest [NSURL URLWithString: @"https://easyview.auroravision.net/easyview/services/gmi/summary/WeatherStation.json?eids=1167815&fields=Irradiance%2CCumIrradiance%2CCellTemp%2CAmbientTemp&locale=&tz=US%2FMountain&v=1.3.2"]

#define servicesSummaryRequest [NSURL URLWithString: @"https://easyview.auroravision.net/easyview/services/gmi/summary/PlantEnergy.json?eids=1167815&tz=US%2FMountain&hasUsage=false&dateLabel=E+MMM+d%2C+yyyy+h%3Amm%3Ass+a+z&locale=en&v=1.3.2"]

#define servicesDocumentRequest [NSURL URLWithString: @"https://easyview.auroravision.net/easyview/services/document/1167815/plant_layout_canvas_config?version=1&v=1.3.2&_=1370148563070"]

#define easyViewStatus [NSURL URLWithString: @"https://easyview.auroravision.net/easyview/services/events/status/plant/easyview.json?plantEId=1167815&tz=US%2FMountain&v=1.4.7"]

#define plantLayoutCanvasBackground [NSURL URLWithString: @"https://easyview.auroravision.net/easyview/services/document/1167815/plant_layout_canvas_background"]



#define degreesToRadians(x) (M_PI * x / 180.0)


# pragma mark - JSON Interface
@interface NSDictionary(JSONCategories)

+(NSDictionary*)dictionaryWithContentsOfJSONURLString:(NSString*)urlAddress;
-(NSData*)toJSON;

@end

# pragma mark - JSON Implementation
@implementation NSDictionary(JSONCategories)

+(NSDictionary*)dictionaryWithContentsOfJSONURLString:(NSString*)urlAddress
{
    NSData* data = [NSData dataWithContentsOfURL: [NSURL URLWithString: urlAddress] ];
    __autoreleasing NSError* error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    if (error != nil) return nil;
    return result;
}

-(NSData*)toJSON
{
    NSError* error = nil;
    id result = [NSJSONSerialization dataWithJSONObject:self options:kNilOptions error:&error];
    if (error != nil) return nil;
    return result;
}

@end


#pragma mark -
#pragma mark Implementation

@implementation MainViewController

@synthesize solarStatus;// = _solarStatus;
@synthesize fields;// = _fields;
@synthesize nowType;// = _type;
@synthesize nowField;// = _field;
@synthesize nowLabel;// = _label;
@synthesize nowEntityID;// = _entityID;
@synthesize nowEntityName;// = _entityName;
@synthesize nowTimeZone;// = _timeZone;
@synthesize nowUnits;// = _units;
@synthesize nowParameters;// = _parameters;
@synthesize nowParam_Value;// = _param_Value;
@synthesize nowParam_Name;// = _param_Name;
@synthesize nowStart;// = _start;
@synthesize nowStartLabel;// = _startLabel;
@synthesize nowEnd;// = _end;
@synthesize nowEndLabel;// = _endLabel;
@synthesize nowSampleStart;// = _start;
@synthesize nowSampleStartLabel;// = _startLabel;
@synthesize nowSampleEnd;// = _end;
@synthesize nowSampleEndLabel;// = _endLabel;
@synthesize nowValue;

@synthesize maxGaugeValue;
@synthesize currentGaugeValue;

//@synthesize loginView = _loginView;


#pragma mark - View lifecycle

-(id) init
{
    self = [super init];
    if (self) {
        // Custom initialization
        
        
        
    }
    return self;
}






#pragma mark - Core Location Methods


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}


- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    
    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    
    NSString *point = [NSString stringWithFormat:@"coordinates %@", NSStringFromCGPoint(location)];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:point delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Done" otherButtonTitles:nil, nil];
    
    [actionSheet showInView:self.view];
}


- (IBAction)pushToPowerOneWebsite:(id)sender
{
    
    /*
    UIViewController *webViewController = [[UIViewController alloc] init];
    UIWebView *uiWebView = [[UIWebView alloc] initWithFrame: CGRectMake(0,0,320,420)];
    
    // Produces Error "CGContextSetLineDash: invalid dash array: at least one element must be non-zero" but ignore it
    // Causes no issues with the application execution
    [uiWebView loadRequest:[NSURLRequest requestWithURL: [NSURL URLWithString:@"https://easyview.auroravision.net/easyview/index.html?entityId=1167815"]]];
    
    uiWebView.scalesPageToFit = YES;
    [webViewController.view addSubview:uiWebView];
    [self.navigationController pushViewController:webViewController animated:YES];
    */
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

#pragma mark - CLLocationManager Delegate Methods

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    
    
    float latitudeValue = newLocation.coordinate.latitude;
    NSLog(@"Latitude--------%f",latitudeValue);
    
    float longitudeValue = newLocation.coordinate.longitude;
    NSLog(@"Longitude--------%f", longitudeValue);
    
    [locationManager stopUpdatingLocation];
    
    [geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        if (error == nil && [placemarks count] > 0) {
            placemark = [placemarks lastObject];
            _zipCode = [[NSString alloc] init];

            _zipCode = placemark.postalCode;
            NSLog(@"%@", [NSString stringWithFormat:@"%@ ", placemark.postalCode]);
            NSLog(@"placemark: %d", [placemark.postalCode intValue]);
            
        } else {
            NSLog(@"%@", error.debugDescription);
        }
    } ];
    
    //NSLog(@"%@", [YWHelper getWOEID:latitudeValue longitude:longitudeValue]);// yahooAPIKey:@"10ad604a55fc5801bb880c1e06e35f24e68fd3a5"]);
}

#pragma mark - Popover Views

/*

- (void)inverters:(id)sender
{
    InvertersTableViewController *invertersView = [[InvertersTableViewController alloc] initWithStyle:UITableViewStylePlain];
    invertersView.title = @"Inverters";
    invertersView.delegate = self;
    popover = [[FPPopoverController alloc] initWithViewController:invertersView];
    popover.contentSize = CGSizeMake(310, 420);
    [popover presentPopoverFromView:sender];
}

- (void)modules:(id)sender
{
    ModulesTableViewController *modulesView = [[ModulesTableViewController alloc] initWithStyle:UITableViewStylePlain];
    modulesView.delegate = self;
    modulesView.title = @"Modules";
    popover = [[FPPopoverController alloc] initWithViewController:modulesView];
    popover.contentSize = CGSizeMake(310, 420);
    [popover presentPopoverFromView:sender];
}





- (void)notifications:(id)sender
{

    NotificationsTableViewController *notificationsView = [[NotificationsTableViewController alloc] initWithStyle:UITableViewStylePlain];
    notificationsView.delegate = self;
    notificationsView.title = @"Events";
    popover = [[FPPopoverController alloc] initWithViewController:notificationsView];
    popover.contentSize = CGSizeMake(310, 420);
    [popover presentPopoverFromView:sender];

}

*/
 

- (void)share:(UIButton *)share
{
    SharingActivityProvider *sharing = [[SharingActivityProvider alloc] init];
    
    NSArray *array = @[sharing];
    
    UIActivityViewController *activity = [[UIActivityViewController alloc] initWithActivityItems:array applicationActivities:nil];
    
    activity.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePostToWeibo];
    
    activity.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:activity animated:YES completion:nil];
    
}




#pragma mark - PopOver Push Functions

- (void)selectedShare
{
    [popover dismissPopoverAnimated:YES];
    
    //ShareViewController *shareView = [[ShareViewController alloc] init];
    
}


/*
- (void)selectedInverters:(NSUInteger) rowNumber
{
    NSLog(@"%lu", (unsigned long)rowNumber);
    [popover dismissPopoverAnimated:YES];
    
    InvertersDetailView *inverterDetailView = [[InvertersDetailView alloc] initWithNibName:@"InvertersDetailView" bundle:nil];
    
    [self.navigationController pushViewController:inverterDetailView animated:YES];
}



- (void)selectedModules:(NSUInteger) rowNumber
{
    NSLog(@"%lu", (unsigned long)rowNumber);
    [popover dismissPopoverAnimated:YES];
    
    //ModulesDetailView *modulesDetailView = [[ModulesDetailView alloc] initWithNibName:@"ModulesDetailView" bundle:nil];
    
    //[self.navigationController pushViewController:modulesDetailView animated:YES];

    DayGraphViewController *scatter = [[DayGraphViewController alloc] init];
    [self.navigationController pushViewController:scatter animated:YES];
 
}


- (void)selectedNotifications:(NSUInteger) rowNumber
{
    NSLog(@"%lu", (unsigned long)rowNumber);
    [popover dismissPopoverAnimated:YES];
    
    //NotificationsDetailView *notificationsDetailView = [[NotificationsDetailView alloc] initWithNibName:@"NotificationsDetailView" bundle:nil];
    
    //[self.navigationController pushViewController:notificationsDetailView animated:YES];

    _0DGraphViewController *bar = [[_0DGraphViewController alloc] init];
    [self.navigationController pushViewController:bar animated:YES];
}
*/




+(NSArray *)getMonthData
{
    NSArray *array;
    
    return array;
}


#pragma mark - View Life Cycle

- (void)viewDidAppear:(BOOL)animated
{
    
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
 
    
       
}

- (void)viewDidLoad
{
    
        
    SWRevealViewController *revealController = [self revealViewController];
    
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];

    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"]
                                                                         style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    UIBarButtonItem *rightRevealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"]
                                                                              style:UIBarButtonItemStyleBordered target:revealController action:@selector(rightRevealToggle:)];
    
    self.navigationItem.rightBarButtonItem = rightRevealButtonItem;
    
    UIImageView *iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"easyview-logo.png"]];

    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationItem.titleView = iv;
    
    
    
    // Create a PullEasyViewData object to access it's instance (-) methods
    PullEasyViewData *pulledData = [[PullEasyViewData alloc] init];
    
    
    
    // An asyncronous call on the main queue/thread of the application
    // Purpose: Used to allow the application to load successfully with allocated information
    // NSData objects are created to hold the JSON objects of the URLs (faster processing speeds
    // than storing as regular JSON objects and several class methods are called from "PullEasyViewData.h/.m"
    // to perform the methods implementation of parsing the JSON obejcts from NSData objects
    dispatch_async(dispatch_get_main_queue(), ^{
        

        // Briefing of whats going on
        // "pulled Data" is the object we are performing the current METHOD from on the main queue/thread
        // and we call a selector method "fetchEasyViewData" defined in pulledData whose object is the NSData
        // object defined on the previous line, we also wait until the method is done to move onto the next
        // line of code the compiler will execute
        NSData* easyViewNSData = [NSData dataWithContentsOfURL: easyViewData];
        [pulledData performSelectorOnMainThread:@selector(fetchEasyViewData:) withObject:easyViewNSData waitUntilDone:YES];
        
        NSData* weatherStationNSData = [NSData dataWithContentsOfURL: weatherStationRequest];
        [pulledData performSelectorOnMainThread:@selector(fetchWeatherStationRequestData:) withObject:weatherStationNSData waitUntilDone:YES];
        
        NSData* servicesSummaryNSData = [NSData dataWithContentsOfURL: servicesSummaryRequest];
        [pulledData performSelectorOnMainThread:@selector(fetchServicesSummaryRequestData:) withObject:servicesSummaryNSData waitUntilDone:YES];
        
        NSData* servicesDocumentNSData = [NSData dataWithContentsOfURL: servicesDocumentRequest];
        [pulledData performSelectorOnMainThread:@selector(fetchServicesDocumentRequestData:) withObject:servicesDocumentNSData waitUntilDone:YES];
        
        NSData* easyViewStatusNSData = [NSData dataWithContentsOfURL: easyViewStatus];
        [pulledData performSelectorOnMainThread:@selector(fetchEasyViewStatus:) withObject:easyViewStatusNSData waitUntilDone:YES];
        
        NSData* hierarchyNSData = [NSData dataWithContentsOfURL: hierarchyRequest];
        [pulledData performSelectorOnMainThread:@selector(fetchHierarchyRequestData:) withObject:hierarchyNSData waitUntilDone:YES];
        
        // Another asyncronous call to allow for the instance methods to finish their job in getting information
        // Once the information is fetched ... the labels text field is set to the respective text.
        dispatch_async(dispatch_get_main_queue(), ^{
            _entityName.text = [pulledData getEntityName];
            _energyGeneratedTodayData.text = [pulledData getTodayValue];
            _energyGeneratedWeekData.text = [pulledData getWeekValue];
            _energyGeneratedMonthData.text = [pulledData getMonthValue];
            _energyGeneratedLifetimeData.text = [pulledData getLifeValue];
            _InsolationValue.text = [pulledData getInsolation];
            _IrradianceValue.text = [pulledData getIrradiance];
            _CellTempValue.text = [pulledData getCellTemp];
            _AmbientTempValue.text = [pulledData getAmbientTemp];
            _PowerUnitsInKiloWattHours.text = [NSString stringWithFormat:@"%f", [pulledData getRealGuageValue]];
        });
    });
    
    

    //self.view.backgroundColor = [UIImage imageNamed:@"brown_solid.jpg"];
    UIColor *color = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"wallpaper.jpg"]];
    self.view.backgroundColor = color;
    
    _weatherCurrentCode = [[UILabel alloc] init];
    _weatherCurrentCondition = [[UILabel alloc] init];
    _weatherCurrentTemp = [[UILabel alloc] init];
    _weatherIconImageView = [[UIImageView alloc] init];
    _weatherCurrentDate = [[UILabel alloc] init];
    _weatherCurrentTemp = [[UILabel alloc] init];
    _weatherTodaysHigh = [[UILabel alloc] init];
    _weatherTodaysLow = [[UILabel alloc] init];
    _weatherCurrentState = [[UILabel alloc] init];
    _weatherCurrentCity = [[UILabel alloc] init];
    
    

    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    

    

    
    
    _entityName = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 300, 15)];
    [_entityName setFont:[UIFont fontWithName:@"Arial-BoldMT" size:12.0f]];
    _entityName.textAlignment = NSTextAlignmentCenter;
    _entityName.textColor = [UIColor yellowColor];
    _entityName.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_entityName];
    
    
    _StaticSystemStatus = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 20)];
    _SystemStatusIcon = [[UIImageView alloc] initWithFrame:CGRectMake(30, 33, 60, 55)];
    _SystemStatus = [[UILabel alloc] initWithFrame:CGRectMake(10, 90, 100, 20)];
    
    _StaticPowerText = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 15)];
    _UpdatedForCurrentPowerTime = [[UILabel alloc] initWithFrame:CGRectMake(10, 25, 100, 15)];
    _PowerUnitsInKiloWattHours = [[UILabel alloc] initWithFrame:CGRectMake(10, 40, 100, 20)];
    
    _StaticCarbonOffsetText = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 20)];
    _CarbonOffsetMetricTonsUnits = [[UILabel alloc] initWithFrame:CGRectMake(10, 30, 100, 20)];
    _CarbonTreeImage = [[UIImageView alloc] initWithFrame:CGRectMake(30, 50, 60, 30)];
    _CarbonOffsetAcresUnits = [[UILabel alloc] initWithFrame:CGRectMake(10, 90, 100, 20)];
    _CurrentDateText = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 15)];
    _EnergyIcon = [[UIImageView alloc] initWithFrame:CGRectMake(45, 40, 30, 40)];
    _KilowattHoursUnitsText = [[UILabel alloc] initWithFrame:CGRectMake(10, 85, 100, 15)];
    _StaticKilowattHoursUnit = [[UILabel alloc] initWithFrame:CGRectMake(10, 100, 100, 10)];
    
    _StaticEnvironmentalsFromWeatherStationText = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 15)];
    _UpdatedForCurrentEnvironmentsTime = [[UILabel alloc] initWithFrame:CGRectMake(10, 25, 100, 20)];
    
    
    _WeMoDevices = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 20)];
    _WeMoImage = [[UIImageView alloc] initWithFrame:CGRectMake(30, 30, 60, 60)];
    
    

    
    _IrradianceValue = [[UILabel alloc] initWithFrame:CGRectMake(50, 30, 100, 20)];
    _InsolationValue = [[UILabel alloc] initWithFrame:CGRectMake(50, 70, 100, 20)];
    _CellTempValue = [[UILabel alloc] initWithFrame:CGRectMake(175, 30, 90, 20)];
    _AmbientTempValue = [[UILabel alloc] initWithFrame:CGRectMake(175, 70, 90, 20)];
    
    
    
    

    
    YWForecast *forecast = [YWHelper getWeather:94947];
    
    _weatherCurrentCode.text = [NSString stringWithFormat:@"%i", forecast.currentCode];
    NSData *data = [YWHelper getWeatherIcon:forecast.currentCode];
    
    UIImage *forecastImage = [UIImage imageWithData:data];
    
    _weatherIconImageView.image = forecastImage;
    _weatherCurrentCondition.text = forecast.currentCondition;
    _weatherCurrentDate.text = forecast.currentDate;
    _weatherCurrentTemp.text = [NSString stringWithFormat:@"%i", forecast.currentTemp];
    _weatherTodaysLow.text = [NSString stringWithFormat:@"L: %i", forecast.todayLow];
    _weatherTodaysHigh.text = [NSString stringWithFormat:@"H: %i", forecast.todayHigh];
    _weatherCurrentState.text = forecast.currentState;
    _weatherCurrentCity.text = forecast.currentCity;
    
    
    
    
#pragma mark - Left Navigation Button Selector
    //navigationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //[navigationButton setBackgroundImage:[UIImage imageNamed:@"lines.png"] forState:UIControlStateNormal];
    //[navigationButton setFrame:CGRectMake(10, 10, 25, 25)];
    //[navigationButton addTarget:self action:@selector(sidebarButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    //[navBar addSubview:navigationButton];
    
#pragma mark - Right Navigation Button Selector
    //shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //[shareButton setBackgroundImage:[UIImage imageNamed:@"ActionButton.png"] forState:UIControlStateNormal];
    //[shareButton setFrame:CGRectMake(270, 7, 40, 30)];
    //[shareButton addTarget:self action:@selector(share:) forControlEvents:UIControlEventTouchUpInside];
    //[navBar addSubview:shareButton];
    
    /*
#pragma mark - Middle Navigation Buttons
    modules = [UIButton buttonWithType:UIButtonTypeCustom];
    [modules setBackgroundImage:[UIImage imageNamed:@"80uSn9_web.gif"] forState:UIControlStateNormal];
    [modules setFrame:CGRectMake(100, 7, 30, 30)];
    [modules addTarget:self action:@selector(modules:) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:modules];
    
    inverters = [UIButton buttonWithType:UIButtonTypeCustom];
    [inverters setFrame:CGRectMake(145, 8, 30, 30)];
    [inverters setBackgroundImage:[UIImage imageNamed:@"solar_panel_icon.png"] forState:UIControlStateNormal];
    [inverters addTarget:self action:@selector(inverters:) forControlEvents:UIControlEventTouchUpInside];
    [navBar addSubview:inverters];
    
    events = [UIButton buttonWithType:UIButtonTypeCustom];
    [events setBackgroundImage:[UIImage imageNamed:@"c11f6261c76e.png"] forState:UIControlStateNormal];
    //[events. setBadgeValue:@"42"];
    [events setFrame:CGRectMake(190, 7, 30, 30)];
    [events addTarget:self action:@selector(notifications:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    CustomBadge *customBadge1 = [CustomBadge customBadgeWithString:@"2"  withStringColor:[UIColor whiteColor]	withInsetColor:[UIColor redColor]	withBadgeFrame:YES  withBadgeFrameColor:[UIColor whiteColor] withScale:1.0 withShining:YES];
	[customBadge1 setFrame:CGRectMake(15,-5,20,20)];
    [events addSubview:customBadge1];
     [navBar addSubview:events];
    */
    
    /*
    
    //SideBarViewController * sidebarViewController = [[SideBarViewController alloc] initWithStyle:UITableViewStylePlain];
    //BOASidebarNavigationController * sidebarNavigationController = [[BOASidebarNavigationController alloc] initWithRootViewController:sidebarViewController];
    //[self addSidebar:sidebarNavigationController];
    
    
    
    PowerOneLogin * loginView = [[PowerOneLogin alloc] initWithNibName:@"PowerOneLogin" bundle:nil];
    [self presentViewController:loginView animated:NO completion:nil];
    
     
     */
    
    
    
    
    /* -------------------------System Status View----------------------------- */
    statusView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    //statusView.backgroundColor = [UIColor brownColor];
    statusView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:statusView];
    
    _StaticSystemStatus = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, 100, 20)];
    _StaticSystemStatus.text = @"System Status";
    [_StaticSystemStatus setFont:[UIFont fontWithName:@"Arial-BoldMT" size:12.0f]];
    _StaticSystemStatus.textAlignment = NSTextAlignmentCenter;
    //_StaticSystemStatus.textColor = [UIColor blackColor];
    _StaticSystemStatus.textColor = [UIColor yellowColor];
    _StaticSystemStatus.backgroundColor = [UIColor clearColor];
    [statusView addSubview:_StaticSystemStatus];
    
    _SystemStatusIcon = [[UIImageView alloc] initWithFrame:CGRectMake(20, 25, 60, 55)];
    _SystemStatusIcon.backgroundColor = [UIColor clearColor];
    [statusView addSubview:_SystemStatusIcon];
    
    _SystemStatus = [[UILabel alloc] initWithFrame:CGRectMake(0, 80, 100, 20)];
    [_SystemStatus setFont:[UIFont fontWithName:@"Arial-BoldMT" size:12.0f]];
    _SystemStatus.textAlignment = NSTextAlignmentCenter;
    //_SystemStatus.textColor = [UIColor blackColor];
    _SystemStatus.textColor = [UIColor yellowColor];
    _SystemStatus.backgroundColor = [UIColor clearColor];
    [statusView addSubview:_SystemStatus];
    /* -------------------------System Status View----------------------------- */
    
    
    
    /* -----------------------------Date View--------------------------------- */
    dateView = [[UIView alloc] initWithFrame:CGRectMake(100, 20, 120, 80)];
    //dateView.backgroundColor = [UIColor orangeColor];
    dateView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:dateView];
    
    _currentDayStringText = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, 120, 20)];
    [_currentDayStringText setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10.0f]];
    _currentDayStringText.textAlignment = NSTextAlignmentCenter;
    _currentDayStringText.textColor = [UIColor yellowColor];
    _currentDayStringText.backgroundColor = [UIColor clearColor];
    [dateView addSubview:_currentDayStringText];
    
    _currentDayNumberText = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, 120, 30)];
    [_currentDayNumberText setFont:[UIFont fontWithName:@"Arial-BoldMT" size:24.0f]];
    _currentDayNumberText.textAlignment = NSTextAlignmentCenter;
    _currentDayNumberText.textColor = [UIColor yellowColor];
    _currentDayNumberText.backgroundColor = [UIColor clearColor];
    [dateView addSubview:_currentDayNumberText];
    
    _currentMonthText = [[UILabel alloc] initWithFrame:CGRectMake(0, 60, 120, 20)];
    [_currentMonthText setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10.0f]];
    _currentMonthText.textAlignment = NSTextAlignmentCenter;
    _currentMonthText.textColor = [UIColor yellowColor];
    _currentMonthText.backgroundColor = [UIColor clearColor];
    [dateView addSubview:_currentMonthText];
    
    NSDate *date = [NSDate date];
    NSString *str = [date descriptionWithLocale:[NSLocale currentLocale]];
    NSArray *chunks = [str componentsSeparatedByString: @","];
    
    NSTimeZone * tz = [NSTimeZone localTimeZone];
    CFAbsoluteTime at = CFDateGetAbsoluteTime((CFDateRef)date);
    int day = CFAbsoluteTimeGetGregorianDate(at, (__bridge CFTimeZoneRef)tz).day;
    int month = CFAbsoluteTimeGetGregorianDate(at, (__bridge CFTimeZoneRef)tz).month;
    
    NSUInteger i = month;
    NSDateFormatter *df = [NSDateFormatter new];
    // change locale if the standard is not what you want
    NSArray *monthNames = [df standaloneMonthSymbols];
    NSString *monthName = [monthNames objectAtIndex:(i - 1)];
    
    _currentDayStringText.text = [chunks objectAtIndex:0];
    _currentDayNumberText.text = [NSString stringWithFormat:@"%i", day];
    _currentMonthText.text = [NSString stringWithFormat:@"%@", monthName];
    
    /* -----------------------------Date View--------------------------------- */
    
    
    
    /* -------------------------------Power Gauge View------------------------------*/
    gagueView = [[UIView alloc] initWithFrame:CGRectMake(220, 0, 100, 100)];
    //gagueView.backgroundColor = [UIColor yellowColor];
    gagueView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:gagueView];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        CALayer *tachLayer = [CALayer layer];
        tachLayer.bounds = CGRectMake(0, 0, 90, 75);
        tachLayer.position = CGPointMake(50, 87);
        tachLayer.contents = (id)[UIImage imageNamed:@"speed.png"].CGImage;
        [gagueView.layer addSublayer:tachLayer];
        
        CALayer *pinLayer = [CALayer layer];
        pinLayer.bounds = CGRectMake(0, 0, 20, 18);
        pinLayer.contents = (id)[UIImage imageNamed:@"pin.png"].CGImage;
        pinLayer.position = CGPointMake(44, 37);
        pinLayer.anchorPoint = CGPointMake(1.0, 1.0);
        pinLayer.transform = CATransform3DRotate(pinLayer.transform, degreesToRadians(-55), 0, 0, 1);
        [tachLayer addSublayer:pinLayer];
        
        CABasicAnimation *rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        rotationAnimation.toValue = [NSNumber numberWithFloat:degreesToRadians(145)];
        rotationAnimation.duration = 2.0f;
        rotationAnimation.autoreverses = YES;
        rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        [pinLayer addAnimation:rotationAnimation forKey:@"revItUpAnimation"];
        
        pinLayer.transform = CATransform3DRotate(pinLayer.transform, degreesToRadians([pulledData getRealGuageValue]), 0, 0, 1);
    });
    
    _StaticPowerText = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, 100, 15)];
    _StaticPowerText.text = @"Power Right Now";
    [_StaticPowerText setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10.0f]];
    _StaticPowerText.textAlignment = NSTextAlignmentCenter;
    _StaticPowerText.textColor = [UIColor yellowColor];
    _StaticPowerText.backgroundColor = [UIColor clearColor];
    [gagueView addSubview:_StaticPowerText];
    
    _UpdatedForCurrentPowerTime = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, 100, 15)];
    [_UpdatedForCurrentPowerTime setFont:[UIFont fontWithName:@"Arial" size:6.0f]];
    _UpdatedForCurrentPowerTime.textAlignment = NSTextAlignmentCenter;
    _UpdatedForCurrentPowerTime.textColor = [UIColor yellowColor];
    _UpdatedForCurrentPowerTime.backgroundColor = [UIColor clearColor];
    [gagueView addSubview:_UpdatedForCurrentPowerTime];
    
    _PowerUnitsInKiloWattHours = [[UILabel alloc] initWithFrame:CGRectMake(0, 32, 100, 20)];
    [_PowerUnitsInKiloWattHours setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10.0f]];
    _PowerUnitsInKiloWattHours.textAlignment = NSTextAlignmentCenter;
    _PowerUnitsInKiloWattHours.textColor = [UIColor yellowColor];
    _PowerUnitsInKiloWattHours.backgroundColor = [UIColor clearColor];
    [gagueView addSubview:_PowerUnitsInKiloWattHours];
    
    //_CanvasMonitorImage = [[UIImageView alloc] initWithFrame:CGRectMake(5, 50, 90, 50)];
    //_CanvasMonitorImage.image = [UIImage imageNamed:@"canvas.png"];
    //[gagueView addSubview:_CanvasMonitorImage];
    
    /* --------------------------- Gauge Implementation ----------------------------- */
    


    /*

    */

    /* --------------------------- Gauge Implementation ----------------------------- */
    
    /* -------------------------------Power Gauge View------------------------------*/
    
    
    
    /* ----------------------- Environmentals View ---------------------------------*/
    environmentalsView = [[UIView alloc] initWithFrame:CGRectMake(0, 100, 320, 98)];
    //environmentalsView.backgroundColor = [UIColor grayColor];
    environmentalsView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:environmentalsView];
    
    _sunIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 30, 40, 40)];
    _sunIcon.image = [UIImage imageNamed:@"sun-yellow.png"];
    [environmentalsView addSubview:_sunIcon];
    
    _IrradianceText = [[UILabel alloc] initWithFrame:CGRectMake(62, 10, 75, 20)];
    _IrradianceText.text = @"Irradiance";
    [_IrradianceText setFont:[UIFont fontWithName:@"Arial-BoldMT" size:12.0f]];
    _IrradianceText.textAlignment = NSTextAlignmentCenter;
    _IrradianceText.textColor = [UIColor yellowColor];
    _IrradianceText.backgroundColor = [UIColor clearColor];
    [environmentalsView addSubview:_IrradianceText];
    
    
    [_IrradianceValue setFont:[UIFont fontWithName:@"Arial" size:12.0f]];
    _IrradianceValue.textAlignment = NSTextAlignmentCenter;
    _IrradianceValue.textColor = [UIColor yellowColor];
    _IrradianceValue.backgroundColor = [UIColor clearColor];
    [environmentalsView addSubview:_IrradianceValue];
    
    _InsolationText = [[UILabel alloc] initWithFrame:CGRectMake(62, 50, 75, 20)];
    _InsolationText.text = @"Insolation";
    [_InsolationText setFont:[UIFont fontWithName:@"Arial-BoldMT" size:12.0f]];
    _InsolationText.textAlignment = NSTextAlignmentCenter;
    _InsolationText.textColor = [UIColor yellowColor];
    _InsolationText.backgroundColor = [UIColor clearColor];
    [environmentalsView addSubview:_InsolationText];
    
    
    [_InsolationValue setFont:[UIFont fontWithName:@"Arial" size:12.0f]];
    _InsolationValue.textAlignment = NSTextAlignmentCenter;
    _InsolationValue.textColor = [UIColor yellowColor];
    _InsolationValue.backgroundColor = [UIColor clearColor];
    [environmentalsView addSubview:_InsolationValue];
    
    _CellTempText = [[UILabel alloc] initWithFrame:CGRectMake(183, 10, 75, 20)];
    [_CellTempText setFont:[UIFont fontWithName:@"Arial-BoldMT" size:12.0f]];
    _CellTempText.text = @"Cell Temp.";
    _CellTempText.textAlignment = NSTextAlignmentCenter;
    _CellTempText.textColor = [UIColor yellowColor];
    _CellTempText.backgroundColor = [UIColor clearColor];
    [environmentalsView addSubview:_CellTempText];
    
    
    [_CellTempValue setFont:[UIFont fontWithName:@"Arial" size:12.0f]];
    _CellTempValue.textAlignment = NSTextAlignmentCenter;
    _CellTempValue.textColor = [UIColor yellowColor];
    _CellTempValue.backgroundColor = [UIColor clearColor];
    [environmentalsView addSubview:_CellTempValue];
    
    _AmbientTempText = [[UILabel alloc] initWithFrame:CGRectMake(175, 50, 90, 20)];
    [_AmbientTempText setFont:[UIFont fontWithName:@"Arial-BoldMT" size:12.0f]];
    _AmbientTempText.text = @"Ambient Temp.";
    _AmbientTempText.textAlignment = NSTextAlignmentCenter;
    _AmbientTempText.textColor = [UIColor yellowColor];
    _AmbientTempText.backgroundColor = [UIColor clearColor];
    [environmentalsView addSubview:_AmbientTempText];
    
    
    [_AmbientTempValue setFont:[UIFont fontWithName:@"Arial" size:12.0f]];
    _AmbientTempValue.textAlignment = NSTextAlignmentCenter;
    _AmbientTempValue.textColor = [UIColor yellowColor];
    _AmbientTempValue.backgroundColor = [UIColor clearColor];
    [environmentalsView addSubview:_AmbientTempValue];
    
    _thermometerIcon = [[UIImageView alloc] initWithFrame:CGRectMake(270, 30, 40, 40)];
    _thermometerIcon.image = [UIImage imageNamed:@"temp-red.png"];
    [environmentalsView addSubview:_thermometerIcon];
    /* ----------------------- Environmentals View ---------------------------------*/
    
    
    /* ----------------------- Power Generation View ------------------------------------*/
    powerGenerationView = [[UIView alloc] initWithFrame:CGRectMake(0, 198, 320, 78)];
    //powerGenerationView.backgroundColor = [UIColor greenColor];
    powerGenerationView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:powerGenerationView];
    
    _EnergyIcon.image = [UIImage imageNamed:@"up.png"];
    _EnergyIcon.frame = CGRectMake(10, 5, 40, 55);
    [powerGenerationView addSubview:_EnergyIcon];
    
    _StaticEnergyText = [[UILabel alloc] initWithFrame:CGRectMake(110, 5, 110, 20)];
    _StaticEnergyText.text = @"Energy Generation";
    [_StaticEnergyText setFont:[UIFont fontWithName:@"Arial-BoldMT" size:12.0f]];
    _StaticEnergyText.textColor = [UIColor yellowColor];
    _StaticEnergyText.backgroundColor = [UIColor clearColor];
    [powerGenerationView addSubview:_StaticEnergyText];
    
    _energyGeneratedToday = [[UILabel alloc] initWithFrame:CGRectMake(70, 35, 50, 20)];
    _energyGeneratedToday.text = @"Today";
    [_energyGeneratedToday setFont:[UIFont fontWithName:@"Arial-BoldMT" size:12.0f]];
    _energyGeneratedToday.textAlignment = NSTextAlignmentCenter;
    _energyGeneratedToday.textColor = [UIColor yellowColor];
    _energyGeneratedToday.backgroundColor = [UIColor clearColor];
    [powerGenerationView addSubview:_energyGeneratedToday];
    
    _energyGeneratedTodayData = [[UILabel alloc] initWithFrame:CGRectMake(57, 50, 60, 20)];
    [_energyGeneratedTodayData setFont:[UIFont fontWithName:@"Arial" size:10.0f]];
    _energyGeneratedTodayData.textAlignment = NSTextAlignmentCenter;
    _energyGeneratedTodayData.textColor = [UIColor yellowColor];
    _energyGeneratedTodayData.backgroundColor = [UIColor clearColor];
    [powerGenerationView addSubview:_energyGeneratedTodayData];
    
    _energyGeneratedWeek = [[UILabel alloc] initWithFrame:CGRectMake(125, 35, 50, 20)];
    _energyGeneratedWeek.text = @"Week";
    [_energyGeneratedWeek setFont:[UIFont fontWithName:@"Arial-BoldMT" size:12.0f]];
    _energyGeneratedWeek.textAlignment = NSTextAlignmentCenter;
    _energyGeneratedWeek.textColor = [UIColor yellowColor];
    _energyGeneratedWeek.backgroundColor = [UIColor clearColor];
    [powerGenerationView addSubview:_energyGeneratedWeek];
    
    _energyGeneratedWeekData = [[UILabel alloc] initWithFrame:CGRectMake(120, 50, 60, 20)];
    [_energyGeneratedWeekData setFont:[UIFont fontWithName:@"Arial" size:10.0f]];
    _energyGeneratedWeekData.textAlignment = NSTextAlignmentCenter;
    _energyGeneratedWeekData.textColor = [UIColor yellowColor];
    _energyGeneratedWeekData.backgroundColor = [UIColor clearColor];
    [powerGenerationView addSubview:_energyGeneratedWeekData];
    
    _energyGeneratedMonth = [[UILabel alloc] initWithFrame:CGRectMake(190, 35, 50, 20)];
    _energyGeneratedMonth.text = @"Month";
    [_energyGeneratedMonth setFont:[UIFont fontWithName:@"Arial-BoldMT" size:12.0f]];
    _energyGeneratedMonth.textAlignment = NSTextAlignmentCenter;
    _energyGeneratedMonth.textColor = [UIColor yellowColor];
    _energyGeneratedMonth.backgroundColor = [UIColor clearColor];
    [powerGenerationView addSubview:_energyGeneratedMonth];
    
    _energyGeneratedMonthData = [[UILabel alloc] initWithFrame:CGRectMake(185, 50, 60, 20)];
    [_energyGeneratedMonthData setFont:[UIFont fontWithName:@"Arial" size:10.0f]];
    _energyGeneratedMonthData.textAlignment = NSTextAlignmentCenter;
    _energyGeneratedMonthData.textColor = [UIColor yellowColor];
    _energyGeneratedMonthData.backgroundColor = [UIColor clearColor];
    [powerGenerationView addSubview:_energyGeneratedMonthData];
    
    _energyGeneratedLifetime = [[UILabel alloc] initWithFrame:CGRectMake(257, 35, 50, 20)];
    _energyGeneratedLifetime.text = @"Lifetime";
    [_energyGeneratedLifetime setFont:[UIFont fontWithName:@"Arial-BoldMT" size:12.0f]];
    _energyGeneratedLifetime.textAlignment = NSTextAlignmentCenter;
    _energyGeneratedLifetime.textColor = [UIColor yellowColor];
    _energyGeneratedLifetime.backgroundColor = [UIColor clearColor];
    [powerGenerationView addSubview:_energyGeneratedLifetime];
    
    _energyGeneratedLifetimeData = [[UILabel alloc] initWithFrame:CGRectMake(245, 50, 75, 20)];
    [_energyGeneratedLifetimeData setFont:[UIFont fontWithName:@"Arial" size:10.0f]];
    _energyGeneratedLifetimeData.textAlignment = NSTextAlignmentCenter;
    _energyGeneratedLifetimeData.textColor = [UIColor yellowColor];
    _energyGeneratedLifetimeData.backgroundColor = [UIColor clearColor];
    [powerGenerationView addSubview:_energyGeneratedLifetimeData];
    /* ----------------------- Power Generation View ------------------------------------*/
    
    
    /* -------------------------------- Weather View ------------------------------------*/
    weatherView = [[UIView alloc] initWithFrame:CGRectMake(0, 276, 120, 140)];
    //weatherView.backgroundColor = [UIColor blueColor];
    weatherView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:weatherView];
    
    _weatherIconImageView.frame = CGRectMake(10, 20, 20, 20);
    _weatherIconImageView.backgroundColor = [UIColor redColor];
    [weatherView addSubview:_weatherIconImageView];
    
    _weatherCurrentCondition.frame = CGRectMake(35, 20, 50, 20);
    [_weatherCurrentCondition setFont:[UIFont fontWithName:@"Arial" size:12.0f]];
    _weatherCurrentCondition.textColor = [UIColor yellowColor];
    _weatherCurrentCondition.backgroundColor = [UIColor clearColor];
    [weatherView addSubview:_weatherCurrentCondition];
    
    _weatherCurrentCity.frame = CGRectMake(20, 2, 70, 20);
    [_weatherCurrentCity setFont:[UIFont fontWithName:@"Arial" size:10.0f]];
    _weatherCurrentCity.textColor = [UIColor yellowColor];
    _weatherCurrentCity.backgroundColor = [UIColor clearColor];
    [weatherView addSubview:_weatherCurrentCity];
    
    _weatherCurrentState.frame = CGRectMake(70, 2, 50, 20);
    [_weatherCurrentState setFont:[UIFont fontWithName:@"Arial" size:10.0f]];
    _weatherCurrentState.textColor = [UIColor yellowColor];
    _weatherCurrentState.backgroundColor = [UIColor clearColor];
    [weatherView addSubview:_weatherCurrentState];
    
    _weatherTodaysHigh.frame = CGRectMake(12, 40, 40, 20);
    [_weatherTodaysHigh setFont:[UIFont fontWithName:@"Arial" size:14.0f]];
    _weatherTodaysHigh.textColor = [UIColor yellowColor];
    _weatherTodaysHigh.backgroundColor = [UIColor clearColor];
    [weatherView addSubview:_weatherTodaysHigh];
    
    _weatherTodaysLow.frame = CGRectMake(50, 40, 40, 20);
    [_weatherTodaysLow setFont:[UIFont fontWithName:@"Arial" size:14.0f]];
    _weatherTodaysLow.textColor = [UIColor yellowColor];
    _weatherTodaysLow.backgroundColor = [UIColor clearColor];
    [weatherView addSubview:_weatherTodaysLow];
    
    _weatherCurrentTemp.frame = CGRectMake(10, 60, 80, 60);
    [_weatherCurrentTemp setFont:[UIFont fontWithName:@"Arial" size:70.0f]];
    _weatherCurrentTemp.textColor = [UIColor yellowColor];
    _weatherCurrentTemp.backgroundColor = [UIColor clearColor];
    [weatherView addSubview:_weatherCurrentTemp];
    
    //forecast.currentDate, forecast.currentTemp, forecast.currentCode, forecast.currentCondition
    //UIImageView *weather = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"yahoo-weather-app-is-beautiful.jpg"]];
    //weather.frame = CGRectMake(0, 0, 120, 120);
    //[weatherView addSubview:weather];
    /* -------------------------------- Weather View ------------------------------------*/
    
    
    
    /* -------------------------------- Graph View --------------------------------------*/
    
    graphView = [[UIView alloc] initWithFrame:CGRectMake(100, 276, 220, 140)];
    //graphView.backgroundColor = [UIColor redColor];
    graphView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:graphView];
    
    
    
    //CPTGraphHostingView *host = [(CPTGraphHostingView *) [CPTGraphHostingView alloc] initWithFrame:CGRectMake(0, 0, 220, 140)];
    //host.backgroundColor = [UIColor redColor];
    
    
    DayGraphViewController *scatterPlot = [[DayGraphViewController alloc] init];
    scatterPlot.view.frame = CGRectMake(0, 0, 220, 140);
    [graphView addSubview:scatterPlot.view];
    
    
    //BarGraphViewController *barPlot = [[BarGraphViewController alloc] init];
    //barPlot.view.frame = CGRectMake(0, 0, 220, 140);
    //[graphView addSubview:barPlot.view];
    
    //NSArray *itemArray = [NSArray arrayWithObjects: @"D", @"W", @"M", @"Y", nil];
    //UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:itemArray];
    //segmentedControl.frame = CGRectMake(50, 115, 125, 25);
    //segmentedControl.segmentedControlStyle = UISegmentedControlStylePlain;
    //segmentedControl.selectedSegmentIndex = 2;
    //[scatterPlot.view addSubview:segmentedControl];
    //[barPlot.view addSubview:segmentedControl];
    
     
     
    /* -------------------------------- Graph View --------------------------------------*/
    
}




@end
