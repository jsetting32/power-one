//
//  CDDTabBarViewController.m
//  PowerOneApp
//
//  Created by John Setting on 7/19/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "CDDTabBarViewController.h"

@interface CDDTabBarViewController ()

@end

@implementation CDDTabBarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.delegate = self;
    
    /*
     CGRect viewFrame=self.tabBar.frame;
     //change these parameters according to you.
     viewFrame.origin.y = self.view.bounds.size.height - 35;
     viewFrame.origin.x = 0;
     viewFrame.size.height=35;
     viewFrame.size.width=320;
     self.tabBar.frame=viewFrame;
     */
    
    
    UIImage* tabBarBackground = [UIImage imageNamed:@"graybackground.jpg"];
    [[UITabBar appearance] setBackgroundImage:tabBarBackground];
    [[UITabBar appearance] setSelectionIndicatorImage:[[UIImage alloc] init]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSLog(@"%@", encodedInfo);
    
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    
    CDDDetailsViewController *detailsView = [[CDDDetailsViewController alloc] initWithNibName:@"CDDDetailsViewController" bundle:nil];
    UIImage *powerOneSignUpImage = [[UIImage imageNamed:@"fountain-pen-icon-614x460.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
    UITabBarItem *powerOneSignUpItem = [[UITabBarItem alloc] initWithTitle:@"View" image:powerOneSignUpImage tag:0];
    [detailsView setTabBarItem:powerOneSignUpItem];
    UINavigationController *detailNav = [[UINavigationController alloc] initWithRootViewController:detailsView];
    [controllers addObject:detailNav];
    
    /*
    CDDConfigSegments *configView = [[CDDConfigSegments alloc] init];
    configView->ipAddress = ipAddress;
    configView->encodedInfo = encodedInfo;
    UIImage *powerOneLoginImage = [[UIImage imageNamed:@"project-leader.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
    UITabBarItem *powerOneLoginItem = [[UITabBarItem alloc] initWithTitle:@"Config" image:powerOneLoginImage tag:1];
    [configView setTabBarItem:powerOneLoginItem];
    UINavigationController *configNav = [[UINavigationController alloc] initWithRootViewController:configView];
    [controllers addObject:configNav];
    */
    CDDConfigPlantViewController *plant = [[CDDConfigPlantViewController alloc] initWithNibName:@"CDDConfigPlantViewController" bundle:NULL];
    plant->ipAddress = ipAddress;
    plant->encode = encodedInfo;
    UIImage *powerOneLoginImage = [[UIImage imageNamed:@"project-leader.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
    UITabBarItem *powerOneLoginItem = [[UITabBarItem alloc] initWithTitle:@"Config" image:powerOneLoginImage tag:1];
    [plant setTabBarItem:powerOneLoginItem];
    UINavigationController *configNav = [[UINavigationController alloc] initWithRootViewController:plant];
    [controllers addObject:configNav];
     
    CDDTableViewController *tableView = [[CDDTableViewController alloc] initWithNibName:@"CDDTableViewController" bundle:nil];
    UIImage *powerOneTutorialImage = [[UIImage imageNamed:@"Subculture-Glasses-icon.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
    UITabBarItem *powerOneTutorialItem = [[UITabBarItem alloc] initWithTitle:@"Events" image:powerOneTutorialImage tag:2];
    [tableView setTabBarItem:powerOneTutorialItem];
    UINavigationController *tableNav = [[UINavigationController alloc] initWithRootViewController:tableView];
    [controllers addObject:tableNav];

    
    CDDRegisterViewController *registerView = [[CDDRegisterViewController alloc] initWithNibName:@"CDDRegisterViewController" bundle:nil];
    UIImage *registerViewImage = [[UIImage imageNamed:@"Subculture-Glasses-icon.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, -10, 0, 0)];
    UITabBarItem *registerViewItem = [[UITabBarItem alloc] initWithTitle:@"Events" image:registerViewImage tag:3];
    [registerView setTabBarItem:registerViewItem];
    UINavigationController *registerNav = [[UINavigationController alloc] initWithRootViewController:registerView];
    [controllers addObject:registerNav];
    
    
    self.viewControllers = controllers;
    
    [self setSelectedIndex:1];
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    int controllerIndex = [[tabBarController viewControllers] indexOfObject:viewController];
    
    if(controllerIndex == self.selectedIndex || self.isAnimating){
        return NO;
    }
    
    // Get the views.
    UIView * fromView = tabBarController.selectedViewController.view;
    UIView * toView = [viewController view];
    
    // Get the size of the view area.
    CGRect viewSize = fromView.frame;
    BOOL scrollRight = controllerIndex > tabBarController.selectedIndex;
    
    // Add the to view to the tab bar view.
    [fromView.superview addSubview:toView];
    
    // Position it off screen.
    toView.frame = CGRectMake((scrollRight ? 320 : -320), viewSize.origin.y, 320, viewSize.size.height);
    
    [UIView animateWithDuration:0.2 animations: ^{
        
        // Animate the views on and off the screen. This will appear to slide.
        fromView.frame =CGRectMake((scrollRight ? -320 : 320), viewSize.origin.y, 320, viewSize.size.height);
        toView.frame =CGRectMake(0, viewSize.origin.y, 320, viewSize.size.height);
        
    } completion:^(BOOL finished) {
        
        if (finished) {
            // Remove the old view from the tabbar view.
            [fromView removeFromSuperview];
            tabBarController.selectedIndex = controllerIndex;
        }
    }
     ];
    
    return NO;
}
@end
