//
//  AuthenticationData.h
//  PowerOneApp
//
//  Created by John Setting on 7/16/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Base64.h"
@interface AuthenticationData : NSObject 
{
    NSString *firstName;
    NSString *lastName;
    NSString *userId;
    NSString *email;
    NSString *siteListEid;
    NSMutableArray *assetDictionary;
    
    NSString *uniqueSiteName;
    
    NSString *NowValue;
    NSString *TodayValue;
    NSString *WeekValue;
    NSString *MonthValue;
    NSString *LifeTimeValue;
    
    NSArray *graphValuesDay;
    NSArray *graphStartDateDAY;
    NSArray *graphValues7D;
    NSArray *graphStartDate7D;
    NSArray *graphValues30D;
    NSArray *graphStartDate30D;
    NSArray *graphValues365D;
    NSArray *graphStartDate365D;
    NSArray *graphValuesWTD;
    NSArray *graphStartDateWTD;
    NSArray *graphValuesMTD;
    NSArray *graphStartDateMTD;
    NSArray *graphValuesYTD;
    NSArray *graphStartDateYTD;
    
    NSNumber *maxDayValue;
    NSNumber *max7DValue;
    NSNumber *max30DValue;
    NSNumber *max365DValue;
    NSNumber *maxWTDValue;
    NSNumber *maxMTDValue;
    NSNumber *maxYTDValue;
    
    NSString *irradiance;
    NSString *insolation;
    NSString *cellTemp;
    NSString *ambientTemp;
    
    NSArray *loggerIDs;
    
}

+ (id)sharedManager;

- (void)fetchUserServiceData:(NSString*)url authKey:(NSString *)authKey;
- (BOOL)fetchAssetServiceData:(NSString*)url authKey:(NSString *)authKey;
- (void)fetchLoggerIDs:(NSString*)url authKey:(NSString *)authKey;
- (void)fetchLoggerData:(NSString*)url authKey:(NSString *)authKey;
- (void)fetchInverterData:(NSString*)url authKey:(NSString *)authKey;
- (void)fetchPlantData:(NSString*)url authKey:(NSString *)authKey;
- (void)fetchWeatherStation:(NSString*)url authKey:(NSString *)authKey;
- (void)fetchPVEnergy_PVPowerDataDay:(NSString*)url authKey:(NSString *)authKey;
- (void)fetchPVEnergy_PVPowerData7D:(NSString*)url authKey:(NSString *)authKey;
- (void)fetchPVEnergy_PVPowerData30D:(NSString*)url authKey:(NSString *)authKey;
- (void)fetchPVEnergy_PVPowerData365D:(NSString*)url authKey:(NSString *)authKey;
- (void)fetchPVEnergy_PVPowerDataWTD:(NSString*)url authKey:(NSString *)authKey;
- (void)fetchPVEnergy_PVPowerDataMTD:(NSString*)url authKey:(NSString *)authKey;
- (void)fetchPVEnergy_PVPowerDataYTD:(NSString*)url authKey:(NSString *)authKey;
- (NSString *)fetchAuthToken:(NSString*)url username:(NSString*)username password:(NSString*)password apiKey:(NSString*)apiKey;

- (NSString *)getFirstName;
- (NSString *)getLastName;
- (NSString *)getUserId;
- (NSString *)getEmail;
- (NSString *)getSiteListEid;
- (NSMutableArray *)getAssetDictionary;

- (NSString *)getUniqueSiteName;

- (NSString *)getNowValue;
- (NSString *)getTodayValue;
- (NSString *)getWeekValue;
- (NSString *)getMonthValue;
- (NSString *)getLifeTimeValue;

- (NSArray*) getGraphValuesDay;
- (NSArray*) getGraphDatesDay;
- (NSNumber*) getMaxDayValue;

- (NSArray*) getGraphValues7D;
- (NSArray*) getGraphDates7D;
- (NSNumber*) getMax7DValue;

- (NSArray*) getGraphValues30D;
- (NSArray*) getGraphDates30D;
- (NSNumber*) getMax30DValue;

- (NSArray*) getGraphValues365D;
- (NSArray*) getGraphDates365D;
- (NSNumber*) getMax365DValue;

- (NSArray*) getGraphValuesWTD;
- (NSArray*) getGraphDatesWTD;
- (NSNumber*) getMaxWTDValue;

- (NSArray*) getGraphValuesMTD;
- (NSArray*) getGraphDatesMTD;
- (NSNumber*) getMaxMTDValue;

- (NSArray*) getGraphValuesYTD;
- (NSArray*) getGraphDatesYTD;
- (NSNumber*) getMaxYTDValue;

- (NSString*) getIrradiance;
- (NSString*) getInsolation;
- (NSString*) getCellTemp;
- (NSString*) getAmbientTemp;

- (NSArray*) getLoggerIDs;

- (void) ConfirmAction:(NSString *)url;


@end
