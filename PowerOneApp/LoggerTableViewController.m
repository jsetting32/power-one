//
//  LoggerTableViewController.m
//  PowerOneApp
//
//  Created by John Setting on 7/10/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "LoggerTableViewController.h"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) //1


@interface LoggerTableViewController ()

@end

@implementation LoggerTableViewController

@synthesize configData, statusData;
@synthesize allEntries = _allEntries;
@synthesize configEntries = _configEntries;
@synthesize statusEntries = _statusEntries;
@synthesize feeds = _feeds;
@synthesize queue = _queue;

- (void)refresh {
    
    for (NSString *feed in _feeds) {
        
        NSURL *url = [NSURL URLWithString:feed];
        ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
        [request setDelegate:self];
        [_queue addOperation:request];
    }
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSDate *now = [NSDate date];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    //formatter.dateFormat = @"ss";
    //NSString *secs = [formatter stringFromDate:now];
    
    //formatter.dateFormat = @"mm";
    //NSString *min = [formatter stringFromDate:now];
    
    //formatter.dateFormat = @"HH";
    //NSString *hour = [formatter stringFromDate:now];
    
    formatter.dateFormat = @"dd";
    NSString *day = [formatter stringFromDate:now];
    
    formatter.dateFormat = @"D";
    NSString *daynum = [formatter stringFromDate:now];
    
    formatter.dateFormat = @"MM";
    NSString *month = [formatter stringFromDate:now];
    
    formatter.dateFormat = @"yyyy";
    NSString *year = [formatter stringFromDate:now];
    
    
    NSLog(@"%@-%@-%@", month, day, year);
    //NSLog(@"%f : %f", lat, lon);
    //[AccelerometerSolarInstallationViewController solarPosition:TRUE secs:[secs intValue] mins:[min intValue] hours:[hour intValue] lat:lat lon:lon timeZone:-5.0 daynum:[daynum intValue] year:[year intValue] day:[day intValue] month:[month intValue] temp:27.0 aspect:135.0 pressure:1006.0];
    
    
    
    float solarIrradiance = 0.0;
    
    for (int i = 0; i < 24; i++)
    {
        for (int j = 0; j < 60; j = j+15)
        {
            //solarIrradiance += [AccelerometerSolarInstallationViewController solarPosition:TRUE secs:0 mins:j hours:i lat:lat lon:lon timeZone:-5.0 daynum:[daynum intValue] year:[year intValue] day:[day intValue] month:[month intValue] temp:27.0 aspect:135.0 pressure:1006.0];
            
            solarIrradiance += [AccelerometerSolarInstallationViewController solarPosition:TRUE secs:0 mins:j hours:i lat:lat lon:lon timeZone:-5.0 daynum:[daynum intValue] year:[year intValue] day:[day intValue] month:[month intValue]];
            //solarIrradiance += [AccelerometerSolarInstallationViewController solarPosition:TRUE secs:0 mins:j hours:i lat:34.43 lon:-112.02 timeZone:-5.0 daynum:[daynum intValue] year:1988 day:1 month:1 temp:27.0 aspect:135.0 pressure:1006.0];
        }
    }
    
    NSLog(@"%f kWh/m\u00B2", solarIrradiance * 0.001 * 24 / 4);
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationController.navigationBar.tintColor = [UIColor blackColor];

    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

    //NSLog(@"%@", ipAddress);
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move, location is updated
    locationManager.desiredAccuracy = kCLLocationAccuracyBest; // get best current locaton coords
    [locationManager startUpdatingLocation];
    
    
    scroller = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, self.view.bounds.size.height)];
    scroller.center = CGPointMake(self.view.bounds.size.width/2.0, self.view.bounds.size.height/2.0 - 50);
    scroller.hidesWhenStopped = YES;
    [scroller setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview:scroller];
    [scroller startAnimating];
    
    self.allEntries = [NSMutableArray array];
    self.configEntries = [NSMutableArray array];
    self.statusEntries = [NSMutableArray array];
    self.queue = [[NSOperationQueue alloc] init];
    NSString *formattedConfig = [NSString stringWithFormat:@"http://%@/au/logger/v1/config", ipAddress];
    NSString *formattedStatus = [NSString stringWithFormat:@"http://%@/au/logger/v1/status", ipAddress];
    
    //NSString *plantInfo = [[NSString stringWithFormat:@"http://%@", ipAddress];
    
    self.feeds = [NSArray arrayWithObjects:formattedConfig, formattedStatus, nil];
    
    [self refresh];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_allEntries count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    
    // Configure the cell...
    NSArray *entry = [_allEntries objectAtIndex:indexPath.row];

    if ([entry count] == 4) {
        cell.textLabel.text = [entry objectAtIndex:0];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@", [entry objectAtIndex:1], [entry objectAtIndex:2]];
    } else if ([entry count] == 5) {
        cell.textLabel.text = [entry objectAtIndex:0];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@ - %@", [entry objectAtIndex:1], [entry objectAtIndex:2], [entry objectAtIndex:3]];

        //if ([[entry objectAtIndex:0] hasSuffix:@"status"] || [[entry objectAtIndex:0] hasSuffix:@"last_error"] || [[entry objectAtIndex:0] hasSuffix:@"last_sample"])
        //{
            
        if ([[entry objectAtIndex:2] isEqualToString:@"ok"]) {
            cell.imageView.image = [UIImage imageNamed:@"norm.png"];
        } else if ([[entry objectAtIndex:2] isEqualToString:@"err"]) {
            cell.imageView.image = [UIImage imageNamed:@"high.png"];
        } else if ([[entry objectAtIndex:2] isEqualToString:@"warn"]) {
            cell.imageView.image = [UIImage imageNamed:@"medium.png"];
        } else {
            cell.imageView.image = NULL;
        }
    } else {
        cell.textLabel.text = [entry objectAtIndex:0];
    }
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

- (void)requestFinished:(ASIHTTPRequest *)request {
    
    [_queue addOperationWithBlock:^{
        int i = 0;
        TBXML *configTBXML = [[TBXML alloc] initWithXMLData:[request responseData]];
        TBXMLElement *config = configTBXML.rootXMLElement;
        TBXMLElement *configItemElement = [TBXML childElementNamed:@"item" parentElement:config];
        do
        {
            NSMutableArray *anArray = [[NSMutableArray alloc] init];
            if ([[TBXML valueOfAttributeNamed:@"key" forElement:configItemElement] hasPrefix:@"ser.3.device."] || [[TBXML valueOfAttributeNamed:@"key" forElement:configItemElement] hasPrefix:@"ser.4.device."]) {

                NSString *key = [TBXML valueOfAttributeNamed:@"key" forElement:configItemElement];
                                
                [anArray addObject:key];
                
                if ([TBXML valueOfAttributeNamed:@"type" forElement:configItemElement]) {
                    
                    NSString *type = [TBXML valueOfAttributeNamed:@"type" forElement:configItemElement];
                    [anArray addObject:type];
                    
                } if ([TBXML valueOfAttributeNamed:@"origin" forElement:configItemElement]) {
                    
                    NSString *origin = [TBXML valueOfAttributeNamed:@"origin" forElement:configItemElement];
                    [anArray addObject:origin];
                    
                } if ([TBXML valueOfAttributeNamed:@"label" forElement:configItemElement]) {
                    
                    NSString *label = [TBXML valueOfAttributeNamed:@"label" forElement:configItemElement];
                    [anArray addObject:label];
                    
                } if ([TBXML valueOfAttributeNamed:@"sev" forElement:configItemElement]) {
                    
                    NSString *sev = [TBXML valueOfAttributeNamed:@"sev" forElement:configItemElement];
                    [anArray addObject:sev];
                    
                } if ([TBXML valueOfAttributeNamed:@"time" forElement:configItemElement]) {
                    
                    NSString *time = [TBXML valueOfAttributeNamed:@"time" forElement:configItemElement];
                    [anArray addObject:time];
                }
                
                NSError *error = nil;
                NSString *itemValue = [TBXML textForElement:configItemElement];
                NSData* jsonData = [itemValue dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
                //NSLog(@"%@", json);
                if (!json || [json isKindOfClass:[NSNull class]]) {
                    [anArray addObject:jsonData];
                } else {
                    [anArray addObject:json];
                }
                
                //NSLog(@"%@", anArray);
                
                [_allEntries insertObject:anArray atIndex:i];
                [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
                i++;
            }


            /*if ([TBXML valueOfAttributeNamed:@"type" forElement:configItemElement]) {
                
                NSString *type = [TBXML valueOfAttributeNamed:@"type" forElement:configItemElement];
                [anArray addObject:type];

            } if ([TBXML valueOfAttributeNamed:@"origin" forElement:configItemElement]) {
                
                NSString *origin = [TBXML valueOfAttributeNamed:@"origin" forElement:configItemElement];
                [anArray addObject:origin];

            } if ([TBXML valueOfAttributeNamed:@"label" forElement:configItemElement]) {
                
                NSString *label = [TBXML valueOfAttributeNamed:@"label" forElement:configItemElement];
                [anArray addObject:label];
            
            } if ([TBXML valueOfAttributeNamed:@"sev" forElement:configItemElement]) {
            
                NSString *sev = [TBXML valueOfAttributeNamed:@"sev" forElement:configItemElement];
                [anArray addObject:sev];
            
            } if ([TBXML valueOfAttributeNamed:@"time" forElement:configItemElement]) {
            
                NSString *time = [TBXML valueOfAttributeNamed:@"time" forElement:configItemElement];
                [anArray addObject:time];
            }
            
            NSError *error;
            NSString *itemValue = [TBXML textForElement:configItemElement];
            

            
            NSDictionary* json = [[NSDictionary alloc] init];
            if (!jsonData || [jsonData isKindOfClass:[NSNull class]])
            {
                [anArray addObject:jsonData];
                NSLog(@"NULL");
            } else {
                jsonData = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
                [anArray addObject:json];
            }
            */

            //NSArray *readJsonArray = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
            //NSLog(@"Key = %@ : Type = %@ : Label = %@ : Sev = %@ : Time = %@ : Origin = %@ : Data = %@", key, type, label, sev, time, origin, itemValue);
            //NSLog(@"Key = %@ : Type = %@ : Label = %@ : Sev = %@ : Time = %@ : Origin = %@", key, type, label, sev, time, origin);
            //NSLog(@"Key = %@ : Label = %@ : Sev = %@ : Time = %@ : Data = %@", key, label, sev, time, itemValue);
            //NSArray *anArray = [[NSArray alloc] initWithObjects:key, type, origin, label, sev, time, itemValue, nil];
            
            /*
            if (!type || [type isKindOfClass:[NSNull class]]) {
                [anArray removeObject:type];
            } if (!origin || [origin isKindOfClass:[NSNull class]]) {
                [anArray removeObject:origin];
            } if (!label || [label isKindOfClass:[NSNull class]]) {
                [anArray removeObject:label];
            } if (!sev || [sev isKindOfClass:[NSNull class]]) {
                [anArray removeObject:sev];
            } if (!time || [time isKindOfClass:[NSNull class]]) {
                [anArray removeObject:time];
            }
             */
            
            //[array insertObject:anArray atIndex:i];
            
            //NSLog(@"%@", anArray);

            //[_allEntries insertObject:anArray atIndex:i];
            //[self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
        } while ((configItemElement = configItemElement->nextSibling));
        
        [scroller stopAnimating];
    }];
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    NSError *error = [request error];
    NSLog(@"Error: %@", error);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Issue" message:@"We are having issues connecting to the logger. Please pull the tableview down to refresh and try again." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
    [alert show];
}


/*
//+ (NSMutableDictionary *) getConfigInfo:(int)timeStamp
+ (NSMutableDictionary *) getConfigInfo : (NSMutableDictionary *)configData
{
    NSString *configURL = @"http://10.0.2.127/au/logger/v1/config";
    NSURL *url = [NSURL URLWithString:configURL];
    
    TBXML *tbxml = [[TBXML alloc] initWithURL:url];
    //NSLog(@"TBXML: %@", tbxml);
    
    TBXMLElement *status = tbxml.rootXMLElement;
    //NSString *statusString = [TBXML elementName:status];
    //NSLog(@"ROOT: %@", statusString);
    
    TBXMLElement *itemElement = [TBXML childElementNamed:@"item" parentElement:status];
    
    configData = [[NSMutableDictionary alloc] init];
    
    do
    {
        
        NSString *key = [TBXML valueOfAttributeNamed:@"key" forElement:itemElement];
        NSString *type = [TBXML valueOfAttributeNamed:@"type" forElement:itemElement];
        NSString *origin = [TBXML valueOfAttributeNamed:@"origin" forElement:itemElement];
        
        NSString *itemValue = [TBXML textForElement:itemElement];

        //NSLog(@"Key = %@ : Label = %@ : Sev = %@ : Time = %@ : Data = %@", key, label, sev, time, data);
        NSArray *array = [[NSArray alloc] initWithObjects:type, origin, itemValue, nil];
        
        [configData setObject:array forKey:key];
        
    } while ((itemElement = itemElement->nextSibling));
    
    NSLog(@"%i", [configData count]);

    return configData;
}


//+ (NSMutableDictionary *) getStatusInfo:(int)timeStamp
+ (NSMutableDictionary *) getStatusInfo:(NSMutableDictionary *)statusData keys:(id)keys
{
    
    //NSString *urlString = [NSString stringWithFormat:@"http://10.0.2.127/au/logger/v1/status?_=%i", timeStamp];
    NSString *statusURL = @"http://10.0.2.127/au/logger/v1/status";
    NSURL *url = [NSURL URLWithString:statusURL];
    
    TBXML *tbxml = [[TBXML alloc] initWithURL:url];
    //NSLog(@"TBXML: %@", tbxml);
    
    TBXMLElement *status = tbxml.rootXMLElement;
    //NSString *statusString = [TBXML elementName:status];
    //NSLog(@"ROOT: %@", statusString);
    
    TBXMLElement *itemElement = [TBXML childElementNamed:@"item" parentElement:status];
    
    statusData = [[NSMutableDictionary alloc] init];
    
    do
    {
        NSString *key = [TBXML valueOfAttributeNamed:@"key" forElement:itemElement];
        NSString *label = [TBXML valueOfAttributeNamed:@"label" forElement:itemElement];
        NSString *sev = [TBXML valueOfAttributeNamed:@"sev" forElement:itemElement];
        NSString *time = [TBXML valueOfAttributeNamed:@"time" forElement:itemElement];
        NSString *itemValue = [TBXML textForElement:itemElement];
        //NSLog(@"Key = %@ : Label = %@ : Sev = %@ : Time = %@ : Data = %@", key, label, sev, time, data);
        NSArray *array = [[NSArray alloc] initWithObjects:label, sev, time, itemValue, nil];
        
        [statusData setObject:array forKey:key];
        
    } while ((itemElement = itemElement->nextSibling));
    
    keys = [statusData allKeys];
    
    NSLog(@"%@", keys);
    //NSLog(@"%i", [statusData count]);
    
    return statusData;
}
 */



- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    int degrees = newLocation.coordinate.latitude;
    double decimal = fabs(newLocation.coordinate.latitude - degrees);
    int minutes = decimal * 60;
    double seconds = decimal * 3600 - minutes * 60;
    NSString *latitude = [NSString stringWithFormat:@"%d° %d' %1.4f\"",
                     degrees, minutes, seconds];
    //NSLog(@" Current Latitude : %@",latitude);
    lat = [latitude floatValue];
    degrees = newLocation.coordinate.longitude;
    decimal = fabs(newLocation.coordinate.longitude - degrees);
    minutes = decimal * 60;
    seconds = decimal * 3600 - minutes * 60;
    NSString *longt = [NSString stringWithFormat:@"%d° %d' %1.4f\"",
                       degrees, minutes, seconds];
    //NSLog(@" Current Longitude : %@",longt);
    lon = [longt floatValue];
}


@end
