//
//  InvertersDetailViewController.m
//  PowerOneApp
//
//  Created by John Setting on 6/18/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "InvertersDetailViewController.h"

@implementation InvertersDetailViewController

@synthesize valuesString = _valuesString;
@synthesize dateString = _dateString;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    //NSLog(@"%@", _inverterValues);
    //NSLog(@"%@", _inverterDates);
    SWRevealViewController *revealController = [self revealViewController];
    
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    //[self.view addGestureRecognizer:revealController.panGestureRecognizer];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"] style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    UIBarButtonItem *rightRevealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"19-gear.png"] style:UIBarButtonItemStyleBordered target:revealController action:@selector(rightRevealToggle:)];
    
    //UIBarButtonItem *rightRevealButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(rightRevealToggle)];
    self.navigationItem.rightBarButtonItem = rightRevealButtonItem;
    
    
    _myInverterValue.text = _valuesString;
    _myInverterValue.textAlignment = NSTextAlignmentCenter;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    
    
    numbers = [[NSMutableArray alloc] init];
    [numbers addObject:[NSNumber numberWithInt:-3]];
    [numbers addObject:[NSNumber numberWithInt:0]];
    [numbers addObject:[NSNumber numberWithInt:2]];
    [numbers addObject:[NSNumber numberWithInt:4]];
    [numbers addObject:[NSNumber numberWithInt:7]];
    [numbers addObject:[NSNumber numberWithInt:10]];
    [numbers addObject:[NSNumber numberWithInt:12]];
    
    //int fullSize = [myBottle.barBottem intValue] - [myBottle.barTop intValue];
    CGRect frame = CGRectMake(50, 150, 220, 23);
    slider = [[UISlider alloc] initWithFrame:frame];
    [slider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    [slider setBackgroundColor:[UIColor clearColor]];
    slider.continuous = YES; // Make the slider 'stick' as it is moved.
    [slider setMinimumValue:0];
    
    // Minus 2 to count because the last date value is allocated but may not have a value
    [slider setMaximumValue:((float)[_inverterDates count] - 2)];
    
    //slider.transform = CGAffineTransformMakeRotation(M_PI * - 0.5);
    [self.view addSubview:slider];
    
    _label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 120)];
    _label.textColor = [UIColor whiteColor];
    [self.view addSubview:_label];

}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)valueChanged:(UISlider*)sender
{
    NSUInteger index = (NSUInteger)(slider.value + 0.5); // Round the number.
    [slider setValue:index animated:NO];
    //NSLog(@"index: %i", index);
    
    NSString *number = [_inverterValues objectAtIndex:index]; // <-- This is the number you want.
    //NSLog(@"number: %@", number);
    NSString *dates = [_inverterDates objectAtIndex:index];
    //NSLog(@"date: %@", dates);
    _myInverterDates.text = dates;
    
    NSString *str = [_inverterValues objectAtIndex:index];
    if (!str || [str isKindOfClass:[NSNull class]]) {
        _label.text = @"-";
        _label.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.5];
    } else {
        _label.text = number;

        if (([number integerValue] - 19.0) < 0.0) {
            _label.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:[number integerValue]/255.0 alpha:0.5];
            
        } else if (([number integerValue] - 82.0 < 0.0)) {
            _label.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:([number integerValue] - 19.0)/255.0 blue:[number integerValue]/255.0 alpha:0.5];
            
        } else {
            _label.backgroundColor = [UIColor colorWithRed:([number integerValue] - 82.0)/255.0 green:([number integerValue] - 19.0)/255.0 blue:[number integerValue]/255.0 alpha:0.5];
        }
        //NSLog(@"%f", ([number integerValue] - 82.0));
        //NSLog(@"%f", ([number integerValue] - 19.0));
        //NSLog(@"%i", [number integerValue]);
    }
}

@end
