//
//  FeedbackTypes.m
//  PowerOneApp
//
//  Created by John Setting on 6/26/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "FeedbackTypes.h"

@interface FeedbackTypes ()

@end

@implementation FeedbackTypes

@synthesize delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.title = @"FeedBack";
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 12;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (indexPath.row == 0) {
        cell.textLabel.text = @"Animations";
    } else if (indexPath.row == 1) {
        cell.textLabel.text = @"Events";
    } else if (indexPath.row == 2) {
        cell.textLabel.text = @"Graphs";
    } else if (indexPath.row == 3) {
        cell.textLabel.text = @"Ads";
    } else if (indexPath.row == 4) {
        cell.textLabel.text = @"Ads";
    } else if (indexPath.row == 5) {
        cell.textLabel.text = @"Language and Translation";
    } else if (indexPath.row == 6) {
        cell.textLabel.text = @"Location and Nearby";
    } else if (indexPath.row == 7) {
        cell.textLabel.text = @"News Feeds";
    } else if (indexPath.row == 8) {
        cell.textLabel.text = @"Notifications"; 
    } else if (indexPath.row == 9) {
        cell.textLabel.text = @"Pages";
    } else if (indexPath.row == 10) {
        cell.textLabel.text = @"Weather";
    } else if (indexPath.row == 11) {
        cell.textLabel.text = @"Other";
    }
    // Configure the cell...
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    //NSLog(@"Cell's text: %@",cell.textLabel.text);
    [delegate didReceiveType:cell.textLabel.text];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
