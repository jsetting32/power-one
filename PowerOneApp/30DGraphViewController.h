//
//  30DGraphViewController.h
//  PowerOneApp
//
//  Created by John Setting on 6/12/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"
#import "PullEasyViewData.h"
#import "AuthenticationData.h"
#import "PullEasyViewData.h"
@interface _0DGraphViewController : UIViewController <CPTPlotDataSource, CPTBarPlotDelegate, CPTPlotSpaceDelegate>
{
    AuthenticationData *authData;
    PullEasyViewData *graphData;
}
@property (nonatomic, strong) CPTGraphHostingView *hostView;

@property (nonatomic, strong) CPTPlotSpaceAnnotation *priceAnnotation;
@property (nonatomic, strong) NSString *authToken;

@end
