//
//  CDDConfigCDDCloneViewController.m
//  PowerOneApp
//
//  Created by John Setting on 7/22/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "CDDConfigCDDCloneViewController.h"

@interface CDDConfigCDDCloneViewController ()

@end

@implementation CDDConfigCDDCloneViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Clone";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)nextViewAction:(id)sender {

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/protect/clone.htm", ipAddress]]];
    
    NSLog(@"%@", encode);
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
    [request setValue:@"gzip, deflate" forHTTPHeaderField:@"Accept-Encoding"];
    [request setValue:[NSString stringWithFormat:@"Basic %@", encode] forHTTPHeaderField:@"Authorization"];
    [request setValue:@"keep-alive" forHTTPHeaderField:@"Connection"];
    [request setValue:ipAddress forHTTPHeaderField:@"Host"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    NSString* astr = @"own=02%3A29%3AE3%3AFA&pan=E3%3AFA";
    
    [request setHTTPBody:[astr dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSError *error;
    NSURLResponse *response;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString* another = [NSString stringWithUTF8String:[data bytes]];
    
    NSLog(@"%@ : %@", newStr, another);
    NSLog(@"Error: %@", error);
    
    CDDConfigGroundfaultEnergyViewController *network = [[CDDConfigGroundfaultEnergyViewController alloc] initWithNibName:@"CDDConfigGroundfaultEnergyViewController" bundle:NULL];
    network->ipAddress = ipAddress;
    network->encode = encode;
    [self.navigationController pushViewController:network animated:YES];
    
}
@end
