//
//  PortfolioViewController.h
//  PowerOneApp
//
//  Created by John Setting on 7/18/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AuthenticationData.h"
#import "MBProgressHUD.h"

@interface PortfolioViewController : UITableViewController <MBProgressHUDDelegate>
{
    AuthenticationData *authData;
    MBProgressHUD *HUD;
}


@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *authKey;
@property (nonatomic, strong) NSString *apiKey;

@property(nonatomic, strong) NSArray *portfolioArray;

@property (nonatomic) BOOL checker;

@end
