//
//  CDDRegisterAddressViewController.h
//  PowerOneApp
//
//  Created by John Setting on 7/23/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDDRegisterSecurityQsViewController.h"

@interface CDDRegisterAddressViewController : UIViewController


- (IBAction)pushToSecurityQsAction:(id)sender;

@end
