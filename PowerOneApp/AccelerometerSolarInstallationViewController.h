//
//  AccelerometerSolarInstallationViewController.h
//  PowerOneApp
//
//  Created by John Setting on 7/3/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "SWRevealViewController.h"

@interface AccelerometerSolarInstallationViewController : UIViewController < UIAccelerometerDelegate, CLLocationManagerDelegate>
{
    UIAccelerometer * theAccelerometer;
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    
    IBOutlet UILabel *xLabel;
    IBOutlet UILabel *yLabel;
    IBOutlet UILabel *zLabel;
    
    IBOutlet UILabel *latitudeLocation;
    IBOutlet UILabel *longitudeLocation;
    
    IBOutlet UILabel *compassHeading;
    
}

@property (strong, nonatomic) IBOutlet UILabel *xLabel;
@property (strong, nonatomic) IBOutlet UILabel *yLabel;
@property (strong, nonatomic) IBOutlet UILabel *zLabel;

@property (strong, nonatomic) IBOutlet UILabel *latitudeLocation;
@property (strong, nonatomic) IBOutlet UILabel *longitudeLocation;

@property (strong, nonatomic) IBOutlet UILabel *compassHeading;


@property CGPoint point;

//+ (float) solarPosition :(BOOL)go secs:(int)secs mins:(int)mins hours:(int)hours lat:(float)lat lon:(float)lon timeZone:(float)timeZone daynum:(int)daynum year:(int)year day:(int)day month:(int)month temp:(float)temp aspect:(float)aspect pressure:(float)pressure;
+ (float) solarPosition :(BOOL)go secs:(int)secs mins:(int)mins hours:(int)hours lat:(float)lat lon:(float)lon timeZone:(float)timeZone daynum:(int)daynum year:(int)year day:(int)day month:(int)month;

@end
