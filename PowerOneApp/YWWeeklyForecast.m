//
//  YWWeeklyForecast.m
//  PowerOneApp
//
//  Created by John Setting on 6/24/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "YWWeeklyForecast.h"

@implementation YWWeeklyForecast

@synthesize day, date, text;
@synthesize low, high, code;
@synthesize pic, currentTemp;
@synthesize currentCity, currentState;


- (YWWeeklyForecastCell *) getCell:(UITableView *)tableView
{
    YWWeeklyForecastCell *cell = (YWWeeklyForecastCell *)[tableView dequeueReusableCellWithIdentifier:@"YWWeeklyForecastCell"];
    cell.day.text = day;
    cell.date.text = date;
    cell.currentTemp.text = [[NSString alloc] initWithFormat:@"%@", currentTemp];
    cell.low.text = [[NSString alloc] initWithFormat:@"%i", low];
    cell.high.text = [[NSString alloc] initWithFormat:@"%i", high];
    cell.currentCity.text = [[NSString alloc] initWithFormat:@"%@", currentCity];
    cell.currentState.text = [[NSString alloc] initWithFormat:@"%@", currentState];
    

    
    
    
    //cell.pic = [[NSData alloc] initWithData:pic];
    
    return cell;
}

@end
