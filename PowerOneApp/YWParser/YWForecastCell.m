//
//  ForecastCell.m
//  Yahoo Weather
//
//  Created by Trent Milton on 7/12/11.
//  Copyright (c) 2011 shaydes.dsgn. All rights reserved.
//

#import "YWForecastCell.h"

@implementation YWForecastCell

@synthesize todayHigh, todayLow;
@synthesize currentCode, currentDate, currentCondition, currentTemp;
@synthesize currentConditionIcon, currentState, currentCity, sunSet, sunRise;
@synthesize windDirection, windSpeed, humidity, pressure, visibility;
@end
