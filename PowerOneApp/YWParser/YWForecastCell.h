//
//  ForecastCell.h
//  Yahoo Weather
//
//  Created by Trent Milton on 7/12/11.
//  Copyright (c) 2011 shaydes.dsgn. All rights reserved.
//


@interface YWForecastCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *todayLow;
@property (nonatomic, retain) IBOutlet UILabel *todayHigh;
@property (nonatomic, retain) IBOutlet UILabel *currentState;
@property (nonatomic, retain) IBOutlet UILabel *currentCity;
@property (nonatomic, retain) IBOutlet NSData *currentConditionIcon;

@property (nonatomic, retain) IBOutlet UILabel *currentDate;
@property (nonatomic, retain) IBOutlet UILabel *currentTemp;
@property (nonatomic, retain) IBOutlet UILabel *currentCode;
@property (nonatomic, retain) IBOutlet UILabel *currentCondition;

@property (nonatomic, retain) IBOutlet UILabel *sunRise;
@property (nonatomic, retain) IBOutlet UILabel *sunSet;

@property (nonatomic, retain) IBOutlet UILabel *windSpeed;
@property (nonatomic, retain) IBOutlet UILabel *windDirection;

@property (nonatomic, retain) IBOutlet UILabel *visibility;
@property (nonatomic, retain) IBOutlet UILabel *pressure;
@property (nonatomic, retain) IBOutlet UILabel *humidity;

@end
