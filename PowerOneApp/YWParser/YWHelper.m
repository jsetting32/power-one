//
//  YWHelper.m
//  Yahoo Weather
//
//  Created by Trent Milton on 4/12/11.
//  Copyright (c) 2011 shaydes.dsgn. All rights reserved.
//

#import "YWHelper.h"

@implementation YWHelper

#pragma mark - Weather

+ (YWForecast *) getWeather:(int)woeid
{
    NSString *urlString = [NSString stringWithFormat:@"http://xml.weather.yahoo.com/forecastrss/%d_f.xml", woeid];
    NSURL *url = [NSURL URLWithString:urlString];
    TBXML *tbxml = [TBXML tbxmlWithURL:url];
    TBXMLElement *root = tbxml.rootXMLElement;
    TBXMLElement *channelElement = [TBXML childElementNamed:@"channel" parentElement:root];
    TBXMLElement *itemElement = [TBXML childElementNamed:@"item" parentElement:channelElement];
    TBXMLElement *yweatherElement = [TBXML childElementNamed:@"yweather:condition" parentElement:itemElement];
    TBXMLElement *yweatherTodayElement = [TBXML childElementNamed:@"yweather:forecast" parentElement:itemElement];
    TBXMLElement *yweatherRegion = [TBXML childElementNamed:@"yweather:location" parentElement:channelElement];
    TBXMLElement *yweatherAstronomy = [TBXML childElementNamed:@"yweather:astronomy" parentElement:channelElement];
    TBXMLElement *yweatherWind = [TBXML childElementNamed:@"yweather:wind" parentElement:channelElement];
    TBXMLElement *yweatherAtmosphere = [TBXML childElementNamed:@"yweather:atmosphere" parentElement:channelElement];
    
    YWForecast *forecast = [[YWForecast alloc] init];
    forecast.currentCondition = [TBXML valueOfAttributeNamed:@"text" forElement:yweatherElement];
    forecast.currentCode = [[TBXML valueOfAttributeNamed:@"code" forElement:yweatherElement] intValue];
    forecast.currentTemp = [[TBXML valueOfAttributeNamed:@"temp" forElement:yweatherElement] intValue];
    
    forecast.currentDate = [TBXML valueOfAttributeNamed:@"date" forElement:yweatherElement];
    forecast.currentConditionIcon = [YWHelper getWeatherIcon:forecast.currentCode];
    
    forecast.todayLow = [[TBXML valueOfAttributeNamed:@"low" forElement:yweatherTodayElement] intValue];
    forecast.todayHigh = [[TBXML valueOfAttributeNamed:@"high" forElement:yweatherTodayElement] intValue];
    
    forecast.currentCity = [TBXML valueOfAttributeNamed:@"city" forElement:yweatherRegion];
    forecast.currentState = [TBXML valueOfAttributeNamed:@"region" forElement:yweatherRegion];
    
    forecast.sunRise = [TBXML valueOfAttributeNamed:@"sunrise" forElement:yweatherAstronomy];
    forecast.sunSet = [TBXML valueOfAttributeNamed:@"sunset" forElement:yweatherAstronomy];
    
    forecast.windSpeed = [TBXML valueOfAttributeNamed:@"speed" forElement:yweatherWind];
    forecast.windDirection = [TBXML valueOfAttributeNamed:@"direction" forElement:yweatherWind];
    
    forecast.humidity = [TBXML valueOfAttributeNamed:@"humidity" forElement:yweatherAtmosphere];
    forecast.visibility = [TBXML valueOfAttributeNamed:@"visibility" forElement:yweatherAtmosphere];
    forecast.pressure = [TBXML valueOfAttributeNamed:@"pressure" forElement:yweatherAtmosphere];

    return forecast;
}

+ (NSData *) getWeatherIcon: (NSInteger)code
{
    NSData *icon = [[NSData alloc] init];
    
    if (code == 1) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810646.png"]);
    } else if (code == 2) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 3) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810648.png"]);
    } else if (code == 4) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810649.png"]);
    } else if (code == 5) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810651.png"]);
    } else if (code == 6) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810652.png"]);
    } else if (code == 7) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810657.png"]);
    } else if (code == 8) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810658.png"]);
    } else if (code == 9) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810659.png"]);
    } else if (code == 10) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810660.png"]);
    } else if (code == 11) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810662.png"]);
    } else if (code == 12) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810663.png"]);
    } else if (code == 13) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810664.png"]);
    } else if (code == 14) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810668.png"]);
    } else if (code == 15) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810673.png"]);
    } else if (code == 16) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810674.png"]);
    } else if (code == 17) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810679.png"]);
    } else if (code == 18) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810683.png"]);
    } else if (code == 19) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810701.png"]);
    } else if (code == 20) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810702.png"]);
    } else if (code == 21) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810703.png"]);
    } else if (code == 22) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810704.png"]);
    } else if (code == 23) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810705.png"]);
    } else if (code == 24) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810700.png"]);
    } else if (code == 25) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 26) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 27) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 28) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 29) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 30) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 31) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 32) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 33) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 34) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 35) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 36) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 37) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 38) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 39) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 40) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 41) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 42) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 43) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 44) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 45) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 46) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else if (code == 47) {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    } else {
        icon = UIImagePNGRepresentation([UIImage imageNamed:@"1211810647.png"]);
    }
    
    
    return icon;
}


#pragma mark - WOEID

+ (YWWOEID *) getWOEID:(float)latitude longitude:(float)longitude yahooAPIKey:(NSString *)yahooAPIKey
{
    NSString *urlString = [NSString stringWithFormat:@"http://where.yahooapis.com/geocode?q=%f,%f&gflags=R&appid=%@", latitude, longitude, yahooAPIKey];
    //NSString *urlString = [NSString stringWithFormat:@"http://where.yahooapis.com/geocode?q=%f,%f&gflags=R&appid=10ad604a55fc5801bb880c1e06e35f24e68fd3a5", latitude, longitude];

    NSURL *url = [NSURL URLWithString:urlString];
    TBXML *tbxml = [TBXML tbxmlWithURL:url];
    TBXMLElement *root = tbxml.rootXMLElement;
    TBXMLElement *resultElement = [TBXML childElementNamed:@"Result" parentElement:root];

    YWWOEID *woeid = [[YWWOEID alloc] init];
    
    TBXMLElement *woeidElement = [TBXML childElementNamed:@"woeid" parentElement:resultElement];
    woeid.woeid = [[TBXML textForElement:woeidElement] intValue];
    
    TBXMLElement *cityElement = [TBXML childElementNamed:@"city" parentElement:resultElement];
    woeid.city = [TBXML textForElement:cityElement];

    return woeid;
}

@end
