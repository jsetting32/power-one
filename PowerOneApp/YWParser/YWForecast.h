//
//  YWForecast.h
//  Yahoo Weather
//
//  Created by Trent Milton on 4/12/11.
//  Copyright (c) 2011 shaydes.dsgn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YWForecastCell.h"

@interface YWForecast : NSObject

@property (nonatomic, retain) NSString *currentCity;
@property (nonatomic, retain) NSString *currentState;

@property (nonatomic) int todayLow;
@property (nonatomic) int todayHigh;
@property (nonatomic, retain) NSData *currentConditionIcon;

@property (nonatomic, retain) NSString *currentCondition;
@property (nonatomic) int currentCode;
@property (nonatomic) int currentTemp;
@property (nonatomic, retain) NSString *currentDate;

@property (nonatomic, retain) NSString *sunRise;
@property (nonatomic, retain) NSString *sunSet;

@property (nonatomic, retain) NSString *windDirection;
@property (nonatomic, retain) NSString *windSpeed;

@property (nonatomic, retain) NSString *humidity;
@property (nonatomic, retain) NSString *pressure;
@property (nonatomic, retain) NSString *visibility;



- (YWForecastCell *) getCell:(UITableView *)tableView;

@end
