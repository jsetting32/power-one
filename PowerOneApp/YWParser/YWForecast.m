//
//  YWForecast.m
//  Yahoo Weather
//
//  Created by Trent Milton on 4/12/11.
//  Copyright (c) 2011 shaydes.dsgn. All rights reserved.
//

#import "YWForecast.h"

@implementation YWForecast

//@synthesize day, date, text;
@synthesize todayLow, todayHigh;
@synthesize currentConditionIcon;
@synthesize currentCity, currentState;
@synthesize currentCode, currentCondition, currentDate, currentTemp;
@synthesize sunRise, sunSet, windSpeed, windDirection;
@synthesize humidity, pressure, visibility;

- (YWForecastCell *) getCell:(UITableView *)tableView
{
    YWForecastCell *cell = (YWForecastCell *)[tableView dequeueReusableCellWithIdentifier:@"YWForecastCell"];
    //cell.day.text = day;
    //cell.date.text = date;
    

    cell.currentCondition.text = currentCondition;
    cell.currentDate.text = currentDate;
    cell.currentCity.text = currentCity;
    cell.currentState.text = currentState;
    cell.currentCode.text = [[NSString alloc] initWithFormat:@"%i", currentCode];
    cell.currentTemp.text = [[NSString alloc] initWithFormat:@"%i", currentTemp];
    cell.todayLow.text = [[NSString alloc] initWithFormat:@"%i", todayLow];
    cell.todayHigh.text = [[NSString alloc] initWithFormat:@"%i", todayHigh];
    cell.currentConditionIcon = currentConditionIcon;
    //[[NSString alloc] initWithFormat:@"%i", low];
    //cell.high.text = [[NSString alloc] initWithFormat:@"%i", high];
    cell.sunRise.text = [[NSString alloc] initWithFormat:@"%@", sunRise];
    cell.sunSet.text = [[NSString alloc] initWithFormat:@"%@", sunSet];
    
    cell.windDirection.text = [[NSString alloc] initWithFormat:@"%@", windDirection];
    cell.windSpeed.text = [[NSString alloc] initWithFormat:@"%@", windSpeed];
    
    cell.humidity.text = [[NSString alloc] initWithFormat:@"%@", humidity];
    cell.visibility.text = [[NSString alloc] initWithFormat:@"%@", visibility];
    cell.pressure.text = [[NSString alloc] initWithFormat:@"%@", pressure];
    

    return cell;
}

@end
