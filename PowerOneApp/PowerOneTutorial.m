//
//  PowerOneTutorial.m
//  PowerOneApp
//
//  Created by John Setting on 7/6/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "PowerOneTutorial.h"

@interface PowerOneTutorial ()

@end

@implementation PowerOneTutorial

/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}
 */

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //self.view.backgroundColor = [UIColor blackColor];
    
    csView = [[XLCycleScrollView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y + 36, self.view.bounds.size.width, self.view.bounds.size.height - 173)];
    csView.backgroundColor = [UIColor blackColor];
    csView.delegate = self;
    csView.datasource = self;
    [self.view addSubview:csView];

    
    UIView *tabBar = [[UIView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, 36)];
    tabBar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"graybackground.jpg"]];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 32)];
    title.center = CGPointMake(tabBar.bounds.size.width / 2, tabBar.bounds.size.height / 2);
    title.text = @"Tutorial";
    title.textAlignment = NSTextAlignmentCenter;
    [title setFont:[UIFont fontWithName:@"Times New Roman-BoldMT" size:20.0f]];
    title.textColor = [UIColor whiteColor];
    title.textAlignment = NSTextAlignmentCenter;
    title.backgroundColor = [UIColor clearColor];
    [tabBar addSubview:title];
    
    UIImageView *leftImage = [[UIImageView alloc] initWithFrame:CGRectMake(100, 2, 24, 32)];
    leftImage.image = [UIImage imageNamed:@"Screen Shot 2013-07-07 at 1.35.32 PM.png"];
    [tabBar addSubview:leftImage];
    
    UIImageView *rightImage = [[UIImageView alloc] initWithFrame:CGRectMake(195, 2, 24, 32)];
    rightImage.image = [UIImage imageNamed:@"Screen Shot 2013-07-07 at 1.35.32 PM.png"];
    [tabBar addSubview:rightImage];
    
    
    [self.view addSubview:tabBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfPages
{
    return 6;
}

- (UIView *)pageAtIndex:(NSInteger)index
{
    //UILabel *l = [[UILabel alloc] initWithFrame:csView.bounds];
    //l.text = [NSString stringWithFormat:@"%d",index];
    //l.font = [UIFont systemFontOfSize:72];
    //l.textAlignment = NSTextAlignmentCenter;
    //l.backgroundColor = [UIColor whiteColor];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(csView.bounds.origin.x, csView.bounds.origin.y, csView.bounds.size.width, csView.bounds.size.height)];
    
    
    if (index == 0) {
        imageView.image = [UIImage imageNamed:@"Image1.png"];
    } else if (index == 1) {
        imageView.image = [UIImage imageNamed:@"image2.png"];
    } else if (index == 2) {
        imageView.image = [UIImage imageNamed:@"image3.png"];
    } else if (index == 3) {
        imageView.image = [UIImage imageNamed:@"image4.png"];
    } else if (index == 4) {
        imageView.image = [UIImage imageNamed:@"image5.png"];
    } else if (index == 5) {
        imageView.image = [UIImage imageNamed:@"image6.png"];
    }
    
    return imageView;
}

- (void)didClickPage:(XLCycleScrollView *)csView atIndex:(NSInteger)index
{
    //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"当前点击第%d个页面",index] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
    //[alert show];
}


@end
