//
//  CDDConfigGroundfaultEnergyViewController.m
//  PowerOneApp
//
//  Created by John Setting on 7/22/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "CDDConfigGroundfaultEnergyViewController.h"

@interface CDDConfigGroundfaultEnergyViewController ()

@end

@implementation CDDConfigGroundfaultEnergyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"GroundFault/Energy";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)finishButtonAction:(id)sender {

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/protect/resetgroundenergy.htm", ipAddress]]];
    
    NSLog(@"%@", encode);
    
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"Basic %@", encode] forHTTPHeaderField:@"Authorization"];
    [request setValue:ipAddress forHTTPHeaderField:@"Host"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    NSString* str = @"grnd=1";
    NSString* astr = @"enrg=2";
    
    [request setHTTPBody:[str dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSError *error;
    NSURLResponse *response;
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSLog(@"Error: %@", error);
    
    NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString* another = [NSString stringWithUTF8String:[data bytes]];
    
    NSLog(@"%@ : %@", newStr, another);
    
    [[[UIAlertView alloc] initWithTitle:@"Complete" message:@"You have successfully configured the CDD Logger" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil] show];
}


@end
