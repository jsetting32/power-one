
/*

 Copyright (c) 2013 Joan Lluch <joan.lluch@sweetwilliamsl.com>
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is furnished
 to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 Original code:
 Copyright (c) 2011, Philip Kluz (Philip.Kluz@zuui.org)
 
*/

#import "RearViewController.h"
#import "SWRevealViewController.h"
#import "FranksVC.h"

@interface RearViewController()

@end

@implementation RearViewController

@synthesize rearTableView = _rearTableView;

#pragma marl - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 3;
            break;
        case 1:
            return 9;
            break;
        case 2:
            return 7;
            break;
        default:
            break;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    switch (section)
    {
        case 0:
            sectionName = @"";
            break;
        case 1:
            sectionName = NSLocalizedString(@"Information", @"Information");
            break;
        case 2:
            sectionName = NSLocalizedString(@"Tools", @"Tools");
            break;
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    
    UIView *tempView = [[UIView alloc]initWithFrame:CGRectMake(0,0,300,20)];
    tempView = [[UIView alloc]initWithFrame:CGRectMake(0,0,300,20)];
    tempView.backgroundColor=[UIColor grayColor];
    
    UILabel *tempLabel = [[UILabel alloc]initWithFrame:CGRectMake(10,0,300,20)];
    tempLabel.backgroundColor = [UIColor clearColor];
    tempLabel.shadowColor = [UIColor blackColor];
    tempLabel.shadowOffset = CGSizeMake(0,2);
    tempLabel.textColor = [UIColor whiteColor]; //here you can change the text color of header.
    tempLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0f];
    tempLabel.font = [UIFont boldSystemFontOfSize:14.0f];
    
    switch (section)
    {
        case 0:
        {
            sectionName = NSLocalizedString(@"General", @"General");
            tempLabel.text = sectionName;
            [tempView addSubview:tempLabel];
        }
            break;
            
        case 1:
        {

            sectionName = NSLocalizedString(@"Information", @"Information");
            tempLabel.text = sectionName;
            [tempView addSubview:tempLabel];
        }
            break;
            
        case 2:
        {
            sectionName = NSLocalizedString(@"Tools", @"Tools");
            tempLabel.text = sectionName;
            [tempView addSubview:tempLabel];
        }
            break;
        default:
            sectionName = @"";
            break;
    }
    return tempView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellIdentifier = @"Cell";
	TDBadgedCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    NSInteger row = indexPath.row;
    data = [AuthenticationData sharedManager];

	if (nil == cell)
	{
		cell = [[TDBadgedCell alloc] initWithFrame:CGRectMake(0, 0, 320, cell.bounds.size.height)];
        UIButton *myAccessoryButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 10, 16)];
        [myAccessoryButton setBackgroundColor:[UIColor clearColor]];
        [myAccessoryButton setImage:[UIImage imageNamed:@"left-arrow-right-hi.png"] forState:UIControlStateNormal];
        [myAccessoryButton setImage:[UIImage imageNamed:@"left-arrow-right-hi.png"] forState:UIControlStateSelected];
        [cell setAccessoryView:myAccessoryButton];
        [cell.textLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10.0f]];
        cell.textLabel.textColor = [UIColor lightGrayColor];
    }
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = cell.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor darkGrayColor]CGColor], (id)[[UIColor blackColor]CGColor], nil];
    [cell.textLabel setFont:[UIFont fontWithName:@"Arial-BoldMT" size:16.0f]];
	
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    switch (indexPath.section) {
            
        case 0:
            
            if (row == 0) {
                cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", [data getFirstName], [data getLastName]];
                [cell.layer insertSublayer:gradient atIndex:0];

            } else if (row == 1) {
                cell.textLabel.text = @"Presentation Mode";
                [cell.layer insertSublayer:gradient atIndex:0];

            } else if (row == 2) {
                cell.textLabel.text = @"Resign Presentation Mode";
                [cell.layer insertSublayer:gradient atIndex:0];
            }

            break;
            
        case 1:
            
            if (row == 0) {
                
                cell.textLabel.text = @"Map View";
                [cell.layer insertSublayer:gradient atIndex:0]; 

            } else if (row == 1) {
                
                cell.textLabel.text = @"Installers";
                [cell.layer insertSublayer:gradient atIndex:0];
                
            } else if (row == 2) {
                
                cell.textLabel.text = @"Homeowners";
                [cell.layer insertSublayer:gradient atIndex:0];

            } else if (row == 3) {
                
                cell.textLabel.text = @"Designers";
                [cell.layer insertSublayer:gradient atIndex:0];

            } else if (row == 4) {
                
                cell.textLabel.text = @"Products";
                [cell.layer insertSublayer:gradient atIndex:0];

            } else if (row == 5) {
                
                cell.textLabel.text = @"Solar Energy News Feeds";
                [cell.layer insertSublayer:gradient atIndex:0];

            } else if (row == 6) {
                
                cell.textLabel.text = @"Break-Even Calculator";
                [cell.layer insertSublayer:gradient atIndex:0];

            } else if (row == 7) {
                
                cell.textLabel.text = @"Solar Installation Calculator";
                [cell.layer insertSublayer:gradient atIndex:0];
            
            } else if (row == 8) {
                
                cell.textLabel.text = @"Evo Data";
                [cell.layer insertSublayer:gradient atIndex:0];
            }
            
            break;
            
        case 2:
            
            if (row == 0) {
                cell.textLabel.text = @"Send Feedback";
                [cell.layer insertSublayer:gradient atIndex:0];

            } else if (row == 1) {
                cell.textLabel.text = @"Help Center";
                [cell.layer insertSublayer:gradient atIndex:0];

            } else if (row == 2) {
                cell.textLabel.text = @"Account Settings";
                [cell.layer insertSublayer:gradient atIndex:0];

            } else if (row == 3) {
                cell.textLabel.text = @"Terms & Policies";
                [cell.layer insertSublayer:gradient atIndex:0];

            } else if (row == 4) {
                cell.textLabel.text = @"Report a Problem";
                [cell.layer insertSublayer:gradient atIndex:0];
                
            } else if (row == 5) {
                cell.textLabel.text = @"Personalize";
                [cell.layer insertSublayer:gradient atIndex:0];
                
            } else if (row == 6) {
                cell.textLabel.text = @"Logout";
                [cell.layer insertSublayer:gradient atIndex:0];

            }
            
            break;
            
        default:
            break;
    }

	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	// Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    SWRevealViewController *revealController = self.revealViewController;
    
    // We know the frontViewController is a NavigationController
    UINavigationController *frontNavigationController = (id)revealController.frontViewController;  // <-- we know it is a NavigationController
    NSInteger row = indexPath.row;

	// Here you'd implement some of your own logic... I simply take for granted that the first row (=0) corresponds to the "FrontViewController".
    
    switch (indexPath.section) {
        case 0:
            
            // Here you'd implement some of your own logic... I simply take for granted that the first row (=0) corresponds to the "FrontViewController".
            if (row == 0) {
                
                // Now let's see if we're not attempting to swap the current frontViewController for a new instance of ITSELF, which'd be highly redundant.
                if ( ![frontNavigationController.topViewController isKindOfClass:[FranksVC class]] )
                {
                    FranksVC *frontViewController = [[FranksVC alloc] init];
                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:frontViewController];
                    [revealController setFrontViewController:navigationController animated:YES];
                }
                // Seems the user attempts to 'switch' to exactly the same controller he came from!
                else
                {
                    [revealController revealToggleAnimated:YES];
                }
                
            } else if (row == 1) {
                
                [revealController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
            } else if (row == 2) {
                
                [revealController setFrontViewPosition:FrontViewPositionRight animated:YES];
            }
            
            break;
            
        case 1:
            if (row == 0) {
                // Now let's see if we're not attempting to swap the current frontViewController for a new instance of ITSELF, which'd be highly redundant.
                if ( ![frontNavigationController.topViewController isKindOfClass:[MapViewController class]] )
                {
                    MapViewController *mapViewController = [[MapViewController alloc] initWithNibName:@"MapViewController" bundle:nil];
                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mapViewController];
                    [revealController setFrontViewController:navigationController animated:YES];
                }
                
                // Seems the user attempts to 'switch' to exactly the same controller he came from!
                else
                {
                    [revealController revealToggleAnimated:YES];
                }
            } else if (row == 1) {
                
                // Now let's see if we're not attempting to swap the current frontViewController for a new instance of ITSELF, which'd be highly redundant.
                if ( ![frontNavigationController.topViewController isKindOfClass:[InstallersViewController class]] )
                {
                    InstallersViewController *mapViewController = [[InstallersViewController alloc] initWithNibName:@"InstallersViewController" bundle:nil];
                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mapViewController];
                    [revealController setFrontViewController:navigationController animated:YES];
                }
                
                // Seems the user attempts to 'switch' to exactly the same controller he came from!
                else
                {
                    [revealController revealToggleAnimated:YES];
                }
            } else if (row == 2) {
                
                if ( ![frontNavigationController.topViewController isKindOfClass:[HomeownersViewController class]] )
                {
                    HomeownersViewController *mapViewController = [[HomeownersViewController alloc] initWithNibName:@"HomeownersViewController" bundle:nil];
                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mapViewController];
                    [revealController setFrontViewController:navigationController animated:YES];
                }
                
                // Seems the user attempts to 'switch' to exactly the same controller he came from!
                else
                {
                    [revealController revealToggleAnimated:YES];
                }
                
            } else if (row == 3) {
                
                if ( ![frontNavigationController.topViewController isKindOfClass:[DesignersViewController class]] )
                {
                    DesignersViewController *mapViewController = [[DesignersViewController alloc] init];
                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mapViewController];
                    [revealController setFrontViewController:navigationController animated:YES];
                }
                
                // Seems the user attempts to 'switch' to exactly the same controller he came from!
                else
                {
                    [revealController revealToggleAnimated:YES];
                }
                
            } else if (row == 4) {
                
                if ( ![frontNavigationController.topViewController isKindOfClass:[ProductsViewController class]] )
                {
                    ProductsViewController *mapViewController = [[ProductsViewController alloc] initWithNibName:@"ProductsViewController" bundle:nil];
                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mapViewController];
                    [revealController setFrontViewController:navigationController animated:YES];
                }
                
                // Seems the user attempts to 'switch' to exactly the same controller he came from!
                else
                {
                    [revealController revealToggleAnimated:YES];
                }
                
            } else if (row == 5) {
                
                if ( ![frontNavigationController.topViewController isKindOfClass:[WhySolarTableViewController class]] )
                {
                    WhySolarTableViewController *mapViewController = [[WhySolarTableViewController alloc] initWithNibName:@"WhySolarTableViewController" bundle:nil];
                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mapViewController];
                    [revealController setFrontViewController:navigationController animated:YES];
                }
                
                // Seems the user attempts to 'switch' to exactly the same controller he came from!
                else
                {
                    [revealController revealToggleAnimated:YES];
                }
                
            } else if (row == 6) {
                
                if ( ![frontNavigationController.topViewController isKindOfClass:[PowerCalculatorViewController class]] )
                {
                    PowerCalculatorViewController *mapViewController = [[PowerCalculatorViewController alloc] initWithNibName:@"PowerCalculatorViewController" bundle:nil];
                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mapViewController];
                    [revealController setFrontViewController:navigationController animated:YES];
                }
                
                // Seems the user attempts to 'switch' to exactly the same controller he came from!
                else
                {
                    [revealController revealToggleAnimated:YES];
                }
                
            } else if (row == 7) {
                
                if ( ![frontNavigationController.topViewController isKindOfClass:[AccelerometerSolarInstallationViewController class]] )
                {
                    AccelerometerSolarInstallationViewController *mapViewController = [[AccelerometerSolarInstallationViewController alloc] initWithNibName:@"AccelerometerSolarInstallationViewController" bundle:nil];
                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mapViewController];
                    [revealController setFrontViewController:navigationController animated:YES];
                }
                
                // Seems the user attempts to 'switch' to exactly the same controller he came from!
                else
                {
                    [revealController revealToggleAnimated:YES];
                }
                
            } else if (row == 8) {
                
                if ( ![frontNavigationController.topViewController isKindOfClass:[LoggerTableViewController class]] )
                {
                    LoggerLoginViewController *mapViewController = [[LoggerLoginViewController alloc] initWithNibName:@"LoggerLoginViewController" bundle:nil];
                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mapViewController];
                    [revealController setFrontViewController:navigationController animated:YES];
                }
                
                // Seems the user attempts to 'switch' to exactly the same controller he came from!
                else
                {
                    [revealController revealToggleAnimated:YES];
                }
            }
            
            
            break;
            
        case 2:
            
            if (row == 0) {
                
                if ( ![frontNavigationController.topViewController isKindOfClass:[SendFeedBackViewController class]] )
                {
                    SendFeedBackViewController *mapViewController = [[SendFeedBackViewController alloc] initWithNibName:@"SendFeedBackViewController" bundle:nil];
                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mapViewController];
                    [revealController setFrontViewController:navigationController animated:YES];
                }
                
                // Seems the user attempts to 'switch' to exactly the same controller he came from!
                else
                {
                    [revealController revealToggleAnimated:YES];
                }
                
            } else if (row == 1) {
                
                if ( ![frontNavigationController.topViewController isKindOfClass:[HelpCenterViewController class]] )
                {
                    HelpCenterViewController *mapViewController = [[HelpCenterViewController alloc] initWithNibName:@"HelpCenterViewController" bundle:nil];
                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mapViewController];
                    [revealController setFrontViewController:navigationController animated:YES];
                }
                
                // Seems the user attempts to 'switch' to exactly the same controller he came from!
                else
                {
                    [revealController revealToggleAnimated:YES];
                }
                
            } else if (row == 2) {
                
                if ( ![frontNavigationController.topViewController isKindOfClass:[AccountSettingsViewController class]] )
                {
                    AccountSettingsViewController *mapViewController = [[AccountSettingsViewController alloc] initWithNibName:@"AccountSettingsViewController" bundle:nil];
                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mapViewController];
                    [revealController setFrontViewController:navigationController animated:YES];
                }
                
                // Seems the user attempts to 'switch' to exactly the same controller he came from!
                else
                {
                    [revealController revealToggleAnimated:YES];
                }
                
            } else if (row == 3) {
                
                if ( ![frontNavigationController.topViewController isKindOfClass:[TermsAndPoliciesViewController class]] )
                {
                    TermsAndPoliciesViewController *mapViewController = [[TermsAndPoliciesViewController alloc] initWithNibName:@"TermsAndPoliciesViewController" bundle:nil];
                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mapViewController];
                    [revealController setFrontViewController:navigationController animated:YES];
                }
                
                // Seems the user attempts to 'switch' to exactly the same controller he came from!
                else
                {
                    [revealController revealToggleAnimated:YES];
                }
                
            } else if (row == 4) {
                
                if ( ![frontNavigationController.topViewController isKindOfClass:[ReportAProblemViewController class]] )
                {
                    ReportAProblemViewController *mapViewController = [[ReportAProblemViewController alloc] initWithNibName:@"ReportAProblemViewController" bundle:nil];
                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mapViewController];
                    [revealController setFrontViewController:navigationController animated:YES];
                }
                
                // Seems the user attempts to 'switch' to exactly the same controller he came from!
                else
                {
                    [revealController revealToggleAnimated:YES];
                }
                
            } else if (row == 5) {
                if ( ![frontNavigationController.topViewController isKindOfClass:[PersonalizationsTableViewController class]] )
                {
                    PersonalizationsTableViewController *mapViewController = [[PersonalizationsTableViewController alloc] initWithNibName:@"PersonalizationsTableViewController" bundle:nil];
                    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mapViewController];
                    [revealController setFrontViewController:navigationController animated:YES];
                }
                // Seems the user attempts to 'switch' to exactly the same controller he came from!
                else
                {
                    [revealController revealToggleAnimated:YES];
                }
            } else if (row == 6) {
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logoutHandler:) name:LOGOUT_NOTIFICATION object:self.view];
                PowerOneLoginTabBar *frontViewController = [[PowerOneLoginTabBar alloc] init];
                [self presentViewController:frontViewController animated:YES completion:nil];
                [revealController revealToggleAnimated:YES];
            }
    
            break;
        
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

//- (void)viewWillAppear:(BOOL)animated
//{   [self.navigationController setNavigationBarHidden:YES animated:animated];
//    [super viewWillAppear:animated];
//    NSLog( @"%@: REAR", NSStringFromSelector(_cmd));
//}
//
//- (void)viewWillDisappear:(BOOL)animated
//{   [self.navigationController setNavigationBarHidden:YES animated:animated];
//    [super viewWillDisappear:animated];
//    NSLog( @"%@: REAR", NSStringFromSelector(_cmd));
//}
//
//- (void)viewDidAppear:(BOOL)animated
//{
//    [self.navigationController setNavigationBarHidden:YES animated:animated];
//    [super viewDidAppear:animated];
//    NSLog( @"%@: REAR", NSStringFromSelector(_cmd));
//}
//
//- (void)viewDidDisappear:(BOOL)animated
//{
//    [self.navigationController setNavigationBarHidden:YES animated:animated];
//    [super viewDidDisappear:animated];
//    NSLog( @"%@: REAR", NSStringFromSelector(_cmd));
//}

@end