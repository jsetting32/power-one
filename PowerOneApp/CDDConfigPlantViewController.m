//
//  CDDConfigViewController.m
//  PowerOneApp
//
//  Created by John Setting on 7/19/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "CDDConfigPlantViewController.h"
#import "PullEasyViewData.h"


@interface CDDConfigPlantViewController ()

@end

@implementation CDDConfigPlantViewController

@synthesize PlantName, Location, Latitude, Longitude, Address;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Plant";
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStyleBordered target:self action:@selector(logout)];
    
    
    [[[UIAlertView alloc] initWithTitle:@"Configuration" message:@"You are now able to configure your Logger device you received to start the setup of your site. Please go through the configuration process and once completed, your site will be presented." delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil] show];
    
    PlantName.delegate = self;
    Location.delegate = self;
    Address.delegate = self;
    Latitude.delegate = self;
    Longitude.delegate = self;
    
    theAccelerometer = [UIAccelerometer sharedAccelerometer];
    theAccelerometer.updateInterval = 1/50;
    theAccelerometer.delegate = self;
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move, location is updated
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation; // get best current locaton coords
    locationManager.headingFilter = 1;
    [locationManager startUpdatingLocation];
	[locationManager startUpdatingHeading];
    
    
    NSString *configAddress = [NSString stringWithFormat:@"http://%@/plant.xml", ipAddress];
    NSLog(@"%@", configAddress);
    
}

- (void) logout
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [[[UIAlertView alloc] initWithTitle:@"Logout" message:@"You have successfully logged out!" delegate:self.parentViewController cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil] show];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if (textField == PlantName) {
        PlantName.text = textField.text;
    } else if (textField == Location) {
        Location.text = textField.text;
    } else if (textField == Location) {
        Location.text = textField.text;
    } else if (textField == Location) {
        Location.text = textField.text;
    } else if (textField == Location) {
        Location.text = textField.text;
    }
    
    return YES;
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)ConfirmAction:(id)sender {
    
    NSString *configAddress = [NSString stringWithFormat:@"http://%@/plant.xml", ipAddress];
    NSLog(@"%@", configAddress);
    
    NSLog(@"%@", encode);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/protect/plant.htm", ipAddress]]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:ipAddress forHTTPHeaderField:@"Host"];
    [request setValue:[NSString stringWithFormat:@"Basic %@", encode] forHTTPHeaderField:@"Authorization"];
    //[request setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
    //[request setValue:@"gzip, deflate" forHTTPHeaderField:@"Accept-Encoding"];
    //[request setValue:@"keep-alive" forHTTPHeaderField:@"Connection"];
    NSString* str = [NSString stringWithFormat:@"name=%@&loc=%@&addr=%@%%26lat=%@&long=%@&cstd=10", PlantName.text, Location.text, Address.text, Latitude.text, Longitude.text];
    [request setHTTPBody:[str dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSError *error;
    NSURLResponse *response;
    
    NSLog(@"%@", response);
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString* another = [NSString stringWithUTF8String:[data bytes]];
    NSLog(@"%@", error);
    NSLog(@"%@ : %@", newStr, another);
    
    CDDConfigNetworkViewController *network = [[CDDConfigNetworkViewController alloc] initWithNibName:@"CDDConfigNetworkViewController" bundle:NULL];
    network->ipAddress = ipAddress;
    network->encode = encode;
    [self.navigationController pushViewController:network animated:YES];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    int degrees = newLocation.coordinate.latitude;
    double decimal = fabs(newLocation.coordinate.latitude - degrees);
    int minutes = decimal * 60;
    double seconds = decimal * 3600 - minutes * 60;
    NSString *lat = [NSString stringWithFormat:@"%d° %d' %1.4f\"",
                     degrees, minutes, seconds];
    NSLog(@" Current Latitude : %@",lat);
    Latitude.text = lat;
    degrees = newLocation.coordinate.longitude;
    decimal = fabs(newLocation.coordinate.longitude - degrees);
    minutes = decimal * 60;
    seconds = decimal * 3600 - minutes * 60;
    NSString *longt = [NSString stringWithFormat:@"%d° %d' %1.4f\"",
                       degrees, minutes, seconds];
    NSLog(@" Current Longitude : %@",longt);
    Longitude.text = longt;
}

#pragma mark CLLocationManager Delegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    currentLocation = [locations objectAtIndex:0];
    [locationManager stopUpdatingLocation];
    NSLog(@"Detected Location : %f, %f", currentLocation.coordinate.latitude, currentLocation.coordinate.longitude);
    Latitude.text = [NSString stringWithFormat:@"%f", currentLocation.coordinate.latitude];
    Longitude.text = [NSString stringWithFormat:@"%f", currentLocation.coordinate.longitude];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
            if (error){
                NSLog(@"Geocode failed with error: %@", error);
                return;
            }
            CLPlacemark *placemark = [placemarks objectAtIndex:0];
            Address.text = [NSString stringWithFormat:@"%@, %@, %@, %@", [placemark.addressDictionary valueForKey:@"Street"], [placemark.addressDictionary valueForKey:@"SubAdministrativeArea"], [placemark.addressDictionary valueForKey:@"State"], [placemark.addressDictionary valueForKey:@"CountryCode"]];

    }];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    // Convert Degree to Radian and move the needle
	float oldRad =  -manager.heading.trueHeading * M_PI / 180.0f;
	float newRad =  -newHeading.trueHeading * M_PI / 180.0f;
    
	NSLog(@"%f (%f) => %f (%f)", manager.heading.trueHeading, oldRad, newHeading.trueHeading, newRad);
    
}

@end
