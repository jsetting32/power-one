//
//  YWWeeklyForecastCell.m
//  PowerOneApp
//
//  Created by John Setting on 6/24/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "YWWeeklyForecastCell.h"

@implementation YWWeeklyForecastCell

@synthesize day, date, text, low, high, code, pic, currentTemp, currentCity, currentState;

@end
