//
//  CDDLoginViewController.h
//  PowerOneApp
//
//  Created by John Setting on 7/19/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "CDDTableViewController.h"
#import "CDDDetailsViewController.h"
#import "Base64.h"

@interface CDDLoginViewController : UIViewController <UITextFieldDelegate>
{
    IBOutlet UITextField *ipAddressTextField;
    IBOutlet UITextField *username;
    IBOutlet UITextField *password;

    IBOutlet UIButton *loginButton;

    NSOperationQueue *_queue;
    NSArray *_feeds;
    
    NSMutableArray *_allEntries;
    
    NSMutableArray *_configEntries;
    NSMutableArray *_statusEntries;
    NSMutableArray *allData;
    NSMutableArray *cddData;
    
}


@property (retain) NSOperationQueue *queue;
@property (retain) NSArray *feeds;
@property (retain) NSMutableArray *configEntries;
@property (retain) NSMutableArray *statusEntries;
@property (retain) NSMutableArray *allEntries;

@property (strong, nonatomic) IBOutlet UITextField *ipAddressTextField;
@property (strong, nonatomic) IBOutlet UITextField *username;
@property (strong, nonatomic) IBOutlet UITextField *password;

@property (strong, nonatomic) IBOutlet UIButton *loginButton;
- (IBAction)cancelButtonAction:(id)sender;

@end
