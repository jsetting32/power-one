//
//  PerformanceViewController.m
//  PowerOneApp
//
//  Created by John Setting on 6/21/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "PerformanceViewController.h"
#define LABEL_TAG 1000
#define IMAGE_TAG 500
#define PERCENTAGE_TAG 250
@interface PerformanceViewController ()

@end

@implementation PerformanceViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:self action:@selector(popped)];
        
        UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(singleTapping:)];
        self.navigationItem.rightBarButtonItem = button;

    }
    return self;
}


-(void)singleTapping:(UIGestureRecognizer *)recognizer
{
    
    UIImagePickerControllerSourceType type =
    UIImagePickerControllerSourceTypePhotoLibrary;
    BOOL ok = [UIImagePickerController isSourceTypeAvailable:type];
    if (!ok) {
        NSLog(@"alas");
        return;
    } else {
    
        NSLog(@"image click");
        NSString *viewPhoto = @"View profile photo";
        NSString *takePhoto = @"Take Photo";
        NSString *editPhoto = @"Choose Existing Photo";
        NSString *cancelTitle = @"Cancel";
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Profile Photo" delegate:self cancelButtonTitle:cancelTitle destructiveButtonTitle:nil otherButtonTitles:viewPhoto, takePhoto, editPhoto, nil];
        [actionSheet showInView:self.view];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //Get the name of the current pressed button
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    //if  ([buttonTitle isEqualToString:@"Destructive Button"]) {
    //    NSLog(@"Destructive pressed --> Delete Something");
    //}
    if ([buttonTitle isEqualToString:@"View profile photo"]) {
        NSLog(@"View profile photo presses");
        //UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        //picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        //picker.delegate = self;
        //[self presentModalViewController:picker animated:YES];
        //[picker release];
        
    }
    if ([buttonTitle isEqualToString:@"Take Photo"]) {
        NSLog(@"Take Photo pressed");
        [self takePicture:nil];
    }
    if ([buttonTitle isEqualToString:@"Choose Existing Photo"]) {
        NSLog(@"Choose Existing Photo pressed");
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        //[self.view addSubview:imagePicker];*/
        [self showSavedMediaBrowser];
    }
    if ([buttonTitle isEqualToString:@"Cancel"]) {
        NSLog(@"Cancel pressed --> Cancel ActionSheet");
    }
}

- (void) showSavedMediaBrowser {
    [self startMediaBrowserFromViewController:self usingDelegate:self];
}

- (BOOL) startMediaBrowserFromViewController: (UIViewController*) controller usingDelegate: (id <UIImagePickerControllerDelegate, UINavigationControllerDelegate>) delegate
{
    if (([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO) || (delegate == nil) || (controller == nil))
        return NO;
    
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    // Displays saved pictures and movies, if both are available, from the
    // Camera Roll album.
    mediaUI.mediaTypes =
    [UIImagePickerController availableMediaTypesForSourceType:
     UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    mediaUI.allowsEditing = NO;
    
    mediaUI.delegate = delegate;
    
    [controller presentViewController:mediaUI animated:YES completion:nil];
    return YES;
}

-(void) takePicture:(id) sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    }
    else
    {
        [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
    
    [imagePicker setDelegate:self];

    
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    NSData *data = UIImagePNGRepresentation(image);
    NSString *myGrabbedImage = @"myGrabbedImage.png";
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [path objectAtIndex:0];
    NSString *fullPathToFile = [documentDirectory stringByAppendingPathComponent:myGrabbedImage];
    
    [data writeToFile:fullPathToFile atomically:YES];
    
    [[self imageView] setImage:image];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSString *myGrabbedImage = @"myGrabbedImage.png";
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES);
    NSString *docdumentDirectory = [path objectAtIndex:0];
    NSString *fullPath = [docdumentDirectory stringByAppendingPathComponent:myGrabbedImage];
    NSData *data = [NSData dataWithContentsOfFile:fullPath];
    [[self imageView] setImage:[UIImage imageWithData:data]];
    

    
    UIImage *buttonImage = [UIImage imageNamed:@"Very-Basic-Info-icon.png"];
    //create the button and assign the image
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    //set the frame of the button to the size of the image
    button.frame = CGRectMake(self.view.frame.size.width - buttonImage.size.width - 5, self.view.frame.origin.y + 5, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(InformationPopUp:) forControlEvents:UIControlEventTouchUpInside];
    //create a UIBarButtonItem with the button as a custom view
    [self.view addSubview:button];
    
    
    _views = [[NSMutableArray alloc] init];
    
    _dates = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 50, 20)];
    _dates.font = [UIFont systemFontOfSize:8.0];
    [self.view addSubview:_dates];
    
    NSArray *keys = [_inverterValuesAndNames allKeys];
    id aKey = [keys objectAtIndex:0];
    id anObject = [_inverterValuesAndNames objectForKey:aKey];
    
    int j = 0;
    for (int i = 0; i < [_inverterValuesAndNames count]; ++i) {
        

        
        UIView *view = [[UIView alloc] init];
        view.backgroundColor = [UIColor grayColor];
        view.layer.cornerRadius = 5.0f;
        view.layer.masksToBounds = YES;
        view.layer.shadowColor = [[UIColor blackColor] CGColor];
        view.layer.shadowOffset = CGSizeMake(10, 10);
        
        UIPanGestureRecognizer *move = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panPiece:)];
        UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scalePiece:)];
        UIRotationGestureRecognizer *rotate = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotatePiece:)];
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(showResetMenu:)];
        [view addGestureRecognizer:move];
        [view addGestureRecognizer:pinch];
        [view addGestureRecognizer:rotate];
        [view addGestureRecognizer:longPress];
        
        
        UILabel *title;
        UILabel *values;
        UILabel *percentage;
        if (i % 3 == 0) j++;
    
        if (i < 3 * j) view.frame = CGRectMake(10 + i%3 * 100, 40 + 90*(j-1), 90, 80);
        
        title = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 70, 20)];
        title.font = [UIFont systemFontOfSize:8.0];
        id aKey = [keys objectAtIndex:i];
        id anObject = [_inverterValuesAndNames objectForKey:aKey];
        
        for(NSString *str in anObject)
        {
            if (!str || [str isKindOfClass:[NSNull class]])
                NSLog(@"Null Value at index: %@", str);
            else {}
        }
        
        title.text = aKey;
        title.backgroundColor = [UIColor clearColor];
        title.textAlignment = NSTextAlignmentCenter;
        [view addSubview:title];
        
        UIImageView *MeterBar = [[UIImageView alloc] initWithFrame:CGRectMake(view.bounds.origin.x + 5, view.bounds.origin.y + 42, 50, 40)];
        MeterBar.image = [UIImage imageNamed:@"battery-empty-icon.png"];
        MeterBar.backgroundColor = [UIColor clearColor];
        [view addSubview:MeterBar];
        
        UIImageView *batteryIcon = [[UIImageView alloc] initWithFrame:CGRectMake(view.bounds.origin.x - 10, view.bounds.origin.y + 63, 38, 14)];
        batteryIcon.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"michigan_vacation_rentals_green_bar-997x301.jpg"]];
        [batteryIcon setTag:IMAGE_TAG+i];
        [view addSubview:batteryIcon];
        
        values = [[UILabel alloc] initWithFrame:CGRectMake(10, 30, 70, 20)];
        values.font = [UIFont systemFontOfSize:8.0];
        values.textAlignment = NSTextAlignmentCenter;
        [values setTag:LABEL_TAG+i];
        [view addSubview:values];
        
        percentage = [[UILabel alloc] initWithFrame:CGRectMake(view.bounds.origin.x + 60, view.bounds.origin.y + 42, 20, 40)];
        percentage.font = [UIFont systemFontOfSize:8.0];
        percentage.textAlignment = NSTextAlignmentCenter;
        [percentage setTag:PERCENTAGE_TAG+i];
        [view addSubview:percentage];
        
        [_views addObject:view];
        [self.view addSubview:view];
        
    }
    
    //int fullSize = [myBottle.barBottem intValue] - [myBottle.barTop intValue];
    CGRect frame = CGRectMake(50, 0, 220, 23);
    slider = [[UISlider alloc] initWithFrame:frame];
    [slider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    [slider setBackgroundColor:[UIColor clearColor]];
    slider.continuous = YES;                                            // Make the slider 'stick' as it is moved.
    [slider setMinimumValue:0];

    [slider setMaximumValue:((float)[anObject count] - 2)];             // Minus 2 to count because the last date value is allocated but may not have a value
    [self valueChanged:slider];                                         // Add the method transformations to the UISlider
    //slider.transform = CGAffineTransformMakeRotation(M_PI * - 0.5);   // Used to flip the UISlider
    self.navigationItem.titleView = slider;

    //_label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 30)];
    //[self.view addSubview:_label];

    // Doesn't work //
    timer = [NSTimer timerWithTimeInterval:0.25 target:self selector:@selector(animateSlider:) userInfo:nil repeats:YES];
    [timer fire];
    // Doesn't work //

}
/* ---- Doesn't Work ---- */
- (void)animateSlider:(NSTimer *)theTimer
{
    NSLog(@"Animating");
    NSUInteger index = (NSUInteger)(slider.value + 0.5); // Round the number.
    [slider setValue:index animated:YES];
}
/* ---- Doesn't Work ---- */



- (void)valueChanged:(UISlider*)sender
{
    NSUInteger index = (NSUInteger)(slider.value + 0.5); // Round the number.
    [slider setValue:index animated:YES];
    //NSLog(@"index: %i", index);
    NSArray *keys = [_inverterValuesAndNames allKeys];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:2];
    [formatter setMinimumFractionDigits:2];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setRoundingMode:NSNumberFormatterRoundHalfUp];
    
    for (int i = 0; i < [_views count]; i++)
    {
        
        UILabel *label = (UILabel*)[self.view viewWithTag:LABEL_TAG+i];
        id aKey = [keys objectAtIndex:i];
        id anObject = [_inverterValuesAndNames objectForKey:aKey];
        NSString *str = [anObject objectAtIndex:index];
        if (!str || [str isKindOfClass:[NSNull class]]) {
            
        } else {
            _number = [anObject objectAtIndex:index]; // <-- This is the number you want.
        }
        [formatter stringFromNumber:_number];
        label.text = [NSString stringWithFormat:@"%@", _number];
        label.backgroundColor = [UIColor clearColor];
        NSLog(@"%@", _number);
        
        UILabel *percentageLabel = (UILabel*)[self.view viewWithTag:PERCENTAGE_TAG+i];
        percentageLabel.text = [NSString stringWithFormat:@"%f%%", [_number floatValue]/235 * 100];
        percentageLabel.backgroundColor = [UIColor clearColor];
        
        // ------------------- Battery Animation ------------------ //
        UIImageView *newBar = (UIImageView*)[self.view viewWithTag:IMAGE_TAG+i];
        newBar.layer.borderColor = [[UIColor clearColor] CGColor];
        newBar.layer.borderWidth = 0.0f;
        newBar.layer.cornerRadius = 2.0f;
        CABasicAnimation *scaleToValue = [CABasicAnimation animationWithKeyPath:@"transform.scale.x"];
        scaleToValue.duration = 0.0f;
        scaleToValue.delegate = self;
        newBar.layer.anchorPoint = CGPointMake(0.0, 1.0);
        scaleToValue.toValue = [NSNumber numberWithFloat:[_number floatValue]/235];
        [newBar.layer addAnimation:scaleToValue forKey:@"scaleRight"];
        CGAffineTransform scaleTo = CGAffineTransformMakeScale([_number floatValue]/235, 1.0f );
        newBar.transform = scaleTo;
        // ------------------- Battery Animation ------------------ //

        
        
        if (!str || [str isKindOfClass:[NSNull class]]) {
            [[_views objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.5]];
        } else {
            if (([_number integerValue] - 19.0) < 0.0) {
                [[_views objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:[_number integerValue]/255.0 alpha:0.5]];
            } else if (([_number integerValue] - 82.0) < 0.0) {
                [[_views objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:0.0/255.0 green:([_number integerValue] - 19.0)/255.0 blue:[_number integerValue]/255.0 alpha:0.5]];
            } else {
                [[_views objectAtIndex:i] setBackgroundColor:[UIColor colorWithRed:([_number integerValue] - 82.0)/255.0 green:([_number integerValue] - 19.0)/255.0 blue:[_number integerValue]/255.0 alpha:0.5]];
            }
        }
    }
    
    _dates.text = [_inverterDates objectAtIndex:index]; // <-- This is the number you want.


}

- (void)InformationPopUp:(id)sender{
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 200)];
    
    NSNumberFormatter *nFormatter = [[NSNumberFormatter alloc] init];
    nFormatter = [[NSNumberFormatter alloc] init];
    [nFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [nFormatter setRoundingMode:NSNumberFormatterRoundHalfUp];
    [nFormatter setMaximumFractionDigits:2];
    [nFormatter setMinimumFractionDigits:2];
        
    CGRect welcomeLabelRect = contentView.bounds;
    welcomeLabelRect.origin.y = 20;
    welcomeLabelRect.size.height = 20;
    UIFont *welcomeLabelFont = [UIFont boldSystemFontOfSize:14];
    UILabel *welcomeLabel = [[UILabel alloc] initWithFrame:welcomeLabelRect];
    welcomeLabel.text = @"kWh Accumulated : Money Saved";
    welcomeLabel.font = welcomeLabelFont;
    welcomeLabel.textColor = [UIColor whiteColor];
    welcomeLabel.textAlignment = NSTextAlignmentCenter;
    welcomeLabel.backgroundColor = [UIColor clearColor];
    welcomeLabel.shadowColor = [UIColor blackColor];
    welcomeLabel.shadowOffset = CGSizeMake(0, 1);
    [contentView addSubview:welcomeLabel];
    
    CGRect todayInfoLabelRect = CGRectInset(contentView.bounds, 5, 5);
    todayInfoLabelRect.origin.y = CGRectGetMaxY(welcomeLabelRect)-100;
    todayInfoLabelRect.size.height -= CGRectGetMinY(todayInfoLabelRect);
    UILabel *todayInfoLabel = [[UILabel alloc] initWithFrame:todayInfoLabelRect];
    todayInfoLabel.font = [UIFont systemFontOfSize:12.0];
    todayInfoLabel.numberOfLines = 6;
    todayInfoLabel.textColor = [UIColor whiteColor];
    todayInfoLabel.textAlignment = NSTextAlignmentCenter;
    todayInfoLabel.backgroundColor = [UIColor clearColor];
    todayInfoLabel.shadowColor = [UIColor blackColor];
    todayInfoLabel.shadowOffset = CGSizeMake(0, 1);
    [contentView addSubview:todayInfoLabel];
    
    CGRect weekInfoLabelRect = CGRectInset(contentView.bounds, 5, 5);
    weekInfoLabelRect.origin.y = CGRectGetMaxY(welcomeLabelRect)-50;
    weekInfoLabelRect.size.height -= CGRectGetMinY(weekInfoLabelRect);
    UILabel *weekInfoLabel = [[UILabel alloc] initWithFrame:weekInfoLabelRect];
    weekInfoLabel.font = [UIFont systemFontOfSize:12.0];
    weekInfoLabel.numberOfLines = 6;
    weekInfoLabel.textColor = [UIColor whiteColor];
    weekInfoLabel.textAlignment = NSTextAlignmentCenter;
    weekInfoLabel.backgroundColor = [UIColor clearColor];
    weekInfoLabel.shadowColor = [UIColor blackColor];
    weekInfoLabel.shadowOffset = CGSizeMake(0, 1);
    [contentView addSubview:weekInfoLabel];
    
    CGRect monthInfoLabelRect = CGRectInset(contentView.bounds, 5, 5);
    monthInfoLabelRect.origin.y = CGRectGetMaxY(welcomeLabelRect)+0;
    monthInfoLabelRect.size.height -= CGRectGetMinY(monthInfoLabelRect);
    UILabel *monthInfoLabel = [[UILabel alloc] initWithFrame:monthInfoLabelRect];

    monthInfoLabel.font = [UIFont systemFontOfSize:12.0];
    monthInfoLabel.numberOfLines = 6;
    monthInfoLabel.textColor = [UIColor whiteColor];
    monthInfoLabel.textAlignment = NSTextAlignmentCenter;
    monthInfoLabel.backgroundColor = [UIColor clearColor];
    monthInfoLabel.shadowColor = [UIColor blackColor];
    monthInfoLabel.shadowOffset = CGSizeMake(0, 1);
    [contentView addSubview:monthInfoLabel];
    
    CGRect lifetimeInfoLabelRect = CGRectInset(contentView.bounds, 5, 5);
    lifetimeInfoLabelRect.origin.y = CGRectGetMaxY(welcomeLabelRect)+50;
    lifetimeInfoLabelRect.size.height -= CGRectGetMinY(lifetimeInfoLabelRect);
    UILabel *lifetimeInfoLabel = [[UILabel alloc] initWithFrame:lifetimeInfoLabelRect];
    lifetimeInfoLabel.font = [UIFont systemFontOfSize:12.0];
    lifetimeInfoLabel.numberOfLines = 6;
    lifetimeInfoLabel.textColor = [UIColor whiteColor];
    lifetimeInfoLabel.textAlignment = NSTextAlignmentCenter;
    lifetimeInfoLabel.backgroundColor = [UIColor clearColor];
    lifetimeInfoLabel.shadowColor = [UIColor blackColor];
    lifetimeInfoLabel.shadowOffset = CGSizeMake(0, 1);
    [contentView addSubview:lifetimeInfoLabel];
    
    CGRect astriskLabelRect = CGRectInset(contentView.bounds, 5, 5);
    astriskLabelRect.origin.y = CGRectGetMaxY(welcomeLabelRect)+110;
    astriskLabelRect.size.height -= CGRectGetMinY(astriskLabelRect);
    UILabel *astriskLabel = [[UILabel alloc] initWithFrame:astriskLabelRect];
    astriskLabel.text = @"* The cost per kiloWatt/hour is assumed to be at the lowest Tier\nconsumption. If you want the saved amount to be as accurate as possible,\nplease enter your monthly electricity bill in account settings.";
    astriskLabel.font = [UIFont systemFontOfSize:8.0];
    astriskLabel.numberOfLines = 4;
    astriskLabel.textColor = [UIColor whiteColor];
    astriskLabel.textAlignment = NSTextAlignmentCenter;
    astriskLabel.backgroundColor = [UIColor clearColor];
    astriskLabel.shadowColor = [UIColor blackColor];
    astriskLabel.shadowOffset = CGSizeMake(0, 1);
    [contentView addSubview:astriskLabel];
    
    
    [[KGModal sharedInstance] showWithContentView:contentView andAnimated:YES];
}



- (void) popped
{
    //NSLog(@"Popped");
    
    [UIView  transitionWithView:self.navigationController.view duration:0.4  options:UIViewAnimationOptionTransitionFlipFromRight animations:^(void) {
        BOOL oldState = [UIView areAnimationsEnabled];
        [UIView setAnimationsEnabled:NO];
        [self.navigationController popViewControllerAnimated:YES];
        [UIView setAnimationsEnabled:oldState];
    } completion:nil];
}


#pragma mark - Gesture Recognizers
/**
 Scale and rotation transforms are applied relative to the layer's anchor point this method moves a gesture recognizer's view's anchor point between the user's fingers.
 */
- (void)adjustAnchorPointForGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        UIView *piece = gestureRecognizer.view;
        CGPoint locationInView = [gestureRecognizer locationInView:piece];
        CGPoint locationInSuperview = [gestureRecognizer locationInView:piece.superview];
        
        piece.layer.anchorPoint = CGPointMake(locationInView.x / piece.bounds.size.width, locationInView.y / piece.bounds.size.height);
        piece.center = locationInSuperview;
    }
}


/**
 Shift the piece's center by the pan amount.
 Reset the gesture recognizer's translation to {0, 0} after applying so the next callback is a delta from the current position.
 */
- (void)panPiece:(UIPanGestureRecognizer *)gestureRecognizer
{
    UIView *piece = [gestureRecognizer view];
    
    [self adjustAnchorPointForGestureRecognizer:gestureRecognizer];
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
        CGPoint translation = [gestureRecognizer translationInView:[piece superview]];
        
        [piece setCenter:CGPointMake([piece center].x + translation.x, [piece center].y + translation.y)];
        [gestureRecognizer setTranslation:CGPointZero inView:[piece superview]];
    }
}

/**
 Rotate the piece by the current rotation.
 Reset the gesture recognizer's rotation to 0 after applying so the next callback is a delta from the current rotation.
 */
- (void)rotatePiece:(UIRotationGestureRecognizer *)gestureRecognizer
{
    [self adjustAnchorPointForGestureRecognizer:gestureRecognizer];
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
        [gestureRecognizer view].transform = CGAffineTransformRotate([[gestureRecognizer view] transform], [gestureRecognizer rotation]);
        [gestureRecognizer setRotation:0];
    }
}

/**
 Scale the piece by the current scale.
 Reset the gesture recognizer's rotation to 0 after applying so the next callback is a delta from the current scale.
 */
- (void)scalePiece:(UIPinchGestureRecognizer *)gestureRecognizer
{
    [self adjustAnchorPointForGestureRecognizer:gestureRecognizer];
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
        [gestureRecognizer view].transform = CGAffineTransformScale([[gestureRecognizer view] transform], [gestureRecognizer scale], [gestureRecognizer scale]);
        [gestureRecognizer setScale:1];
    }
}


/**
 Ensure that the pinch, pan and rotate gesture recognizers on a particular view can all recognize simultaneously.
 Prevent other gesture recognizers from recognizing simultaneously.
 */
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    // If the gesture recognizers's view isn't one of our pieces, don't allow simultaneous recognition.
    //if (gestureRecognizer.view != self.firstPieceView && gestureRecognizer.view != self.secondPieceView && gestureRecognizer.view != self.thirdPieceView) {
    //    return NO;
    //}
    
    // If the gesture recognizers are on different views, don't allow simultaneous recognition.
    if (gestureRecognizer.view != otherGestureRecognizer.view) {
        return NO;
    }
    
    // If either of the gesture recognizers is the long press, don't allow simultaneous recognition.
    if ([gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]] || [otherGestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]]) {
        return NO;
    }
    
    return YES;
}

/**
 Display a menu with a single item to allow the piece's transform to be reset.
 */
- (void)showResetMenu:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan) {
        
        [self becomeFirstResponder];
        self.pieceForReset = [gestureRecognizer view];
        
        /*
         Set up the reset menu.
         */
        NSString *menuItemTitle = NSLocalizedString(@"Reset", @"Reset menu item title");
        UIMenuItem *resetMenuItem = [[UIMenuItem alloc] initWithTitle:menuItemTitle action:@selector(resetPiece:)];
        
        UIMenuController *menuController = [UIMenuController sharedMenuController];
        [menuController setMenuItems:@[resetMenuItem]];
        
        CGPoint location = [gestureRecognizer locationInView:[gestureRecognizer view]];
        CGRect menuLocation = CGRectMake(location.x, location.y, 0, 0);
        [menuController setTargetRect:menuLocation inView:[gestureRecognizer view]];
        
        [menuController setMenuVisible:YES animated:YES];
    }
}

/**
 Animate back to the default anchor point and transform.
 */
- (void)resetPiece:(UIMenuController *)controller
{
    UIView *pieceForReset = self.pieceForReset;
    
    CGPoint centerPoint = CGPointMake(CGRectGetMidX(pieceForReset.bounds), CGRectGetMidY(pieceForReset.bounds));
    CGPoint locationInSuperview = [pieceForReset convertPoint:centerPoint toView:[pieceForReset superview]];
    
    [[pieceForReset layer] setAnchorPoint:CGPointMake(0.5, 0.5)];
    [pieceForReset setCenter:locationInSuperview];
    
    [UIView beginAnimations:nil context:nil];
    [pieceForReset setTransform:CGAffineTransformIdentity];
    [UIView commitAnimations];
}



#pragma mark - Received Memory Warning
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
