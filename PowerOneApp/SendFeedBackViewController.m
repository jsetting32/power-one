//
//  SendFeedBackViewController.m
//  PowerOneApp
//
//  Created by John Setting on 6/21/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "SendFeedBackViewController.h"

@interface SendFeedBackViewController ()

@end

@implementation SendFeedBackViewController

@synthesize bgimage,spinner,loadingLabel, TextViewBody, reportButton, feedbackTableView, Scroller, stars;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) didReceiveType:(NSString *) message
{
    subject = message;
    [feedbackTableView reloadData];
}

-(void)newRating:(DLStarRatingControl *)control :(float)rating {
	self.stars.text = [NSString stringWithFormat:@"%0.1f",rating];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Send Feedback";
    NSLog(@"%@", TextViewBody.text);

    self.navigationController.navigationBar.tintColor = [UIColor blackColor];

    // add this in ViewDidLoad
    //set loading label to alpha 0 so its not displayed
    loadingLabel.alpha = 0;
    
    SWRevealViewController *revealController = [self revealViewController];
    
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    //[self.view addGestureRecognizer:revealController.panGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"] style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    UIBarButtonItem *rightRevealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"19-gear.png"] style:UIBarButtonItemStyleBordered target:revealController action:@selector(rightRevealToggle:)];
    self.navigationItem.rightBarButtonItem = rightRevealButtonItem;
    
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    [toolbar setBarStyle:UIBarStyleBlackTranslucent];
    [toolbar sizeToFit];
    
    UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *doneButton =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(resignKeyboard)];
    
    NSArray *itemsArray = [NSArray arrayWithObjects:flexButton, doneButton, nil];
    
    //[flexButton release];
    //[doneButton release];
    [toolbar setItems:itemsArray];
    
    [TextViewBody setInputAccessoryView:toolbar];
    TextViewBody.layer.cornerRadius = 5.0;
    TextViewBody.layer.borderColor = [[UIColor grayColor] CGColor];
    TextViewBody.layer.borderWidth = 2.0;
    
    UILabel *label = [[UILabel alloc] init];
    label.text = @"Feedback";
    label.frame = CGRectMake(20, Scroller.frame.origin.y + 256, 100, 15);
    label.font = [UIFont systemFontOfSize:12.0];
    label.backgroundColor = [UIColor clearColor];
    [Scroller addSubview:label];
    
    
    DLStarRatingControl *customNumberOfStars = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(0, Scroller.frame.origin.y + 250, 320, 90) andStars:5 isFractional:YES];
    customNumberOfStars.delegate = self;
	customNumberOfStars.backgroundColor = [UIColor groupTableViewBackgroundColor];
	customNumberOfStars.autoresizingMask =  UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
	customNumberOfStars.rating = 2.5;
	[Scroller addSubview:customNumberOfStars];
    
}

-(void)resignKeyboard {
    [TextViewBody resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)sendEmail:(id)sender
{
    // create soft wait overlay so the user knows whats going on in the background.
    [self createWaitOverlay];
    
    //the guts of the message.
    SKPSMTPMessage *testMsg = [[SKPSMTPMessage alloc] init];
    testMsg.fromEmail = @"jsetting32@icloud.com";
    testMsg.toEmail = @"jsetting32@gmail.com";
    testMsg.relayHost = @"smtp.gmail.com";
    //testMsg.relayPorts = [NSArray arrayWithObject:@"465"];
    testMsg.requiresAuth = YES;
    testMsg.login = @"jsetting32";
    testMsg.pass = @"Football33!";
    
    //testMsg.requiresAuth = NO;
    //testMsg.login = @"jsetting32";
    //testMsg.pass = @"football33";
    
    testMsg.subject = [NSString stringWithFormat:@"%@, with a rating of %@", subject, stars.text];
    testMsg.wantsSecure = YES; // smtp.gmail.com doesn't work without TLS!
    
    
    
    // Only do this for self-signed certs!
    // testMsg.validateSSLChain = NO;
    testMsg.delegate = self;
    
    //email contents
    //NSString * bodyMessage = [NSString stringWithFormat:@"This is the body of the email. You can put anything in here that you want."];
    
    NSString * bodyMessage = TextViewBody.text;
    
    NSDictionary *plainPart = [NSDictionary dictionaryWithObjectsAndKeys:@"text/plain",kSKPSMTPPartContentTypeKey,
                               bodyMessage ,kSKPSMTPPartMessageKey,@"8bit",kSKPSMTPPartContentTransferEncodingKey,nil];
    
    testMsg.parts = [NSArray arrayWithObjects:plainPart,nil];
    
    
    if (TextViewBody.text == [NSNull class]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Explain the issue" message:@"Please describe your issue with at least a sentence." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
    } else if (subject == [NSNull class]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Enter a subject" message:@"Please enter a subject you noticed a problem with" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [alert show];
    } else {
        [testMsg send];
    }
    
}


- (void)messageSent:(SKPSMTPMessage *)message
{
    //[message release];
    
    //message has been successfully sent . you can notify the user of that and remove the wait overlay
    [self removeWaitOverlay];
    
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message Sent" message:@"Thanks, we have sent your feedback" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    //[alert release];
}

- (void)messageFailed:(SKPSMTPMessage *)message error:(NSError *)error
{
    //[message release];
    [self removeWaitOverlay];
    
    NSLog(@"delegate - error(%d): %@", [error code], [error localizedDescription]);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Error" message:@"Sending Failed - Unknown Error :-("
                                                   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    //[alert release];
}



-(void)createWaitOverlay {
    
    // fade the overlay in
    loadingLabel.text = @"Sending...";
    bgimage = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,320,480)];
    bgimage.image = [UIImage imageNamed:@"waitOverLay.png"];
    [self.view addSubview:bgimage];
    bgimage.alpha = 0;
    [bgimage addSubview:loadingLabel];
    loadingLabel.alpha = 0;
    
    
    [UIView beginAnimations: @"Fade In" context:nil];
    [UIView setAnimationDelay:0];
    [UIView setAnimationDuration:.5];
    bgimage.alpha = 1;
    loadingLabel.alpha = 1;
    [UIView commitAnimations];
    [self startSpinner];
    
    //[bgimage release];
    
}

-(void)removeWaitOverlay {
    
    //fade the overlay out
    
    [UIView beginAnimations: @"Fade Out" context:nil];
    [UIView setAnimationDelay:0];
    [UIView setAnimationDuration:.5];
    bgimage.alpha = 0;
    loadingLabel.alpha = 0;
    [UIView commitAnimations];
    [self stopSpinner];
    
    
}

-(void)startSpinner {
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.hidden = FALSE;
    spinner.frame = CGRectMake(137, 160, 50, 50);
    [spinner setHidesWhenStopped:YES];
    [self.view addSubview:spinner];
    [self.view bringSubviewToFront:spinner];
    [spinner startAnimating];
}

-(void)stopSpinner {
    
    [spinner stopAnimating];
    [spinner removeFromSuperview];
    //[spinner release];
    
}

#pragma mark - Table view delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

#pragma mark - Table view delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [feedbackTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (indexPath.section == 0) {
        if (subject == nil) {
            cell.textLabel.text = @"Select a Product";
        } else {
            cell.textLabel.text = subject;
        }
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [feedbackTableView deselectRowAtIndexPath:indexPath animated:YES];
    FeedbackTypes *view = [[FeedbackTypes alloc] init];
    view.delegate = self;
    [self.navigationController pushViewController:view animated:YES];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isKindOfClass:[DLStarRatingControl class]])
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

@end
