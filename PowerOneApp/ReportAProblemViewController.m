//
//  ReportAProblemViewController.m
//  PowerOneApp
//
//  Created by John Setting on 6/21/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "ReportAProblemViewController.h"

@interface ReportAProblemViewController ()

@end

@implementation ReportAProblemViewController

@synthesize bgimage,spinner,loadingLabel, TextViewBody, reportButton, reportAProblemTableView;

- (void)didReceiveType:(NSString *)problemType {
	newLabel = problemType;
    [reportAProblemTableView reloadData];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Report A Problem";
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];

    // add this in ViewDidLoad
    //set loading label to alpha 0 so its not displayed
    loadingLabel.alpha = 0;
    
    SWRevealViewController *revealController = [self revealViewController];
    
    TextViewBody.delegate = self;
    self.scroller.delegate = self;
    

    
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    [self.view addGestureRecognizer:revealController.panGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"] style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
    
    UIBarButtonItem *rightRevealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"19-gear.png"] style:UIBarButtonItemStyleBordered target:revealController action:@selector(rightRevealToggle:)];
    self.navigationItem.rightBarButtonItem = rightRevealButtonItem;
    
    
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    [toolbar setBarStyle:UIBarStyleBlackTranslucent];
    [toolbar sizeToFit];
    
    UIBarButtonItem *flexButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *doneButton =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(keyboardWillBeHidden:)];
    
    NSArray *itemsArray = [NSArray arrayWithObjects:flexButton, doneButton, nil];
    

    
    [TextViewBody setInputAccessoryView:toolbar];
    TextViewBody.layer.cornerRadius = 5.0;
    TextViewBody.layer.borderColor = [[UIColor grayColor] CGColor];
    TextViewBody.layer.borderWidth = 2.0;
    
    //[flexButton release];
    //[doneButton release];
    [toolbar setItems:itemsArray];
    

    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(IBAction)sendEmail:(id)sender
{
    
    // create soft wait overlay so the user knows whats going on in the background.
    [self createWaitOverlay];
    
    //the guts of the message.
    SKPSMTPMessage *testMsg = [[SKPSMTPMessage alloc] init];
    testMsg.fromEmail = @"jsetting32@icloud.com";
    testMsg.toEmail = @"jsetting32@gmail.com";
    testMsg.relayHost = @"smtp.gmail.com";
    testMsg.requiresAuth = YES;
    testMsg.login = @"jsetting32";
    testMsg.pass = @"Football33!";
    testMsg.subject = [NSString stringWithFormat:@"%@", newLabel];
    testMsg.wantsSecure = YES; // smtp.gmail.com doesn't work without TLS!
        
    // Only do this for self-signed certs!
    // testMsg.validateSSLChain = NO;
    testMsg.delegate = self;

    //email contents
    NSString * bodyMessage = TextViewBody.text;
    NSDictionary *plainPart = [NSDictionary dictionaryWithObjectsAndKeys:@"text/plain",kSKPSMTPPartContentTypeKey, bodyMessage ,kSKPSMTPPartMessageKey,@"8bit",kSKPSMTPPartContentTransferEncodingKey,nil];
    testMsg.parts = [NSArray arrayWithObjects:plainPart,nil];
    [testMsg send];

}

-(void) performSpinnerOnMainThread    {
    //[self performSelectorInBackground:@selector(installerTour) withObject:nil];
}

- (void)messageSent:(SKPSMTPMessage *)message
{
    //[message release];
    
    //message has been successfully sent . you can notify the user of that and remove the wait overlay
    [self removeWaitOverlay];
    
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Message Sent" message:@"Thanks, we have sent your message" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    //[alert release];
}

- (void)messageFailed:(SKPSMTPMessage *)message error:(NSError *)error
{
    //[message release];
    [self removeWaitOverlay];
    
    NSLog(@"delegate - error(%d): %@", [error code], [error localizedDescription]);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Error" message:@"Sending Failed - Unknown Error :-("
                                                   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    //[alert release];
}



-(void)createWaitOverlay {
    
    // fade the overlay in
    loadingLabel.text = @"Sending Test Drive...";
    bgimage = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,320,480)];
    bgimage.image = [UIImage imageNamed:@"waitOverLay.png"];
    [self.view addSubview:bgimage];
    bgimage.alpha = 0;
    [bgimage addSubview:loadingLabel];
    loadingLabel.alpha = 0;
    
    
    [UIView beginAnimations: @"Fade In" context:nil];
    [UIView setAnimationDelay:0];
    [UIView setAnimationDuration:.5];
    bgimage.alpha = 1;
    loadingLabel.alpha = 1;
    [UIView commitAnimations];
    [self performSpinnerOnMainThread];
    
    //[bgimage release];
    
}

-(void)removeWaitOverlay {
    
    //fade the overlay out
    
    [UIView beginAnimations: @"Fade Out" context:nil];
    [UIView setAnimationDelay:0];
    [UIView setAnimationDuration:.5];
    bgimage.alpha = 0;
    loadingLabel.alpha = 0;
    [UIView commitAnimations];
    [self stopSpinner];
    
    
}



-(void)startSpinner {
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.hidden = FALSE;
    spinner.frame = CGRectMake(137, 160, 50, 50);
    [spinner setHidesWhenStopped:YES];
    [self.view addSubview:spinner];
    [self.view bringSubviewToFront:spinner];
    [spinner startAnimating];
}

-(void)stopSpinner {
    
    [spinner stopAnimating];
    [spinner removeFromSuperview];
    //[spinner release];
    
}


#pragma mark - Table view delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [reportAProblemTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (newLabel == nil) {
        cell.textLabel.text = @"Select a Product";
    } else {
        cell.textLabel.text = newLabel;
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ProblemTypes *view = [[ProblemTypes alloc] init];
    view.delegate = self;
    [self.navigationController pushViewController:view animated:YES];
    
}


-(void)resignKeyboard {
}
- (void) registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)notification {
    NSDictionary* info = [notification userInfo];
    
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    
    self.scroller.contentInset = contentInsets;
    
    self.scroller.scrollIndicatorInsets = contentInsets;
    
    
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    
    // Your application might not need or want this behavior.
    
    CGRect aRect = self.view.frame;
    
    aRect.size.height -= kbSize.height;
    
    if (!CGRectContainsPoint(aRect, self.TextViewBody.frame.origin) ) {
        
        CGPoint scrollPoint = CGPointMake(0.0, self.TextViewBody.frame.origin.y-kbSize.height);
        
        [self.scroller setContentOffset:scrollPoint animated:YES];
        
    }
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification

{
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    self.scroller.contentInset = contentInsets;
    
    self.scroller.scrollIndicatorInsets = contentInsets;
    
    [self.TextViewBody resignFirstResponder];
    
    
}


- (void)dealloc {
    //[reportAProblemTableView release];
    //[reportButton release];
    //[TextViewBody release];
    //[bgimage release];
	//[loadingLabel release];
    //[spinner release];
    //[super dealloc];
}

@end
