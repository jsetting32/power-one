//
//  PullEasyViewData.h
//  PowerOneApp
//
//  Created by John Setting on 6/11/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface PullEasyViewData : NSObject

+ (id)sharedManager;

@property (nonatomic, strong) NSString *entityName;
@property (nonatomic, strong) NSString *entityID;
@property (nonatomic, strong) NSString *entityDescription;
@property (nonatomic, strong) NSString *entityInstallDate;

@property (nonatomic, strong) NSString *SystemStatus;
@property (nonatomic, strong) UIImage *SystemStatusIcon;
@property (nonatomic, strong) NSDictionary *SystemStatusAlerts;

@property (nonatomic, strong) NSString *nowValue;
@property (nonatomic, strong) NSString *nowValueFormatted;

@property (nonatomic, strong) NSString *todayValue;
@property (nonatomic, strong) NSString *todayValueFormatted;

@property (nonatomic, strong) NSString *weekValue;
@property (nonatomic, strong) NSString *weekValueFormatted;

@property (nonatomic, strong) NSString *monthValue;
@property (nonatomic, strong) NSString *monthValueFormatted;

@property (nonatomic, strong) NSString *lifeValue;
@property (nonatomic, strong) NSString *lifeValueFormatted;

@property (nonatomic, strong) NSString * systemSizeValue;
@property (nonatomic) float currentGaugeValue;
@property (nonatomic) float realGaugeValue;
@property (nonatomic, strong) NSString * powerNowPercentage;

@property (nonatomic, strong) NSString *irradianceFormatted;
@property (nonatomic, strong) NSString *insolationFormatted;
@property (nonatomic, strong) NSString *cellTempFormatted;
@property (nonatomic, strong) NSString *ambientTempFormatted;

@property (nonatomic, strong) NSArray *graphValuesDay;
@property (nonatomic, strong) NSArray *graphStartDateDAY;
@property (nonatomic, strong) NSArray *graphValues7D;
@property (nonatomic, strong) NSArray *graphStartDate7D;
@property (nonatomic, strong) NSArray *graphValues30D;
@property (nonatomic, strong) NSArray *graphStartDate30D;
@property (nonatomic, strong) NSArray *graphValues365D;
@property (nonatomic, strong) NSArray *graphStartDate365D;
@property (nonatomic, strong) NSArray *graphValuesWTD;
@property (nonatomic, strong) NSArray *graphStartDateWTD;
@property (nonatomic, strong) NSArray *graphValuesMTD;
@property (nonatomic, strong) NSArray *graphStartDateMTD;
@property (nonatomic, strong) NSArray *graphValuesYTD;
@property (nonatomic, strong) NSArray *graphStartDateYTD;

@property (nonatomic, strong) NSNumber *maxDayValue;
@property (nonatomic, strong) NSNumber *max7DValue;
@property (nonatomic, strong) NSNumber *max30DValue;
@property (nonatomic, strong) NSNumber *max365DValue;
@property (nonatomic, strong) NSNumber *maxWTDValue;
@property (nonatomic, strong) NSNumber *maxMTDValue;
@property (nonatomic, strong) NSNumber *maxYTDValue;

@property (nonatomic, strong) NSString *siteAddress;
@property (nonatomic, strong) NSString *siteCountry;
@property (nonatomic, strong) NSString *siteCity;
@property (nonatomic, strong) NSString *siteStreet;
@property (nonatomic, strong) NSString *siteZipCode;
@property (nonatomic, strong) NSString *siteRegionCode;

@property (nonatomic, strong) NSNumber *CO2Multiplier;
@property (nonatomic, strong) NSNumber *NOXMultiplier;
@property (nonatomic, strong) NSNumber *SO2Multiplier;

@property (nonatomic, strong) NSString *TVSavings;
@property (nonatomic, strong) NSString *CarSavings;
@property (nonatomic, strong) NSString *ComputerSavings;
@property (nonatomic, strong) NSString *CO2Savings;
@property (nonatomic, strong) NSString *SO2Savings;
@property (nonatomic, strong) NSString *NOXSavings;
@property (nonatomic, strong) NSString *CarbonOffsetMetricTonsSavings;
@property (nonatomic, strong) NSString *CarbonOffsetAcresSavings;

@property (nonatomic, strong) NSString *nowData;
@property (nonatomic, strong) NSString *UpdatedForCurrentPowerTime;

@property (nonatomic, strong) NSDictionary *inverters;
@property (nonatomic, strong) NSMutableArray *inverterTimes;

@property (nonatomic, strong) NSString *EntityLat;
@property (nonatomic, strong) NSString *EntityLong;
@property (nonatomic, strong) NSString *EntityElevation;

@property (nonatomic, strong) NSString *todayString;
@property (nonatomic, strong) NSString *tonightString;
@property (nonatomic, strong) UIImage *currentIcon;
@property (nonatomic, strong) NSString *avgHumidity;
@property (nonatomic, strong) NSString *feelsLike;
@property (nonatomic, strong) NSString *visibility;
@property (nonatomic, strong) NSString *todayDetails;
@property (nonatomic, strong) NSString *tonightDetails;

@property (nonatomic, strong) NSString *nowPrecipitation;
@property (nonatomic, strong) NSString *soonestPrecipitation;
@property (nonatomic, strong) NSString *soonPrecipitation;
@property (nonatomic, strong) NSString *laterPrecipitation;

@property (nonatomic, strong) NSString *windPower;
@property (nonatomic, strong) NSString *barometerPower;

@property (nonatomic, strong) NSString *moonPhase;
@property (nonatomic, strong) UIImage *moonIcon;
@property (nonatomic, strong) NSString *SunsetTime;
@property (nonatomic, strong) NSString *SunriseTime;

@property (nonatomic, strong) NSMutableArray *hourlyTimes;
@property (nonatomic, strong) NSMutableArray *hourlyTemps;
@property (nonatomic, strong) NSMutableArray *hourlyIcons;

@property (nonatomic, strong) NSString *currentEnergyCosts;
@property (nonatomic, strong) NSString *currentEnergyCostsDate;

@property (nonatomic, strong) NSArray *systemStatusArrayAlerts;

- (void)fetchEasyViewData:(NSData *)responseData;
- (void)fetchInstallDateData:(NSData *)responseData;
- (void)fetchHierarchyRequestData:(NSData *)responseData;
- (void)fetchWeatherStationRequestData:(NSData *)responseData;
- (void)fetchServicesSummaryRequestData:(NSData *)responseData;
- (void)fetchInverterServicesSummaryData:(NSData *)responseData;
- (void)fetchInverterPositions:(NSData *)responseData;
- (void)fetchServicesDocumentRequestData:(NSData *)responseData;
- (void)fetchEasyViewStatus:(NSData *)responseData;
- (void)fetchGraphDataRequestDataDay:(NSData *)responseData;
- (void)fetchGraphDataRequestData7D:(NSData *)responseData;
- (void)fetchGraphDataRequestData30D:(NSData *)responseData;
- (void)fetchGraphDataRequestData365D:(NSData *)responseData;
- (void)fetchGraphDataRequestDataWTD:(NSData *)responseData;
- (void)fetchGraphDataRequestDataMTD:(NSData *)responseData;
- (void)fetchGraphDataRequestDataYTD:(NSData *)responseData;


- (void)fetchFullForeCast:(NSData *)responseData;
- (void)fetchHourlyForecast:(NSData *)responseData;
- (void)fetchForecastConditions:(NSData *)responseData;
//- (void)fetchSunriseAndSunsetTimes:(NSData *)responseData;

- (void)fetchEnergyCosts:(NSData *)responseData;

- (NSString*) getEntityName;
- (NSString*) getEntityId;
- (NSString*) getEntityDescription;
- (NSString*) getEntityInstallDate;


- (NSString*) getSystemStatus;
- (UIImage *) getSystemStatusIcon;
- (NSDictionary *) getSystemStatusAlerts;

- (NSString*) getNowValue;
- (NSString*) getTodayValue;
- (NSString*) getWeekValue;
- (NSString*) getMonthValue;
- (NSString*) getLifeValue;

- (NSNumber*)getRealLifeValue;
- (NSString*) getSystemSizeValue;
- (float) getRealGuageValue;

- (NSString*) getIrradiance;
- (NSString*) getInsolation;
- (NSString*) getCellTemp;
- (NSString*) getAmbientTemp;

- (NSString*) getSiteAddress;
- (NSString*) getSiteZipCode;
- (NSString*) getSiteRegionCode;
- (NSString*) getSiteCity;

- (NSString*) getTVSavings;
- (NSString*) getCarSavings;
- (NSString*) getComputerSavings;
- (NSString*) getCO2Savings;
- (NSString*) getNOXSavings;
- (NSString*) getSO2Savings;
- (NSString*) getCarbonOffsetMetricTonsSavings;
- (NSString*) getCarbonOffsetAcresSavings;

- (NSString*) getPerformanceNowPercentage;

- (NSMutableArray*) getInverterDates;
- (NSDictionary*) getInverterNamesAndValues;

- (BOOL) percentageChecker;


- (NSArray*) getGraphValuesDay;
- (NSArray*) getGraphDatesDay;


- (NSArray*) getGraphValues7D;
- (NSArray*) getGraphDates7D;


- (NSArray*) getGraphValues30D;
- (NSArray*) getGraphDates30D;


- (NSArray*) getGraphValues365D;
- (NSArray*) getGraphDates365D;


- (NSArray*) getGraphValuesWTD;
- (NSArray*) getGraphDatesWTD;


- (NSArray*) getGraphValuesMTD;
- (NSArray*) getGraphDatesMTD;


- (NSArray*) getGraphValuesYTD;
- (NSArray*) getGraphDatesYTD;

- (NSNumber*) getMaxDayValue;
- (NSNumber*) getMax7DValue;
- (NSNumber*) getMax30DValue;
- (NSNumber*) getMax365DValue;
- (NSNumber*) getMaxWTDValue;
- (NSNumber*) getMaxMTDValue;
- (NSNumber*) getMaxYTDValue;

- (NSString*) getEntityLat;
- (NSString*) getEntityLong;
- (NSString*) getEntityElevation;

- (NSString *) getTodayString;
- (NSString *) getTodayDetails;
- (UIImage *) getCurrentIcon;
- (NSString *) getTonightString;
- (NSString *) getTonightDetails;
- (NSString *) getAvgHumidity;
- (NSString *) getFeelsLike;
- (NSString *) getVisibility;

- (NSString *) getNowPrecipitation;
- (NSString *) getSoonestPrecipitation;
- (NSString *) getSoonPrecipitation;
- (NSString *) getLaterPrecipitation;

- (NSString *) getWindPower;
- (NSString *) getBarometerPower;

- (NSString *) getMoonPhase;
- (UIImage *) getMoonIcon;
- (NSString *) getSunriseTime;
- (NSString *) getSunsetTime;

- (NSMutableArray *) getHourlyTimes;
- (NSMutableArray *) getHourlyTemps;
- (NSMutableArray *) getHourlyIcons;

- (NSString *) getEnergyCosts;
- (NSString *) getEnergyCostsDate;


@end
