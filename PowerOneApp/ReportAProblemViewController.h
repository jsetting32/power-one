//
//  ReportAProblemViewController.h
//  PowerOneApp
//
//  Created by John Setting on 6/21/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "SWRevealViewController.h"
#import "SKPSMTPMessage.h"
#import "ProblemTypes.h"
#import "UITextfieldScrollViewController.h"

@protocol ReportAProblemDelegate

- (void) didReceiveType:(NSString *)problemType;

@end

@interface ReportAProblemViewController : UITextfieldScrollViewController <UITableViewDelegate, UITableViewDataSource, SKPSMTPMessageDelegate, ReportAProblemDelegate, UITextViewDelegate, UIScrollViewDelegate>
{
    IBOutlet UITableView *reportAProblemTableView;
    UIActivityIndicatorView * spinner;
    UIImageView * bgimage;
    IBOutlet UILabel * loadingLabel;
    IBOutlet UITextView *TextViewBody;
    IBOutlet UIButton *reportButton;
    NSString *newLabel;
    
}
@property (strong, nonatomic) IBOutlet UIScrollView *scroller;



@property (strong, nonatomic) IBOutlet UITableView *reportAProblemTableView;
@property (strong, nonatomic) IBOutlet UIButton *reportButton;
@property (strong, nonatomic) IBOutlet UITextView *TextViewBody;
@property (nonatomic, retain) IBOutlet UILabel * loadingLabel;
@property (nonatomic, retain)UIImageView * bgimage;
@property (nonatomic, retain)UIActivityIndicatorView * spinner;
-(IBAction)sendEmail:(id)sender;
-(void)removeWaitOverlay;
-(void)createWaitOverlay;
-(void)stopSpinner;
-(void)startSpinner;


@end
