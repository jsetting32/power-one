//
//  PowerOneSignUp.m
//  PowerOneApp
//
//  Created by John Setting on 7/6/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "PowerOneSignUp.h"

@interface PowerOneSignUp ()

@end

@implementation PowerOneSignUp

@synthesize image1, image2, image3, loginButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"Yablon-solar1.jpg"]];
    
    _desiredUsername.delegate = self;
    _desiredPassword.delegate = self;
    _emailAddress.delegate = self;
    
    self.loginButton.layer.cornerRadius = 5;
    self.loginButton.layer.borderWidth = 2;
    self.loginButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.loginButton.layer.masksToBounds = YES;
    
    UIView *tabBar = [[UIView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, 36)];
    tabBar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"graybackground.jpg"]];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 32)];
    title.center = CGPointMake(tabBar.bounds.size.width / 2, tabBar.bounds.size.height / 2);
    title.text = @"Sign Up";
    title.textAlignment = NSTextAlignmentCenter;
    [title setFont:[UIFont fontWithName:@"Times New Roman-BoldMT" size:20.0f]];
    title.textColor = [UIColor whiteColor];
    title.backgroundColor = [UIColor clearColor];
    [tabBar addSubview:title];
    
    UIImageView *leftImage = [[UIImageView alloc] initWithFrame:CGRectMake(90, 2, 24, 32)];
    leftImage.image = [UIImage imageNamed:@"Screen Shot 2013-07-07 at 1.35.32 PM.png"];
    [tabBar addSubview:leftImage];
    
    UIImageView *rightImage = [[UIImageView alloc] initWithFrame:CGRectMake(205, 2, 24, 32)];
    rightImage.image = [UIImage imageNamed:@"Screen Shot 2013-07-07 at 1.35.32 PM.png"];
    [tabBar addSubview:rightImage];
    
    [self.view addSubview:tabBar];
    
    [NSTimer timerWithTimeInterval:0.05 target:self selector:@selector(changeImage) userInfo:nil repeats:YES];

}

- (void) changeImage
{
    UIImage *holder = self.image1.image;
    self.image1.image = self.image2.image;
    self.image2.image = holder;
    
    holder = self.image2.image;
    self.image2.image = self.image3.image;
    self.image3.image = holder;
    
    holder = self.image3.image;
    self.image3.image = self.image1.image;
    self.image1.image = holder;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    if (textField == self.desiredPassword) self.desiredPassword.text = textField.text;
    else if(textField == self.desiredUsername) self.desiredUsername.text = textField.text;
    else if(textField == self.emailAddress) self.emailAddress.text = textField.text;
    
    return NO;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqual:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    [textView resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)signUpAction:(id)sender {
    CDDLoginViewController *cdd = [[CDDLoginViewController alloc] initWithNibName:@"CDDLoginViewController" bundle:nil];
    [self presentViewController:cdd animated:YES completion:nil];
}
@end
