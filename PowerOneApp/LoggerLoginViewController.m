//
//  LoggerLoginViewController.m
//  PowerOneApp
//
//  Created by John Setting on 7/11/13.
//  Copyright (c) 2013 John Setting. All rights reserved.
//

#import "LoggerLoginViewController.h"

@interface LoggerLoginViewController ()

@end

@implementation LoggerLoginViewController

@synthesize ipAddressTextField;
@synthesize loginButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Logger Login";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.ipAddressTextField.text = @"10.0.2.127";
    self.ipAddressTextField.text = @"10.1.4.138";
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"1-carbon-fiber-standard-weave-iphone-background.jpg"]];
    
    
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    //SWRevealViewController *revealController = [self revealViewController];
    
    //[self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    //[self.view addGestureRecognizer:revealController.panGestureRecognizer];
    
    //UIBarButtonItem *leftRevealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"reveal-icon.png"] style:UIBarButtonItemStyleBordered target:revealController action:@selector(revealToggle:)];
    //self.navigationItem.leftBarButtonItem = leftRevealButtonItem;
    
    //UIBarButtonItem *rightRevealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"19-gear.png"] style:UIBarButtonItemStyleBordered target:revealController action:@selector(rightRevealToggle:)];
    //self.navigationItem.rightBarButtonItem = rightRevealButtonItem;
    
     
     
    ipAddressTextField.delegate = self;
    
    [loginButton addTarget:self action:@selector(pushToLoggerStatusAndConfig) forControlEvents:UIControlEventTouchUpInside];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (textField == ipAddressTextField) { ipAddressTextField.text = textField.text; }
    NSLog(@"%@", textField.text);
    
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) pushToLoggerStatusAndConfig
{
    LoggerTableViewController *logger = [[LoggerTableViewController alloc] initWithNibName:@"LoggerTableViewController" bundle:nil];
    logger->ipAddress = ipAddressTextField.text;
    logger.title = @"Logger Data";
    [self.navigationController pushViewController:logger animated:YES];
    //[self presentViewController:logger animated:YES completion:nil];
}

@end
